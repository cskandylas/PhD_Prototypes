#include "control.h"

#include "difc_io_task.h"
#include "difc_msg_handler_task.h"
#include "tokenize.h"

//TODO:Proper logging
void difc_controller_init(struct difc_controller *ctrl, char *ip, int port,int pool_size)
{

    thread_pool_init(&ctrl->t_p,pool_size);
    ctrl->conn=malloc(sizeof(struct difc_connection));
    ctrl->conn->port=port;
    ctrl->conn->ip=ip;
    ctrl->conn->max_accept=pool_size-1;
    ctrl->max_children=pool_size-1;
    ctrl->children=malloc(ctrl->max_children*sizeof(struct difc_entity));
    ctrl->num_children=0;
    ctrl->uid=0;
    ctrl->max_io_tasks=pool_size-1;
    ctrl->io_tasks=malloc(ctrl->max_io_tasks*sizeof(struct task));
    ctrl->num_io_tasks=0;
    
}

void difc_connection_setup(struct difc_controller *ctrl)
{
    
    struct difc_connection *conn=ctrl->conn;
    
    //Create the socket. 
    conn->sock = socket(PF_INET, SOCK_STREAM, 0);
    conn->addr.sin_family = AF_INET;
    conn->addr.sin_port = htons(conn->port);
    conn->addr.sin_addr.s_addr = inet_addr(conn->ip);
    memset(conn->addr.sin_zero, '\0', sizeof conn->addr.sin_zero);
    bind(conn->sock, (struct sockaddr *) &conn->addr, sizeof(conn->addr));
    
    if(listen(conn->sock,conn->max_accept)==0)
    {
	//All went well!
    }
    else
    {
	fprintf(stderr,"Failed to listen for connections\n");
    }


    //create the message manager task
    struct difc_msg_handler_ctx *msg_handler_ctx=malloc(sizeof(struct difc_msg_handler_ctx));
    msg_handler_ctx->ctrl=ctrl;
    
    create_task(&ctrl->msg_handler,0,difc_msg_handler_init,difc_msg_handler_run,difc_msg_handler_destroy,msg_handler_ctx);
    thread_pool_add_task(&ctrl->t_p,&ctrl->msg_handler);
}

void difc_connection_run(struct difc_controller *ctrl)
{
    struct difc_connection *conn=ctrl->conn;
    
    unsigned int task_id=1;
    
    struct sockaddr_storage serverStorage;



    //SEE IF WE NEED TO HOLD TASKS IN THE CONTROLLER STRUCT
    struct task difc_tasks[1024];
    //struct difc_io_ctx difc_ctx[1024];
    
    while(1)
    {
	if(ctrl->num_io_tasks < ctrl->max_io_tasks)
	{
	    struct difc_io_ctx *io_ctx=malloc(sizeof(struct difc_io_ctx));
	    io_ctx->server_sock=conn->sock;
	    io_ctx->ctrl=ctrl;
	    
	    create_task(&ctrl->io_tasks[ctrl->num_io_tasks],task_id,difc_io_init,difc_io_run,difc_io_destroy,io_ctx);
	    thread_pool_add_task(&ctrl->t_p,&ctrl->io_tasks[ctrl->num_io_tasks]);
	    ctrl->num_io_tasks++;
	}
    }
}

bool have_entity(struct difc_controller *ctrl, char *entity_name)
{
    
    for(int i=0;i<ctrl->num_children;i++)
    {
	if(strncmp(ctrl->children[i].name,entity_name,strlen(entity_name))==0)
	{
	    return true;
	}
    }
    return false;
}

struct difc_entity *difc_register_entity(struct difc_controller *ctrl, char *msg, struct difc_io_ctx *task)
{

    //1. Check that the entity does not already exist
    //2. Check that the entity type matches the context children type
    //3. Generate UUID for entity and register it.    
    //4. Communicate the UUID
    struct tokenizer registratation_tok;
    tokenizer_init(&registratation_tok,msg,"[;]");
    
    skip_token(&registratation_tok);
    skip_token(&registratation_tok);
    char *entity_name=next_token(&registratation_tok);
    if(!have_entity(ctrl,entity_name) && ctrl->num_children < ctrl->max_children)
    {
	difc_entity_init(&ctrl->children[ctrl->num_children],entity_name,NULL,task);
	ctrl->num_children++;
	fprintf(stdout,"Registered %s\n",entity_name);
	return &ctrl->children[ctrl->num_children-1];
    }
    else
    {
	fprintf(stderr,"Failed to register %s\n",entity_name);
	return NULL;
    }
    
}

bool difc_deregister_entity(struct difc_controller *ctrl, struct difc_entity *ent)
{
    for(int i=0;i<ctrl->num_children;i++)
    {
	if(strcmp(ctrl->children[i].name,ent->name)==0)
	{
	    difc_entity_destroy(ent);
	    if(i!=ctrl->num_children-1)
	    {
		ctrl->children[i]=ctrl->children[ctrl->num_children-1];
	    }
	    ctrl->num_children--;
	    break;
	    
	}
	
    }
}



int main(int argc, char *argv[])
{
    int port=-1;
    int num_elements=-1;
    char *ip=NULL;
    if(argc != 4)
    {
	fprintf(stderr,"Usage: difc_control ip port num_elements\n");
	exit(-1);
    }
    else
    {
	ip=argv[1];
	port=atoi(argv[2]);
	num_elements=atoi(argv[3]);
    }
    
    struct difc_controller ctrl;
    difc_controller_init(&ctrl,ip,port,num_elements);
    difc_connection_setup(&ctrl);
    difc_connection_run(&ctrl);
    return 0;
}
