#include<stdio.h>
#include<stdlib.h>
#include <stdbool.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<string.h>
#include<arpa/inet.h>
#include<fcntl.h> 
#include<unistd.h> 
#include<pthread.h>

#include "thread_pool.h"
#include "message_queue.h"
#include "entity.h"
#include "task.h"


#ifndef DIFC_CONTROL_H
#define DIFC_CONTROL_H


#define MAX_MSG_SIZE 256

struct difc_connection
{
    char *ip;
    int port;
    int sock;
    struct sockaddr_in addr;
    size_t max_accept;
};



struct difc_controller
{
    //DIFC Related elements
    struct difc_entity *parent;
    struct difc_entity *children;
    size_t num_children;
    size_t max_children;
    
    //Thread pool and tasks
    struct thread_pool t_p;
    struct task msg_handler;
    struct task *io_tasks;
    size_t num_io_tasks;
    size_t max_io_tasks;

    //Connection
    struct difc_connection *conn;
    

    //SHOULD REALY BY MID
    int uid;
    
};


void difc_controller_init(struct difc_controller *ctrl, char *ip, int port,int pool_size);
struct difc_entity *difc_register_entity(struct difc_controller *ctrl, char *msg, struct difc_io_ctx *task);
bool difc_deregister_entity(struct difc_controller *ctrl, struct difc_entity *ent);

bool have_entity(struct difc_controller *difc, char *entity_name);
bool have_descendant(struct difc_controller *ctrl, char *entity_name);
bool can_resolve_message(struct difc_controller *ctrl, char *from, char *to);
#endif
