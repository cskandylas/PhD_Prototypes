#define  _XOPEN_SOURCE  600

#include "difc_message_handler.h"
#include "tokenize.h"
#include "difc_io_task.h"

#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/sockios.h>

bool difc_validate_policy(char *policy, char *trust_resp)
{

    struct tokenizer semitok;
    tokenizer_init(&semitok,trust_resp,"[;]");
    skip_token(&semitok); //id
    skip_token(&semitok); //op
    char *trust_str=next_token(&semitok);
    float trust=atof(trust_str);

    bool valid=true;

    struct tokenizer tag_tok;
    tokenizer_init(&tag_tok,policy,"<,>");
    char *tag_str=next_token(&tag_tok);
    while(tag_str)
    {
	struct tokenizer colon_tok;
	tokenizer_init(&colon_tok,tag_str,":");
	skip_token(&colon_tok); //tag name
	char *min_trust_str=next_token(&colon_tok);
	skip_token(&colon_tok); //op
	float min_trust=atof(min_trust_str);

	free(tag_str);

	if(trust < min_trust)
	{
	    valid=false;
	}
	
	tag_str=next_token(&tag_tok);
    }

    return valid;
}

bool difc_validate_message(struct difc_controller *ctrl, char *from, char *msg)
{
    //1. Check if we can validate the message at this level,
    //   i.e. if we have both modules in this context or our descendants.
    //2. If we do, we can validate and return the result.    
    //2. If we don't propagate upwards and forget.

    int mID=ctrl->uid++;
    char *return_message=difc_return_message(ctrl, from, msg,mID);
    
    if(return_message)
    {
	//We received a reply, hence we can resolve at this level
	//First let's get the trust both ways
	
	struct tokenizer semitok;
	tokenizer_init(&semitok,msg,"[;]");
	skip_token(&semitok); //id
	skip_token(&semitok); //op
	char *to=next_token(&semitok); //to
	skip_token(&semitok); //iface
	char *caller_policy=next_token(&semitok); //label
	
	char *trust_from_to_str=difc_trust_message(ctrl, from, to,mID);
	char *trust_to_from_str=difc_trust_message(ctrl, to, from, mID);

	//Let's do the caller policy first
	bool caller_sat=difc_validate_policy(caller_policy,trust_to_from_str);
	
	//Now let's do the callee
	tokenizer_init(&semitok,return_message,"[;]");
	skip_token(&semitok);
	skip_token(&semitok);
	skip_token(&semitok);
	char *callee_policy=next_token(&semitok);
	bool callee_sat=difc_validate_policy(callee_policy,trust_from_to_str);

	//cleanup
	free(to);
	free(caller_policy);
	free(callee_policy);
	free(trust_from_to_str);
	free(trust_to_from_str);

	return (caller_sat && callee_sat);
    }
    else
    {
	//Propagate upwards
    }
    
    return false;
}

bool difc_validate_add_tag(struct difc_controller *ctrl, char *from, char *msg)
{
    //1. Check if we can validate the message at this level,
    //   i.e. if we have both modules in this context or our descendants.
    //2. If we do, we can validate and return the result.    
    //2. If we don't propagate upwards and forget.
    
    int mID=ctrl->uid++;
    char *return_tag=difc_return_tag(ctrl,from, msg, mID);

    //TODO: Also check owner and provided owner
    struct tokenizer semitok;
    tokenizer_init(&semitok,return_tag,"[;]");
    skip_token(&semitok);
    skip_token(&semitok);
    skip_token(&semitok);
    char *res=next_token(&semitok);

    if(strcmp(res,"DIFC_OK")==0)
    {
	return true;
    }
    else if(strcmp(res,"DIFC_NO")==0)
    {
	return false;
    }
    else
    {
	fprintf(stderr,"Unexpected response for tag validation:%s\nPanic Exiting\n",return_tag);
    }
}

char *difc_return_tag(struct difc_controller *ctrl, char *from, char *msg,int mID)
{
    char *tag_resp=NULL;
    struct tokenizer semitok;
    tokenizer_init(&semitok,msg,"[;]");
    skip_token(&semitok);
    skip_token(&semitok);
    char *to=next_token(&semitok);
    char *tag=next_token(&semitok);
    
    
    
    char response_request[MAX_MSG_SIZE];
    sprintf(response_request,"[%d;TAG;%s;%s;%s]",mID,to,tag,from);

    //need locks?
    for(int i=0;i<ctrl->num_children;i++)
    {
	ssize_t nbytes=send(ctrl->children[i].task->difc_sock,response_request,MAX_MSG_SIZE,0);
	if(nbytes<=0)
	{
	    fprintf(stderr,"Failed to send:%s\n",response_request);
	}
    }

    bool all_replies=false;
    struct message *replies[ctrl->num_children];

    for(size_t i=0;i<ctrl->num_children;i++)
    {
	replies[i]=NULL;
    }
    while(!all_replies)
    {
	
	//small hack to know when we're done
	all_replies=true;
	for(size_t i=0;i<ctrl->num_children;i++)
	{
	    
	    //we just incremented uid so we're looking for that -1
	    if(!replies[i])
	    {
		
		replies[i]=message_queue_find_id(&ctrl->children[i].task->m_q,mID);
		if(replies[i])
		{
		    
		    message_queue_rem(&ctrl->children[i].task->m_q,replies[i]);
		}
	    }
	    
	    
	    if(!replies[i])
		all_replies=false;
	}

	
	
    }
    

    //inspect replies
    for(int i=0;i<ctrl->num_children;i++)
    {
	if(strcmp(replies[i]->type,"DIFC_NO")==0)
	{
	    
	}
	else if(strcmp(replies[i]->type,"RESPONSE")==0)
	{
	    tag_resp=strdup(replies[i]->content);
	}
	
    }    

    //cleanup
    free(to);
    free(tag);
    
    return tag_resp;
}


char *difc_return_message(struct difc_controller *ctrl, char *from, char *msg,int mID)
{
    
    char *ret_resp=NULL;
    //For every child send a reply with interface message;
    struct tokenizer semitok;
    tokenizer_init(&semitok,msg,"[;]");
    skip_token(&semitok);
    skip_token(&semitok);
    char *to=next_token(&semitok);
    char *calling=next_token(&semitok);
    char *label=next_token(&semitok);
    char *version=next_token(&semitok);
    
    char response_request[MAX_MSG_SIZE];
    sprintf(response_request,"[%d;SEEK;%s;%s;%s;]",mID,to,calling,version);

    //need locks?
    for(int i=0;i<ctrl->num_children;i++)
    {
	ssize_t nbytes=send(ctrl->children[i].task->difc_sock,response_request,MAX_MSG_SIZE,0);
	if(nbytes<=0)
	{
	    fprintf(stderr,"Failed to send:%s\n",response_request);
	}
    }

    bool all_replies=false;
    struct message *replies[ctrl->num_children];

    for(size_t i=0;i<ctrl->num_children;i++)
    {
	replies[i]=NULL;
    }
    while(!all_replies)
    {
	
	//small hack to know when we're done
	all_replies=true;
	for(size_t i=0;i<ctrl->num_children;i++)
	{
	    
	    //we just incremented uid so we're looking for that -1
	    if(!replies[i])
	    {
		
		replies[i]=message_queue_find_id(&ctrl->children[i].task->m_q,mID);
		if(replies[i])
		{
		    message_queue_rem(&ctrl->children[i].task->m_q,replies[i]);
		}
	    }
	    
	    
	    if(!replies[i])
		all_replies=false;
	}

	
	
    }

    //inspect replies
    for(int i=0;i<ctrl->num_children;i++)
    {
	if(strcmp(replies[i]->type,"DIFC_NO")==0)
	{
	}
	else if(strcmp(replies[i]->type,"RESPONSE")==0)
	{
	    ret_resp=strdup(replies[i]->content);
	}
	
    }    

    //cleanup
    free(to);
    free(calling);
    free(label);
    free(version);
    
    return ret_resp;
}


char *difc_trust_message(struct difc_controller *ctrl, char *from, char *to,int mID)
{

    char *trust_resp=NULL;
    char trust_msg[MAX_MSG_SIZE];
    sprintf(trust_msg,"[%d;TRUST;%s;%s;]",mID,from,to);

    for(int i=0;i<ctrl->num_children;i++)
    {
	ssize_t nbytes=send(ctrl->children[i].task->difc_sock,trust_msg,MAX_MSG_SIZE,0);
	if(nbytes<=0)
	{
	    fprintf(stderr,"Failed to send:%s\n",trust_msg);
	}
    }
	
    bool all_replies=false;
    struct message *replies[ctrl->num_children];

    for(size_t i=0;i<ctrl->num_children;i++)
    {
	replies[i]=NULL;
    }
    while(!all_replies)
    {
	
	//small hack to know when we're done
	all_replies=true;
	for(size_t i=0;i<ctrl->num_children;i++)
	{

	    if(!replies[i])
	    {
		replies[i]=message_queue_find_id(&ctrl->children[i].task->m_q,mID);
		if(replies[i])
		{

		    message_queue_rem(&ctrl->children[i].task->m_q,replies[i]);
		}
	    }
	    
	    
	    if(!replies[i])
		all_replies=false;
	}

	
	
    }


    //inspect replies
    for(int i=0;i<ctrl->num_children;i++)
    {
	if(strcmp(replies[i]->type,"DIFC_NO")==0)
	{
	}
	else if(strcmp(replies[i]->type,"RESPONSE")==0)
	{
	    trust_resp=strdup(replies[i]->content);
	}
	
    }

    //We don't know at this level we have to elevate
    if(!trust_resp)
    {
	
    }

    //cleanup
    //free(to);
    
    return trust_resp;
}

