
#include "control.h"

#ifndef DIFC_MESSAGE_H
#define DIFC_MESSAGE_H

//High level messages
bool difc_validate_message(struct difc_controller *ctrl, char *from, char *msg);
bool difc_validate_label_change(struct difc_controller *ctrl, char *from, char *msg);
bool difc_validate_add_tag(struct difc_controller *ctrl, char *from, char *msg);


//Messages used to perform the high level operations
char *difc_return_message(struct difc_controller *ctrl, char *from, char *msg,int mID);
char *difc_return_tag(struct difc_controller *ctrl, char *from, char *msg,int mID);
char *difc_trust_message(struct difc_controller *ctrl, char *from, char *to,int mID);
#endif

