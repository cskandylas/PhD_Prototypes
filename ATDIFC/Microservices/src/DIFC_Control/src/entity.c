#include "entity.h"
#include <stdlib.h>

void difc_entity_init(struct difc_entity *ent,char *name, char *UUID,struct difc_io_ctx *task)
{
    ent->name=name;
    ent->UUID=UUID;
    ent->task=task;
    ent->state=ESTATE_CONNECTED;
}


void difc_entity_destroy(struct difc_entity *ent)
{
    if(ent->name)
    {
	free(ent->name);
    }
    if(ent->UUID)
    {
	free(ent->UUID);
    }
    ent->state=ESTATE_DISCONNECTED;
    //TODO: Rethink ownership!!!!!!!!!!!!!!!!!!!!!!!
    //Note, we do not close the socket since it never belonged to the entity
}
