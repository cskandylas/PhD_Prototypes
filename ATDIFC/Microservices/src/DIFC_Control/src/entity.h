

#ifndef DIFC_ENTITY_H
#define DIFC_ENTITY_H


enum ENTITY_STATE  { ESTATE_CONNECTED, ESTATE_REGISTERED, ESTATE_DISCONNECTED };

struct difc_io_ctx;

struct difc_entity
{
    char *name;
    char *UUID;
    enum ENTITY_STATE state;
    struct difc_io_ctx *task;
};


void difc_entity_init(struct difc_entity *ent,char *name, char *UUID,struct difc_io_ctx *task);
void difc_entity_destroy(struct difc_entity *ent);

#endif
