#include "task.h"

void create_task(struct task *task,
		 uint32_t task_id,
		 task_init init,
		 task_run run,
		 task_destroy destroy,
		 void *task_ctx)
{
    task->task_id=task_id;
    task->status=TASK_CREATED;
    task->init=init;
    task->run=run;
    task->destroy=destroy;
    task->task_ctx=task_ctx;
    task->ret=NULL;
    task->init(task);
}

void destroy_task(struct task *task)
{
    task->destroy(task);
}
