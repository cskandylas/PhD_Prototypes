#include <pthread.h>
#include <stdint.h>

#ifndef TASK_H
#define TASK_H

/* 
 This is a tiny task implementation that wraps detatched pthreads.
*/


enum TASK_STATUS
{
    TASK_CREATED,
    TASK_RUNNING,
    TASK_COMPLETE,
    TASK_ABORTED
};


//forward declare to make it a bit easier to read
struct task;

typedef void (*task_init)(struct task *task);
typedef void (*task_run)(struct task *task);
typedef void  (*task_destroy)(struct task *task);

struct task
{
    uint32_t task_id;
    enum TASK_STATUS status;

    //context and return
    void *task_ctx;
    void *ret;

    //function pointers
    task_init init;
    task_run run;
    task_destroy destroy;
};

void create_task(struct task *task,
		 uint32_t task_id,
		 task_init init,
		 task_run run,
		 task_destroy destroy,
		 void *task_ctx);
void destroy_task(struct task *task);

#endif
