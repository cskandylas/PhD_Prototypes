#define  _XOPEN_SOURCE  600

#include "difc_io_task.h"
#include "tokenize.h"


#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/sockios.h>


void difc_io_init(struct task *task)
{
    struct difc_io_ctx *ctx=task->task_ctx;
    ctx->addr_size = sizeof(struct sockaddr_storage);
    ctx->difc_sock=accept(ctx->server_sock, (struct sockaddr *) &ctx->storage, &ctx->addr_size);
    ctx->entity=NULL;
    message_queue_init(&ctx->m_q);
}

void difc_io_destroy(struct task *task)
{
    struct difc_io_ctx *ctx=task->task_ctx;
    message_queue_destroy(&ctx->m_q);
}


static struct message *message_from_str(char *msg_str)
{
    
    struct tokenizer msg_tok;
    tokenizer_init(&msg_tok,msg_str,"[;]");
    char *uuid=next_token(&msg_tok);
    int mID=atoi(uuid);
    char *msg_type=next_token(&msg_tok);
    //fprintf(stdout,"Message type: %s\n",msg_type);
    //we can only fill the UID and the type at this point for now  
    struct message *msg=malloc(sizeof(struct message));
    message_init(msg,msg_type,mID,msg_str);
	
    return msg;
    
}

void difc_io_run(struct task *task)
{
    struct difc_io_ctx *ctx=task->task_ctx;

    char msg_buff[MAX_MSG_SIZE];
    bool conn_alive=true;
    while(conn_alive)
    {
	
	ssize_t nbytes=recv(ctx->difc_sock , msg_buff , MAX_MSG_SIZE , MSG_DONTWAIT);
	    
	if(nbytes>0)
	{
	    //pack it up nicely
	    struct message *new_msg=message_from_str(msg_buff);
		
	    //add it to the message_queue
	    if(new_msg)
		message_queue_add(&ctx->m_q,new_msg);
	}    
	
	if(nbytes==0)
	{
	    fprintf(stdout,"Bye from %d\n",ctx->difc_sock);
	    conn_alive=false;
	}
	


	usleep(1000);
    }
    close(ctx->difc_sock);
    if(ctx->entity)
    {
	difc_deregister_entity(ctx->ctrl,ctx->entity);
    }
    
}
