#include "task.h"
#include "control.h"
#include "entity.h"
#include "message_queue.h"

#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<unistd.h>



#ifndef DIFC_IO_H
#define DIFC_IO_H


struct difc_io_ctx
{
    int server_sock;
    int difc_sock;
    struct sockaddr_storage storage;
    socklen_t addr_size;
    struct difc_controller *ctrl;
    struct difc_entity *entity;
    struct message_queue m_q;
};

void difc_io_init(struct task *task);
void difc_io_destroy(struct task *task);
void difc_io_run(struct task *task);


#endif

