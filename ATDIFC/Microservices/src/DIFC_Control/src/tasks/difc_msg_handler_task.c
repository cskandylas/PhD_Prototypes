#define  _XOPEN_SOURCE  600

#include "difc_msg_handler_task.h"
#include "difc_message_handler.h"
#include "difc_io_task.h"

#include "message_queue.h"
#include <unistd.h>
#include <stdio.h>


void difc_msg_handler_init(struct task *task)
{
    
}



void difc_msg_handler_destroy(struct task *task)
{
    
}


static void difc_message_handler(struct difc_io_ctx *ctx)
{

    //Walk the queue and try to service any message we know how to.
    struct message *curr=ctx->m_q.head;

    while(curr)
    {

	
	
	if(strcmp(curr->type,"REGISTER")==0)
	{
	    struct difc_entity *entity=difc_register_entity(ctx->ctrl,curr->content,ctx);
	    if(entity)
	    {
		ctx->entity=entity;
		send(ctx->difc_sock,"DIFC_OK",strlen("DIFC_OK"),0);
	    }
	    else
	    {
		send(ctx->difc_sock,"DIFC_NO",strlen("DIFC_NO"),0);
	    }
	    message_queue_rem_front(&ctx->m_q);
	}
	else if(strcmp(curr->type,"MESSAGE")==0)
	{
	    
	    bool valid= difc_validate_message(ctx->ctrl,ctx->entity->name,curr->content);
	    if(valid)
	    {
		ssize_t nbytes=send(ctx->difc_sock,"DIFC_OK",strlen("DIFC_OK"),0);
	    }
	    else
	    {
		ssize_t nbytes=send(ctx->difc_sock,"DIFC_NO",strlen("DIFC_NO"),0);
	    }
	    message_queue_rem_front(&ctx->m_q);
	}
	else if(strcmp(curr->type,"ADDTAG")==0)
	{
	    bool valid= difc_validate_add_tag(ctx->ctrl,ctx->entity->name,curr->content);
	    if(valid)
	    {
		ssize_t nbytes=send(ctx->difc_sock,"DIFC_OK",strlen("DIFC_OK"),0);
	    }
	    else
	    {
		ssize_t nbytes=send(ctx->difc_sock,"DIFC_NO",strlen("DIFC_NO"),0);
	    }
	    message_queue_rem_front(&ctx->m_q);
	}
	
	curr=curr->next;
    }
}


void difc_msg_handler_run(struct task *task)
{
    
    struct difc_msg_handler_ctx *ctx=task->task_ctx;
    struct difc_controller *ctrl=ctx->ctrl;

    //forever
    while(1)
    {
	//Walk through each message queue and try to service messages	
	for(int i=0;i<ctrl->num_io_tasks;i++)
	{
	    struct difc_io_ctx *io_ctx=ctrl->io_tasks[i].task_ctx;;
	    difc_message_handler(io_ctx);
	    
	}
	
	usleep(1000);
	
    }
    
    
}

