#include "task.h"
#include "control.h"



#ifndef DIFC_MSG_HANDLER_H
#define DIFC_MSG_HANDLER_H



struct difc_msg_handler_ctx
{
    struct difc_controller *ctrl;
};



void difc_msg_handler_init(struct task *task);
void difc_msg_handler_destroy(struct task *task);
void difc_msg_handler_run(struct task *task);



#endif

