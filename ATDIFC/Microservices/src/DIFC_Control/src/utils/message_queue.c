#define  _XOPEN_SOURCE  600

#include "message_queue.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


void message_init(struct message *msg, char *type,
		  int mID, char *content)
{
    msg->type=strdup(type);
    msg->mID=mID;
    msg->content=strdup(content);
}


void message_destroy(struct message *msg)
{
    free(msg->type);
    free(msg->content);
}




void message_queue_init(struct message_queue *m_q)
{
    m_q->head=m_q->tail=NULL;
    pthread_mutex_init(&m_q->lock,NULL);
}

void message_queue_destroy(struct message_queue *m_q)
{
    pthread_mutex_destroy(&m_q->lock);
}

void message_queue_add(struct message_queue *m_q, struct message *msg)
{
    pthread_mutex_lock(&m_q->lock);
    
    msg->prev=m_q->tail;
    msg->next=NULL;
    if(!m_q->head)
    {
	m_q->head=m_q->tail=msg;
	m_q->head->next=m_q->tail->next=NULL;
    }
    else
    {
	
	m_q->tail->next=msg;
	m_q->tail=msg;
    }
    pthread_mutex_unlock(&m_q->lock);
}

struct message *message_queue_rem_front(struct message_queue *m_q)
{

    struct message *ret=NULL;
    pthread_mutex_lock(&m_q->lock);
    if(m_q->head)
    {
	
	ret=m_q->head;
	if(m_q->head->next)
	{
	    m_q->head=m_q->head->next;
	    m_q->head->prev=NULL;
	}
	else
	{
	    m_q->head=m_q->tail=NULL;
	}
	
    }
    pthread_mutex_unlock(&m_q->lock);

    return ret;
}


void message_queue_rem(struct message_queue *m_q, struct message *msg)
{
    pthread_mutex_lock(&m_q->lock);
    struct message *prev = msg->prev;
    struct message *next = msg->next;
    if(prev)
	prev->next=next;
    if(next)
	next->prev=prev;
    if(msg==m_q->head)
    {
	m_q->head=msg->next;
    }
    if(msg==m_q->tail)
    {
	m_q->tail=msg->prev;
    }
    
    pthread_mutex_unlock(&m_q->lock);
	 
}

struct message *message_queue_find_id(struct message_queue *m_q, uint32_t mID)
{
    struct message *curr=m_q->head;
    while(curr)
    {
	//printf("%s%p%p\n",curr->content,curr->next,curr->prev);
	
	if(curr->mID==mID)
	{
	    return curr;
	}
	
	curr=curr->next;
    }

    return NULL;
}

