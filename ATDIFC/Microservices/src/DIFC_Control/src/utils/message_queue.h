#include <stdint.h>
#include <pthread.h>

#ifndef MESSAGE_QUEUE_H
#define MESSAGE_QUEUE_H

struct message
{
    char *type;
    int mID;
    char *content;
    struct message *next, *prev;
};


struct message_queue
{
    struct message *head, *tail;
    pthread_mutex_t lock;
};


void message_init(struct message *msg, char *type,
		  int mID, char *content);
void message_destroy(struct message *msg);

void message_queue_init(struct message_queue *m_q);
void message_queue_destroy(struct message_queue *m_q);
void message_queue_add(struct message_queue *m_q, struct message *msg);
struct message *message_queue_rem_front(struct message_queue *m_q);
void message_queue_rem(struct message_queue *m_q, struct message *msg);
struct message *message_queue_find_id(struct message_queue *m_q, uint32_t mID);
    
#endif
