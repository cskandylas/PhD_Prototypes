#include "tokenize.h"
#include <stdlib.h>
#include <string.h>


#include <stdio.h>

static inline int in_string(char c, char *str)
{
    char *start=str;
    while(*start!='\0')
    {
	if(c == *start)
	{
	    return 1;
	}
	start++;
    }
    return 0;
}



void tokenizer_init(struct tokenizer *tok, char *str,char *delims)
{
    tok->pos=str;
    tok->str=str;
    tok->delims=delims;
    
}

char* next_token(struct tokenizer *tok)
{
    size_t size=0;
    while(*tok->pos!='\0' && in_string(*tok->pos,tok->delims))
    {
	tok->pos++;
    }
    char *start=tok->pos;
    while(*tok->pos!='\0' && !in_string(*tok->pos,tok->delims))
    {
	size++;
	tok->pos++;
    }
    if(size>0)
    {
	char *ret=malloc(size+1);
	strncpy(ret,start,size);
	while(*tok->pos!='\0' && in_string(*tok->pos,tok->delims))
	{
	    tok->pos++;
	}
	ret[size]='\0';
	return ret;
    }
   
    return NULL;
}

int   count_tokens(struct tokenizer *tok)
{
    if(!tok->str)
    {
	return 0;
    }
    else 
    {
	int num_tokens=0;
	char *pos=tok->str;
	char *delims=tok->delims;
	while(*pos!='\0')
	{
	    while(in_string(*pos,delims) && *pos!='\0')
	    {
		pos++;
	    }
	    char *prev=pos;
	    while(!in_string(*pos,delims) && *pos!='\0')
	    {
		pos++;
	    }
	    if(prev!=pos)
		num_tokens++;
	}

	return num_tokens;
    }
}


char next_delim(struct tokenizer *tok, char *delims)
{
    size_t size=0;
    while(*tok->pos!='\0' && !in_string(*tok->pos,delims))
    {
	tok->pos++;
    }
    return *tok->pos++;
}

void skip_token(struct tokenizer *tok)
{
    char *next=next_token(tok);
    free(next);
}
