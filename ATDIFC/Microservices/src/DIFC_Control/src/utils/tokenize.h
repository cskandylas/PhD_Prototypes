

#include <stddef.h>

#ifndef TOKENIZER_H

#define TOKENIZER_H

struct tokenizer
{
    char *str;
    char *delims;
    char *pos;
};

void tokenizer_init(struct tokenizer *tok, char *str,char *delims);

char* next_token(struct tokenizer *tok);
void skip_token(struct tokenizer *tok);
char next_delim(struct tokenizer *tok, char *delims);
int   count_tokens(struct tokenizer *tok);
#endif
