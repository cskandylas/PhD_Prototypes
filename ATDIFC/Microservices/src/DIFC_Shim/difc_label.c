#define  _XOPEN_SOURCE  600

#include "difc_label.h"

#include <stdlib.h>
#include <string.h>


void difc_label_init(struct difc_label *label, char *interface, size_t num_params)
{
    //assumes ownership of the interface string
    label->interface=strdup(interface);
    label->num_params=num_params;
    label->param_idx=0;
    label->param_tags=malloc(label->num_params*sizeof(struct difc_tag*));
    label->ret_tag=NULL;
    
    
}
bool difc_label_add_param_tag(struct difc_label *label, struct difc_tag *tag)
{
    if(label->param_idx<label->num_params)
    {
	label->param_tags[label->param_idx]=tag;
	label->param_idx++;
	return true;
    }

    return false;
}

void difc_label_add_ret_tag(struct difc_label *label, struct difc_tag *tag)
{
    label->ret_tag=tag;
}

bool difc_label_rem_param_tag(struct difc_label *label, struct difc_tag *tag)
{
    for(int i=0;i<label->param_idx;i++)
    {
	if(label->param_tags[i]==tag)
	{
	    label->param_tags[i]=NULL;
	}
	return true;
    }

    return false;
}


void difc_label_destroy(struct difc_label *label)
{
    free(label->interface);
}


void difc_label_dump_info(struct difc_label *label, FILE *fp)
{
    fprintf(fp,"Label Interface:%s\nLabel Parameters:\n",label->interface);
    
    for(int i=0;i<label->param_idx;i++)
    {
	difc_tag_dump_info(label->param_tags[i],fp);
    }
    if(label->ret_tag)
    {
	fprintf(fp,"Return:\n");
	difc_tag_dump_info(label->ret_tag,fp);
    }
}
