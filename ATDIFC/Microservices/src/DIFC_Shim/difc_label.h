#include "difc_tag.h"

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

#ifndef DIFC_LABEL_H
#define DIFC_LABEL_H

//we should not duplicate tags,so we just keep pointers
struct difc_label
{
    char *interface;
    struct difc_tag **param_tags;
    size_t param_idx;
    size_t num_params;
    struct difc_tag *ret_tag;
};


void difc_label_init(struct difc_label *label, char *interface, size_t num_params);
bool difc_label_add_param_tag(struct difc_label *label, struct difc_tag *tag);
void difc_label_add_ret_tag(struct difc_label *label, struct difc_tag *tag);
bool difc_label_rem_param_tag(struct difc_label *label, struct difc_tag *tag);
void difc_label_destroy(struct difc_label *label);

void difc_label_dump_info(struct difc_label *label, FILE *fp);


#endif
