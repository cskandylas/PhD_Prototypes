#define  _XOPEN_SOURCE  600

#include "difc_module.h"

#include <stdlib.h>
#include <string.h>



void difc_module_init(struct difc_module *module, char *name)
{
    //copy name over
    module->name=strdup(name);
    //create labels array possibly realloc if needed
    module->max_labels=DEFAULT_MAX_LABELS;
    module->num_labels=0;
    module->labels=malloc(module->max_labels*sizeof(struct difc_label));
    //create trusted values array possibly realloc if needed
    module->max_trusted=DEFAULT_MAX_TRUSTED;
    module->num_trusted=0;
    module->trusted=malloc(module->max_trusted*sizeof(struct trust_record));
    module->max_tags=DEFAULT_MAX_TAGS;
    module->num_tags=0;
    module->tags=malloc(module->max_tags*sizeof(struct difc_tag));
}

void difc_module_destroy(struct difc_module *module)
{
    free(module->name);
    for(int i=0;i<module->num_labels;i++)
    {
	difc_label_destroy(&module->labels[i]);
    }
    free(module->labels);

    for(int i=0;i<module->num_trusted;i++)
    {
	trust_record_destroy(&module->trusted[i]);
    }
    free(module->trusted);
}

void difc_module_add_tag(struct difc_module *module, struct difc_tag *tag)
{
    if(module->num_tags<module->max_tags)
    {
	module->tags[module->num_tags]=*tag;
	module->num_tags++;
    }
}
void difc_module_add_label(struct difc_module *module, struct difc_label *label)
{
    if(module->num_labels<module->max_labels)
    {
	module->labels[module->num_labels]=*label;
	module->num_labels++;
    }
}
void difc_module_add_trust_record(struct difc_module *module, struct trust_record *t_r)
{
    if(module->num_trusted<module->max_trusted)
    {
	module->trusted[module->num_trusted]=*t_r;
	module->num_trusted++;
    }
}


struct difc_tag *difc_module_find_tag(struct difc_module *module, char *tag_name)
{
    for(int i=0;i<module->num_tags;i++)
    {
	//TODO: Also check we own the tag
	if(strcmp(module->tags[i].name,tag_name)==0)
	{
	    return &module->tags[i];
	}
    }

    return NULL;
}


struct difc_label *difc_module_find_label(struct difc_module *module, char *interface)
{


    for(int i=0;i<module->num_labels;i++)
    {
	if(strcmp(module->labels[i].interface,interface)==0)
	{
	    
	    return &module->labels[i];
	}
    }
    
    
    return NULL;
}


struct trust_record *difc_module_find_trust_record(struct difc_module *module, char *who)
{
    for(int i=0;i<module->num_trusted;i++)
    {
	if(strcmp(module->trusted[i].who,who)==0)
	{
	    return &module->trusted[i];
	}
    }

    return NULL;
}

void difc_module_dump_all_info(struct difc_module *module,FILE *fp)
{
    fprintf(fp,"Info dump for module: %s\n",module->name);

    fprintf(fp,"Tag Info (%zu)\n", module->num_tags);
    for(int i=0;i<module->num_tags;i++)
    {
	difc_tag_dump_info(&module->tags[i],fp);
    }
    fprintf(fp,"Tag Info Done\n");
    fprintf(fp,"Label Info (%zu)\n",module->num_labels);
    for(int i=0;i<module->num_labels;i++)
    {
	difc_label_dump_info(&module->labels[i],fp);
    }
    fprintf(fp,"Label Info Done\n");
    fprintf(fp,"Trust Records Info (%zu)\n",module->num_trusted);
    for(int i=0;i<module->num_trusted;i++)
    {
	trust_record_dump_info(&module->trusted[i],fp);
    }
    fprintf(fp,"Trust Records Info Done\n");    
}
