#include <stdint.h>
#include <stddef.h>
#include <stdio.h>

#include "difc_label.h"
#include "difc_tag.h"
#include "trust_record.h"

#ifndef DIFC_MODULE_H
#define DIFC_MODULE_H

#define DEFAULT_MAX_TRUSTED 32
#define DEFAULT_MAX_LABELS 24
#define DEFAULT_MAX_TAGS 128



struct difc_module
{
    //name of the module the shim controls
    char *name;
    //labels 
    struct difc_label *labels;
    size_t num_labels;
    size_t max_labels;
    //elements with trust values
    struct trust_record *trusted;
    size_t num_trusted;
    size_t max_trusted;
    struct difc_tag *tags;
    size_t num_tags;
    size_t max_tags;
};



void difc_module_init(struct difc_module *module, char *name);
void difc_module_destroy(struct difc_module *module);
void difc_module_add_tag(struct difc_module *module, struct difc_tag *tag);
void difc_module_add_label(struct difc_module *module, struct difc_label *label);
void difc_module_add_trust_record(struct difc_module *module, struct trust_record *t_r);
void difc_module_dump_all_info(struct difc_module *module,FILE *fp);

struct difc_tag *difc_module_find_tag(struct difc_module *module, char *tag_name);
struct difc_label *difc_module_find_label(struct difc_module *module, char *interface);
struct trust_record *difc_module_find_trust_record(struct difc_module *module, char *who);

#endif
