#include "difc_tag.h"

#include <stdlib.h>

void fill_tag(struct difc_tag *tag, char *owner, char *name, float min_trust, int min_dist)
{
    tag->owner=owner;
    tag->name=name;
    tag->min_trust=min_trust;
    tag->min_dist=min_dist;
}


void destroy_tag(struct difc_tag *tag)
{
    free(tag->owner);
    free(tag->name);

}


void difc_tag_dump_info(struct difc_tag *tag, FILE *fp)
{
    fprintf(fp,"Owner:%s\nName:%s\nMinTrust:%f\nMinDist:%d\n",
	    tag->owner,tag->name,tag->min_trust,tag->min_dist);
}

