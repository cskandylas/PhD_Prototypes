#include <stdio.h>

#ifndef DIFC_TAG_H
#define DIFC_TAG_H

struct difc_tag
{
    char *owner;
    char *name;
    float min_trust;
    int min_dist;
};



void fill_tag(struct difc_tag *tag, char *owner, char *name, float min_trust, int min_dist);
void destroy_tag(struct difc_tag *tag);

void difc_tag_dump_info(struct difc_tag *tag, FILE *fp);



#endif
