#define  _XOPEN_SOURCE  600

#include "shim.h"
#include "tokenize.h"

#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <fcntl.h> 
#include <unistd.h>
#include <pthread.h>
#include <sys/ioctl.h>
#include <linux/sockios.h>

//There is only ever one shim so declare it as a static variable
static struct difc_shim *the_shim=NULL;

//TODO: Proper logging?
void difc_shim_init(char *module_name, char *controller_ip)
{
    if(!the_shim)
    {

	the_shim=malloc(sizeof(struct difc_shim));
	fprintf(stdout,"Registering new shim for %s with controller at  %s\n",
		module_name,controller_ip);
	
	difc_module_init(&the_shim->module,module_name);
	pthread_mutex_init(&the_shim->lock,NULL);
	struct tokenizer ip_tok;
	tokenizer_init(&ip_tok,controller_ip,":");
	the_shim->ip=next_token(&ip_tok);
	char *port_str=next_token(&ip_tok);
	the_shim->port=atoi(port_str);
	difc_shim_connect();
    }
}

void difc_shim_connect()
{

    if(!the_shim)
    {
	fprintf(stderr,"Shim not initialized\n");
	return;
    }

//    fprintf(stdout,"Connecting to: %s:%d\n",the_shim->ip,the_shim->port);
    
    struct sockaddr_in server_addr;
    socklen_t addr_size;
    
    
    the_shim->ctrl_sock=socket(PF_INET, SOCK_STREAM, 0);
    server_addr.sin_family = AF_INET;
    
    server_addr.sin_port = htons(the_shim->port);

    server_addr.sin_addr.s_addr = inet_addr(the_shim->ip);
    memset(server_addr.sin_zero, '\0', sizeof server_addr.sin_zero);
    
    addr_size = sizeof(server_addr);
    int res=connect(the_shim->ctrl_sock, (struct sockaddr *) &server_addr, addr_size);
    if(res)
    {
	fprintf(stderr,"connect() returned %d\n",res);
    }
    char register_msg_buff[MAX_MSG_LEN];
    snprintf(register_msg_buff,MAX_MSG_LEN,"[-1;REGISTER;%s]",the_shim->module.name);
    ssize_t nbytes=send(the_shim->ctrl_sock,register_msg_buff,MAX_MSG_LEN,0);
//    fprintf(stdout,"%ld bytes sent\n",nbytes);
    memset(register_msg_buff,0,MAX_MSG_LEN);
    nbytes=recv(the_shim->ctrl_sock,register_msg_buff,MAX_MSG_LEN,0);
//    fprintf(stdout,"Received:%s\n",register_msg_buff);
    if(strcmp(register_msg_buff,"DIFC_OK")==0)
    {
	//All is well
    }
    else if(strcmp(register_msg_buff,"DIFC_NO")==0)
    {
	fprintf(stderr,"Could not register with controller, killing the shim\n");
	exit(-1);
    }
    else
    {
	fprintf(stderr,"Unexpected reply received %s\n",register_msg_buff);
	exit(-1);
    }

    
    pthread_t listening_thread_id;
    pthread_create(&listening_thread_id,NULL,difc_response_fnc,NULL);

}

void difc_shim_destroy()
{
    if(the_shim)
    {
	fprintf(stdout,"Destroying Shim for %s\n",the_shim->module.name);
	difc_module_destroy(&the_shim->module);
	free(the_shim->ip);
	free(the_shim);
	close(the_shim->ctrl_sock);
    }
}


bool difc_initial_trust(char *trust_str)
{
    struct tokenizer trusted_tok;
    tokenizer_init(&trusted_tok,trust_str,"[;]");
    char *next_trusted=next_token(&trusted_tok);
    while(next_trusted)
    {
	struct tokenizer  colon_tok;
	tokenizer_init(&colon_tok,next_trusted,":");
	char *who=next_token(&colon_tok);
	char *how_much_str=next_token(&colon_tok);
	float how_much=atof(how_much_str);

	//Bounds check
	struct trust_record *t_r=&the_shim->module.trusted[the_shim->module.num_trusted++];

	trust_record_init(t_r,who,how_much);

	//clean up
	free(how_much_str);
	//get next thing
	next_trusted=next_token(&trusted_tok);
    }
    //difc_module_dump_all_info(&the_shim->module,stderr);

    return true;
}


float difc_trust(char *target)
{

    printf("Trust \n");
    struct difc_module *module=&the_shim->module;
    //first check our trusted
    for(int i=0;i<module->num_trusted;i++)
    {
	
	if(strcmp(module->trusted[i].who,target)==0)
	{
	    return module->trusted[i].how_much;
	}
    }


    //we don't know so let's ask our parent
    pthread_mutex_lock(&the_shim->lock);
    printf("Don't know %s asking parent\n",target);
    char msg_buff[MAX_MSG_LEN];
    snprintf(msg_buff,MAX_MSG_LEN,"[-1;TRUST;%s;%s]",module->name, target);
    ssize_t nbytes=send(the_shim->ctrl_sock,msg_buff,MAX_MSG_LEN,0);
    if(nbytes<=0)
    {
	fprintf(stderr,"Failed to send difc message %s\n",msg_buff);
    }
    memset(msg_buff,0,MAX_MSG_LEN);
    
    memset(msg_buff,0,MAX_MSG_LEN);
    nbytes=recv(the_shim->ctrl_sock,msg_buff,MAX_MSG_LEN,0);
    
    
    pthread_mutex_unlock(&the_shim->lock);

}



bool difc_message(char *module,char *interface,char *label)
{
    bool res=false;
    if(the_shim)
    {

	pthread_mutex_lock(&the_shim->lock);
	//get the shim lock
//	fprintf(stdout,"Message to %s:%s with label %s\n",module,interface,label);
	char difc_msg_buff[MAX_MSG_LEN];
	snprintf(difc_msg_buff,MAX_MSG_LEN,"[-1;MESSAGE;%s;%s;%s]",
		 module,interface,label);
	ssize_t nbytes=send(the_shim->ctrl_sock,difc_msg_buff,MAX_MSG_LEN,0);
	if(nbytes<=0)
	{
	    fprintf(stderr,"Failed to send difc message %s\n",difc_msg_buff);
	}

	memset(difc_msg_buff,0,MAX_MSG_LEN);
	nbytes=recv(the_shim->ctrl_sock,difc_msg_buff,MAX_MSG_LEN,0);

	difc_seek_response(difc_msg_buff);

	
	memset(difc_msg_buff,0,MAX_MSG_LEN);
	nbytes=recv(the_shim->ctrl_sock,difc_msg_buff,MAX_MSG_LEN,0);
	
	difc_trust_response(difc_msg_buff);



	memset(difc_msg_buff,0,MAX_MSG_LEN);
	nbytes=recv(the_shim->ctrl_sock,difc_msg_buff,MAX_MSG_LEN,0);
	
	difc_trust_response(difc_msg_buff);


	
	memset(difc_msg_buff,0,MAX_MSG_LEN);
	nbytes=recv(the_shim->ctrl_sock,difc_msg_buff,MAX_MSG_LEN,0);


	
	if(strcmp(difc_msg_buff,"DIFC_OK")==0)
	{
	    res=true;
	}
	else if(strcmp(difc_msg_buff,"DIFC_NO")==0)
	{
	    res=false;
	}
	else
	{
	    fprintf(stderr,"Unexpected reply received %s\n",difc_msg_buff);
	    exit(-1);
	}
	pthread_mutex_unlock(&the_shim->lock);
    }
    else
    {
	fprintf(stderr,"Shim not initialized.\n");
    }
    return res;
}


bool difc_label_change()
{
    return true;
}

bool difc_add_tag(char *owner, char *tag_name)
{
    //get the shim lock
    pthread_mutex_lock(&the_shim->lock);
//    fprintf(stdout,"[-1;ADDTAG;%s;%s]\n",owner, tag_name);
    char msg_buff[MAX_MSG_LEN];
    snprintf(msg_buff,MAX_MSG_LEN,"[-1;ADDTAG;%s;%s]",owner, tag_name);
    ssize_t nbytes=send(the_shim->ctrl_sock,msg_buff,MAX_MSG_LEN,0);
    if(nbytes<=0)
    {
	fprintf(stderr,"Failed to send difc message %s\n",msg_buff);
    }
   
    //Wait to see if we suceeded

    //we're holding the lock so we'll need to handle messages

    
    
    memset(msg_buff,0,MAX_MSG_LEN);
    nbytes=recv(the_shim->ctrl_sock,msg_buff,MAX_MSG_LEN,0);

    difc_tag_response(msg_buff);

    memset(msg_buff,0,MAX_MSG_LEN);
    nbytes=recv(the_shim->ctrl_sock,msg_buff,MAX_MSG_LEN,0);

    
    pthread_mutex_unlock(&the_shim->lock);
    return true;
}


bool difc_rem_tag(char *owner, char *tag_name)
{
    
}


bool difc_add_policy(char *interface,char *tags)
{
    
    fprintf(stderr,"Adding policy for interface:%s with tags:%s\n",interface,tags);

    
    
    struct tokenizer tag_tokenizer;
    tokenizer_init(&tag_tokenizer,tags,"<>");
    char *param_str=next_token(&tag_tokenizer);

    char *ret_str=next_token(&tag_tokenizer);

    
    if(param_str)
    {

	struct tokenizer semicolon_tok;
	tokenizer_init(&semicolon_tok,param_str,";");
	
	int num_tags=count_tokens(&semicolon_tok);

	if(num_tags>0)
	{
	    //TODO:BOUNDS CHECK
	    //Takes the address of the current label and increments num_labels in one go
	    struct difc_label *lbl= &the_shim->module.labels[the_shim->module.num_labels++];
	    difc_label_init(lbl,interface,num_tags);

	    char *param_tag_str=next_token(&semicolon_tok);

	    while(param_tag_str)
	    {
	    
		struct tokenizer colon_tok;
		tokenizer_init(&colon_tok,param_tag_str, ":");
		char *owner=next_token(&colon_tok);
		char *name=next_token(&colon_tok);
		char *min_trust_str=next_token(&colon_tok);
		char *min_dist_str=next_token(&colon_tok);
		float min_trust=atof(min_trust_str);
		int min_dist=atoi(min_dist_str);
		

		//TODO BOUNDS CHECK
		//Takes the address of the current tag and increments num_tags in one go.
		struct difc_tag *tag=&the_shim->module.tags[the_shim->module.num_tags++];
		fill_tag(tag,owner,name,min_trust,min_dist);
		difc_label_add_param_tag(lbl,tag);
		
		//cleanup
		free(min_trust_str);
		free(min_dist_str);
		    
		//next tag or NULL
		param_tag_str=next_token(&semicolon_tok);
	    }
	    
	}

	//cleanup
	free(param_str);
    }
    else
    {
	fprintf(stderr," label %s could not be parsed\n",tags);
	return false;
    }

    
    
    ret_str=next_token(&tag_tokenizer);
    if(ret_str)
    {
	

	free(ret_str);
    }
    //Uncomment for sanity check
    //difc_module_dump_all_info(&the_shim->module,stdout);
    return true;
}




static char *compose_tag_str(struct difc_tag *tag)
{
    //TODO:Error check
    //should be big enough
    char buff[64];

    size_t nbytes=sprintf(buff,"%s:%.2f:%d",tag->name,tag->min_trust,tag->min_dist);
    
    char *tag_str=strdup(buff);
    
    return tag_str;
}


static char *compose_param_str(struct difc_label *lbl)
{

    //TODO:Error check
    char buff[256];
    strcpy(buff,"<");
    for(int i=0;i<lbl->param_idx;i++)
    {
	char *tag_str=compose_tag_str(lbl->param_tags[i]);
	strcat(buff,tag_str);
	free(tag_str);
	if(i<lbl->param_idx-1)
	{
	    strcat(buff,",");
	}
    }
    strcat(buff,">");

    char *params_str=strdup(buff);

    return params_str;
}


void difc_seek_response(char *msg)
{
    struct tokenizer msg_tok;
    tokenizer_init(&msg_tok,msg,"[;]");
    char *uuid=next_token(&msg_tok);
    skip_token(&msg_tok);
    char *to = next_token(&msg_tok);
    char *calling=next_token(&msg_tok);
    char *version=next_token(&msg_tok);
    
    char response_buff[MAX_MSG_LEN];
    memset(response_buff,0,MAX_MSG_LEN);
	
    if(strcmp(to,the_shim->module.name)!=0)
    {

	memset(response_buff,0,MAX_MSG_LEN);
	
	sprintf(response_buff,"[%s;DIFC_NO]",uuid);
	ssize_t nbytes=send(the_shim->ctrl_sock,response_buff,MAX_MSG_LEN,0);
	if(nbytes==0)
	{
	    fprintf(stderr,"Failed to send %s\n",response_buff);
	}
    }
    else
    {
	struct difc_label *lbl = difc_module_find_label(&the_shim->module,calling);
	memset(response_buff,0,MAX_MSG_LEN);
	if(!lbl)
	{
	    sprintf(response_buff,"[%s;DIFC_NO]",uuid);
	}
	else
	{


	    //We are the calee so we need to label the params
	    char *params_str=compose_param_str(lbl);
	    
	    
	    snprintf(response_buff,MAX_MSG_LEN,"[%s;RESPONSE;%s;%s]",uuid,the_shim->module.name,params_str);
	}
	ssize_t nbytes=send(the_shim->ctrl_sock,response_buff,MAX_MSG_LEN,0);
	if(nbytes==0)
	{
	    fprintf(stderr,"Failed to send %s\n",response_buff);
	}
    }

    //cleanup
    free(to);
    free(calling);
    free(version);
    free(uuid);
    
}


void difc_tag_response(char *msg)
{
    struct tokenizer msg_tok;
    tokenizer_init(&msg_tok,msg,"[;]");
    char *uuid=next_token(&msg_tok);
    skip_token(&msg_tok);
    char *to = next_token(&msg_tok);
    char *tag=next_token(&msg_tok);
    char *from=next_token(&msg_tok);

    
    char response_buff[MAX_MSG_LEN];
    memset(response_buff,0,MAX_MSG_LEN);
	
    if(strcmp(to,the_shim->module.name)!=0)
    {

	memset(response_buff,0,MAX_MSG_LEN);
	
	sprintf(response_buff,"[%s;DIFC_NO]",uuid);
	ssize_t nbytes=send(the_shim->ctrl_sock,response_buff,MAX_MSG_LEN,0);
	if(nbytes==0)
	{
	    fprintf(stderr,"Failed to send %s\n",response_buff);
	}
    }
    else
    {
	

	
	struct difc_tag *found = difc_module_find_tag(&the_shim->module,tag);

	if(!found)
	{
	    sprintf(response_buff,"[%s;DIFC_NO]",uuid);
	    ssize_t nbytes=send(the_shim->ctrl_sock,response_buff,MAX_MSG_LEN,0);
	    if(nbytes==0)
	    {
		fprintf(stderr,"Failed to send %s\n",response_buff);
	    }   
	}
	else
	{
	
	    float trust = difc_trust(from);
	
	    //fake it to test the socket interaction
	    memset(response_buff,0,MAX_MSG_LEN);
	    if(trust>=found->min_trust)
	    {
		snprintf(response_buff,MAX_MSG_LEN,"[%s;RESPONSE;%s;DIFC_OK]",
			 uuid,the_shim->module.name);
	    }
	    else
	    {
		snprintf(response_buff,MAX_MSG_LEN,"[%s;RESPONSE;%s;DIFC_NO]",
			 uuid,the_shim->module.name);
	    }
	    ssize_t nbytes=send(the_shim->ctrl_sock,response_buff,MAX_MSG_LEN,0);
	}
    }

    //cleanup
    free(uuid);
    free(to);
    free(tag);
    free(from);
    
    
}


void difc_trust_response(char *msg)
{   
    struct tokenizer msg_tok;
    tokenizer_init(&msg_tok,msg,"[;]");
    char *uuid=next_token(&msg_tok);
    skip_token(&msg_tok);
    char *from = next_token(&msg_tok);
    char *to=next_token(&msg_tok);

    
    char response_buff[MAX_MSG_LEN];
    memset(response_buff,0,MAX_MSG_LEN);
	
    if(strcmp(from,the_shim->module.name)!=0)
    {
	
	memset(response_buff,0,MAX_MSG_LEN);
	
	sprintf(response_buff,"[%s;DIFC_NO]",uuid);
	ssize_t nbytes=send(the_shim->ctrl_sock,response_buff,MAX_MSG_LEN,0);
	if(nbytes==0)
	{
	    fprintf(stderr,"Failed to send %s\n",response_buff);
	}
    }
    else
    {
	//difc_module_dump_all_info(&the_shim->module,stdout);
        
	struct trust_record *t_r=difc_module_find_trust_record(&the_shim->module,to);

	memset(response_buff,0,MAX_MSG_LEN);
	if(!t_r)
	{
	    sprintf(response_buff,"[%s;DIFC_NO]",uuid);
	}
	else
	{
	    snprintf(response_buff,MAX_MSG_LEN,"[%s;RESPONSE;%.2f]",uuid,t_r->how_much);    
	}
	
	
	
	
	ssize_t nbytes=send(the_shim->ctrl_sock,response_buff,MAX_MSG_LEN,0);
	//also wait to find out if the call succeeded
    }
    
    //cleanup
    free(from);
    free(to);
    free(uuid);
}


void difc_response_handler(char *msg)
{
//    fprintf(stdout,"Handling response:%s\n",msg);
    struct tokenizer msg_tok;
    tokenizer_init(&msg_tok,msg,"[;]");
    skip_token(&msg_tok);
    char *msg_type=next_token(&msg_tok);
//    fprintf(stdout,"Message type: %s\n",msg_type);
    if(strcmp(msg_type,"SEEK")==0)
    {
	difc_seek_response(msg);
    }
    else if(strcmp(msg_type,"TRUST")==0)
    {
	difc_trust_response(msg);
    }
    else if(strcmp(msg_type,"TAG")==0)
    {
	difc_tag_response(msg);
    }
    
}

void* difc_response_fnc(void*arg)
{
    
    //wait forever to receive a query
    char difc_msg_buff[MAX_MSG_LEN];
    while(1)
    {
	//lock and unlock to not trample over sending.
	pthread_mutex_lock(&the_shim->lock);
	ssize_t nbytes=recv(the_shim->ctrl_sock,difc_msg_buff,MAX_MSG_LEN,MSG_DONTWAIT);
	if(nbytes>0)
	{
	    
	    fprintf(stdout,"Received:%ld:%s\n",nbytes,difc_msg_buff);
	    difc_response_handler(difc_msg_buff);
	}

	pthread_mutex_unlock(&the_shim->lock);
	usleep(1000);
    }
}
