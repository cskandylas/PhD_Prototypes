#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#include "difc_module.h"

#define MAX_MSG_LEN 256

struct difc_shim
{
    struct difc_module module;
    FILE *log;
    int ctrl_sock;
    int port;
    char *ip;
    pthread_mutex_t lock;    
};


void difc_shim_init(char *module_name, char *controller_ip);
void difc_shim_destroy();


//shim interface
bool difc_add_policy(char *interface,char *tags);
bool difc_initial_trust(char *trust_str);
void difc_shim_connect();

//difc messages
bool difc_message(char *module,char *interface,char *label);
bool difc_add_tag(char *owner, char *tag_name);
bool difc_rem_tag(char *owner, char *tag_name);



//trust handling
float difc_trust(char *target);

//listening thread function
void* difc_response_fnc(void*arg);

//response handlers
void difc_seek_response(char *msg);
void difc_trust_response(char *msg);
void difc_tag_response(char *msg);
