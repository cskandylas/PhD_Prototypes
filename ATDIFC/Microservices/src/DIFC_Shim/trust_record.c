#include "trust_record.h"

#include <stdlib.h>



void trust_record_init(struct trust_record *t_r, char *who, float how_much)
{
    t_r->who=who;
    t_r->how_much=how_much;
}


void trust_record_destroy(struct trust_record *t_r)
{
    free(t_r->who);
}


void trust_record_dump_info(struct trust_record *t_r, FILE *fp)
{
    fprintf(fp,"Who:%s\nHowMuch:%f\n",t_r->who,t_r->how_much);
}
