#include <stdio.h>

#ifndef TRUST_RECORD_H 
#define TRUST_RECORD_H


struct trust_record
{
    char *who;
    float how_much;
};

void trust_record_init(struct trust_record *t_r, char *who, float how_much);
void trust_record_destroy(struct trust_record *t_r);

void trust_record_dump_info(struct trust_record *t_r, FILE *fp);

#endif
