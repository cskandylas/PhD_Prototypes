from concurrent import futures
import logging
import DIFC_Shim_Interface

import grpc
import Common_pb2
import Common_pb2_grpc
import Account_pb2
import Account_pb2_grpc


class AccountServicer(Account_pb2_grpc.AccountServicer):

    def __init__(self):
        None

    def Register(self,request,context):
        print("Registering new user...")
        return Common_pb2.Confirmation(success=True)
        

    def Login(self, request, context):
        print("Login Attempt...")
        return Common_pb2.Confirmation(success=True)

    def UserExists(self,request,context):
        print("User Lookup...")
        return Common_pb2.Confirmation(success=True)

def AccountService_serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    Account_pb2_grpc.add_AccountServicer_to_server(AccountServicer(), server)
    DIFC_Shim_Interface.register(str.encode("Account"),str.encode("127.0.0.1:40000"))
    DIFC_Shim_Interface.initial_trust(str.encode("[Frontend:0.6]"))
    DIFC_Shim_Interface.add_policy(str.encode("Register"),str.encode("<Account:AccountInfo:0.5:5><Account:AccountID:0.5:5>"))
    server.add_insecure_port('[::]:50052')
    server.start()
    server.wait_for_termination()
    DIFC_Shim_Interface.unregister()



AccountService_serve()
