
from concurrent import futures
import logging
import DIFC_Shim_Interface

import grpc
import Common_pb2
import Common_pb2_grpc
import Cart_pb2
import Cart_pb2_grpc


class CartServicer(Cart_pb2_grpc.CartServicer):

    def __init__(self):
        None

    def AddItemToCart(self,request,context):
        print("Adding item to Cart...")
        return Common_pb2.Confirmation(success=True)

    
    def RemItemFromCart(self,request,context):
        print("Removing item to Cart...")
        return Common_pb2.Confirmation(success=True)

    def CheckOutCart(self,request,context):
        print("Checking out Cart...")
        return Common_pb2.Confirmation(success=True)
                
    

def CartService_serve():
  server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
  Cart_pb2_grpc.add_CartServicer_to_server(CartServicer(), server)
  DIFC_Shim_Interface.register(str.encode("Cart"),str.encode("127.0.0.1:40000"))
  server.add_insecure_port('[::]:50053')
  server.start()
  server.wait_for_termination()
  DIFC_Shim_Interface.unregister()


CartService_serve()
