import ctypes


libdifcshim = ctypes.CDLL('../DIFC_Shim/libdifcshim.so')
libdifcshim.difc_shim_init.argtypes=(ctypes.c_char_p,ctypes.c_char_p)
libdifcshim.difc_shim_destroy.argtypes=()
libdifcshim.difc_message.argtypes=(ctypes.c_char_p,ctypes.c_char_p,ctypes.c_char_p)
libdifcshim.difc_add_policy.argtypes=(ctypes.c_char_p,ctypes.c_char_p)
libdifcshim.difc_add_tag.argtypes=(ctypes.c_char_p,ctypes.c_char_p)
libdifcshim.difc_initial_trust.argtypes= ( ctypes.c_char_p, )


def register(moduleName,ip):
    libdifcshim.difc_shim_init(ctypes.c_char_p(moduleName),ctypes.c_char_p(ip))
    


def unregister():
    libdifcshim.difc_shim_destroy(None)


def difc_message(target_module,target_interface,label):
    res=libdifcshim.difc_message(target_module,target_interface,label)
    return bool(res)

def add_policy(interface, label):
    res=libdifcshim.difc_add_policy(ctypes.c_char_p(interface),ctypes.c_char_p(label))
    return bool(res)

def add_tag(owner, tag):
    res=libdifcshim.difc_add_tag(ctypes.c_char_p(owner),ctypes.c_char_p(tag))
    return bool(res)

def initial_trust(trust):
    res=libdifcshim.difc_initial_trust(trust)
    return bool(res)

