import grpc
import Common_pb2
import Common_pb2_grpc
import Item_pb2
import Item_pb2_grpc
import Account_pb2
import Account_pb2_grpc
import Cart_pb2
import Cart_pb2_grpc
import DIFC_Shim_Interface

def run():
    #register the shim
    DIFC_Shim_Interface.register(str.encode("Frontend"),str.encode("127.0.0.1:40000"))
    #Initial Trust
    DIFC_Shim_Interface.initial_trust(str.encode("[Item:0.6;Cart:0.8;Account:0.7]"))
    #get the tags we need
    DIFC_Shim_Interface.add_tag(str.encode("Item"),str.encode("ItemID"))
    DIFC_Shim_Interface.add_tag(str.encode("Account"),str.encode("AccountInfo"))
    
    #Channels to the ItemShop's services
    item_channel = grpc.insecure_channel('localhost:50051')
    account_channel = grpc.insecure_channel('localhost:50052')
    cart_channel = grpc.insecure_channel('localhost:50053')

    #GRPC Stubs
    item_stub = Item_pb2_grpc.ItemStub(item_channel)
    account_stub = Account_pb2_grpc.AccountStub(account_channel)
    cart_stub = Cart_pb2_grpc.CartStub(cart_channel)
    
    itemID=Common_pb2.ItemID(itemID=0)
    DIFC_Shim_Interface.difc_message(str.encode("Item"),str.encode("GetItem"),str.encode("<ItemInfo:0.5:1>;v0]"))
    print(item_stub.GetItem(itemID))
    itemID=Common_pb2.ItemID(itemID=1)
    print(item_stub.GetItem(itemID))
    DIFC_Shim_Interface.difc_message(str.encode("Item"),str.encode("GetItem"),str.encode("<ItemInfo:0.5:1>;v0]"))
    DIFC_Shim_Interface.difc_message(str.encode("Account"),str.encode("Register"),str.encode("[<AccountInfo:0.9:1>;v0]"))
    accountInfo=Account_pb2.AccountInfo(userName="Longbow", password="Password")
    print(account_stub.Register(accountInfo))
    
    #DIFC_Shim_Interface.difc_message(str.encode("Account"),str.encode("Login"),str.encode("[< >,v0]"))
    #print(account_stub.Login(accountInfo))
    #accountID=Common_pb2.AccountID(accountID=1)
    #cartItem = Cart_pb2.CartItemDesc(itemID=0,quantity=1,accountID=0)
    #DIFC_Shim_Interface.difc_message(str.encode("Account"),str.encode("UserExists"),str.encode("[< >,v0]"))
    #print(account_stub.UserExists(accountID))
    #DIFC_Shim_Interface.difc_message(str.encode("Cart"),str.encode("AddItemToCart"),str.encode("[< >,v0]"))
    #print(cart_stub.AddItemToCart(cartItem))
    #DIFC_Shim_Interface.difc_message(str.encode("Account"),str.encode("RemItemFromCart"),str.encode("[< >,v0]"))
    #print(cart_stub.RemItemFromCart(cartItem))
    #DIFC_Shim_Interface.difc_message(str.encode("Account"),str.encode("Checkout"),str.encode("[< >,v0]"))
    #print(cart_stub.CheckOutCart(accountID))
    
    DIFC_Shim_Interface.unregister()

run()
