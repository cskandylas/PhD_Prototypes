from concurrent import futures
import logging
import DIFC_Shim_Interface

import grpc
import Item_pb2
import Item_pb2_grpc


class ItemServicer(Item_pb2_grpc.ItemServicer):

    def __init__(self):
        None

    def GetItem(self,request,context):
         itemInfo = request
         return Item_pb2.ItemInfo(itemID=-1,description="No Item with that ID found",price=-1)
    

#TODO hide the encode stuff in the DIFC interface
def ItemService_serve():
  server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
  Item_pb2_grpc.add_ItemServicer_to_server(ItemServicer(), server)
  server.add_insecure_port('[::]:50051')
  DIFC_Shim_Interface.register(str.encode("Item"),str.encode("127.0.0.1:40000"))
  DIFC_Shim_Interface.initial_trust(str.encode("[Frontend:0.6;Cart:0.8]"))
  DIFC_Shim_Interface.add_policy(str.encode("GetItem"),str.encode("<Item:ItemID:0.5:5><Item:ItemInfo:0.5:5>"))
  
  server.start()
  server.wait_for_termination()
  DIFC_Shim_Interface.unregister()



ItemService_serve()
