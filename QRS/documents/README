
REQUIREMENTS
- HaiQ needs to be installed: https://javier-camara.github.io/HaiQ/
- For the haiq_gen tool, gmake and a recent C compiler are required.

WORKFLOW STEPS

Step 1:
- Describe the possible components, their interfaces and invocations in YAADL.
  For a high level description of YAADL see YAADL in this directory.

Step 2:
- Add the security characteristics of the system to the YAADL specification    

Step 3:
- Run haiqgen providing the YAADL description as input and the name of the file
  to be passed to haiq as output. This step will generate a partial haiq
  specification.

Step 4:
- Edit the generated file to add/modify the attack effects, the countermeasure
  effects and the takeover rules for each component in addition to making any
  other changes to more precisely specify the attacker/defenses behavior.
  Specify how capabilities are chained to allow deeper attacks if necessary.

Step 5:
- Edit the generated file to add incompatibilities between the components and
  defenses as facts. Add the properties to be model checked and the number
  of allowed defenses.

Step 6:
- Run Haiq.

SECURITY CHARACTERISTICS SPECIFICATION

Vulnerabilities:
Vulnerabilities are specified as architectural properties of the interface that
is vulnerable to them. Specifically, each vulnerability is specified by an
exploit property. For each vulnerability specify its name,  success probability
and cost using the pattern: Property exploit = 'name:sucessprobability:cost'
For example, a login interface with an authentication bypass vulnerability is
specified as follows where its success probability is 0.8 and its cost is 1:
Interface login {
Property exploit = 'cwe288:0.8:1'
}

Defenses (Countermeasures):
Defenses are also specified as architectural properties. When a defense counters
a specific vulnerability of a specific interface of a  component it is specified
as a property of that interface. In that case the architectural property is
named defense and has the following pattern:
Property defense = 'name:successprob:cost:mitigation' where name is the defense
name, successprob corresponds to the probability that the defense will succeed
cost is the associated cost and mitigation is the new success probability
of the attack when the defense is active. For example, adding two-factor
authentication as a defense against the authentication bypass vulnerability
described previously is done as follows:
Interface login {
Property exploit = 'cwe288:0.8:1'
Property defense = 'twofactorauthentication:0.8:1:0.3'
}
Note that the probability that cwe288 will succeed after twofactorauthentication
has been activated is 0.3.
Defenses that do not counter specific vulnerabilities are specified as component
properties instead, for example a database rollback defense is specified as
follows:
Component database {
Interface execquery {
Property exploit = 'cwe89:0.9:2'
}
Property defense = 'rollbackdb:0.45:2'
}

Features:
The features a component can satisfy are described as an architectural property
at the component level. Specifically by a features property with a colon
separated list of features. For example a component A providing features a, b,c:
Component A {
Property features 'a:b:c'
}

Attacker Capabilities:
Attacker capabilities are specified at the system level as the capabilities
property. They are given as a comma separated list of capabilities. For example:
Property capabilities = 'authbypass,leakadminpass,leakuserinfo,leakuserfiles'







  
