ModelType: mdp

// some definitions to make things more readable and keep the state space small
// Turn## global Turn : [0..1] init 0;
## formula attacker = 0;
## formula defender = 1;
//used to sequence attacker and defender actions
## global aDone : bool init false;
## global dDone : bool init false;
//give up variable for attacker resiliency
## global give_up : bool init false;
## global all_exploited : bool init false;

//Did an attack succeed?
## global cwe288backendsuccess : bool init false;
## global cwe305backendsuccess : bool init false;
## global cwe20accountmanagersuccess : bool init false;
## global cwe20documentmanagersuccess : bool init false;
## global cwe89databasesuccess : bool init false;
## global cwe73filemanagersuccess : bool init false;

//some more ugliness required to handle attacker state being split among attacker and components
## formula none = 0;
## formula failed = 1;
## formula effect = 2;
## formula cwe288 = 3;
## formula cwe305 = 4;
## formula cwe20 = 5;
## formula cwe20 = 6;
## formula cwe89 = 7;
## formula cwe73 = 8;
## global selected : [0..8] init 0;

//Did a defense succeed?
## global cm_twofactorauthentication: bool init false;
## global cm_whitelist: bool init false;
## global cm_regeneratebackend: bool init false;
## global cm_sanitizecommandsa: bool init false;
## global cm_sanitizecommandsd: bool init false;
## global cm_sanitizequeries: bool init false;
## global cm_rollbackdb: bool init false;
## global cm_backuprestore: bool init false;

//simulate and selection similar to how haiq does the selectedAttack and selected vars.
## formula defnone = 0;
## formula twofactorauthentication = 1;
## formula whitelist = 2;
## formula regeneratebackend = 3;
## formula sanitizecommandsa = 4;
## formula sanitizecommandsd = 5;
## formula sanitizequeries = 6;
## formula rollbackdb = 7;
## formula backuprestore = 8;

//hack to not enable extraneous attacks
## global en_frontend : bool init false;
## global en_backend : bool init false;
## global en_accountmanager : bool init false;
## global en_documentmanager : bool init false;
## global en_database : bool init false;
## global en_filemanager : bool init false;

//attacker capabilities
## global cap_authbypass : bool init false;
## global cap_leakadminpass : bool init false;
## global cap_leakuserinfo : bool init false;
## global cap_leakuserfiles : bool init false;
## global cap_userquerycontrol : bool init false;
## global cap_filequerycontrol : bool init false;


one sig TheSystem {
   components : some Component
}
</
/>

abstract sig Feature {
implementedBy: one Component
}
</
/>
one sig Defender { defenses: some Defense }
</

var selectionMade : bool init false;

[defenses:enableDef] (!selectionMade) & (Turn=defender) & (dDone=false)  -> 1: (selectionMade'=true);

//do nothing
[] (Turn=defender) & (!selectionMade) & (dDone=false) -> (dDone'=true) & selectionMade'=true);

// back to the attacker
[defDone] (Turn=defender) & (dDone=true) -> 1: (Turn'=attacker) & (dDone'=false) & (defSelected=defnone) & (selectionMade'=false);
/>

abstract sig Component { provides: some Feature, belongsTo: one TheSystem }
</

var takenOver: bool init false;

/>

long sig frontend extends Component { }
</

//isalive
[] (!en_frontend) -> 1 : (en_frontend'=true)

//attack effects

//countermeasure effects

//takenOver rule

/>

long sig backend extends Component { }
</

//isalive
[] (!en_backend) -> 1 : (en_backend'=true)

//attack effects

//countermeasure effects

//takenOver rule

/>

long sig accountmanager extends Component { }
</

//isalive
[] (!en_accountmanager) -> 1 : (en_accountmanager'=true)

//attack effects

//countermeasure effects

//takenOver rule

/>

long sig documentmanager extends Component { }
</

//isalive
[] (!en_documentmanager) -> 1 : (en_documentmanager'=true)

//attack effects

//countermeasure effects

//takenOver rule

/>

long sig database extends Component { }
</

//isalive
[] (!en_database) -> 1 : (en_database'=true)

//attack effects

//countermeasure effects

//takenOver rule

/>

long sig filemanager extends Component { }
</

//isalive
[] (!en_filemanager) -> 1 : (en_filemanager'=true)

//attack effects

//countermeasure effects

//takenOver rule

/>

one sig CommandDelegateHandle extends Feature { } {   }
</

one sig Attacker { }
</

//Here we have the attacker options, each option is selected based on what exploits are currently available to the attacker
formula cwe288backendprob = cm_twofactorauthentication? 0.3 : 0.8;
formula cwe288backendfail = (1 - cwe288backendprob)/2
formula cwe288backendgiveup = (1 - cwe288backendprob)/2
[]  (!give_up) & (en_backend) & (Turn=attacker) & (aDone=false) & (selected=none) & (cwe288backendsuccess=false) -> cwe288backendprob   : (Turn'=attacker)  & (selected'=cwe288) & (cwe288backendsuccess'=true) + cwe288backendfail : (Turn'=attacker) & (aDone'=true) & (selected'=failed) + cwe288backendgiveup : (give_up'=true)

formula cwe305backendprob = cm_whitelist? 0.3 : 0.75;
formula cwe305backendfail = (1 - cwe305backendprob)/2
formula cwe305backendgiveup = (1 - cwe305backendprob)/2
[]  (!give_up) & (en_backend) & (Turn=attacker) & (aDone=false) & (selected=none) & (cwe305backendsuccess=false) -> cwe305backendprob   : (Turn'=attacker)  & (selected'=cwe305) & (cwe305backendsuccess'=true) + cwe305backendfail : (Turn'=attacker) & (aDone'=true) & (selected'=failed) + cwe305backendgiveup : (give_up'=true)

formula cwe20accountmanagerprob = cm_sanitizecommandsa? 0.3 : 0.9;
formula cwe20accountmanagerfail = (1 - cwe20accountmanagerprob)/2
formula cwe20accountmanagergiveup = (1 - cwe20accountmanagerprob)/2
[]  (!give_up) & (en_accountmanager) & (Turn=attacker) & (aDone=false) & (selected=none) & (cwe20accountmanagersuccess=false) -> cwe20accountmanagerprob   : (Turn'=attacker)  & (selected'=cwe20) & (cwe20accountmanagersuccess'=true) + cwe20accountmanagerfail : (Turn'=attacker) & (aDone'=true) & (selected'=failed) + cwe20accountmanagergiveup : (give_up'=true)

formula cwe20documentmanagerprob = cm_sanitizecommandsd? 0.3 : 0.9;
formula cwe20documentmanagerfail = (1 - cwe20documentmanagerprob)/2
formula cwe20documentmanagergiveup = (1 - cwe20documentmanagerprob)/2
[]  (!give_up) & (en_documentmanager) & (Turn=attacker) & (aDone=false) & (selected=none) & (cwe20documentmanagersuccess=false) -> cwe20documentmanagerprob   : (Turn'=attacker)  & (selected'=cwe20) & (cwe20documentmanagersuccess'=true) + cwe20documentmanagerfail : (Turn'=attacker) & (aDone'=true) & (selected'=failed) + cwe20documentmanagergiveup : (give_up'=true)

formula cwe89databaseprob = cm_sanitizequeries? 0.3 : 0.9;
formula cwe89databasefail = (1 - cwe89databaseprob)/2
formula cwe89databasegiveup = (1 - cwe89databaseprob)/2
[]  (!give_up) & (en_database) & (Turn=attacker) & (aDone=false) & (selected=none) & (cwe89databasesuccess=false) -> cwe89databaseprob   : (Turn'=attacker)  & (selected'=cwe89) & (cwe89databasesuccess'=true) + cwe89databasefail : (Turn'=attacker) & (aDone'=true) & (selected'=failed) + cwe89databasegiveup : (give_up'=true)

formula cwe73filemanagerprob = 0.8
formula cwe73filemanagerfail = (1 - cwe73filemanagerprob)/2
formula cwe73filemanagergiveup = (1 - cwe73filemanagerprob)/2
[]  (!give_up) & (en_filemanager) & (Turn=attacker) & (aDone=false) & (selected=none) & (cwe73filemanagersuccess=false) -> cwe73filemanagerprob   : (Turn'=attacker)  & (selected'=cwe73) & (cwe73filemanagersuccess'=true) + cwe73filemanagerfail : (Turn'=attacker) & (aDone'=true) & (selected'=failed) + cwe73filemanagergiveup : (give_up'=true)


//Here maybe we want the minimum possible attack cost.
//Same here, minimum possible attack cost required maybe, removing this for now to make model smaller
//force attacker to take an action or deadlock
[]  (!give_up) & (Turn=attacker) & (aDone=true) & (selected!=none)  -> 1: (Turn'=defender) & (aDone'=false) & (selected'=none);

reward turns [] Turn=attacker & selected=cwe288  & !give_up & aDone=true  : 1;
reward turns [] Turn=attacker & selected=cwe305  & !give_up & aDone=true  : 1;
reward turns [] Turn=attacker & selected=cwe20  & !give_up & aDone=true  : 1;
reward turns [] Turn=attacker & selected=cwe20  & !give_up & aDone=true  : 1;
reward turns [] Turn=attacker & selected=cwe89  & !give_up & aDone=true  : 2;
reward turns [] Turn=attacker & selected=cwe73  & !give_up & aDone=true  : 2;
/>

abstract sig Defense { ddefender : one Defender}
</

formula defSuccProb = 0.9;
enum defenseState: {defNone, defSuccess, defFailed,defDone};
var defState : [defenseState] init none;

[ddefender:enableDef]   (defState=defNone) & (Turn=defender) & (dDone=false)  -> defSuccProb : (defState'=defSuccess) +  1 - defSuccProb: (defState'=defFailed);
[] (Turn=defender)  & (dDone=false) & (defState=defFailed)   -> 1 : (dDone'=true) & (defState'=defDone) ;
[] (Turn=defender)   & (dDone=false) & (defState=defSuccess) & (defEffApplied)   -> 1 : (dDone'=true) & (defState'=defDone) & (defEffApplied'=false);

reward defRew    (defEffApplied) & (defSelected=twofactorauthentication) : 1;
reward defRew    (defEffApplied) & (defSelected=whitelist) : 1;
reward defRew    (defEffApplied) & (defSelected=regeneratebackend) : 2;
reward defRew    (defEffApplied) & (defSelected=sanitizecommandsa) : 1;
reward defRew    (defEffApplied) & (defSelected=sanitizecommandsd) : 1;
reward defRew    (defEffApplied) & (defSelected=sanitizequeries) : 1;
reward defRew    (defEffApplied) & (defSelected=rollbackdb) : 2;
reward defRew    (defEffApplied) & (defSelected=backuprestore) : 2;


/>

lone sig twofactorauthentication extends Defense {   }
</

formula defSuccProb = 0.8; [] (cm_twofactorauthentication=false)  & (Turn=defender)  & (dDone=false) & (defState=defSuccess)  -> 1 :  (cm_twofactorauthentication'=true) & (defSelected'=twofactorauthentication) & (defEffApplied'=true) ;

/>

lone sig whitelist extends Defense {   }
</

formula defSuccProb = 0.9; [] (cm_whitelist=false)  & (Turn=defender)  & (dDone=false) & (defState=defSuccess)  -> 1 :  (cm_whitelist'=true) & (defSelected'=whitelist) & (defEffApplied'=true) ;

/>

lone sig regeneratebackend extends Defense {   }
</

formula defSuccProb = 0.4; [] (cm_regeneratebackend=false)  & (Turn=defender)  & (dDone=false) & (defState=defSuccess)  -> 1 :  (cm_regeneratebackend'=true) & (defSelected'=regeneratebackend) & (defEffApplied'=true) ;

/>

lone sig sanitizecommandsa extends Defense {   }
</

formula defSuccProb = 0.75; [] (cm_sanitizecommandsa=false)  & (Turn=defender)  & (dDone=false) & (defState=defSuccess)  -> 1 :  (cm_sanitizecommandsa'=true) & (defSelected'=sanitizecommandsa) & (defEffApplied'=true) ;

/>

lone sig sanitizecommandsd extends Defense {   }
</

formula defSuccProb = 0.75; [] (cm_sanitizecommandsd=false)  & (Turn=defender)  & (dDone=false) & (defState=defSuccess)  -> 1 :  (cm_sanitizecommandsd'=true) & (defSelected'=sanitizecommandsd) & (defEffApplied'=true) ;

/>

lone sig sanitizequeries extends Defense {   }
</

formula defSuccProb = 0.85; [] (cm_sanitizequeries=false)  & (Turn=defender)  & (dDone=false) & (defState=defSuccess)  -> 1 :  (cm_sanitizequeries'=true) & (defSelected'=sanitizequeries) & (defEffApplied'=true) ;

/>

lone sig rollbackdb extends Defense {   }
</

formula defSuccProb = 0.45; [] (cm_rollbackdb=false)  & (Turn=defender)  & (dDone=false) & (defState=defSuccess)  -> 1 :  (cm_rollbackdb'=true) & (defSelected'=rollbackdb) & (defEffApplied'=true) ;

/>

lone sig backuprestore extends Defense {   }
</

formula defSuccProb = 0.5; [] (cm_backuprestore=false)  & (Turn=defender)  & (dDone=false) & (defState=defSuccess)  -> 1 :  (cm_backuprestore'=true) & (defSelected'=backuprestore) & (defEffApplied'=true) ;

/>


fact {
    defenses= ~ddefender
    provides = ~implementedBy
    components = ~belongsTo
}

fact {

}

pred  test{

}

//Restrict max number of enabled defenses
run test for 1 Attacker, 1 Defender, 5 Defense 