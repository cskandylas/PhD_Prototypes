#include "arc_property.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void init_int_property(struct arc_property *property, char *name, int value)
{
    property->name=malloc(strlen(name)+1);
    strcpy(property->name,name);
    property->value.int_value=value;
    property->type=INT;
}
void init_dbl_property(struct arc_property *property, char *name, double value)
{
    property->name=malloc(strlen(name)+1);
    strcpy(property->name,name);
    property->value.double_value=value;
    property->type=DOUBLE;
}
void init_str_property(struct arc_property *property, char *name, char *value)
{
    property->name=malloc(strlen(name)+1);
    strcpy(property->name,name);
    
    property->value.str_value=malloc(strlen(value)+1);
    strcpy(property->value.str_value,value);
    property->type=STRING;
}

void print_property(struct arc_property *property)
{
    printf("Property ");
    if(property->type==INT)
	printf("%s:%d\n",property->name,property->value.int_value);
    else if(property->type==DOUBLE)
	printf("%s:%f\n",property->name,property->value.double_value);
    else
	printf("%s:%s\n",property->name,property->value.str_value);
}

void destroy_int_property(struct arc_property *property)
{
    free(property->name);
}

void destroy_dbl_property(struct arc_property *property)
{
    free(property->name);
}
void destroy_str_property(struct arc_property *property)
{
    free(property->name);
    if(property->value.str_value)
    {
	//free(property->value.str_value);
    }
    
}

void destroy_property(struct arc_property *property)
{
    if(property->type==INT)
	destroy_int_property(property);
    else if(property->type==DOUBLE)
	destroy_dbl_property(property);
    else
	destroy_str_property(property);
}

struct arc_property *clone_property(struct arc_property *property)
{
    struct arc_property *clone=malloc(sizeof(struct arc_property));
    clone->name=malloc(strlen(property->name)+1);
    strcpy(clone->name,property->name);
    if(property->type==INT)
	clone->value.int_value=property->value.int_value;
    else if(property->type==DOUBLE)
	clone->value.double_value=property->value.double_value;
    else
    {
	clone->value.str_value=malloc(strlen(property->value.str_value)+1);
	strcpy(clone->value.str_value,property->value.str_value);
    }
    return clone;
}
