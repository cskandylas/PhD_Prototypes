#ifndef ARC_PROPERTY
#define ARC_PROPERTY


enum property_type {INT=0,DOUBLE,STRING,INVALID};

union property_value
{
    int int_value;
    double double_value;
    char *str_value;
};


struct arc_property
{
    char *name;
    enum property_type type;
    union property_value value;
};

void init_int_property(struct arc_property *property, char *name, int value);
void init_dbl_property(struct arc_property *property, char *name, double value);
void init_str_property(struct arc_property *property, char *name, char *value);
struct arc_property *clone_property(struct arc_property *property);

void destroy_property(struct arc_property *property);
void destroy_int_property(struct arc_property *property);
void destroy_dbl_property(struct arc_property *property);
void destroy_str_property(struct arc_property *property);

void print_property(struct arc_property *property);


#endif
