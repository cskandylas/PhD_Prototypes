#include "haiq_model.h"

#include <stdlib.h>
#include <string.h>

#include "tokenize.h"

void defense_init_str(struct defense *d, char *text)
{

    //pattern: name:prob:cost:effect    
    struct tokenizer dtok;
    tokenizer_init(&dtok,text,":");
    d->name = next_token(&dtok);
    d->prob = next_token(&dtok);
    d->cost = next_token(&dtok);
    d->effect = next_token(&dtok);
    d->target = NULL;
    
}

void attack_init_str(struct attack *a, char *text)
{

    //pattern: name:prob:cost
    struct tokenizer atok;
    tokenizer_init(&atok,text,":");
    a->name = next_token(&atok);
    a->prob = next_token(&atok);
    a->cost = next_token(&atok);
    a->target = NULL;
    a->defense = NULL;
}



void haiq_model_init(struct haiq_model *h_m)
{
    h_m->num_attacks = 0;
    h_m->max_attacks = HAIQ_MODEL_INIT_ATTACKS;
    h_m->attacks = malloc(h_m->max_attacks * sizeof(struct attack));

    h_m->num_defenses = 0;
    h_m->max_defenses = HAIQ_MODEL_INIT_DEFENSES;
    h_m->defenses = malloc(h_m->max_defenses * sizeof(struct defense));

    h_m->num_features = 0;
    h_m->max_features = HAIQ_MODEL_INIT_FEATURES;
    h_m->features = malloc(h_m->max_features * sizeof(struct feature));

    h_m->num_capabilities = 0;
    h_m->max_capabilities = HAIQ_MODEL_INIT_CAPABILITIES;
    h_m->capabilities = malloc(h_m->max_capabilities * sizeof(char*));
}


void haiq_model_destroy(struct haiq_model *h_m)
{
    //let OS do the cleanup this is a short-lived program
}
void haiq_model_add_attack(struct haiq_model *h_m, struct attack *a)
{
    if(h_m->num_attacks >= h_m->max_attacks)
    {
	h_m->max_attacks *=2;
	h_m->attacks = realloc(h_m->attacks, h_m->max_attacks*sizeof(struct attack));
    }
    h_m->attacks[h_m->num_attacks] = *a;
    h_m->num_attacks++;
}

void haiq_model_add_defense(struct haiq_model *h_m, struct defense *d)
{
    if(h_m->num_defenses >= h_m->max_defenses)
    {
	h_m->max_defenses *=2;
	h_m->defenses = realloc(h_m->defenses, h_m->max_defenses*sizeof(struct defense));
    }
    h_m->defenses[h_m->num_defenses] = *d;
    h_m->num_defenses++;
}
void haiq_model_add_feature(struct haiq_model *h_m, struct feature *f)
{
    if(h_m->num_features >= h_m->max_features)
    {
	h_m->max_features *=2;
	h_m->features = realloc(h_m->features, h_m->max_features*sizeof(struct feature));
    }
    h_m->features[h_m->num_features] = *f;
    h_m->num_features++;
}

void haiq_model_add_capability(struct haiq_model *h_m, char *c)
{
    if(h_m->num_capabilities >= h_m->max_capabilities)
    {
	h_m->max_capabilities *=2;
	h_m->capabilities = realloc(h_m->capabilities, h_m->max_capabilities*sizeof(char*));
    }
    h_m->capabilities[h_m->num_capabilities] = c;
    h_m->num_capabilities++;
}



void build_haiq_model(struct haiq_model *h_m, struct arc_system *sys)
{
    for(int i=0;i<sys->num_components;i++)
    {
	struct arc_component *c = sys->components[i];
	
	for(int k=0;k<c->num_interfaces;k++)
	{
	    
	    struct arc_interface *i_f = c->interfaces[k];
	    struct attack *p_a = NULL;
	    //check interface properties for attacks and defenses
	    for(int l=0;l<i_f->num_properties;l++)
	    {
		struct arc_property *prop = i_f->properties[l];
		
		if(strcmp(prop->name, "exploit")==0)
		{
		    struct attack a;
		    attack_init_str(&a, prop->value.str_value);
		    
		    a.target = c->comp_name;
		    haiq_model_add_attack(h_m,&a);
		    p_a = &h_m->attacks[h_m->num_attacks-1];
		    
		}
		else if(strcmp(prop->name, "defense")==0)
		{
		    struct defense d;
		    defense_init_str(&d, prop->value.str_value);
		    d.target = c->comp_name;
		    haiq_model_add_defense(h_m,&d);
		    p_a->defense = &h_m->defenses[h_m->num_defenses-1];
		    
		}
	    }
	    
	}


	//Check component properties for defenses and features
	for(int j=0;j<c->num_properties;j++)
	{
	    struct arc_property *prop = c->properties[j];
	    if(strcmp(prop->name, "defense")==0)
	    {
		struct defense d;
		defense_init_str(&d, prop->value.str_value);
		d.target = c->comp_name;
		haiq_model_add_defense(h_m,&d);
	    }
	    else if(strcmp(prop->name, "features")==0)
	    {
		struct tokenizer ftok;
		tokenizer_init(&ftok,prop->value.str_value,":");
		char *name = next_token(&ftok);
		while(name)
		{
		    struct feature f;
		    f.name = name;
		    f.implemented_by = c->comp_name;
		    haiq_model_add_feature(h_m,&f);
		    name= next_token(&ftok);
		}
	    }
	    
	}
    }


    //parse capabilities
    struct arc_property *cp = sys_find_property(sys,"capabilities");
    if(cp)
    {
	struct tokenizer ftok;
	tokenizer_init(&ftok,cp->value.str_value,",");
	char *cap = next_token(&ftok);
	while(cap)
	{
	    
	    haiq_model_add_capability(h_m,cap);
	    cap= next_token(&ftok);
	}
    }
    
}




