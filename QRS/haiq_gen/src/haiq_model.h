#include <stddef.h>

#include "arc_system.h"

#ifndef HAIQ_MODEL_H
#define HAIQ_MODEL_H


struct defense
{
    char *name;
    char *prob;
    char *cost;
    char *target;
    char *effect;
};

void defense_init_str(struct defense *d, char *text);

struct attack
{
    char *name;
    char *prob;
    char *cost;
    char *target;
    struct defense *defense;
};


void attack_init_str(struct attack *a, char *text);

struct feature
{
    char *name;
    char *implemented_by;
};

struct haiq_model
{
    struct attack *attacks;
    size_t num_attacks;
    size_t max_attacks;
    struct defense *defenses;
    size_t num_defenses;
    size_t max_defenses;
    struct feature *features;
    size_t num_features;
    size_t max_features;
    char **capabilities;
    size_t num_capabilities;
    size_t max_capabilities;
};


#define HAIQ_MODEL_INIT_ATTACKS 8
#define HAIQ_MODEL_INIT_DEFENSES 8
#define HAIQ_MODEL_INIT_FEATURES 8
#define HAIQ_MODEL_INIT_CAPABILITIES 8

void haiq_model_init(struct haiq_model *h_m);
void haiq_model_destroy(struct haiq_model *h_m);
void haiq_model_add_attack(struct haiq_model *h_m, struct attack *a);
void haiq_model_add_defense(struct haiq_model *h_m, struct defense *d);
void haiq_model_add_feature(struct haiq_model *h_m, struct feature *f);
void haiq_model_add_capability(struct haiq_model *h_m, char *c);



void build_haiq_model(struct haiq_model *h_m, struct arc_system *sys);


#endif
