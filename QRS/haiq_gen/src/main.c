#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>

// for read_file
#include "file_utils.h"
// for arc_parse
#include "arc_parse.h"
// for arc_system
#include "arc_system.h"
//for string_buffer
#include "string_buffer.h"
//for haiq_model
#include "haiq_model.h"




void write_boilerplate(struct string_buffer *s_b)
{
    string_buffer_add_str(s_b, "ModelType: mdp\n\n");
    string_buffer_add_str(s_b, "// some definitions to make things more readable and keep the state space small\n");
    string_buffer_add_str(s_b, "// Turn");
    string_buffer_add_str(s_b, "## global Turn : [0..1] init 0;\n");
    string_buffer_add_str(s_b, "## formula attacker = 0;\n");
    string_buffer_add_str(s_b, "## formula defender = 1;\n");
    string_buffer_add_str(s_b, "//used to sequence attacker and defender actions\n");
    string_buffer_add_str(s_b, "## global aDone : bool init false;\n");
    string_buffer_add_str(s_b, "## global dDone : bool init false;\n");
    string_buffer_add_str(s_b, "//give up variable for attacker resiliency\n");
    string_buffer_add_str(s_b, "## global give_up : bool init false;\n");
    string_buffer_add_str(s_b, "## global all_exploited : bool init false;\n");
}

void write_common_sigs(struct string_buffer *s_b)
{
    string_buffer_add_str(s_b, "\n\none sig TheSystem {\n");
    string_buffer_add_str(s_b, "   components : some Component\n");
    string_buffer_add_str(s_b, "}\n</\n/>\n\n");
    string_buffer_add_str(s_b, "abstract sig Feature {\n");
    string_buffer_add_str(s_b, "implementedBy: one Component\n");
    string_buffer_add_str(s_b, "}\n</\n/>\n");
    string_buffer_add_str(s_b, "one sig Defender { defenses: some Defense }\n");
    string_buffer_add_str(s_b, "</\n\n");
    string_buffer_add_str(s_b, "var selectionMade : bool init false;\n\n");
    string_buffer_add_str(s_b, "[defenses:enableDef] (!selectionMade) & (Turn=defender) & (dDone=false)  -> 1: (selectionMade'=true);\n\n");
    string_buffer_add_str(s_b, "//do nothing\n");
    string_buffer_add_str(s_b, "[] (Turn=defender) & (!selectionMade) & (dDone=false) -> (dDone'=true) & selectionMade'=true);\n\n");
    string_buffer_add_str(s_b, "// back to the attacker\n");
    string_buffer_add_str(s_b, "[defDone] (Turn=defender) & (dDone=true) -> 1: (Turn'=attacker) & (dDone'=false) & (defSelected=defnone) & (selectionMade'=false);\n");
    string_buffer_add_str(s_b, "/>\n\n");
    string_buffer_add_str(s_b, "abstract sig Component { provides: some Feature, belongsTo: one TheSystem }\n");
    string_buffer_add_str(s_b, "</\n\n");
    string_buffer_add_str(s_b, "var takenOver: bool init false;\n\n");
    string_buffer_add_str(s_b, "/>\n\n");

}

void write_component(struct string_buffer *s_b, struct arc_component *c)
{
    //For each component make a component signature
    string_buffer_add_fmt(s_b, "long sig %s extends Component { }\n</\n\n",c->comp_name);
    string_buffer_add_str(s_b,"//isalive\n");
    string_buffer_add_fmt(s_b, "[] (!en_%s) -> 1 : (en_%s'=true)\n\n",c->comp_name, c->comp_name);
    string_buffer_add_str(s_b,"//attack effects\n\n");
    string_buffer_add_str(s_b,"//countermeasure effects\n\n");
    string_buffer_add_str(s_b,"//takenOver rule\n\n");
    string_buffer_add_str(s_b,"/>\n\n");
}

void write_components(struct string_buffer *s_b, struct arc_system *sys)
{

    //Iterate through the system and make a component signature for each component
    for(int i=0;i<sys->num_components;i++)
    {
	write_component(s_b, sys->components[i]);
    }
}

void write_attack(struct string_buffer *s_b, struct attack *atck)
{
    if(!atck->defense)
    {
	string_buffer_add_fmt(s_b, "formula %s%sprob = %s\n",atck->name,atck->target,atck->prob);
    }
    else
    {
	string_buffer_add_fmt(s_b, "formula %s%sprob = cm_%s? %s : %s;\n",atck->name,atck->target,atck->defense->name, atck->defense->effect, atck->prob);
    }
    string_buffer_add_fmt(s_b, "formula %s%sfail = (1 - %s%sprob)/2\n",atck->name,atck->target,atck->name,atck->target);
    string_buffer_add_fmt(s_b, "formula %s%sgiveup = (1 - %s%sprob)/2\n",atck->name,atck->target,atck->name,atck->target);

    string_buffer_add_fmt(s_b,"[]  (!give_up) & (en_%s) & (Turn=attacker) & (aDone=false) & (selected=none) & (%s%ssuccess=false)",atck->target,atck->name,atck->target);
    string_buffer_add_fmt(s_b," -> %s%sprob   : (Turn'=attacker)  & (selected'=%s) & (%s%ssuccess'=true)",atck->name,atck->target,atck->name,atck->name,atck->target);
    string_buffer_add_fmt(s_b," + %s%sfail : (Turn'=attacker) & (aDone'=true) & (selected'=failed) + %s%sgiveup : (give_up'=true)\n\n",atck->name,atck->target,atck->name,atck->target);
}

void write_attacker_sig(struct string_buffer *s_b, struct haiq_model *h_m)
{
    string_buffer_add_str(s_b, "one sig Attacker { }\n</\n\n");
    string_buffer_add_str(s_b, "//Here we have the attacker options, each option is selected based on what exploits are currently available to the attacker\n");
    for(int i=0;i<h_m->num_attacks;i++)
    {
	struct attack *a = &h_m->attacks[i]; 
	write_attack(s_b, a);
    }

    string_buffer_add_str(s_b,"\n//Here maybe we want the minimum possible attack cost.\n");
    string_buffer_add_str(s_b,"//Same here, minimum possible attack cost required maybe, removing this for now to make model smaller\n");
    string_buffer_add_str(s_b,"//force attacker to take an action or deadlock\n");
    string_buffer_add_str(s_b,"[]  (!give_up) & (Turn=attacker) & (aDone=true) & (selected!=none)  -> 1: (Turn'=defender) & (aDone'=false) & (selected'=none);\n\n");

    for(int i=0;i<h_m->num_attacks;i++)
    {
	struct attack *a = &h_m->attacks[i]; 
	string_buffer_add_fmt(s_b,"reward turns [] Turn=attacker & selected=%s  & !give_up & aDone=true  : %s;\n",a->name,a->cost);

    }

    string_buffer_add_str(s_b,"/>\n\n");
}

void write_defense_sig(struct string_buffer *s_b, struct defense *d)
{
    string_buffer_add_fmt(s_b,"lone sig %s extends Defense {   }\n</\n\n", d->name);
    string_buffer_add_fmt(s_b,"formula defSuccProb = %s;",d->prob);
    string_buffer_add_fmt(s_b," [] (cm_%s=false)  & (Turn=defender)  & (dDone=false) & (defState=defSuccess) ",d->name);
    string_buffer_add_fmt(s_b, " -> 1 :  (cm_%s'=true) & (defSelected'=%s) & (defEffApplied'=true) ;",d->name,d->name);
    string_buffer_add_str(s_b,"\n\n/>\n\n");
}

void write_defense_sigs(struct string_buffer *s_b, struct haiq_model *h_m)
{


    string_buffer_add_str(s_b,"abstract sig Defense { ddefender : one Defender}\n</\n\n");
    string_buffer_add_str(s_b,"formula defSuccProb = 0.9;\n");
    string_buffer_add_str(s_b,"enum defenseState: {defNone, defSuccess, defFailed,defDone};\n");
    string_buffer_add_str(s_b,"var defState : [defenseState] init none;\n\n");

    string_buffer_add_str(s_b,"[ddefender:enableDef]   (defState=defNone) & (Turn=defender) & (dDone=false)  -> defSuccProb : (defState'=defSuccess) +  1 - defSuccProb: (defState'=defFailed);\n");

    string_buffer_add_str(s_b,"[] (Turn=defender)  & (dDone=false) & (defState=defFailed)   -> 1 : (dDone'=true) & (defState'=defDone) ;\n");
    string_buffer_add_str(s_b,"[] (Turn=defender)   & (dDone=false) & (defState=defSuccess) & (defEffApplied)   -> 1 : (dDone'=true) & (defState'=defDone) & (defEffApplied'=false);\n\n");


    for(int i=0;i<h_m->num_defenses;i++)
    {
	struct defense *d = &h_m->defenses[i];
	string_buffer_add_fmt(s_b,"reward defRew    (defEffApplied) & (defSelected=%s) : %s;\n",d->name, d->cost);
    }
    string_buffer_add_str(s_b,"\n\n/>\n\n");

    
    
    for(int i=0;i<h_m->num_defenses;i++)
    {
	struct defense *d = &h_m->defenses[i];
	write_defense_sig(s_b,d);
    }
}

void write_feature_sigs(struct string_buffer *s_b, struct haiq_model *h_m)
{
    for(int i=0;i<h_m->num_features;i++)
    {
	struct feature *f = &h_m->features[i];
	string_buffer_add_fmt(s_b,"one sig %s extends Feature { } {   }\n</\n\n", f->name);
    }
}


void write_attack_globals(struct string_buffer *s_b, struct haiq_model *h_m)
{

    string_buffer_add_str(s_b, "\n//Did an attack succeed?\n");
    
    for(int i=0;i<h_m->num_attacks;i++)
    {
	struct attack *a = &h_m->attacks[i];
	string_buffer_add_fmt(s_b,"## global %s%ssuccess : bool init false;\n",a->name,a->target);
	
    }

    string_buffer_add_str(s_b, "\n//some more ugliness required to handle attacker state being split among attacker and components\n");
    string_buffer_add_str(s_b, "## formula none = 0;\n");
    string_buffer_add_str(s_b, "## formula failed = 1;\n");
    string_buffer_add_str(s_b, "## formula effect = 2;\n");
    for(int i=0;i<h_m->num_attacks;i++)
    {
	struct attack *a = &h_m->attacks[i];
	string_buffer_add_fmt(s_b,"## formula %s = %d;\n",a->name,i+3);
    }

    string_buffer_add_fmt(s_b,"## global selected : [0..%d] init 0;\n",h_m->num_attacks+2);

}

void write_defense_globals(struct string_buffer *s_b, struct haiq_model *h_m)
{
    string_buffer_add_str(s_b, "\n//Did a defense succeed?\n");

    for(int i=0;i<h_m->num_defenses;i++)
    {
	struct defense *d = &h_m->defenses[i];
	string_buffer_add_fmt(s_b,"## global cm_%s: bool init false;\n",d->name);
    }

    string_buffer_add_str(s_b,"\n//simulate and selection similar to how haiq does the selectedAttack and selected vars.\n");
    string_buffer_add_str(s_b,"## formula defnone = 0;\n");
    for(int i=0;i<h_m->num_defenses;i++)
    {
	struct defense *d = &h_m->defenses[i];
	string_buffer_add_fmt(s_b,"## formula %s = %d;\n",d->name,i+1);
    }

}


void write_capability_globals(struct string_buffer *s_b, struct haiq_model *h_m)
{
    string_buffer_add_str(s_b, "\n//attacker capabilities\n");

    for(int i=0;i<h_m->num_capabilities;i++)
    {
	char *cap= h_m->capabilities[i];
	string_buffer_add_fmt(s_b,"## global cap_%s : bool init false;\n",cap);
	
    }

}

void write_component_enabled_globals(struct string_buffer *s_b, struct arc_system *sys)
{
    string_buffer_add_str(s_b,"\n//hack to not enable extraneous attacks\n");
    for(int i=0;i<sys->num_components;i++)
    {
	struct arc_component *comp = sys->components[i];
	string_buffer_add_fmt(s_b,"## global en_%s : bool init false;\n",comp->comp_name);
    }
}

void write_facts(struct string_buffer *s_b)
{
    string_buffer_add_str(s_b, "\nfact {\n");
    string_buffer_add_str(s_b, "    defenses= ~ddefender\n");
    string_buffer_add_str(s_b, "    provides = ~implementedBy\n");
    string_buffer_add_str(s_b, "    components = ~belongsTo\n");
    string_buffer_add_str(s_b, "}\n");

    string_buffer_add_str(s_b, "\nfact {\n");
    string_buffer_add_str(s_b, "\n}\n");

    string_buffer_add_str(s_b, "\npred  test{\n");
    string_buffer_add_str(s_b, "\n}\n");

    string_buffer_add_str(s_b,"\n//Restrict max number of enabled defenses\n");
    string_buffer_add_str(s_b,"run test for 1 Attacker, 1 Defender, 5 Defense ");

}


int main(int argc, char *argv[])
{

    if(argc!=3)
    {
	fprintf(stderr, "Usage: haiqgen in.arch out.haiq\n");
	return EXIT_FAILURE;
    }

    //make the inputs explicit
    char *arc_file = argv[1];
    char *out_file = argv[2];

    //parse architecture description into an arc_system struct
    char *txt = read_file(arc_file);
    if(!txt)
    {
	return EXIT_FAILURE;
    }
    struct arc_system *sys = arc_parse(txt);

    struct haiq_model h_m;
    haiq_model_init(&h_m);
    build_haiq_model(&h_m,sys);

    
    //initialize the string buffer to hold our transformation.
    struct string_buffer s_b;
    char *b_buff = malloc(10*1024*1024);
    string_buffer_init(&s_b, b_buff, 10*1024*1024);
    write_boilerplate(&s_b);

    write_attack_globals(&s_b, &h_m);
    write_defense_globals(&s_b, &h_m);
    write_component_enabled_globals(&s_b,sys);
    write_capability_globals(&s_b,&h_m);
    
    
    write_common_sigs(&s_b);
    
    
    
    write_components(&s_b,sys);
    write_feature_sigs(&s_b,&h_m);
    write_attacker_sig(&s_b,&h_m);
    write_defense_sigs(&s_b,&h_m);

    write_facts(&s_b);
    
    //printf("%s\n",s_b.b_buff);
    
    //Write haiq file
    FILE *fp = fopen(out_file,"w");
    fprintf(fp, "%s",b_buff);
    fclose(fp);
    
    //cleanup
    string_buffer_clear(&s_b);
    destroy_system(sys);
    free(sys);
    free(txt);
    return EXIT_SUCCESS;
}
