import os
import subprocess
import random

MULVALROOT="/home/longbow/Tools/mulval"


colors=[]
colors_used=[]
fragments={}
reverse_fragments={}
fragment_colors={}


def create_color_list():
    f = open("/etc/X11/rgb.txt") 
    lines = f.read().splitlines()
    for line in lines:
        colors.append(line[line.find(next(filter(str.isalpha, line))):])
    f.close()

def pick_next_color():
    idx=random.randint(0,len(colors))
    color=colors.pop(idx)
    return color

def find_partitions(top_dir):
    
    single_exploit_dirs=os.listdir(top_dir)
    for dir in single_exploit_dirs :
        #if there is a fragment there
        fname=top_dir+"/"+dir+"/VERTICES.CSV"
        
        if os.path.exists(fname) and os.stat(fname).st_size > 0:
            fragment_name=top_dir+"_"+dir
            #print("Found Fragment:"+fragment_name)
            fragment_nodes=[]
            f = open(fname) 
            lines = f.read().splitlines()
            for line in lines:
                
                strip_quote=line[line.find('"')+1:]
                strip_quote=strip_quote[:strip_quote.find('"')]
                #print(strip_quote)
                fragment_nodes.append(strip_quote)
                if not strip_quote in reverse_fragments:
                    reverse_fragments[strip_quote]=fragment_name
            f.close() 
            fragments[fragment_name]=fragment_nodes
            fragment_colors[fragment_name]=pick_next_color()





def color_graph(top_dir):
    #if there is a graph here
    fname=top_dir+"/VERTICES.CSV"
    if os.stat(fname).st_size > 0:
        print("Rendering:"+fname)
        os.chdir(top_dir)
        subprocess.call(['sh', MULVALROOT+"/utils/render.sh"])  
        #read the dot file and crosscheck with vertices
        f = open("AttackGraph.dot") 
        lines = f.read().splitlines()
        f.close()
        cg = open("ColoredGraph.dot", "w")
        for line in lines:
            #if it has a shape we can color it
            if line.find("shape")!=-1:
                
                strip=line[line.find(":")+1:]
                strip=strip[:strip.find(":")]
                if strip in reverse_fragments:
                    print(reverse_fragments[strip])
                    line=line[:-2]
                    line+=", style=filled, color="+fragment_colors[reverse_fragments[strip]]+"];"
            print(line,file=cg)
                

random.seed(0)
create_color_list()

#print(colors)
find_partitions("single_exploit")
find_partitions("multi_exploit")
find_partitions("component_impact")
#print(reverse_fragments)
#print(fragments)
color_graph("all_in_one")


