:-(mvTrc(isCompromised(_h1225,_h1226,_h1227,0)),','(reachable(_h1225,_h1226),','(vulnExists(_h1225,_h1226,_h1227),assert_trace(because(0,rule_desc('Compromised by method invocation of compromised component',4),isCompromised(_h1225,_h1226,_h1227),[vulnExists(_h1225,_h1226,_h1227),reachable(_h1225,_h1226)]))))).

:-(mvTrc(singleExploit(_h1225,_h1226,1)),','(mvTrc(isCompromised(_h1225,_h1229,_h1226,_h1269)),assert_trace(because(1,rule_desc('SingleExploit becuase of component compromise',4),singleExploit(_h1225,_h1226),[isCompromised(_h1225,_h1229,_h1226)])))).

:-(mvTrc(multiExploit(_h1225,maliciousSite,2)),','(mvTrc(singleExploit(_h1225,cwe119,_h1272)),','(mvTrc(singleExploit(_h1237,cwe98,_h1301)),','(canReach(_h1237,_h1225,_h1242),assert_trace(because(2,rule_desc('Malicious Page Inclusion',50),multiExploit(_h1225,maliciousSite),[canReach(_h1237,_h1225,_h1242),singleExploit(_h1237,cwe98),singleExploit(_h1225,cwe119)])))))).

:-(mvTrc(multiExploit(_h1225,maliciousSite,3)),','(mvTrc(singleExploit(_h1225,cwe362,_h1272)),','(mvTrc(singleExploit(_h1237,cwe98,_h1301)),','(canReach(_h1237,_h1225,_h1242),assert_trace(because(3,rule_desc('Malicious Page Inclusion',50),multiExploit(_h1225,maliciousSite),[canReach(_h1237,_h1225,_h1242),singleExploit(_h1237,cwe98),singleExploit(_h1225,cwe362)])))))).

:-(mvTrc(multiExploit(_h1225,maliciousSite,4)),','(mvTrc(singleExploit(_h1225,cwe416,_h1272)),','(mvTrc(singleExploit(_h1237,cwe98,_h1301)),','(canReach(_h1237,_h1225,_h1242),assert_trace(because(4,rule_desc('Malicious Page Inclusion',50),multiExploit(_h1225,maliciousSite),[canReach(_h1237,_h1225,_h1242),singleExploit(_h1237,cwe98),singleExploit(_h1225,cwe416)])))))).

:-(mvTrc(multiExploit(_h1225,maliciousSite,5)),','(mvTrc(singleExploit(_h1225,cwe78,_h1272)),','(mvTrc(singleExploit(_h1237,cwe98,_h1301)),','(canReach(_h1237,_h1225,_h1242),assert_trace(because(5,rule_desc('Malicious Page Inclusion',50),multiExploit(_h1225,maliciousSite),[canReach(_h1237,_h1225,_h1242),singleExploit(_h1237,cwe98),singleExploit(_h1225,cwe78)])))))).

:-(mvTrc(multiExploit(_h1225,newsInjection,6)),','(mvTrc(singleExploit(_h1225,cwe89,_h1272)),','(mvTrc(singleExploit(_h1237,cwe98,_h1301)),','(canReach(_h1237,_h1225,_h1242),assert_trace(because(6,rule_desc('Injecting File via SQLInjection',50),multiExploit(_h1225,newsInjection),[canReach(_h1237,_h1225,_h1242),singleExploit(_h1237,cwe98),singleExploit(_h1225,cwe89)])))))).

:-(mvTrc(multiExploit(_h1225,newsInjection,7)),','(mvTrc(singleExploit(_h1225,cwe119,_h1272)),','(mvTrc(singleExploit(_h1237,cwe98,_h1301)),','(canReach(_h1237,_h1225,_h1242),assert_trace(because(7,rule_desc('Injecting File via code execution and xss',50),multiExploit(_h1225,newsInjection),[canReach(_h1237,_h1225,_h1242),singleExploit(_h1237,cwe98),singleExploit(_h1225,cwe119)])))))).

:-(mvTrc(multiExploit(_h1225,newsInjection,8)),','(mvTrc(singleExploit(_h1225,cwe362,_h1272)),','(mvTrc(singleExploit(_h1237,cwe98,_h1301)),','(canReach(_h1237,_h1225,_h1242),assert_trace(because(8,rule_desc('Injecting File via code execution and xss',50),multiExploit(_h1225,newsInjection),[canReach(_h1237,_h1225,_h1242),singleExploit(_h1237,cwe98),singleExploit(_h1225,cwe362)])))))).

:-(mvTrc(multiExploit(_h1225,newsInjection,9)),','(mvTrc(singleExploit(_h1225,cwe416,_h1272)),','(mvTrc(singleExploit(_h1237,cwe98,_h1301)),','(canReach(_h1237,_h1225,_h1242),assert_trace(because(9,rule_desc('Injecting File via code execution and xss',50),multiExploit(_h1225,newsInjection),[canReach(_h1237,_h1225,_h1242),singleExploit(_h1237,cwe98),singleExploit(_h1225,cwe416)])))))).

:-(mvTrc(componentImpact(_h1225,confidentiality,10)),','(mvTrc(multiExploit(_h1225,maliciousSite,_h1268)),assert_trace(because(10,rule_desc('Loss of Confidentiality due to Malicious Page Injection',55),componentImpact(_h1225,confidentiality),[multiExploit(_h1225,maliciousSite)])))).

:-(mvTrc(componentImpact(_h1225,confidentiality,11)),','(mvTrc(multiExploit(_h1225,dataExtraction,_h1268)),assert_trace(because(11,rule_desc('Loss of Confidentiality due to Malicious Page Injection',55),componentImpact(_h1225,confidentiality),[multiExploit(_h1225,dataExtraction)])))).

:-(mvTrc(componentImpact(_h1225,confidentiality,12)),','(mvTrc(multiExploit(_h1225,newsInjection,_h1268)),assert_trace(because(12,rule_desc('Loss of Confidentiality due to Malicious Page Injection',55),componentImpact(_h1225,confidentiality),[multiExploit(_h1225,newsInjection)])))).

:-(mvTrc(componentImpact(_h1225,integrity,13)),','(mvTrc(multiExploit(_h1225,newsInjection,_h1268)),assert_trace(because(13,rule_desc('Loss of Integrity due to News Injection',55),componentImpact(_h1225,integrity),[multiExploit(_h1225,newsInjection)])))).

:-(mvTrc(componentImpact(_h1225,confidentiality,14)),','(mvTrc(multiExploit(_h1225,maliciousRedirect,_h1268)),assert_trace(because(14,rule_desc('Loss of Confidentiality due to Malicious Redirect',55),componentImpact(_h1225,confidentiality),[multiExploit(_h1225,maliciousRedirect)])))).

:-(mvTrc(componentImpact(_h1225,integrity,15)),','(mvTrc(multiExploit(_h1225,maliciousRedirect,_h1268)),assert_trace(because(15,rule_desc('Loss of Integrity due to Malicious  Redirect',55),componentImpact(_h1225,integrity),[multiExploit(_h1225,maliciousRedirect)])))).

