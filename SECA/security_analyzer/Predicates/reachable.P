reachable(backend,login).
reachable(database,fetchuser).
reachable(database,fetchdoc).
attackerLocated(backend).
attackerLocated(database).
vulnExists(backend,login,cwe305).
vulnExists(database,fetchuser,cwe89).
vulnExists(database,fetchdoc,cwe88).
