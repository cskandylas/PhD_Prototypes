

/******************************************************/
/****       Primitive Predicates                  *****/
/******************************************************/

primitive(reachable(_component,_method)).
primitive(vulnExists(_component,_methodName,_vulnType)).

meta(attackGoal(_)).


/******************************************************/
/****       Derived Predicates                  *****/
/******************************************************/


derived(isCompromised(_component,_methodName,_reason)).
derived(singleExploit(_component,_reason)).

/******************************************************/
/****         Tabling Predicates                  *****/
/*   All derived predicates should be tabled          */
/******************************************************/

:- table isCompromised/3.
:- table singleExploit/2.

interaction_rule(
    (
	isCompromised(Component,MethodName,Vuln):-
	    reachable(Component,MethodName),
	    vulnExists(Component,MethodName,Vuln)),
    rule_desc('Compromised by method invocation of compromised component',4)).

interaction_rule(
    (singleExploit(Component,Reason):-
     isCompromised(Component,_,Reason)),
    rule_desc('Basic Attack',1)).
