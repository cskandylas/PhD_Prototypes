/******************************************************/
/****       Primitive Predicates                  *****/
/******************************************************/


/*internal component vulnerabilities*/
primitive(vulnExists(_component,_methodName,_vulnType)).
primitive(reachable(_component,_methodName)).
primitive(canReach(_src,_dst,_methodName)).

meta(attackGoal(_)).
/******************************************************/
/****       Derived Predicates                  *****/
/******************************************************/


derived(isCompromised(_component,_methodName,_reason)).
derived(singleExploit(_component,_reason)).
derived(multiExploit(_component,_reason)).
derived(componentImpact(_component,_property)).
derived(componentDown(_component)).

derived(dos(_component)).
derived(commandInjection(_component)).
derived(xss(_component)).
derived(httpRSpl(_component)).
derived(sqlInjection(_component)).
derived(fileInclusion(_component)).
derived(overflow(_component)).
derived(unauthorizedAccess(_component)).
derived(bypass(_component)).
derived(sessionFixation(_component)).
derived(crash(_component)).



/******************************************************/
/****         Tabling Predicates                  *****/
/*   All derived predicates should be tabled          */
/******************************************************/

:- table isCompromised/3.
:- table singleExploit/2.
:- table multiExploit/2.
:- table componentImpact/2.
/* Attacks */
:- table dos/1.
:- table commandInjection/1.
:- table xss/1.
:- table sqlInjection/1.
:- table fileInclusion/1.
:- table overflow/1.
:- table unauthorizedAccess/1.
:- table bypass/1.
:- table sessionFixation/1.
:- table crash/1.
:- table httpRSpl/1.


/******************************************************/
/****         Interaction Rules                   *****/
/******************************************************/



interaction_rule(
(
	    isCompromised(Component,MethodName,Vuln):-
	    reachable(Component,MethodName),
	    vulnExists(Component,MethodName,Vuln)),
    rule_desc('Compromised by method invocation of compromised component',4)).

		 
interaction_rule(
    (
	    singleExploit(Component,Vuln):-
	    isCompromised(Component,_,Vuln)),
    rule_desc('SingleExploit becuase of component compromise',4)).

interaction_rule(
    (multiExploit(ComponentA,maliciousSite):-
	 singleExploit(ComponentA,cwe89),
	 singleExploit(ComponentB,cwe98),
	 canReach(ComponentB,ComponentA,_))
,rule_desc('Malicious Page Inclusion',50)).

interaction_rule(
    (multiExploit(ComponentA,maliciousSite):-
singleExploit(ComponentA,cwe119),
	 singleExploit(ComponentB,cwe98),
	 canReach(ComponentB,ComponentA,_))
,rule_desc('Malicious Page Inclusion',50)).


interaction_rule(
    (multiExploit(ComponentA,maliciousSite):-
singleExploit(ComponentA,cwe362),
	 singleExploit(ComponentB,cwe98),
	 canReach(ComponentB,ComponentA,_))
,rule_desc('Malicious Page Inclusion',50)).


interaction_rule(
    (multiExploit(ComponentA,maliciousSite):-
		 singleExploit(ComponentA,cwe416),
	 singleExploit(ComponentB,cwe98),
	 canReach(ComponentB,ComponentA,_))
,rule_desc('Malicious Page Inclusion',50)).


interaction_rule(
    (multiExploit(ComponentA,maliciousSite):-
	 singleExploit(ComponentA,cwe78),
	 singleExploit(ComponentB,cwe98),
		 canReach(ComponentA,ComponentA,_)),
    rule_desc('Malicious Page Inclusion',50)).


interaction_rule(
    (multiExploit(ComponentA, dataExtraction):-
singleExploit(ComponentA,cwe20),
singleExploit(ComponentA,cwe862),
	 canReach(ComponentA,ComponentA,_)),
    rule_desc('Disclosing file through idor',50)).


interaction_rule(
    (multiExploit(ComponentA, dataExtraction):-
	 singleExploit(ComponentA,cwe200),
         singleExploit(ComponentA,cwe862),
	 canReach(ComponentA,ComponentA,_)),
    rule_desc('Disclosing file through idor',50)).


interaction_rule(
    (multiExploit(ComponentA, dataExtraction):-
	 singleExploit(ComponentA,cwe362),
         singleExploit(ComponentA,cwe862),
	 canReach(ComponentA,ComponentA,_)),
    rule_desc('Disclosing file through idor',50)).

interaction_rule(
    (multiExploit(ComponentA,newsInjection):-
	 singleExploit(ComponentA,cwe89),
	 singleExploit(ComponentB,cwe98),
	 canReach(ComponentB,ComponentA,_)),
    rule_desc('Injecting File via SQLInjection',50)).

interaction_rule(
    (multiExploit(ComponentA,newsInjection):-
singleExploit(ComponentA,cwe89),
	 singleExploit(Component,cwe98),
	 canReach(ComponentB,ComponentA,_)),
    rule_desc('Injecting File via code execution and xss',50)).

interaction_rule(
    (multiExploit(ComponentA,newsInjection):-
singleExploit(ComponentA,cwe119),
	 singleExploit(Component,cwe98),
	 canReach(ComponentB,ComponentA,_)),
    rule_desc('Injecting File via code execution and xss',50)).


interaction_rule(
    (multiExploit(ComponentA,newsInjection):-
singleExploit(ComponentA,cwe362),
	 singleExploit(Component,cwe98),
	 canReach(ComponentB,ComponentA,_)),
    rule_desc('Injecting File via code execution and xss',50)).


interaction_rule(
    (multiExploit(ComponentA,newsInjection):-
singleExploit(ComponentA,cwe416),
singleExploit(Component,cwe98),
	 canReach(ComponentB,ComponentA,_)),
    rule_desc('Injecting File via code execution and xss',50)).


interaction_rule(
    (multiExploit(ComponentA,maliciousRedirect):-
	 singleExploit(ComponentA,cwe601),
	 singleExploit(ComponentA,cwe19),
	 canReach(ComponentA,ComponentA,_)),
    rule_desc('Malicious redirection via header injection and httpRSpl',50)).


interaction_rule((multiExploit(ComponentA,maliciousRedirect):-
     singleExploit(ComponentA,cwe601),
	 singleExploit(ComponentA,cwe93),
	 canReach(ComponentA,ComponentA,_)),
    rule_desc('Malicious redirection via header injection and httpRSpl',50)).




interaction_rule(
    (componentImpact(Component,confidentiality):-
	multiExploit(Component,maliciousSite))
    ,rule_desc('Loss of Confidentiality due to Malicious Page Injection',55)).

interaction_rule(
    (componentImpact(Component,confidentiality):-
	 multiExploit(Component,dataExtraction))
    ,rule_desc('Loss of Confidentiality due to Malicious Page Injection',55)).

interaction_rule(
    (componentImpact(Component,confidentiality):-
      multiExploit(Component,newsInjection))
    ,rule_desc('Loss of Confidentiality due to Malicious Page Injection',55)).

interaction_rule(
    (componentImpact(Component,integrity):-
      multiExploit(Component,newsInjection))
    ,rule_desc('Loss of Integrity due to News Injection',55)).

interaction_rule(
    (componentImpact(Component,confidentiality):-
      multiExploit(Component,maliciousRedirect))
    ,rule_desc('Loss of Confidentiality due to Malicious Redirect',55)).

interaction_rule(
    (componentImpact(Component,integrity):-
	 multiExploit(Component,maliciousRedirect))
    ,rule_desc('Loss of Integrity due to Malicious  Redirect',55)).

