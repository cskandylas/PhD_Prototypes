import sys
import csv
#first read the .res file

def file_to_lines(fname):
    with open(fname) as file:
        lines = file.read().splitlines()
    return lines

def parse_verification_results(lines):
    print("HI")
    model_file=""
    prop=""
    construction_time=""
    properties_res={}
    properties_time={}
    states=""
    transitions=""
    count=0
    for line in lines:
        if line.find("Parsing model file")!=-1:
            line=line[line.find('"')+1:]
            line=line[:line.find('"')]
            model_file=line
        if line.find("Time for model construction:")!=-1:
            line=line[line.find(': ')+2:]
            line=line[:line.find(" ")]
            construction_time=line
        if line.find("States:")!=-1:
            line=line[line.find(': ')+2:]
            line=line[:line.find(" (")]
            line=line.strip()
            states=line
        if line.find("Transitions:")!=-1:
            line=line[line.find(': ')+2:]
            transitions=line
        if line.find("Model checking:")!=-1:
            line=line[line.find(': ')+2:]
            prop=line
            #line=line[:line.find(' ')]
            #prop=line
            #print(prop)
        
        if line.find("Time for model checking:") !=-1:
            line=line[line.find(': ')+2:]
            line=line[:line.find(" ")]
            model_checking_time=line
            properties_time[prop]=model_checking_time
        if line.find("Result:") != -1:
            line=line[line.find(': ')+2:]
            line=line[:line.find(" ")]
            model_checking_res=line
            properties_res[prop]=line
    return (model_file,construction_time,states,transitions,properties_res,properties_time)


experiment_time_results={}
def parse_trace_file(lines):
    experiment_name=""
    i=0
    while i < len(lines)-1:
        experiment_name=lines[i]
        i+=1
        #skip before
        i+=1
        #skip initial components
        i+=1
        #skip after
        i+=1
        lines[i]=lines[i][lines[i].find(":")+1:]
        num_components=lines[i][:lines[i].find(" ")]
        lines[i]=lines[i][lines[i].find(":")+1:]
        num_invocations=lines[i][:lines[i].find(" ")]
        i+=1
        lines[i]=lines[i][lines[i].find(":")+1:]
        graph_time=lines[i][:lines[i].find(" ")]
        i+=1
        lines[i]=lines[i][lines[i].find(":")+1:]
        lines[i]=lines[i][lines[i].find(":")+1:]
        higher_time=lines[i][:lines[i].find(" ")]
        i+=1
        lines[i]=lines[i][lines[i].find(":")+1:]
        total_time=lines[i][:lines[i].find(" ")]
        i+=1
        experiment_time_results[experiment_name]=(experiment_name,num_components,num_invocations,graph_time,higher_time,total_time)     

experiment_statistics={}
def parse_experiment_statistics(dir):
    for experiment in experiment_time_results:
        #first identify the fragmetnts
        print(dir+"/"+experiment+"/Automata/fragments.map")
        fragment_map_file=open(dir+"/"+experiment+"/Automata/fragments.map")
        #fragment_lines = fragment_map_file.read().splitlines()
        fragments=[]
        for fragment_line in fragment_lines:
            fragment = fragment_line[:fragment_line.find(":")]
            selected = fragment_line[fragment_line.find(" ")+1:]
            #print(fragment)
            
            if selected == "Y":
                #now lets read the predicates for the fragment
                striped_frag=fragment[fragment.find("_")+1:]
                
                if fragment.startswith("singleExploit"):
                    #record numberof predicates
                    predicate_fname=dir+"/"+experiment+"/Predicates/single_exploit/"+striped_frag+".P"
                    pred_file=open(predicate_fname,"r")
                    num_preds=len(pred_file.read().splitlines())
                    #on to read graph information
                    graph_verts_fname=dir+"/"+experiment+"/Graphs/single_exploit/"+striped_frag+"/VERTICES.CSV"
                    verts_file=open(graph_verts_fname,"r")
                    num_verts=len(verts_file.read().splitlines())
                    graph_edges_fname=dir+"/"+experiment+"/Graphs/single_exploit/"+striped_frag+"/ARCS.CSV"
                    edges_file=open(graph_edges_fname,"r")
                    num_edges=len(edges_file.read().splitlines())
                    #on to the verification results
                    verification_fname=dir+"/"+experiment+"/Verification/single_exploit/"+striped_frag
                    verification_file=open(verification_fname,"r")
                    verification_results=parse_verification_results(verification_file.read().splitlines())
                elif fragment.startswith("multiExploit"):
                    predicate_fname=dir+"/"+experiment+"/Predicates/multi_exploit/"+striped_frag+".P"
                    pred_file=open(predicate_fname,"r")
                    num_preds=len(pred_file.read().splitlines())
                    #on to read graph information
                    graph_verts_fname=dir+"/"+experiment+"/Graphs/multi_exploit/"+striped_frag+"/VERTICES.CSV"
                    verts_file=open(graph_verts_fname,"r")
                    num_verts=len(verts_file.read().splitlines())
                    graph_edges_fname=dir+"/"+experiment+"/Graphs/multi_exploit/"+striped_frag+"/ARCS.CSV"
                    edges_file=open(graph_edges_fname,"r")
                    num_edges=len(edges_file.read().splitlines())
                    #on to the verification results
                    verification_fname=dir+"/"+experiment+"/Verification/multi_exploit/"+striped_frag
                    verification_file=open(verification_fname,"r")
                    verification_results=parse_verification_results(verification_file.read().splitlines())
                elif fragment.startswith("componentImpact"):
                    predicate_fname=dir+"/"+experiment+"/Predicates/component_impact/"+striped_frag+".P"
                    pred_file=open(predicate_fname,"r")
                    num_preds=len(pred_file.read().splitlines())
                    #on to read graph information
                    graph_verts_fname=dir+"/"+experiment+"/Graphs/component_impact/"+striped_frag+"/VERTICES.CSV"
                    verts_file=open(graph_verts_fname,"r")
                    num_verts=len(verts_file.read().splitlines())
                    graph_edges_fname=dir+"/"+experiment+"/Graphs/component_impact/"+striped_frag+"/ARCS.CSV"
                    edges_file=open(graph_edges_fname,"r")
                    num_edges=len(edges_file.read().splitlines())
                    #on to the verification results
                    verification_fname=dir+"/"+experiment+"/Verification/component_impact/"+striped_frag
                    verification_file=open(verification_fname,"r")
                    verification_results=parse_verification_results(verification_file.read().splitlines())
                fragments.append((fragment,num_preds,num_verts,num_edges,verification_results))
        experiment_statistics[experiment]=fragments
        


def main():
    args = sys.argv[1:]
    dir=args[0][:args[0].rfind("/")]
    parse_trace_file(file_to_lines(args[0]))
    #parse_experiment_statistics(dir)
    result_csv_fname=dir+".csv"
    result_csv_file=open(result_csv_fname,"w")
    result_writer = csv.writer(result_csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for experiment in  experiment_time_results:
        print(dir+"/"+experiment+"/Verification/all_in_one.res")
        high_level_res_file=open(dir+"/"+experiment+"/Verification/all_in_one.res","r")

        high_level_automaton_res=parse_verification_results(high_level_res_file.read().splitlines())
        result_writer.writerow([experiment])
        result_writer.writerow(["num_components","num_invocations","graph_time","auto_time","total_time"])
        result_writer.writerow([experiment_time_results[experiment][1],experiment_time_results[experiment][2],experiment_time_results[experiment][3],experiment_time_results[experiment][4],experiment_time_results[experiment][5]])
        result_writer.writerow(["Automaton:"])
        result_writer.writerow(["model_file","construction_time","num_states","num_transitions"])
        result_writer.writerow([high_level_automaton_res[0],high_level_automaton_res[1],high_level_automaton_res[2],high_level_automaton_res[3]])
        result_writer.writerow(["Property","Verification_Result","Verification_Time"])
        for prop in high_level_automaton_res[4]:
            result_writer.writerow([prop,high_level_automaton_res[4][prop],high_level_automaton_res[5][prop]])
        result_writer.writerow([""])
        



if __name__ == "__main__":
    main()
