#!/bin/bash
for file in arch/*; do
    #some string kungfu needed
    #strip arch/
    plainfile=${file: 5}
    
    stripped=${plainfile%.*}
#    echo $stripped
    #    mkdir Results/$stripped
    for adaptation_full_path in `find strat/${stripped}* -type f`
    do
	
	#echo $adaptation_full_path
	adaptation_file=${adaptation_full_path: 7}
	adaptation_name=${adaptation_file%.*}
	echo $adaptation_name
	
	timeout 120m ./incremental_analysis_adaptation $file tactic/${stripped}_plans.list  $adaptation_full_path 2> err_log
	mkdir Results/$adaptation_name
	
	#now let's deal with moving the results around.
	#move automata over
	mkdir Results/$adaptation_name/Automata
	mkdir Results/$adaptation_name/Automata/component_impact
	mv Automata/component_impact/* Results/$adaptation_name/Automata/component_impact
	mkdir Results/$adaptation_name/Automata/single_exploit
	mv Automata/single_exploit/* Results/$adaptation_name/Automata/single_exploit
	mkdir Results/$adaptation_name/Automata/multi_exploit
	mv Automata/multi_exploit/* Results/$adaptation_name/Automata/multi_exploit
	#move graphs over
	mkdir Results/$adaptation_name/Graphs
	mkdir Results/$adaptation_name/Graphs/interaction
	mv Graphs/interaction/* Results/$adaptation_name/Graphs/interaction
	mkdir Results/$adaptation_name/Graphs/single_exploit
	mv  Graphs/single_exploit/* Results/$adaptation_name/Graphs/single_exploit
	mkdir Results/$adaptation_name/Graphs/multi_exploit
	mv  Graphs/multi_exploit/* Results/$adaptation_name/Graphs/multi_exploit
	mkdir Results/$adaptation_name/Graphs/component_impact
	mv  Graphs/component_impact/* Results/$adaptation_name/Graphs/component_impact
	#move predicates over
	mkdir Results/$adaptation_name/Predicates
	mkdir Results/$adaptation_name/Predicates/interaction
	mv Predicates/interaction/* Results/$adaptation_name/Predicates/interaction
	mkdir Results/$adaptation_name/Predicates/single_exploit
	mv Predicates/single_exploit/* Results/$adaptation_name/Predicates/single_exploit
	mkdir Results/$adaptation_name/Predicates/multi_exploit
	mv Predicates/multi_exploit/* Results/$adaptation_name/Predicates/multi_exploit
	mkdir Results/$adaptation_name/Predicates/component_impact
	mv Predicates/component_impact/* Results/$adaptation_name/Predicates/component_impact
	#move rules over
	mkdir Results/$adaptation_name/Rules
	cp Rules/* Results/$adaptation_name/Rules
	#move verification results over
	mkdir Results/$adaptation_name/Verification
	mkdir Results/$adaptation_name/Verification/single_exploit
	mkdir Results/$adaptation_name/Verification/multi_exploit
	mkdir Results/$adaptation_name/Verification/component_impact
	mv Verification/single_exploit/* Results/$adaptation_name/Verification/single_exploit
	mv Verification/multi_exploit/* Results/$adaptation_name/Verification/multi_exploit
	mv Verification/component_impact/* Results/$adaptation_name/Verification/component_impact
	mv Automata/FSM.prism Results/$adaptation_name/Automata/FSM.prism
	mv Verification/FSM.res Results/$adaptation_name/high_level_automaton_results
	mv Automata/fragments.map Results/$adaptation_name/Automata/fragments.map
	cp Automata/valid.props   Results/$adaptation_name/Automata/valid.props
	mv err_log Results/$adaptation_name/err_log
	#move high-level automaton over
	#sleep 2
	
    done
done




