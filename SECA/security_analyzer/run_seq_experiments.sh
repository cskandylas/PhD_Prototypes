#!/bin/bash

for file in arch/*; do
    #some string kungfu needed
    #strip arch/
    plainfile=${file: 5}
    stripped=${plainfile%.*}
    #echo $stripped


    for adaptation_full_path in `find strat/${stripped}* -type f`
    do
	#echo $adaptation_full_path
	adaptation_file=${adaptation_full_path: 7}
	adaptation_name=${adaptation_file%.*}
	echo $adaptation_name
	
	
	timeout 120m ./sequential_analysis_adaptation $file tactic/${stripped}_plans.list $adaptation_full_path Results_Mod/${adaptation_name}/Automata/valid.props 2> err_log
	#now let's deal with moving the results around.
	#move automata over
	mkdir Results/$adaptation_name
	mkdir Results/$adaptation_name/Automata
	mv Automata/all_in_one.prism Results/$adaptation_name/Automata/all_in_one.prism
	#move graphs over
	mkdir Results/$adaptation_name/Graphs
	mkdir Results/$adaptation_name/Graphs/all_in_one
	mv Graphs/all_in_one/* Results/$adaptation_name/Graphs/all_in_one
	#move predicates over
	mkdir Results/$adaptation_name/Predicates
	mv Predicates/all_in_one.P Results/$adaptation_name/Predicates/all_in_one.P
	#move rules over
	mkdir Results/$adaptation_name/Rules
	cp Rules/* Results/$adaptation_name/Rules
	#move verification results over
	mkdir Results/$adaptation_name/Verification
	mv Verification/all_in_one.res Results/$adaptation_name/Verification
	sleep 2
	
    done
done




