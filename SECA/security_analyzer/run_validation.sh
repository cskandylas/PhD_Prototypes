#!/bin/bash
for file in arch/*; do
    #some string kungfu needed
    #strip arch/
    plainfile=${file: 5}
    stripped=${plainfile%.*}
    echo $plainfile
    seq_results=Results_sequential/${stripped}/Verification/all_in_one.res
    mod_results=Results_modular/${stripped}/high_level_automaton_results
    #stat $seq_results
    #stat $mod_results
    python3 result_validate.py $seq_results $mod_results
    
    sleep 1
done




