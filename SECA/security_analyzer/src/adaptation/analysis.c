#include "analysis.h"
#include <stdio.h>
#include <stdbool.h>


struct generic_queue* analysis_enabled_strats(struct knowledge_base *k_b)
{
    struct generic_queue *enabled_strategies=malloc(sizeof(struct generic_queue));
    queue_init(enabled_strategies);
    
    for(int i=0;i<k_b->num_strats;i++)
    {
	
	bool eval_res=arc_complex_cond_eval(k_b->known_strats[i]->strat_cond, k_b->sys);
	printf("Evaluation result for %s:%d\n",k_b->known_strats[i]->name,eval_res);
	if(eval_res)
	{
	    queue_push_back(enabled_strategies,k_b->known_strats[i]);
	}
    }

    return enabled_strategies;
}



void init_archive(struct analysis_archive *archive)
{
    archive->analyses=malloc(sizeof(analysis_fnc*)*NUM_ANALYSES_INIT);
    archive->max_analyses=NUM_ANALYSES_INIT;
    archive->num_analyses=0;
}
void add_analysis(struct analysis_archive  *archive, analysis_fnc *analysis_fn)
{
    if(archive->num_analyses>=archive->max_analyses)
    {
        archive->max_analyses*=2;
	archive->analyses=realloc(archive->analyses,
				archive->max_analyses*sizeof(struct analysis_fn*));
    }
    archive->analyses[archive->num_analyses]=analysis_fn;
    archive->num_analyses++;

}
void rem_analysis(struct analysis_archive  *archive, analysis_fnc *analysis_fn)
{
       for(int i=0;archive->num_analyses;i++)
    {
	if(archive->analyses[i]==analysis_fn)
	    archive->analyses[i]=NULL;
    }
}
void destroy_archive(struct analysis_archive *archive)
{
    for(int i=0;archive->num_analyses;i++)
    {
	archive->analyses[i]=NULL;
    }
    free(archive->analyses);
}

void analysis(struct analysis_archive *archive)
{

}
