#include "knowledge_base.h"
#include "Queue.h"
#include <time.h>

#ifndef ANALYSIS
#define ANALYSIS


typedef void* (*analysis_fnc)(struct arc_strategy *stat);


#define NUM_ANALYSES_INIT 2

struct analysis_archive
{
    analysis_fnc **analyses;
    size_t num_analyses;
    size_t max_analyses;
};




//Retrns a queue containing arc_strategy*
struct generic_queue* analysis_enabled_strats(struct knowledge_base *k_b); 

void init_archive(struct analysis_archive *archive);
void add_analysis(struct analysis_archive  *archive, analysis_fnc *analysis_fn);
void rem_analysis(struct analysis_archive  *archive, analysis_fnc *analysis_fn);
void destroy_archive(struct analysis_archive *archive);
int run_analysis(analysis_fnc analysis, struct knowledge_base *k_b);
void analysis(struct analysis_archive *archive);

#endif
