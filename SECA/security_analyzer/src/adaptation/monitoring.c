#include "monitoring.h"

#include <stdlib.h>


void init_monitor(struct monitor *mon)
{
    mon->probes=malloc(sizeof(struct probe*)*NUM_PROBES_INIT);
    mon->max_probes=NUM_PROBES_INIT;
    mon->num_probes=0;
}

//see if we should do probe init before or at this step.
void monitor_add_probe(struct monitor *mon, struct probe *probe)
{
    if(mon->num_probes>=mon->max_probes)
    {
        mon->max_probes*=2;
	mon->probes=realloc(mon->probes,
				mon->max_probes*sizeof(struct probe*));
    }
    mon->probes[mon->num_probes]=probe;
    mon->num_probes++;

    
}
void monitor_start(struct monitor *mon)
{
    //spawn a thread for each instead
    for(int i=0;i<mon->num_probes;i++)
    {
	start_probe(mon->probes[i]);
    }

}
//possibly by idx?
void monitor_detach_probe(struct monitor *mon, struct probe *probe)
{
    for(int i=0;i<mon->num_probes;i++)
    {
	if(mon->probes[i]==probe)
	{
	    stop_probe(probe);
	}
    }
}
void destroy_monitor(struct monitor *mon)
{
    for(int i=0;i<mon->num_probes;i++)
    {
	destroy_probe(mon->probes[i]);
    }
    free(mon->probes);
}


void monitor_tick(struct monitor *mon)
{
    for(int i=0;i<mon->num_probes;i++)
    {
	struct probe *probe=mon->probes[i];
	if(probe->status==PROBE_DONE || probe->status==PROBE_STOPPED)
	{
	    //clean up the probe;
	}
	else if(probe->status==PROBE_CREATED)
	{
	    start_probe(probe);
	}
    }
}
