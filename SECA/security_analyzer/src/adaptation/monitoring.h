#include <stddef.h>
#include <pthread.h>
#include "probe.h"

#ifndef MONITORING_H
#define MONITORING_H

#define NUM_PROBES_INIT 4

struct monitor
{
    struct probe **probes;
    size_t num_probes;
    size_t max_probes;
};


void init_monitor(struct monitor *mon);

//see this adds an already created probe.
void monitor_add_probe(struct monitor *mon, struct probe *probe);

//runs init on the probes
void monitor_start(struct monitor *mon);
//possibly by idx?
void monitor_detach_probe(struct monitor *mon, struct probe *probe);
void destroy_monitor(struct monitor *mon);
void monitor_tick(struct monitor *mon);

#endif
