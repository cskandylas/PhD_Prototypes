#include "probe.h"
#include <stdio.h>


void create_probe(struct probe *probe, probe_init init, probe_run run, probe_destroy destroy, void *ctx)
{
    probe->ctx=ctx;
    probe->probe_init=init;
    probe->probe_run=run;
    probe->probe_destroy=destroy;
    probe->status=PROBE_CREATED;
}

void start_probe(struct probe *probe)
{
    
    probe->probe_init(probe);
    probe->status=PROBE_RUNNING;
    //pthread_create(probe->probe_run(probe);
        
}


void stop_probe(struct probe *probe)
{
    probe->status=PROBE_STOPPED;
}


void destroy_probe(struct probe *probe)
{
    if(probe->status==PROBE_STOPPED)
    {
	probe->probe_destroy(probe);
    }
    else
    {
	fprintf(stderr,"Please stop the probe before destroying it\n");
    }
}
