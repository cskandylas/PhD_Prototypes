#ifndef PROBE_H
#define PROBE_H


enum PROBE_STATUS{PROBE_CREATED=0,PROBE_RUNNING,PROBE_STOPPED,PROBE_DONE};

struct probe
{
    void *ctx;
    void (*probe_init)(struct probe *probe);
    void* (*probe_run)(void *ctx);
    void (*probe_destroy)(struct probe *probe);
    enum PROBE_STATUS status;
    
};

typedef void (*probe_init)(struct probe *probe);
typedef void* (*probe_run)(void *ctx);
typedef void (*probe_destroy)(struct probe *probe);

void create_probe(struct probe *probe, probe_init init, probe_run run, probe_destroy destroy, void *ctx);
void start_probe(struct probe *probe);
void stop_probe(struct probe *probe);
void destroy_probe(struct probe *probe);

#endif
