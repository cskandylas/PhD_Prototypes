#include "arc_diff.h"
#include <stdio.h>


void arc_diff(struct arc_system *prev,struct arc_system *next,
	      struct rb_tree *removed, struct rb_tree *added)

{

    //find all components that are in prev but not in next
    for(int i=0;i<prev->num_components;i++)
    {
	struct arc_component *match=sys_find_component(next,prev->components[i]->comp_name);
	if(!match)
	{
	    
	    rb_tree_insert(removed,prev->components[i]);
	}
	else
	{
	    for(int j=0;j<prev->components[i]->num_interfaces;j++)
	    {
		struct arc_interface* iface_match = comp_find_iface(match,prev->components[i]->interfaces[j]->if_name );
		if(!iface_match)
		{
		    //interface removed, component updated
		    rb_tree_insert(added,match);
		    
		}
	    }
	}
    }
    //find all components that are in next but not in prev
    for(int i=0;i<next->num_components;i++)
    {
	struct arc_component *match=sys_find_component(prev,next->components[i]->comp_name);
	if(!match)
	{
	    
	    rb_tree_insert(added,next->components[i]);
	}
	else
	{
	    for(int j=0;j<next->components[i]->num_interfaces;j++)
	    {
		struct arc_interface* iface_match = comp_find_iface(match,next->components[i]->interfaces[j]->if_name );
		if(!iface_match)
		{
		    //new interface, i.e., component updated
		    rb_tree_insert(added,match);
		}
	    }

	}
    }


    for(int i=0;i<prev->num_invocations;i++)
    {
	struct arc_invocation *prev_inv= prev->invocations[i];
	struct arc_invocation *match = sys_find_invocation(next,prev_inv->from->comp_name,
							   prev_inv->to->comp_name,
							   prev_inv->iface->if_name);
	if(!match)
	{
	    //mark the components left in the architecture if any.
	    struct arc_component *from_comp=sys_find_component(next,prev_inv->from->comp_name);
	    if(from_comp)
		rb_tree_insert(added,from_comp);
	    struct arc_component *to_comp=sys_find_component(next,prev_inv->to->comp_name);
	    if(to_comp)
		rb_tree_insert(added,to_comp);	   
	}
    }
    
    for(int i=0;i<next->num_invocations;i++)
    {
	struct arc_invocation *next_inv= next->invocations[i];
	struct arc_invocation *match = sys_find_invocation(prev,next_inv->from->comp_name,
							   next_inv->to->comp_name,
							   next_inv->iface->if_name);
	if(!match)
	{

	    //mark both ends of the invocation
	    rb_tree_insert(added,next_inv->from);
	    rb_tree_insert(added,next_inv->to);
	    
	}
    }
    
}
