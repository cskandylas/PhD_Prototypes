#include "arc_system.h"
#include "rbtree.h"

#ifndef ARC_DIFF_H
#define ARC_DIFF_H

void arc_diff(struct arc_system *prev,struct arc_system *next,
	      struct rb_tree *removed, struct rb_tree *added);

#endif
