#include "arc_interface.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>



void init_interface(struct arc_interface *interface, char *if_name)
{

    interface->if_name=malloc(strlen(if_name)+1);
    strcpy(interface->if_name,if_name);
    interface->properties=malloc(sizeof(struct arc_property*)*NUM_PROPERTIES_INIT);
    interface->max_properties=NUM_PROPERTIES_INIT;
    interface->num_properties=0;
}
void iface_add_property(struct arc_interface *interface,
			struct arc_property *prop)
{
    if(interface->num_properties>=interface->max_properties)
    {
	interface->max_properties*=2;
	interface->properties=realloc(interface->properties,
				      interface->max_properties*sizeof(struct arc_property*));
	
    }
    interface->properties[interface->num_properties]=prop;
    interface->num_properties++;    
}




void destroy_interface(struct arc_interface *interface)
{
    free(interface->if_name);
    free(interface->properties);
}


void print_interface(struct arc_interface *interface)
{
    printf("Interface %s{\n",interface->if_name);
    for(int i=0;i<interface->num_properties;i++)
    {
	print_property(interface->properties[i]);
    }
    printf("}\n");
}


struct arc_property* iface_find_property(struct arc_interface *iface,
			   char *prop_name)
{
    for(int i=0;i<iface->num_properties;i++)
    {
	if(iface->properties[i])
	{
	    if(strcmp(iface->properties[i]->name,prop_name)==0)
	    {
		return iface->properties[i];
	    }
	}
    }
    return NULL;
}


struct arc_interface *clone_interface(struct arc_interface *iface)
{
    struct arc_interface *copy=malloc(sizeof(struct arc_interface));
    init_interface(copy,iface->if_name);

    for(int i=0;i<iface->num_properties;i++)
    {
	iface_add_property(copy,clone_property(iface->properties[i]));
    }

    return copy;
}
