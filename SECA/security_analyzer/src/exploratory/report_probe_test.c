#include<stdio.h>
#include<string.h>	
#include<stdlib.h>	
#include<sys/socket.h>
#include<arpa/inet.h>	
#include<unistd.h>	
#include<pthread.h> 

#include "report_probe_test.h"


//the thread function
void *connection_handler(void *);

int report_probe_init(struct probe *probe)
{

    struct report_probe *probe_ctx=probe->ctx;
    
    //Create socket
    probe_ctx->server_sock = socket(AF_INET , SOCK_STREAM , 0);
    if (probe_ctx->server_sock == -1)
    {
	printf("Could not create socket");
    }
    puts("Socket created");


    
    //Prepare the sockaddr_in structure
    probe_ctx->server.sin_family = AF_INET;
    probe_ctx->server.sin_addr.s_addr = INADDR_ANY;
    probe_ctx->server.sin_port = htons( probe_ctx->port );
	
    //Bind
    if( bind(probe_ctx->server_sock,(struct sockaddr *)&probe_ctx->server , sizeof(probe_ctx->server)) < 0)
    {
	//print the error message
	perror("bind failed. Error");
	return 1;
    }
    puts("bind done");
	
    //Listen
    listen(probe_ctx->server_sock , 3);
	
    //Accept and incoming connection
    printf("Waiting for incoming connections on %d\n",probe_ctx->port);
    int size = sizeof (probe_ctx->client);
    probe_ctx->client_sock = accept(probe_ctx->server_sock, (struct sockaddr *)&probe_ctx->client, (socklen_t*)&size);
    puts("Connection accepted");
    return probe_ctx->client_sock;
}

void report_probe_run(struct probe *probe)
{
    pthread_t client_thread;
    
    struct report_probe *probe_ctx=probe->ctx;
		
    if( pthread_create( &client_thread , NULL ,  connection_handler , (void*) &probe_ctx->client_sock) < 0)
    {
	perror("could not create thread");
    }
    probe_ctx->client_thread=client_thread;
    //Now join the thread , so that we dont terminate before the thread
    
    puts("Handler assigned");
    pthread_join(client_thread, NULL);
	
    
    if (probe_ctx->client_sock < 0)
    {
	perror("accept failed");
    }
    
    
}

void report_probe_destroy

int main(int argc , char *argv[])
{
    struct report_probe r_probe;
    r_probe.port=6666;


    struct probe probe;
    probe.ctx=&r_probe;
    probe.probe_init=report_probe_init;
    probe.probe_run=report_probe_run;

    probe.probe_init(&probe);
    probe.probe_run(&probe);
	
    return 0;
}

/*
  This will handle connection for each client
 */
void *connection_handler(void *socket_desc)
{
    //Get the socket descriptor
    int sock = *(int*)socket_desc;
    int read_size;
    char *message , client_message[2000];
    
    //Receive a message from client
    while( (read_size = recv(sock , client_message , 2000 , 0)) > 0 )
    {
	
	//Send the message back to client
	printf("Read:[\n%s\n]\n",client_message);
	memset(client_message,0,2000);
    }
	
    if(read_size == 0)
    {
	puts("Client disconnected");
	fflush(stdout);
    }
    else if(read_size == -1)
    {
	perror("recv failed");
    }
			
    return 0;
}
