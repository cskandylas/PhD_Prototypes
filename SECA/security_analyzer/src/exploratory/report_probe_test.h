
#ifndef REPORT_PROBE_H
#define REPORT_PROBE_H


struct report_probe
{
    int port;
    int server_sock;
    int client_sock;
    struct sockaddr_in server;
    struct sockaddr_in client;
    pthread_t client_thread;
    
};

struct probe
{
    int (*probe_init)(struct probe *probe);
    void(*probe_run)(struct probe *probe);
    void *ctx;
    
};


int report_probe_init(struct probe *probe);
void report_probe_run(struct probe *probe);

#endif
