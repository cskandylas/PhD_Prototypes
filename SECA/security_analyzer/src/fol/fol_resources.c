#include "fol_resources.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tokenize.h"
#include "Queue.h"


void predicates_to_file(char *file, struct sec_predicate *preds, size_t n_preds,char *filemode)
{
    FILE *fp=fopen(file,filemode);
    if(fp)
    {
	for(int i=0;i<n_preds;i++)
	{
	    char *pred_str=predicate_to_str(&preds[i]);
	    fprintf(fp,"%s\n",pred_str);
	    free(pred_str);
	}
	fclose(fp);
    }
    else
    {
	fprintf(stderr,"File %s could not be openned\n",file); 
    }
}


struct sec_predicate* predicates_from_file(char *file,size_t *num_preds)
{
    FILE *fp=fopen(file,"r");
    size_t num_read;
    if(!fp)
    {
	fprintf(stderr,"Could not find file %s\n",file);
	return NULL;
    }
    fseek(fp, 0L, SEEK_END);
    long numbytes = ftell(fp);
    fseek(fp, 0, SEEK_SET);	
    char *text=malloc(numbytes+1);

    if(text == NULL)
    {
	fprintf(stderr,"Error in openning file:%s\n",file);
	return NULL;
    }
    num_read=fread(text, sizeof(char), numbytes, fp);
    fclose(fp);
    text[numbytes]='\0';

    struct tokenizer line_tok;
    tokenizer_init(&line_tok,text,"\n");
    char *line;
    size_t n_lines=0;
    while((line=next_token(&line_tok))!=NULL)
    {
//	printf("Line %lu:%s\n",n_lines,line);
	n_lines++;
	free(line);
    }
//    printf("Found %lu lines in %s\n",n_lines,file);

    struct sec_predicate *preds=malloc(n_lines*sizeof(struct sec_predicate));
    tokenizer_init(&line_tok,text,"\n");
    for(int i=0;i<n_lines;i++)
    {

	char *line=next_token(&line_tok);
	struct tokenizer quote_tok;
	tokenizer_init(&quote_tok,line,",\"");
		       
	struct sec_predicate *tmp_pred=predicate_from_str(line);
	//REWRITE predicate_from_str to PASS pointer to write to, should be much cleaner;
	preds[i]=*tmp_pred;
	free(line);
	free(tmp_pred);
    }

    free(text);
    
    *num_preds=n_lines;
    return preds;
    
}


struct sec_predicate* predicates_from_csv(char *file, size_t *num_preds)
{
    FILE *fp=fopen(file,"r");
    size_t num_read;
    if(!fp)
    {
	fprintf(stderr,"Could not find file %s\n",file);
	return NULL;
    }
    fseek(fp, 0L, SEEK_END);
    long numbytes = ftell(fp);
    fseek(fp, 0, SEEK_SET);	
    char *text=malloc(numbytes+1);

    if(text == NULL)
    {
	fprintf(stderr,"Error in openning file:%s\n",file);
	return NULL;
    }
    num_read=fread(text, sizeof(char), numbytes, fp);
    fclose(fp);
    text[numbytes]='\0';

    struct tokenizer line_tok;
    tokenizer_init(&line_tok,text,"\n");
    char *line;
    size_t n_lines=0;
    size_t n_predicates=0;
    while((line=next_token(&line_tok))!=NULL)
    {
//	printf("Line %lu:%s\n",n_lines,line);
	if(strstr(line,"OR") || strstr(line,"LEAF"))
	    n_predicates++; 
	n_lines++;
	free(line);
    }
//    printf("Found %lu lines in %s\n",n_lines,file);

    struct sec_predicate *preds=malloc(n_predicates*sizeof(struct sec_predicate));
    tokenizer_init(&line_tok,text,"\n");
    size_t idx=0;
    for(int i=0;i<n_lines;i++)
    {
	char *line=next_token(&line_tok);
	struct tokenizer quote_tok;
	tokenizer_init(&quote_tok,line,"\"");
	//discard id for now
	free(next_token(&quote_tok));
	char *pred_str=next_token(&quote_tok);
	
	free(next_token(&quote_tok));
	char *type=next_token(&quote_tok);
	if(type)
	{
	    if(type[0]!='\0' && strcmp(type,"OR") == 0 || strcmp(type,"LEAF") == 0)
	    {
		//printf("predicate: %s\n",pred_str);
		struct sec_predicate *pred=predicate_from_str(pred_str);
		copy_predicate(&preds[idx],pred);
		idx++;
		destroy_predicate(pred);
		free(pred);
	    }
	    free(type);
	}
	free(pred_str);
	
	free(line);
    }

    free(text);
    

    *num_preds=n_predicates;
    return preds;
}


struct sec_predicate* predicates_filter(struct sec_predicate *preds, size_t n_preds, char *pred_name, size_t *n_res)
{
    struct sec_predicate *results=NULL;
    size_t num_results=0;
    for(size_t i=0;i<n_preds;i++)
    {
	if(strcmp(preds[i].name,pred_name)==0)
	{
	    num_results++;
	}
    }
    
    if(num_results>0)
    {
	size_t idx=0;
	results=malloc(num_results*sizeof(struct sec_predicate));
	for(size_t i=0;i<n_preds;i++)
	{
	    if(strcmp(preds[i].name,pred_name)==0)
	    {
		//results[idx]=preds[i];
		copy_predicate(&results[idx],&preds[i]);
		idx++;
	    }
	}
    }

    *n_res=num_results;
    return results;
}


struct sec_predicate* predicates_for_component(struct sec_predicate *preds, size_t n_preds, char *component, size_t *n_res)
{
    struct sec_predicate *results=NULL;
    size_t num_results=0;
    for(size_t i=0;i<n_preds;i++)
    {
	if(strcmp(preds[i].terms[0],component)==0)
	{
	    num_results++;
	}
    }

    if(num_results>0)
    {
	size_t idx=0;
	results=malloc(num_results*sizeof(struct sec_predicate));
	for(size_t i=0;i<n_preds;i++)
	{
	    if(strcmp(preds[i].terms[0],component)==0)
	    {
		copy_predicate(&results[idx],&preds[i]);
		idx++;
	    }
	}
    }
    *n_res=num_results;
    return results;
}

struct sec_clause *clauses_from_file(char *file, size_t *num_clauses)
{
    FILE *fp=fopen(file,"r");
    size_t num_read;
    if(!fp)
    {
	fprintf(stderr,"Could not find file %s\n",file);
	return NULL;
    }
    fseek(fp, 0L, SEEK_END);
    long numbytes = ftell(fp);
    fseek(fp, 0, SEEK_SET);	
    char *text=malloc(numbytes+1);

    if(text == NULL)
    {
	fprintf(stderr,"Error in openning file:%s\n",file);
	return NULL;
    }
    num_read=fread(text, sizeof(char), numbytes, fp);
    fclose(fp);
    text[numbytes]='\0';

    //printf("Read: %s\n",text);
    
    struct tokenizer line_tok;
    struct tokenizer predicate_tok;
    tokenizer_init(&line_tok,text,"\n");
    char *line;
    size_t n_lines=0;
    size_t n_rules;

    struct generic_queue clause_queue;
    queue_init(&clause_queue);

    struct sec_clause *clause=NULL;
    
    while((line=next_token(&line_tok))!=NULL)
    {
	if(!strstr(line,"/*") && !strstr(line,"primitive(")
	   && !strstr(line,"derived(") && !strstr(line,"table")
	   && !strstr(line, "interaction_rule") && !strstr(line,"rule_desc"))
	{
	    
	    n_lines++;
	    //multiExploit rules start with (multiExploit
	    if(strstr(line,"(multiExploit"))
	    {
		
		char *predicate=line;
		while(*predicate==' ' || *predicate=='(')
		    predicate++;
		int len=strlen(predicate);
		if(len>0)
		{
		    //malloc two less since we chop off the :-
		    char *head=malloc(len-1);
		    memcpy(head,predicate,len-1);
		    head[len-2]='\0';
		    if(!clause)
		    {
			struct sec_predicate *head_pred=predicate_from_str(head);
			clause=malloc(sizeof(struct sec_clause));
			init_clause(clause,head_pred);
		    }
		    free(head);
		}
	    }
	    else if(strstr(line,"))"))
	    {
		
		char *predicate=line;
		while(*predicate==' ' || *predicate=='\t')
		    predicate++;

		int len=strlen(predicate);

		if(len>0)
		{
		    //malloc one less character, we are chopping ')'
		    char *tail_end=malloc(len);
		    memcpy(tail_end,predicate,len);
		    tail_end[len-1]='\0';
		    if(clause)
		    {
			struct sec_predicate *tail_pred=predicate_from_str(tail_end);
			clause_add_tail_predicate(clause,tail_pred);
			queue_push_back(&clause_queue,clause);
			clause=NULL;
		    }
		    free(tail_end);
		}
	    }
	    else
	    {
		
		char *predicate=line;
		while(*predicate==' ' || *predicate=='\t')
		    predicate++;

		int len=strlen(predicate);

		if(len>0)
		{
		    //malloc one less character, we are chopping ','
		    char *tail=malloc(len);
		    memcpy(tail,predicate,len);
		    tail[len-1]='\0';
		    
		    if(clause)
		    {
			struct sec_predicate *tail_pred=predicate_from_str(tail);
			clause_add_tail_predicate(clause,tail_pred);
		    }
		    free(tail);
		}
		
	    }
	    
	    
	}
	free(line);
    }
    free(text);
    
    struct sec_clause *clauses=malloc(clause_queue.size*sizeof(struct sec_clause));
    struct queue_entry *pos=clause_queue.head;
    int count=0;
    *num_clauses=clause_queue.size;
    while(pos)
    {
	struct sec_clause *clause=pos->elem;
	clauses[count]=*clause;
	count++;
	pos=pos->next;
	//destroy_clause(clause);
	free(clause);
    }
    queue_destroy(&clause_queue);
    
    return clauses;
    
}
