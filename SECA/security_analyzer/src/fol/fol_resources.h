#include "sec_clause.h"


#ifndef FOL_resources
#define FOL_resources


void predicates_to_file(char *file, struct sec_predicate *preds, size_t n_preds,char *filemode);
struct sec_predicate* predicates_from_file(char *file,size_t *num_preds);
struct sec_predicate* predicates_from_csv(char *file, size_t *num_preds);

struct sec_predicate* predicates_filter(struct sec_predicate *preds, size_t n_preds, char *pred_name, size_t *n_res);
struct sec_predicate* predicates_for_component(struct sec_predicate *preds, size_t n_preds, char *component, size_t *n_res);


struct sec_clause *clauses_from_file(char *file, size_t *num_clauses);


#endif
