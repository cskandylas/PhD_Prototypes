#include "sec_clause.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "tokenize.h"


//DEEP COPYING PREDICATE NAME AND TERMS SEEMS LIKE A BETTER OPTION
void init_predicate(struct sec_predicate *pred,char *name)
{
    pred->name=malloc(strlen(name)+1);
    strcpy(pred->name,name);
    pred->max_terms=MAX_TERMS_INIT;
    pred->num_terms=0;
    pred->terms=malloc(pred->max_terms*sizeof(char*));
}

void predicate_add_term(struct sec_predicate *pred, char *term)
{
    if(pred->num_terms>=pred->max_terms)
    {
	pred->max_terms*=2;
	pred->terms=realloc(pred->terms,
			    pred->num_terms*sizeof(char*));
	
    }
    //deep copy here to make memory handling easier.
    pred->terms[pred->num_terms]=malloc(strlen(term)+1);
    strcpy(pred->terms[pred->num_terms],term);
    pred->num_terms++;
}

void copy_predicate(struct sec_predicate *dst, struct sec_predicate *from)
{
    dst->name=malloc(strlen(from->name)+1);
    strcpy(dst->name,from->name);
    dst->max_terms=from->max_terms;
    dst->num_terms=from->num_terms;
    dst->terms=malloc(from->max_terms*sizeof(char*));
    for(int i=0;i<from->num_terms;i++)
    {
	dst->terms[i]=malloc(strlen(from->terms[i])+1);
	strcpy(dst->terms[i],from->terms[i]);
    }
}

void destroy_predicate(struct sec_predicate *pred)
{
    free(pred->name);
    for(int i=0;i<pred->num_terms;i++)
    {
	free(pred->terms[i]);
    }
    free(pred->terms);
}


char* predicate_to_str(struct sec_predicate *pred)
{
    size_t str_len=0;
    str_len+=strlen(pred->name);
    //one for each of( ).\0
    str_len+=4;
    for(int i=0;i<pred->num_terms;i++)
    {
	str_len+=strlen(pred->terms[i])+1;
    }
    //for , between arguments
    if(pred->num_terms>1)
	str_len+=pred->num_terms-1;
    char *predicate_string=calloc(str_len+1,1);
    strcpy(predicate_string,pred->name);
    predicate_string=strcat(predicate_string,"(");

    for(int i=0;i<pred->num_terms;i++)
    {
	predicate_string=strcat(predicate_string,pred->terms[i]);
	if(i!=pred->num_terms-1)
	{
	    predicate_string=strcat(predicate_string,",");
	}
    }
    
    predicate_string=strcat(predicate_string,").");
    return predicate_string;
}


struct sec_predicate* predicate_from_str(char *pred_string)
{
    struct sec_predicate *pred=malloc(sizeof(struct sec_predicate));
    struct tokenizer name_tok;
    tokenizer_init(&name_tok,pred_string,"().");
    char *pred_name=next_token(&name_tok);
    if(pred_name)
    {
	char *terms=next_token(&name_tok);
	init_predicate(pred,pred_name);
	free(pred_name);
	struct tokenizer term_tok;
	tokenizer_init(&term_tok,terms,",");
	char *term;
	while((term=next_token(&term_tok))!=NULL)
	{
	    predicate_add_term(pred,term);
	    free(term);
	}
	free(terms);
	return pred;
    }
    return NULL;
}

void init_clause(struct sec_clause *clause, struct  sec_predicate *head)
{
    clause->head=head;
    clause->tail_len=0;
    clause->max_tail_len=MAX_PREDS_INIT;
    clause->tail=malloc(clause->max_tail_len*sizeof(struct sec_predicate*));
    
}

void clause_add_tail_predicate(struct sec_clause *clause,struct  sec_predicate *pred)
{
    if(clause->tail_len>=clause->max_tail_len)
    {
	clause->max_tail_len*=2;
	clause->tail=realloc(clause->tail,
			    clause->tail_len*sizeof(char*));
	
    }
    clause->tail[clause->tail_len]=pred;
    clause->tail_len++;
}

void destroy_clause(struct sec_clause *clause)
{
    free(clause->tail);
}


int clause_find_tail_predicates(struct sec_clause *clause, char *pred_name,struct sec_predicate **found)
{
    size_t count=0;
    for(int i=0;i<clause->tail_len;i++)
    {
	if(strcmp(clause->tail[i]->name,pred_name)==0)
	{
	    count++;
	}
    }

    if(count>0)
    {
	int idx=0;
	*found=malloc(count * sizeof(struct sec_predicate));
	for(int i=0;i<clause->tail_len;i++)
	{
	    if(strcmp(clause->tail[i]->name,pred_name)==0)
	    {
		*found[idx]=*clause->tail[i];
	    }
	}
    }
    return count;
}

