
#include <stddef.h>

#ifndef SEC_CLAUSE
#define SEC_CLAUSE


#define MAX_TERMS_INIT 8
#define MAX_PREDS_INIT 8

struct sec_predicate
{
    char *name;
    char **terms;
    size_t num_terms;
    size_t max_terms;
};


struct sec_clause
{
    struct sec_predicate *head;
    struct sec_predicate **tail;
    size_t tail_len;
    size_t max_tail_len;
};


void init_predicate(struct sec_predicate *pred,char *name);
void predicate_add_term(struct sec_predicate *pred, char *term);
void destroy_predicate(struct sec_predicate *pred);
char* predicate_to_str(struct sec_predicate *pred);
struct sec_predicate* predicate_from_str(char *pred_string);
void copy_predicate(struct sec_predicate *dst, struct sec_predicate *from);


void init_clause(struct sec_clause *clause, struct  sec_predicate *head);
void clause_add_tail_predicate(struct sec_clause *clause,struct  sec_predicate *pred);
void destroy_clause(struct sec_clause *clause);
char *clause_to_str(struct sec_clause *clause);
int clause_find_tail_predicates(struct sec_clause *clause, char *pred_name,struct sec_predicate **found);


#endif
