#include "fsm.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
void init_fsm(struct fsm *fsm, char *name)
{

    fsm->name=malloc(strlen(name)+1);
    strcpy(fsm->name,name);

    fsm->num_actions=0;
    fsm->max_actions=FSM_MAX_ACTIONS_INIT;
    fsm->actions=malloc(FSM_MAX_ACTIONS_INIT*sizeof(char*));
    fsm->variables=NULL;
    fsm->probabilities=NULL;
}

void destroy_fsm(struct fsm *fsm)
{
    free(fsm->name);  

    for(int i=0;i<fsm->num_actions;i++)
    {
	free(fsm->actions[i]);
    }
    free(fsm->actions);
    
    if(fsm->variables)
	free(fsm->variables);
}


void fsm_add_action(struct fsm *fsm, char *action)
{
    if(fsm->num_actions >= fsm->max_actions)
    {
	fsm->max_actions*=2;
	fsm->actions=realloc(fsm->actions,fsm->max_actions*sizeof(char*));
    }
    fsm->actions[fsm->num_actions]=action;
    fsm->num_actions++;
}


void fsm_set_variables(struct fsm *fsm, char *variables)
{
    fsm->variables=variables;
}

void fsm_set_probabilities(struct fsm *fsm, char *probabilities)
{
    fsm->probabilities=probabilities;
}


char *fsm_to_prism(struct fsm *fsm)
{

    size_t len=512;
    if(fsm->name)
	len+=strlen(fsm->name);
    if(fsm->variables)
	strlen(fsm->variables);
    if(fsm->probabilities)
	len+=strlen(fsm->probabilities);


    
    for(int i=0;i<fsm->num_actions;i++)
    {
	//TODO:FIX
	len+=strlen(fsm->actions[i])*10;
    }    
    char *fsm_str=malloc(len);
    
    strcpy(fsm_str,"dtmc\n\n");
    if(fsm->probabilities)
	strcat(fsm_str,fsm->probabilities);

    char module_name[256];
    sprintf(module_name,"\n\nmodule %s\n\n",fsm->name);
    strcat(fsm_str,module_name);
    
    if(fsm->variables)
	strcat(fsm_str,fsm->variables);
    
    strcat(fsm_str,"\n\n");
    
    for(int i=0;i<fsm->num_actions;i++)
    {
	strcat(fsm_str,fsm->actions[i]);
    }
    
    strcat(fsm_str,"\nendmodule\n");

    
    
    return fsm_str;
}
