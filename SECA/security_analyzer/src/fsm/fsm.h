
#include <stddef.h>

#ifndef FSM
#define FSM


#define FSM_MAX_ACTIONS_INIT 32

struct fsm
{
    char *name;

    char *variables;
    char *probabilities;
    
    char **actions;
    size_t num_actions;
    size_t max_actions;
};

void init_fsm(struct fsm *fsm, char *name);
void destroy_fsm(struct fsm *fsm);
char *fsm_to_prism(struct fsm *fsm);
void fsm_add_action(struct fsm *fsm, char *action);
void fsm_set_variables(struct fsm *fsm, char *variables);
void fsm_set_probabilities(struct fsm *fsm, char *probabilities);
#endif
