#define _XOPEN_SOURCE 600

#include "fsm_resources.h"
#include "lag.h"
#include "rbtree.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "tokenize.h"


void fsm_to_prism_model_file(char *model_file, struct fsm *fsm)
{
        FILE *fp=fopen(model_file,"w");
	char *fsm_str=fsm_to_prism(fsm);
	fprintf(fp,"%s\n",fsm_str);
	free(fsm_str);
	fclose(fp);
}

void fsm_to_prism_properties_file(char *prop_file, char *content)
{
     FILE *fp=fopen(prop_file,"w");
     fprintf(fp,"%s\n",content);
     fclose(fp);   
}


//function signatures for fsm building
void build_derived(struct fsm *fsm, struct lag *lag, struct lag_node *derived,
		   struct variable_generator *var_gen);
void build_primitive(struct fsm *fsm, struct lag *lag, struct lag_node *primitive,
		     struct variable_generator *var_gen);



//some hardcoded ugliness, fix later
bool node_is_prereq(struct lag *lag, struct lag_node *node)
{
    switch(lag->type)
    {
    case LAG_COMPONENT_INTERACTION:
	return false;
	break;
    case LAG_SINGLE_EXPLOIT:
	return strncmp(node->label,"reachable",strlen("reachable"))==0;
	break;
    case LAG_MULTI_EXPLOIT:
	return strncmp(node->label,"singleExploit",strlen("singleExploit"))==0;
	break;
    case LAG_COMPONENT_IMPACT:
	return strncmp(node->label,"singleExploit",strlen("singleExploit"))==0
	    || strncmp(node->label,"multiExploit",strlen("multiExploit"))==0;
	break;
    case LAG_MAGIC:
	return strncmp(node->label,"reachable",strlen("reachable"))==0
	    || strncmp(node->label,"canReach",strlen("canReach"))==0
	    || strncmp(node->label,"vulnExists",strlen("vulnExists"))==0;
	break;

	/*
    case LAG_OVERALL_SECURITY:
	return strncmp(node->label,"componentImpact", strlen("componentImpact"))==0;
	break;*/
    }    
}

//some hardcoded ugliness, fix later or never
bool node_is_goal(struct lag *lag,struct lag_node *node)
{
    switch(lag->type)
    {
    case LAG_COMPONENT_INTERACTION:
	return strncmp(node->label,"reachable",strlen("reachable"))==0;
	break;
    case LAG_SINGLE_EXPLOIT:
	return strncmp(node->label,"singleExploit",strlen("singleExploit"))==0;
	break;
    case LAG_MULTI_EXPLOIT:
	return strncmp(node->label,"multiExploit",strlen("multiExploit"))==0;
	break;
    case LAG_COMPONENT_IMPACT:
	return strncmp(node->label,"componentImpact",strlen("componentImpact"))==0;
	break;
    case LAG_MAGIC:
	return strncmp(node->label,"componentImpact",strlen("componentImpact"))==0;
	break;
	/*
    case LAG_OVERALL_SECURITY:
	return strncmp(node->label,"systemDown",strlen("systemDown"))==0;
	break;
	*/
    }
}

char * make_capability(char *label)
{
    //printf("%s\n",label);
    char *cap=malloc(strlen(label)+1);

    
    for(int i=0;i<strlen(label);i++)
    {
	if(label[i] == '('  || label[i] == ',')
	{
	    cap[i]='_';
	}
	else if(label[i] ==')')
	{
	    cap[i]='\0';
	}
	else
	{
	    cap[i]=label[i];
	}
    }

    return cap;
}

void build_primitive(struct fsm *fsm, struct lag *lag, struct lag_node *primitive,
		     struct variable_generator *var_gen)
{

    char id[128];
    char *name=make_capability(primitive->label);
    
    

    char guard[256];
    {
	char *cap=make_capability(primitive->label);
	sprintf(guard,"(c_%s=false)",cap);
	//sprintf(guard,"(c_%d=false)",primitive->node_id);
	free(cap);
    }
    char update[128];
    sprintf(update,"  (c_%s' = true)",name);
    //sprintf(update,"  (c_%d' = true)",primitive->node_id);
    
    char *action=malloc(strlen(guard)+strlen(update)+strlen(name)*2+128);
    sprintf(action,"[] (!gave_up) & %s -> (c_%s_prob) : %s + (1-c_%s_prob) : (gave_up'=gave_up);\n",guard,name, update, name);
    //sprintf(action,"[] (!gave_up) & %s -> (c_%d_prob) : %s + (1-c_%d_prob) : (gave_up'=gave_up);\n",guard,primitive->node_id, update, primitive->node_id);
    fsm_add_action(fsm,action);
   
}

void cleanup_guard(char *guard)
{
    for(int i=strlen(guard);i>=0;i--)
    {
	if(guard[i]==' ')
	    return;
	else
	    guard[i]='\0';
    }
}

char *and(struct lag *lag, int *node_ids,size_t num_nodes, size_t *num_and_parts)
{
    
    char *and_str=malloc(128*num_nodes);
    memset(and_str,'\0',128*num_nodes);
    
    int count =0;
    char *and_parts=malloc(128*num_nodes);
    memset(and_parts,'\0',128*num_nodes);
    for(int i=0;i<num_nodes;i++)
    {
	if(lag->nodes[node_ids[i]]->type==LAG_OR)
	{
	    if(count>0)
	    {
		strcat(and_parts,"& ");
	    }
	    char *guard_cap=make_capability(lag->nodes[node_ids[i]]->label);
	    char and_str_part[128];
	    sprintf(and_str_part," (c_%s=true) ",guard_cap);
	    strcat(and_parts,and_str_part);
	    //printf("%d: %s\n",count,and_parts);
	    count++;
	}
	
    }
    if(count>1)
	strcat(and_str,"(");
    strcat(and_str,and_parts);
    if(count>1)
	strcat(and_str,")");

    if(num_and_parts)
    {
	*num_and_parts=count;
    }
    free(and_parts);
    return and_str;
}



char *high_level_and(struct lag *lag, int *node_ids,size_t num_nodes, size_t *num_and_parts)
{
    
    char *and_str=malloc(128*num_nodes);
    memset(and_str,'\0',128*num_nodes);
    
    int count =0;
    char *and_parts=malloc(128*num_nodes);
    memset(and_parts,'\0',128*num_nodes);
    for(int i=0;i<num_nodes;i++)
    {
	if(node_is_prereq(lag,lag->nodes[node_ids[i]]))
	{
	    if(count>0)
	    {
		strcat(and_parts,"& ");
	    }
	    char *guard_cap=make_capability(lag->nodes[node_ids[i]]->label);
	    char and_str_part[128];
	    sprintf(and_str_part," (c_%s=true) ",guard_cap);
	    strcat(and_parts,and_str_part);
	    //printf("%d: %s\n",count,and_parts);
	    count++;
	}
	
    }
    if(count>1)
	strcat(and_str,"(");
    strcat(and_str,and_parts);
    if(count>1)
	strcat(and_str,")");

    if(num_and_parts)
    {
	*num_and_parts=count;
    }
    free(and_parts);
    return and_str;
}


char *make_high_level_guard(struct lag *lag, struct lag_node *node)
{
    char *name=make_capability(node->label);
    char *guard=malloc(1024*1024);
    memset(guard,'\0',1024*1024);
    sprintf(guard, "(!q_%s) ",name);

    char *guard_parts=malloc(1024*1024);
    memset(guard_parts,'\0',1024*1024);
    int count=0;
    for(int i=0;i<node->num_parents;i++)
    {
	
	if(count>0)
	{
	    strcat(guard_parts,"|");
	}

	struct lag_node *rule=lag->nodes[node->parent_ids[i]];
	size_t num_and_parts=0;
	char *and_str=high_level_and(lag,rule->parent_ids,rule->num_parents,&num_and_parts);
	if(num_and_parts>0)
	{
	    //printf("HIGH LEVEL GUARD_PART:%s\n",and_str);
	    strcat(guard_parts,and_str);
	    count++;
	}
    }
	
	
    if(count>0)
    {
	strcat(guard,"& ");
    }
    if(count>1)
    {
	strcat(guard,"(");
    }
    strcat(guard,guard_parts);
    if(count>1)
    {
	strcat(guard,")");
    }
	
    //printf("HIGH LEVEL GUARD:%s:%s\n",node->label,guard);
    char *extra_guard_fragment=malloc(128);
    
    sprintf(extra_guard_fragment,"& (c_%s=false) ",name);
    strcat(guard,extra_guard_fragment);

    return guard;
    
}

char *make_guard(struct lag *lag, struct lag_node *node)
{

    char *name=make_capability(node->label);
    char *guard=malloc(1024*1024);
    memset(guard,'\0',1024*1024);
    sprintf(guard, "(!q_%s) ",name);

    char *guard_parts=malloc(1024*1024);
    memset(guard_parts,'\0',1024*1024);
    int count=0;
    for(int i=0;i<node->num_parents;i++)
    {
	
	if(count>0)
	{
	    strcat(guard_parts,"|");
	}

	struct lag_node *rule=lag->nodes[node->parent_ids[i]];
	size_t num_and_parts=0;
	char *and_str=and(lag,rule->parent_ids,rule->num_parents,&num_and_parts);
	if(num_and_parts>0)
	{
	    strcat(guard_parts,and_str);
	    count++;
	}
    }
	
	
    if(count>0)
    {
	strcat(guard,"& ");
    }
    if(count>1)
    {
	strcat(guard,"(");
    }
    strcat(guard,guard_parts);
    if(count>1)
    {
	strcat(guard,")");
    }
	
    char *extra_guard_fragment=malloc(128);
    
    sprintf(extra_guard_fragment,"& (c_%s=false) ",name);
    strcat(guard,extra_guard_fragment);

    return guard;
    
}


void build_prism_actions(struct fsm *fsm,struct lag *lag, struct lag_node *derived)
{

    char *name=make_capability(derived->label);
    char update[128];	    
    sprintf(update," (c_%s' = true)",name);
    
    for(int i=0;i<derived->num_parents;i++)
    {
	char guard[1024];
	memset(guard,'\0',1024);
	sprintf(guard,"!c_%s ",name);
	struct lag_node *rule=lag->nodes[derived->parent_ids[i]];
	int count=0;
	for(int j=0;j<rule->num_parents;j++)
	{
	    struct lag_node *child=lag->nodes[rule->parent_ids[j]];
	    if(child->type!=LAG_LEAF)
	    {
		char guard_frag[128];
		memset(guard_frag,'\0',128);
		
		strcat(guard, " & ");
		
		
		char *child_cap=make_capability(child->label);
		char guard_part[128];
		sprintf(guard_part,"c_%s",child_cap);
		strcat(guard,guard_part);
		free(child_cap);
		count++;
	    }
	}
	char *action=malloc(strlen(guard)+strlen(update)+strlen(name)*7+128);
	if(!strstr(update,"singleExploit") && !strstr(update,"multiExploit") && !strstr(update,"componentImpact"))
	{
	    //printf("[] %s -> (c_%s_prob) : %s + (1-c_%s_prob) : true;\n", guard,name, update,name);
	    sprintf(action,"[] %s -> (c_%s_prob) : %s + (1-c_%s_prob) : true;\n",guard,name, update,name);
	    //sprintf(action,"[] %s -> (c_%d_prob) : %s + (1-c_%d_prob) : (gave_up'=gave_up);\n", guard,derived->node_id, update,derived->node_id);
	}
	else
	{
	    //printf("[] %s -> (c_%s_prob) : %s + (1-c_%s_prob)/2 : true + 1-(c_%s_prob+(1-c_%s_prob)/2) : (gave_up'=true);\n", guard,name, update,name,name,name);
	    sprintf(action,"[] !q_%s & %s -> (c_%s_prob) : %s + (1-c_%s_prob)/2 : true + 1-(c_%s_prob+(1-c_%s_prob)/2) : (q_%s'=true);\n",name, guard,name, update,name,name,name,name);
	    //sprintf(action,"[] (!%s_gave_up) &  %s -> (c_%s_prob) : %s + (1-c_%s_prob) : (%s_gave_up'=true);\n",frag_name,guard,name,update,name,frag_name);
	    //sprintf(action,"[] %s -> (c_%d_prob) : %s + (1-c_%d_prob)/2 : (gave_up'=gave_up) + 1-(c_%d_prob+(1-c_%d_prob)/2) : (gave_up'=true);\n",
	    //	guard,derived->node_id, update,derived->node_id,derived->node_id,derived->node_id);
	}
	fsm_add_action(fsm,action);
    }
    free(name);
}

void build_derived(struct fsm *fsm, struct lag *lag, struct lag_node *derived,
		   struct variable_generator *var_gen)
{

    for(int i=0;i<derived->num_parents;i++)
    {
	struct lag_node *rule=lag->nodes[derived->parent_ids[i]];
	for(int j=0;j<rule->num_parents;j++)
	{
	    if(lag->nodes[rule->parent_ids[j]]->type==LAG_OR)
	    {
		build_derived(fsm,lag,lag->nodes[rule->parent_ids[j]],var_gen);
	    }
	}
    }

    build_prism_actions(fsm,lag,derived);
    
    /*
    //printf("Building action for:%s\n",derived->label);
    //printf("Num Parents: %zu\n",derived->num_parents);
    char *name=make_capability(derived->label);
    //printf("Building derived node for %s\n",name);
    //printf("Allocating %zu bytes for guard\n",(derived->num_parents+1)*1024);
    char guard[(derived->num_parents+1)*1024];
    memset(guard,'\0',(derived->num_parents+1)*1024);
    sprintf(guard, "(!q_%s) ",name);
 
    char *alt_guard=malloc(1024*1024);
    memset(alt_guard,'\0',1024*1024);
    sprintf(alt_guard, "(!q_%s) ",name);

    int count=0;
    char *alt_guard_parts=malloc(1024*1024);
    memset(alt_guard_parts,'\0',1024*1024);
    for(int i=0;i<derived->num_parents;i++)
    {
	
	if(count>0)
	{
	    strcat(alt_guard_parts,"|");
	}

	struct lag_node *rule=lag->nodes[derived->parent_ids[i]];
	size_t num_and_parts=0;
	char *and_str=and(lag,rule->parent_ids,rule->num_parents,&num_and_parts);
	if(num_and_parts>0)
	{
	    printf("ALT_GUARD_PART:%s\n",and_str);
	    strcat(alt_guard_parts,and_str);
	    count++;
	}
	
	for(int j=0;j<rule->num_parents;j++)
	{
	    
	    //strcat(alt_guard,and(lag,rule->parent_ids,rule->num_parents));
	    if(lag->nodes[rule->parent_ids[j]]->type==LAG_OR)
	    {
		
		
		//guard fragment for parent
		char guard_fragment[128];
		char *guard_cap=make_capability(lag->nodes[rule->parent_ids[j]]->label);
		sprintf(guard_fragment,"& (c_%s=true) ",guard_cap);
		//sprintf(guard_fragment,"& (c_%d=true) ",lag->nodes[rule->parent_ids[j]]->node_id);
		strcat(guard,guard_fragment);
		free(guard_cap);
		//count++;
	    }
	

	}
	
	
    }
    if(count>0)
    {
	strcat(alt_guard,"& ");
    }
    if(count>1)
    {
	strcat(alt_guard,"(");
    }
    strcat(alt_guard,alt_guard_parts);
    if(count>1)
    {
	strcat(alt_guard,")");
    }
	
    printf("ALTGUARD:%s:%s\n",derived->label,alt_guard);
    printf("Guard for %s:%s\n",derived->label,guard);
    

    

    //printf("Guard up to this point:%s\n",guard);
    
    */

    
    //char *name=make_capability(derived->label);
    //char *guard=make_guard(lag,derived);

    //char update[128];	    
    //sprintf(update," (c_%s' = true)",name);
    //sprintf(extra_guard_fragment,"& (c_%s=false) ",cap);
    /*
      sprintf(update," (c_%d' = true)",derived->node_id);
      sprintf(extra_guard_fragment,"& (c_%d=false) ",derived->node_id);
    */	
    //free(cap);
    
    //printf("%s:%d\n",guard,strlen(guard));
    /*
      else
      {
      sprintf(update," (c%d' = true)",var);
      sprintf(extra_guard_fragment,"& (c%d=false) ",var);
      
      }
    */
    //printf("Update:%s\n",update);
    //strcat(guard,extra_guard_fragment);
    //strcat(alt_guard,extra_guard_fragment);
    //cleanup_guard(guard);
    /*
    char *action=malloc(strlen(guard)+strlen(update)+strlen(name)*6+128);
    if(!strstr(update,"singleExploit") && !strstr(update,"multiExploit") && !strstr(update,"componentImpact"))
    {
	//printf("[] %s -> (c_%s_prob) : %s + (1-c_%s_prob) : true;\n", guard,name, update,name);
       sprintf(action,"[] %s -> (c_%s_prob) : %s + (1-c_%s_prob) : true;\n",guard,name, update,name);
	//sprintf(action,"[] %s -> (c_%d_prob) : %s + (1-c_%d_prob) : (gave_up'=gave_up);\n", guard,derived->node_id, update,derived->node_id);
    }
    else
    {
	//printf("[] %s -> (c_%s_prob) : %s + (1-c_%s_prob)/2 : true + 1-(c_%s_prob+(1-c_%s_prob)/2) : (gave_up'=true);\n", guard,name, update,name,name,name);
	sprintf(action,"[] %s -> (c_%s_prob) : %s + (1-c_%s_prob)/2 : true + 1-(c_%s_prob+(1-c_%s_prob)/2) : (q_%s'=true);\n", guard,name, update,name,name,name,name);
	//sprintf(action,"[] (!%s_gave_up) &  %s -> (c_%s_prob) : %s + (1-c_%s_prob) : (%s_gave_up'=true);\n",frag_name,guard,name,update,name,frag_name);
	//sprintf(action,"[] %s -> (c_%d_prob) : %s + (1-c_%d_prob)/2 : (gave_up'=gave_up) + 1-(c_%d_prob+(1-c_%d_prob)/2) : (gave_up'=true);\n",
	//	guard,derived->node_id, update,derived->node_id,derived->node_id,derived->node_id);
    }
    fsm_add_action(fsm,action)
    */
    //printf("%s\n",action);
      
}


void init_variable_generator(struct variable_generator *var_gen)
{
    var_gen->var_count=0;
}

int variable_generator_new_var(struct variable_generator *var_gen)
{
    
    int ret=var_gen->var_count;
    var_gen->var_count++;
    return ret;
}

/*
int str_ccmp(const void *vstr1, const void *vstr2)
{
    const char *str1=vstr1;
    const char *str2=vstr2;

    return strcmp(str1,str2);
}
*/




void create_probabilities_rbtree_rec(struct  rb_node *node , char *probs)
{
    if(!node)
	return;
    
    if(node->left)
    {
	create_probabilities_rbtree_rec(node->left,probs);
    }

    if(node->right)
    {
	create_probabilities_rbtree_rec(node->right,probs);
    }
    if(node->data)
    {
	struct tokenizer space_tok;
	tokenizer_init(&space_tok,node->data," ");
	char *var=next_token(&space_tok);
	char *prob=next_token(&space_tok);
	char prob_decl[128];
	sprintf(prob_decl,"formula c_%s_prob = %s;\n",var,prob);
	strcat(probs,prob_decl);
	free(var);
	free(prob);
	//free(node->data);
    }
    
}


char *create_probabilities_rb_tree(struct rb_tree *tree)
{
    if(tree->size == 0)
	return NULL;
    char *probs=malloc(128*tree->size);
    memset(probs,'\0',128*tree->size);
    create_probabilities_rbtree_rec(tree->root,probs);

    return probs;
}



void create_variables_rbtree_rec(struct  rb_node *node , char *vars)
{

    if(!node)
	return;
    
    if(node->left)
    {
	create_variables_rbtree_rec(node->left,vars);
    }

    if(node->right)
    {
	create_variables_rbtree_rec(node->right,vars);
    }
    if(node->data)
    {
	struct tokenizer space_tok;
	tokenizer_init(&space_tok,node->data," ");
	char *var=next_token(&space_tok);
	char cap_decl[128];
	char quit_decl[128];
	sprintf(cap_decl,"c_%s : bool init false;\n",var);
	sprintf(quit_decl,"q_%s : bool init false;\n",var);
	strcat(vars,cap_decl);
	strcat(vars,quit_decl);
	free(var);
	//free(node->data);
    }
    
}


char *create_variables_rb_tree(struct rb_tree *tree)
{

    if(tree->size == 0)
	return NULL;
    
    char *vars=malloc(256*tree->size);
    memset(vars,'\0',256*tree->size);
    create_variables_rbtree_rec(tree->root,vars);

    return vars;
}


char *create_variables_lag(struct lag *lag)
{
    char *vars=malloc(128*lag->num_nodes*4);
    memset(vars,'\0',128*lag->num_nodes*4);
     for(int i=0;i<lag->num_nodes;i++)
    {
	if(lag->nodes[i] && lag->nodes[i]->type!=LAG_AND && lag->nodes[i]->type!=LAG_LEAF)
	{
	    char *var_name=malloc(128);
	    char *var_quit=malloc(128);
	    char *cap=make_capability(lag->nodes[i]->label);
	    sprintf(var_name,"c_%s : bool init false;\n",cap);
	    sprintf(var_quit,"q_%s : bool init false;\n",cap);
	    //sprintf(var_name,"c_%d : bool init false;\n",lag->nodes[i]->node_id);
	    strcat(vars,var_name);
	    strcat(vars,var_quit);
	    free(cap);
	}
    }

    
    return vars;
}

char *create_probabilities_lag(struct lag *lag)
{
    char *probs=malloc(128*lag->num_nodes*2);
    memset(probs,'\0',128*lag->num_nodes*2);
     for(int i=0;i<lag->num_nodes;i++)
    {
	if(lag->nodes[i] && lag->nodes[i]->type!=LAG_AND && lag->nodes[i]->type!=LAG_LEAF)
	{
	    char *prob_decl=malloc(128);
	    char *cap=make_capability(lag->nodes[i]->label);
	    sprintf(prob_decl," formula c_%s_prob =  %f;\n",cap,lag->nodes[i]->prob);
	    //sprintf(prob_decl," formula c_%d_prob =  %f;\n",lag->nodes[i]->node_id,lag->nodes[i]->prob);
	    strcat(probs,prob_decl);
	    free(cap);
	}
    }
    
    return probs;    
}
    
struct fsm *fsm_from_lag(struct lag *lag, struct variable_generator *var_gen)
{

    //initialize an fsm
    struct fsm *fsm=malloc(sizeof(struct fsm));
    init_fsm(fsm,lag->name);
    //create initial state

    
    size_t num_goals=0;
    for(int i=0;i<lag->num_nodes;i++)
    {
	if(lag->nodes[i])
	{
	    if(node_is_goal(lag,lag->nodes[i]))
	    {
		
		build_derived(fsm,lag,lag->nodes[i],var_gen);
		num_goals++;
	    }
	}
    }

    /*
    printf("FSM Actions:\n");
    for(int i=0;i<fsm->num_actions;i++)
    {
	printf("%s\n",fsm->actions[i]);
    }
    */

    char *variables=create_variables_lag(lag);
    fsm_set_variables(fsm,variables);

    char *probabilities=create_probabilities_lag(lag);
    fsm_set_probabilities(fsm,probabilities);
    
    return fsm;
}
