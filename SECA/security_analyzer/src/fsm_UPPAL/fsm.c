#include "fsm.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

void init_fsm_state(struct fsm_state *state, char *id, char *name, char *exp)
{
    state->id=malloc(strlen(id)+1);
    strcpy(state->id,id);
    
    state->name=malloc(strlen(name)+1);
    strcpy(state->name,name);
    
    state->exp=malloc(strlen(exp)+1);
    strcpy(state->exp,exp);
}


void destroy_fsm_state(struct fsm_state *state)
{
    free(state->id);
    free(state->name);
    free(state->exp);
}


int fsm_state_strlen(struct fsm_state *state)
{
    return 128+strlen(state->id)+strlen(state->name)+strlen(state->exp);
}

char* fsm_state_to_xml(struct fsm_state *state)
{
    /*<location id="%s"> // 24
      <name>%s</name>   // 16
      <label kind="exponentialrate">%s</label> //48
       </location>                            //12
    */
    char *state_string=malloc(fsm_state_strlen(state));
    
    sprintf(state_string,
	    "<location id=\"%s\">\n"
	    "<name>%s</name>\n"
	    "<label kind=\"exponentialrate\">%s</label>\n"
	    "</location>\n",state->id,state->name,state->exp);
	    
    return state_string;
}

void init_fsm_transition(struct fsm_transition *trans, char *src_id,
			 char *tgt_id, int prob, char *guard,
			 char *update, char *sync)
{
    
    trans->src_id=malloc(strlen(src_id)+16);
    strcpy(trans->src_id,src_id);
    trans->tgt_id=malloc(strlen(tgt_id)+1);
    strcpy(trans->tgt_id,tgt_id);
    trans->prob=prob;
    if(guard)
    {
	trans->guard=malloc(strlen(guard)+1);
	strcpy(trans->guard,guard);
    }
    else
    {
	trans->guard=NULL;
    }
    if(update)
    {	
	trans->update=malloc(strlen(update)+1);
	strcpy(trans->update,update);
    }
    else
    {
	trans->update=NULL;
    }
    if(sync)
    {
	trans->sync=malloc(strlen(sync)+1);
	strcpy(trans->sync,sync);
    }
    else
    {
	trans->sync=NULL;
    }
}
void destroy_fsm_transition(struct fsm_transition *trans)
{
    free(trans->src_id);
    free(trans->tgt_id);
    if(trans->guard)
	free(trans->guard);
    if(trans->update)
	free(trans->update);
    if(trans->sync)
	free(trans->sync);
}

int fsm_transition_strlen(struct fsm_transition *trans)
{
    int len=strlen("<transition>\n")+20+strlen(trans->src_id)+20+strlen(trans->tgt_id);
    if(trans->prob<100)
	len+=43;
    if(trans->guard)
	len+=strlen(trans->guard)+64;
    if(trans->update)
	len+=strlen(trans->update)+64;
    if(trans->sync)
	len+=strlen(trans->sync)+64;
	    
    len+=strlen("</transition>\n");
    

    return len;
}

char* fsm_transition_to_xml(struct fsm_transition *trans)
{
    /*<transition>
      <source ref="id3"/>
      <target ref="id5"/>
      <label kind="probability">p1</label>
      </transition>
    */

    
    char *preamble="<transition>\n";
    char *src_ref=malloc(64+strlen(trans->src_id));
    sprintf(src_ref,"<source ref=\"%s\"/>\n",trans->src_id);
    char *tgt_ref=malloc(64+strlen(trans->tgt_id));
    sprintf(tgt_ref,"<target ref=\"%s\"/>\n",trans->tgt_id);

    char *prob_str=NULL;
    if(trans->prob<100)
    {	
	prob_str=malloc(40+16);
	sprintf(prob_str,"<label kind=\"probability\">%d</label>\n",trans->prob);
    }

    char *guard_str=NULL;
    if(trans->guard)
    {
	guard_str=malloc(512+strlen(trans->guard));
	
	sprintf(guard_str,"<label kind=\"guard\">%s</label>\n",trans->guard);
	
    }

    char *update_str=NULL;
    if(trans->update)
    {
	update_str=malloc(48+strlen(trans->update));
	sprintf(update_str,"<label kind=\"assignment\">%s</label>\n",trans->update);
//	printf("UPDATE:%s\n",update_str);
    }
    
    char *epilogue="</transition>\n";

    int len=strlen(preamble)+strlen(epilogue)+strlen(src_ref)+strlen(tgt_ref)+128;
    if(prob_str)
	len+=strlen(prob_str);
    if(guard_str)
	len+=strlen(guard_str);
    if(update_str)
	len+=strlen(update_str);
    
    char *trans_str=malloc(len);
    sprintf(trans_str,"%s%s%s",preamble,src_ref,tgt_ref);

    if(prob_str)
	strcat(trans_str,prob_str);

    if(guard_str)
	strcat(trans_str,guard_str);

    if(update_str)
	strcat(trans_str,update_str);

    strcat(trans_str,epilogue);

    free(src_ref);
    free(tgt_ref);
    

    if(prob_str)
	free(prob_str);

    if(guard_str)
	free(guard_str);

    if(update_str)
	free(update_str);

    
    return trans_str;
}

void init_fsm_branch(struct fsm_branch *branch, struct fsm_transition *trans)
{
    
    branch->id=malloc(strlen(trans->tgt_id)+2);
    sprintf(branch->id,"b%s",trans->tgt_id);
    branch->s_id=malloc(strlen(trans->tgt_id)+1);
    strcpy(branch->s_id,trans->tgt_id);
    branch->f_id=malloc(strlen(trans->tgt_id)+2);
    sprintf(branch->f_id,"f%s",trans->tgt_id);
}
void destroy_fsm_branch(struct fsm_branch *branch)
{
    free(branch->id);
    free(branch->s_id);
    free(branch->f_id);
}

int fsm_branch_strlen(struct fsm_branch *branch)
{
    return 40+strlen(branch->id);
}

char* fsm_branch_to_xml(struct fsm_branch *branch)
{
    /*
    <branchpoint id=%s>"
    </branchpoint>
    */
    char *branch_str=malloc(40+strlen(branch->id));
    sprintf(branch_str,"<branchpoint id=\"%s\">\n</branchpoint>\n",branch->id);
    return branch_str;
}


void init_fsm(struct fsm *fsm, char *name, char *params)
{

    fsm->name=malloc(strlen(name)+1);
    strcpy(fsm->name,name);
    fsm->params=malloc(strlen(params)+1);
    strcpy(fsm->params,params);

    fsm->initial=NULL;
    fsm->declarations=NULL;

    fsm->num_states=0;
    fsm->max_states=MAX_FSM_STATES_INIT;
    fsm->states=malloc(fsm->max_states*sizeof(struct fsm_state*));

    fsm->num_trans=0;
    fsm->max_trans=MAX_FSM_TRANS_INIT;
    fsm->trans=malloc(fsm->max_trans*sizeof(struct fsm_transition*));


    fsm->num_branches=0;
    fsm->max_branches=MAX_FSM_BRANCH_INIT;
    fsm->branches=malloc(fsm->max_branches*sizeof(struct fsm_branch*));
    
}

void destroy_fsm(struct fsm *fsm)
{
    free(fsm->name);
    free(fsm->params);
    free(fsm->initial);
    
    for(int i=0;i<fsm->num_states;i++)
    {
	destroy_fsm_state(fsm->states[i]);
	free(fsm->states[i]);
    }
    free(fsm->states);
    
    for(int i=0;i<fsm->num_trans;i++)
    {
	destroy_fsm_transition(fsm->trans[i]);
	free(fsm->trans[i]);
    }
    free(fsm->trans);

    for(int i=0;i<fsm->num_branches;i++)
    {
	destroy_fsm_branch(fsm->branches[i]);
	free(fsm->branches[i]);
    }
    free(fsm->branches);

    if(fsm->declarations)
    {
	free(fsm->declarations);
    }
}

void fsm_add_state(struct fsm *fsm, struct fsm_state *state)
{
    if(fsm->num_states >= fsm->max_states)
    {
	fsm->max_states*=2;
	fsm->states=realloc(fsm->states,fsm->max_states*sizeof(struct fsm_state*));
    }
    fsm->states[fsm->num_states]=state;
    fsm->num_states++;
}

void fsm_add_trans_inner(struct fsm *fsm,struct fsm_transition *trans)
{
    if(fsm->num_trans >= fsm->max_trans)
    {
	fsm->max_trans*=2;
	fsm->trans=realloc(fsm->trans,fsm->max_trans*sizeof(struct fsm_transition*));
    }
    fsm->trans[fsm->num_trans]=trans;
    fsm->num_trans++;
}

void fsm_add_trans(struct fsm *fsm, struct fsm_transition *trans)
{
    
    if(trans->prob==100)
	fsm_add_trans_inner(fsm,trans);
    else
    {
	//Add a branching point
	struct fsm_branch *branch=malloc(sizeof(struct fsm_branch));
	init_fsm_branch(branch,trans);
	fsm_add_branch(fsm,branch);
	
	//Create a transition from the branch to the original success
	struct fsm_transition *b_to_S=malloc(sizeof(struct fsm_transition));
	init_fsm_transition(b_to_S,branch->id,branch->s_id,trans->prob,NULL,trans->update,NULL);
	fsm_add_trans_inner(fsm,b_to_S);

	//Create a transition to fail
	struct fsm_transition *b_to_F=malloc(sizeof(struct fsm_transition));
	init_fsm_transition(b_to_F,branch->id,branch->f_id,100-trans->prob,NULL,NULL,NULL);
	fsm_add_trans_inner(fsm,b_to_F);

	
	
	
	//Create the fail state
	struct fsm_state *f_state=malloc(sizeof(struct fsm_state));
	init_fsm_state(f_state,branch->f_id,branch->f_id,"1");
	fsm_add_state(fsm,f_state);

	

	//fix original transition
	free(trans->tgt_id);
	
	trans->tgt_id=malloc(strlen(branch->id)+1);
	strcpy(trans->tgt_id,branch->id);
	if(trans->update)
	{
	    free(trans->update);
	    trans->update=NULL;
	}
	trans->prob=100;
	fsm_add_trans_inner(fsm,trans);
	
    }
}



void fsm_add_branch(struct fsm *fsm, struct fsm_branch *branch)
{
    if(fsm->num_branches >= fsm->max_branches)
    {
	fsm->max_branches*=2;
	fsm->branches=realloc(fsm->branches,fsm->max_branches*sizeof(struct fsm_branch*));
    }
    fsm->branches[fsm->num_branches]=branch;
    fsm->num_branches++;
    
}

void fsm_set_inital(struct fsm *fsm, char *initial)
{
    if(fsm->initial)
	free(fsm->initial);
    fsm->initial=malloc(strlen(initial)+1);
    strcpy(fsm->initial,initial);
}

void fsm_set_declarations(struct fsm *fsm, char *declarations)
{
    if(fsm->declarations)
	free(fsm->declarations);
    fsm->declarations=malloc(strlen(declarations)+1);
    strcpy(fsm->declarations,declarations);
}

char *fsm_to_xml(struct fsm *fsm)
{

    char *fsm_string=NULL;

    
    int fsm_str_len=80+strlen(fsm->name)+strlen(fsm->params)+strlen(fsm->initial);
    char *prologue=malloc(64+strlen(fsm->name)+strlen(fsm->params));
    sprintf(prologue,"<template>\n<name>%s</name>\n<parameter>%s</parameter>\n",
	    fsm->name,fsm->params);
    char *epilogue="</template>\n";

    char *declarations=NULL;
    if(fsm->declarations)
    {
	declarations=malloc(strlen(fsm->declarations)+36);
	sprintf(declarations,"<declaration>\n%s\n</declaration>\n",fsm->declarations);	    
	fsm_str_len+=strlen(declarations)+2;
	
    }
    

    for(int i=0;i<fsm->num_states;i++)
	fsm_str_len+=fsm_state_strlen(fsm->states[i]);
    
    for(int i=0;i<fsm->num_trans;i++)
	fsm_str_len+=fsm_transition_strlen(fsm->trans[i]);
    for(int i=0;i<fsm->num_branches;i++)
	fsm_str_len+=fsm_branch_strlen(fsm->branches[i]);
    fsm_str_len+=strlen(epilogue);


    size_t count_copied=0;
    fsm_string=calloc(fsm_str_len+1,1);
    //sprintf(fsm_string,"%s%s",prologue,epilogue);

    strcpy(fsm_string,prologue);
    count_copied+=strlen(prologue);
    if(declarations)
    {
	strcat(fsm_string,declarations);
	free(declarations);
    }

    for(int i=0;i<fsm->num_states;i++)
    {
	char *state_str=fsm_state_to_xml(fsm->states[i]);

	strcat(fsm_string,state_str);
	count_copied+=strlen(state_str);
	free(state_str);
	
    }
    for(int i=0;i<fsm->num_branches;i++)
    {
	
	char *branch_str=fsm_branch_to_xml(fsm->branches[i]);

	strcat(fsm_string,branch_str);
	count_copied+=strlen(branch_str);
	free(branch_str);
    }

    /*<init ref="Start"/>*/

    char *init_str=malloc(64+strlen(fsm->initial));
    
    sprintf(init_str,"<init ref=\"%s\"/>\n",fsm->initial);

    strcat(fsm_string,init_str);
    count_copied+=strlen(init_str);
    free(init_str);

    for(int i=0;i<fsm->num_trans;i++)
    {
	//printf("Transition %d\n",i);
	char *trans_str=fsm_transition_to_xml(fsm->trans[i]);
	//printf("Copied up to here: %zu, length of next transition string:%zu\n, size of buffer: %d\n",count_copied,strlen(trans_str),fsm_str_len);
	//printf("FULLSTRING UP TO HERE:%s",fsm_string);
	strcat(fsm_string,trans_str);
	count_copied+=strlen(trans_str);
	
	free(trans_str);
    }
    

    
    //printf("prologue:%s\n",prologue);
    //printf("epologue:%s\n",epilogue);

    strcat(fsm_string,epilogue);

    
    free(prologue);

    return fsm_string;
}


struct fsm_state *fsm_find_state(struct fsm *fsm, char *name, char *id, char *exp)
{
    for(int i=0;i<fsm->num_states;i++)
    {
	if(fsm->states[i])
	{
	    struct fsm_state *state=fsm->states[i];
	    if(strcmp(state->name,name)==0 && strcmp(state->id,id)==0 && strcmp(state->exp,exp)==0)
	    {
		return state;
	    }
	}
    }
    return NULL;
}


char *fsm_to_prism(struct fsm *fsm)
{
    
}
