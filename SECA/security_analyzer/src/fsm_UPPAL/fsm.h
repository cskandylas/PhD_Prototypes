
#include <stddef.h>

#ifndef FSM
#define FSM

struct fsm_state
{
    char *id;
    char *name;
    char *exp;
};

void init_fsm_state(struct fsm_state *state, char *id, char *name, char *exp);
void destroy_fsm_state(struct fsm_state *state);
char* fsm_state_to_xml(struct fsm_state *state);
int fsm_state_strlen(struct fsm_state *state);

struct fsm_transition
{
    char *src_id;
    char *tgt_id;
    int prob;
    char *guard;
    char *update;
    char *sync;
};

void init_fsm_transition(struct fsm_transition *trans, char *src_id,
			 char *tgt_id, int prob, char *guard,
			 char *update, char *sync);
void destroy_fsm_transition(struct fsm_transition *trans);
char* fsm_transition_to_xml(struct fsm_transition *trans);
int fsm_transition_strlen(struct fsm_transition *trans);

struct fsm_branch
{
    char *id;
    char *s_id;
    char *f_id;
};

void init_fsm_branch(struct fsm_branch *branch, struct fsm_transition *trans);
void destroy_fsm_branch(struct fsm_branch *branch);
char* fsm_branch_to_xml(struct fsm_branch *branch);
int fsm_branch_strlen(struct fsm_branch *branch);


#define MAX_FSM_STATES_INIT 24
#define MAX_FSM_TRANS_INIT 32
#define MAX_FSM_BRANCH_INIT 16

struct fsm
{
    char *name;
    char *params;
    
    char *initial;
    char *declarations;
    
    struct fsm_state **states;
    size_t num_states;
    size_t max_states;

    struct fsm_transition **trans;
    size_t num_trans;
    size_t max_trans;

    struct fsm_branch **branches;
    size_t num_branches;
    size_t max_branches;
    
    
};

void init_fsm(struct fsm *fsm, char *name, char *params);
void destroy_fsm(struct fsm *fsm);
char *fsm_to_xml(struct fsm *fsm);
char *fsm_to_prism(struct fsm *fsm);
void fsm_add_state(struct fsm *fsm, struct fsm_state *state);
void fsm_add_trans(struct fsm *fsm, struct fsm_transition *trans);
void fsm_add_branch(struct fsm *fsm, struct fsm_branch *branch);
void fsm_set_inital(struct fsm *fsm, char *initial);
void fsm_set_declarations(struct fsm *fsm, char *declarations);
struct fsm_state *fsm_find_state(struct fsm *fsm, char *name, char *id, char *exp);
#endif
