#define _XOPEN_SOURCE 600

#include "fsm_resources.h"
#include "lag.h"
#include "rbtree.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>


void fsm_to_xml_file(char *xml_file, struct fsm *fsm,
		     char *global_decls, char *system_str,
		     char **queries, size_t num_queries)
{

    FILE *fp=fopen(xml_file,"w");

    if(!fp)
    {
	fprintf(stderr,"Could not open %s\n",xml_file);
    }

    fprintf(fp,"<nta>\n");
    fprintf(fp,"<declaration>\n");
    fprintf(fp,"%s",global_decls);
    fprintf(fp,"</declaration>\n");
    char *fsm_str=fsm_to_xml(fsm);

    fprintf(fp,"%s",fsm_str);
    free(fsm_str);
    fprintf(fp,"<system>\n");
    fprintf(fp,"%s",system_str);
    fprintf(fp,"</system>\n");

    fprintf(fp,"<queries>\n");

    for(int i=0;i<num_queries;i++)
    {
	fprintf(fp,"<query>\n");
	fprintf(fp,"<formula>%s</formula>\n",queries[i]);
	fprintf(fp,"<comment></comment>\n");
	fprintf(fp,"</query>\n");
    }
    
    fprintf(fp,"</queries>\n");

    fprintf(fp,"</nta>");

    fclose(fp);
	
}

//function signatures for fsm building
void build_derived(struct fsm *fsm, struct lag *lag, struct lag_node *derived,
		   struct variable_generator *var_gen,struct rb_tree *visited);
void build_primitive(struct fsm *fsm, struct lag *lag, struct lag_node *primitive,
		     struct variable_generator *var_gen,struct rb_tree *visited);



//some hardcoded ugliness, fix later
bool node_is_prereq(struct lag *lag, struct lag_node *node)
{
    switch(lag->type)
    {
    case LAG_COMPONENT_INTERACTION:
	return false;
	break;
    case LAG_SINGLE_EXPLOIT:
	return strncmp(node->label,"reachable",strlen("reachable"))==0;
	break;
    case LAG_MULTI_EXPLOIT:
	return strncmp(node->label,"singleExploit",strlen("singleExploit"))==0;
	break;
    case LAG_COMPONENT_IMPACT:
	return strncmp(node->label,"singleExploit",strlen("singleExploit"))==0 || strncmp(node->label,"multiExploit",strlen("multiExploit"))==0;
	break;
    case LAG_OVERALL_SECURITY:
	return strncmp(node->label,"reachable", strlen("reachable"))==0;
	break;
    }    
}

//some hardcoded ugliness, fix later
bool node_is_goal(struct lag *lag,struct lag_node *node)
{
    switch(lag->type)
    {
    case LAG_COMPONENT_INTERACTION:
	return strncmp(node->label,"reachable",strlen("reachable"))==0;
	break;
    case LAG_SINGLE_EXPLOIT:
	return strncmp(node->label,"singleExploit",strlen("singleExploit"))==0;
	break;
    case LAG_MULTI_EXPLOIT:
	return strncmp(node->label,"multiExploit",strlen("multiExploit"))==0;
	break;
    case LAG_COMPONENT_IMPACT:
	return strncmp(node->label,"componentImpact",strlen("componentImpact"))==0;
	break;
    case LAG_OVERALL_SECURITY:
	return strncmp(node->label,"componentImpact",strlen("componentImpact"))==0;
	break;
    }
}


char * make_capability(char *label)
{
    char *cap=malloc(strlen(label)+1);

    
    for(int i=0;i<strlen(label);i++)
    {
	if(label[i] == '('  || label[i] == ',')
	{
	    cap[i]='_';
	}
	else if(label[i] ==')')
	{
	    cap[i]='\0';
	}
	else
	{
	    cap[i]=label[i];
	}
    }

    return cap;
}

void build_primitive(struct fsm *fsm, struct lag *lag, struct lag_node *primitive,
		     struct variable_generator *var_gen,struct rb_tree *visited)
{

//    printf("Bulding primitive Node %s\n",primitive->label);
    //create a new state and add it to the fsm
    
    char id[128];
    char *name=make_capability(primitive->label);

    if(rb_tree_find(visited,name))
    {
	return;
    }
    else
    {
	char *name_copy=malloc(strlen(name)+1);
	strcpy(name_copy,name);
	rb_tree_insert(visited,name_copy);
    }
    
    if(fsm_find_state(fsm,name,name,"1"))
       return;
    int var=variable_generator_new_var(var_gen);
    sprintf(id,"l%d",var);
    
    struct fsm_state *p_state=malloc(sizeof(struct fsm_state));
    init_fsm_state(p_state,name,name,"1");
    fsm_add_state(fsm,p_state);
    
    free(name);
    //create the two transitionsto and from the new state
    char guard[96]="";
    if(node_is_prereq(lag,primitive))
    {
	char *cap=make_capability(primitive->label);
	sprintf(guard,"c_%s",cap);
//	printf("%s is a prerequisite node, crafted guard is: %s\n",primitive->label,guard);
	free(cap);
    }
    else
    {
//	printf("%s is an intermediate node, crafted guard is: %s\n",primitive->label,guard);
    }
    char update[96];
    sprintf(update,"c%d = true",var);

    struct fsm_transition *initial_to_p=malloc(sizeof(struct fsm_transition));
    init_fsm_transition(initial_to_p,fsm->states[0]->id,p_state->id,80,guard,update,NULL);
    fsm_add_trans(fsm,initial_to_p);
    
    struct fsm_transition *p_to_initial=malloc(sizeof(struct fsm_transition));
    init_fsm_transition(p_to_initial,p_state->id,fsm->states[0]->id,100,NULL,NULL,NULL);
    fsm_add_trans(fsm,p_to_initial);

    
    struct fsm_transition *pf_to_initial=malloc(sizeof(struct fsm_transition));
    char *pf_state_id=malloc(strlen(p_state->id)+2);
    sprintf(pf_state_id,"f%s",p_state->id);
    pf_state_id[strlen(p_state->id)+1]='\0';
    init_fsm_transition(pf_to_initial,pf_state_id,fsm->states[0]->id,100,NULL,NULL,NULL);
    fsm_add_trans(fsm,pf_to_initial);
    free(pf_state_id);
    

    
}

void cleanup_guard(char *guard)
{
    for(int i=strlen(guard);i>=0;i--)
    {
	if(guard[i]==' ')
	    return;
	else
	    guard[i]='\0';
    }
}

void build_derived(struct fsm *fsm, struct lag *lag, struct lag_node *derived,
		   struct variable_generator *var_gen,struct rb_tree *visited)
{
    char *name=make_capability(derived->label);

    if(rb_tree_find(visited,name))
    {
	return;
    }
    else
    {
	char *name_copy=malloc(strlen(name)+1);
	strcpy(name_copy,name);
	rb_tree_insert(visited,name_copy);
    }
    
    if(fsm_find_state(fsm,name,name,"1"))
       return;
    //need to keep track of guard.
    char *guard = malloc(1024*1024);
    memset(guard,'\0',1024*1024);
    for(int i=0;i<derived->num_parents;i++)
    {
	struct lag_node *rule=lag->nodes[derived->parent_ids[i]];
	//build_rule(fsm, lag, lag->nodes[]);
	
//	printf("Rule node has %zu parents\n",rule->num_parents);
	for(int j=0;j<rule->num_parents;j++)
	{
	    if(lag->nodes[rule->parent_ids[j]]->type==LAG_OR)
	    {
		build_derived(fsm,lag,lag->nodes[rule->parent_ids[j]],var_gen,visited);
		
	    }
	    else if(lag->nodes[rule->parent_ids[j]]->type==LAG_LEAF)
	    {
		build_primitive(fsm,lag,lag->nodes[rule->parent_ids[j]],var_gen,visited);
	    }

	    //guard fragment for parent
	    //printf("%s\n",guard);
	    char guard_fragment[128];
	    sprintf(guard_fragment," c%d &amp;&amp;",var_gen->var_count-1);
	    strcat(guard,guard_fragment);
	}
    }
    cleanup_guard(guard);

    
    
    //create a new state and add it to the fsm
    
    char id[128];

    
    

    int var=variable_generator_new_var(var_gen);
    sprintf(id,"l%d",var);
    struct fsm_state *d_state=malloc(sizeof(struct fsm_state));
    init_fsm_state(d_state,name,name,"1");
    fsm_add_state(fsm,d_state);
    
    free(name);
    

    char update[96];
    if(node_is_goal(lag,derived))
    {
	char *cap=make_capability(derived->label);
	sprintf(update,"c_%s = true",cap);
	free(cap);
    }
    else
    {
	sprintf(update,"c%d = true",var);
    }
    
//    printf("Bulding derived Node %s with guard:%s and update:%s  \n",derived->label,guard,update);
    //create the two transitionsto and from the new state
    struct fsm_transition *initial_to_d=malloc(sizeof(struct fsm_transition));
    init_fsm_transition(initial_to_d,fsm->states[0]->id,d_state->id,80,guard,update,NULL);
    fsm_add_trans(fsm,initial_to_d);
		    
    struct fsm_transition *ds_to_initial=malloc(sizeof(struct fsm_transition));
    init_fsm_transition(ds_to_initial,d_state->id,fsm->states[0]->id,100,NULL,NULL,NULL);
    fsm_add_trans(fsm,ds_to_initial);

    struct fsm_transition *df_to_initial=malloc(sizeof(struct fsm_transition));
    char *df_state_id=malloc(strlen(d_state->id)+2);
    sprintf(df_state_id,"f%s",d_state->id);
    //df_state_id[strlen(d_state->id)+1]='\0';
    init_fsm_transition(df_to_initial,df_state_id,fsm->states[0]->id,100,NULL,NULL,NULL);
    fsm_add_trans(fsm,df_to_initial);
    free(df_state_id);
    free(guard);
 
}


void init_variable_generator(struct variable_generator *var_gen)
{
    var_gen->var_count=0;
}

int variable_generator_new_var(struct variable_generator *var_gen)
{
    
    int ret=var_gen->var_count;
    var_gen->var_count++;
    return ret;
}

int str_cmp(const void *vstr1, const void *vstr2)
{
    const char *str1=vstr1;
    const char *str2=vstr2;

    return strcmp(str1,str2);
}

void  fill_decls(char **decls, struct rb_node *declaration)
{
    if(declaration->left)
    {
	fill_decls(decls,declaration->left);
    }

    if(declaration->right)
    {
	fill_decls(decls,declaration->right);
    }
    if(declaration->data)
    {
	strcat(*decls,declaration->data);
	free(declaration->data);
    }
}



char *write_declarations(struct lag *lag, struct variable_generator *var_gen)
{
    char *decls=malloc(128*lag->num_nodes*2);
    strcpy(decls,"");
    //ballpark allocate 48 bytes per node
    struct rb_tree declaration_set;
    rb_tree_init(&declaration_set,str_cmp);
    int count=0;
    for(int i=0;i<lag->num_nodes;i++)
    {
	if(lag->nodes[i])
	{
	    char *var_decl=calloc(128,1);
	    if(node_is_goal(lag,lag->nodes[i]))
	    {
		char *cap=make_capability(lag->nodes[i]->label);
		sprintf(var_decl,"bool c_%s=false;\n",cap);
		free(cap);
	    }
	    else if( node_is_prereq(lag,lag->nodes[i]))
	    {
		char *cap=make_capability(lag->nodes[i]->label);
		sprintf(var_decl,"bool c_%s=true;\n",cap);
		free(cap);
	    }
	    //strcat(decls,var_decl);
	    rb_tree_insert(&declaration_set,var_decl);
	    
	}
	count++;
    }

    for(int i=0;i<lag->num_nodes;i++)
    {
	char *var_decl=calloc(128,1);
	sprintf(var_decl,"bool c%d=false;\n",i);
	//strcat(decls,var_decl);
	rb_tree_insert(&declaration_set,var_decl);
    }

    fill_decls(&decls,declaration_set.root);

    rb_tree_destroy(&declaration_set);
    return decls;
}

char **write_queries(struct lag *lag, int *num_queries)
{

    //count the goal nodes;
    int count=0;
    for(int i=0;i<lag->num_nodes;i++)
    {
	if(lag->nodes[i])
	{
	    if(node_is_goal(lag,lag->nodes[i]))
	    {
		count++;
	    }
		
	}
    }

    char **queries=malloc(count*sizeof(char*));
    count=0;
    for(int i=0;i<lag->num_nodes;i++)
    {
	if(lag->nodes[i])
	{
	    if(node_is_goal(lag,lag->nodes[i]))
	    {
		char query[256];
		char *cap=make_capability(lag->nodes[i]->label);
		sprintf(query,"Pr[#&lt;=25](&lt;&gt;fsm.%s)",cap);
		free(cap);
		queries[count]=strdup(query);
		count++;    
	    }
		
	}
    }
    
    *num_queries=count;
    return queries;
}

int str_compare(const void *name1,const void *name2)
{
    const char *name_a=name1;
    const char  *name_b=name2;
    return strcmp(name_a, name_b);
}

    
struct fsm *fsm_from_lag(struct lag *lag, struct variable_generator *var_gen)
{

    //initialize an fsm
    struct fsm *fsm=malloc(sizeof(struct fsm));
    init_fsm(fsm,"FSM","");
    //create initial state
    struct fsm_state *Istate=malloc(sizeof(struct fsm_state));
    init_fsm_state(Istate,"Init","Init","1");
    fsm_add_state(fsm,Istate);
    size_t num_goals;

    struct rb_tree *visited=malloc(sizeof(struct rb_tree));
    rb_tree_init(visited,str_compare);
    
    for(int i=0;i<lag->num_nodes;i++)
    {
	if(lag->nodes[i])
	    if(node_is_goal(lag,lag->nodes[i]))
	    {
		build_derived(fsm,lag,lag->nodes[i],var_gen,visited);
	    }
    }
    
    fsm_set_inital(fsm,Istate->id);
    char *decls=write_declarations(lag,var_gen);
    fsm_set_declarations(fsm,decls);
  
    free(decls);
  

    return fsm;
}
