#include "fsm.h"
#include <stdbool.h>
#include "lag.h"

#ifndef FSM_RESOURCES
#define FSM_RESOURCES

struct lag;

void fsm_to_xml_file(char *xml_file, struct fsm *fsm,
		     char *global_decls, char *system_str,
		     char **queries, size_t num_queries);


struct variable_generator
{
    int var_count;
};

void init_variable_generator(struct variable_generator *var_gen);
int variable_generator_new_var(struct variable_generator *var_gen);

struct fsm *fsm_from_lag(struct lag *lag, struct variable_generator *var_gen);
char **write_queries(struct lag *lag, int *num_queries);
char * make_capability(char *label);
bool node_is_goal(struct lag *lag,struct lag_node *node);
#endif
