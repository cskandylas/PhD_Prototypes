#include "lag.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

void init_lag_node(struct lag_node *node, char *label,int node_id,
		   enum LAG_NODE_TYPE type,double prob)
{
    node->label=malloc(strlen(label)+1);
    strcpy(node->label,label);
    node->node_id=node_id;
    node->type=type;
    node->prob=prob;
    node->num_parents=0;
    node->max_parents=MAX_LAG_NODE_PARENTS_INIT;
    node->parent_ids=malloc(node->max_parents*sizeof(int));
//    printf("Created node with label:[%s]\n",node->label);
}

void lag_node_add_parent(struct lag_node *node,int parent_id)
{
    if(node->num_parents >= node->max_parents)
    {
	node->max_parents*=2;
	node->parent_ids=realloc(node->parent_ids,node->max_parents*sizeof(int));
    }
    node->parent_ids[node->num_parents]=parent_id;
    node->num_parents++;
}


void destroy_lag_node(struct lag_node *node)
{
    free(node->label);
    free(node->parent_ids);
}




void init_lag(struct lag *lag, char *name, enum LAG_ATTACK_FRAGMENT type)
{
    lag->name=malloc(strlen(name)+1);
    strcpy(lag->name,name);
    lag->type=type;
    lag->num_nodes=1;
    lag->max_nodes=MAX_LAG_NODES_INIT;
    lag->nodes=malloc(lag->max_nodes*sizeof(struct lag_node*));
    lag->nodes[0]=NULL;
}

void lag_resize(struct lag *lag, size_t max_nodes)
{
    lag->max_nodes=max_nodes+1;
    lag->nodes=realloc(lag->nodes,lag->max_nodes*sizeof(struct lag_node*));
}


void lag_add_node(struct lag *lag,struct lag_node *node)
{
    if(node->node_id>=lag->max_nodes)
    {
	lag_resize(lag,node->node_id*2);
    }
    lag->nodes[node->node_id]=node;
    lag->num_nodes++;
}

struct lag_node* lag_find_node(struct lag *lag, char *starts_with)
{
    int len=strlen(starts_with);
    for(int i=0;i<lag->num_nodes;i++)
    {
	if(lag->nodes[i])
	{
	    if(strncmp(lag->nodes[i]->label,starts_with,len)==0)
	    {
		return lag->nodes[i];
	    }
	}
    }
    return NULL;
}

struct lag_node* lag_find_nodes(struct lag *lag, char *starts_with, size_t *num_res)
{
   int len=strlen(starts_with);
   int found=0;
    for(int i=0;i<lag->num_nodes;i++)
    {
	if(lag->nodes[i])
	{
	    if(strncmp(lag->nodes[i]->label,starts_with,len)==0)
	    {
		//return lag->nodes[i];
		found++;
	    }
	}
    }
    
    

    
//    printf("Found %d matching nodes\n",found);
    if(found>0)
    {
	int idx=0;
	struct lag_node *res=malloc(found*sizeof(struct lag_node));
	for(int i=0;i<lag->num_nodes;i++)
	{
	    if(lag->nodes[i])
	    {
		if(strncmp(lag->nodes[i]->label,starts_with,len)==0)
		{
		    res[idx++]=*lag->nodes[i];
		}
	    }
	}
	*num_res=found;
	return res;
    }
    return NULL;
    //return res;
}

int* lag_get_parents(struct lag *lag, struct lag_node *child,size_t *num_parents)
{
    int *parents=NULL;
    int node_idx=child->node_id;
    
    if(lag->nodes[node_idx])
    {
//	printf("Node in question is:%d,%s,%lu\n",child->node_id,child->label,lag->nodes[node_idx]->num_parents);
	parents=malloc(lag->nodes[node_idx]->num_parents*sizeof(int));
	for(int i=0;i<lag->nodes[node_idx]->num_parents;i++)
	{
	    parents[i]=lag->nodes[node_idx]->parent_ids[i];
	    
	}
	*num_parents=lag->nodes[node_idx]->num_parents;
    }
    
    return parents;
}

void destroy_lag(struct lag *lag)
{
    free(lag->name);
    for(int i=0;i<lag->num_nodes;i++)
    {
	if(lag->nodes[i])
	{
	    destroy_lag_node(lag->nodes[i]);
	    free(lag->nodes[i]);
	}
    }
    free(lag->nodes);
}

void lag_pretty_print(struct lag *lag)
{
    printf("Logical Attack Graph %s\n",lag->name);
    for(int i=0;i<lag->num_nodes;i++)
    {
	struct lag_node *node=lag->nodes[i];
	if(node)
	{
	    printf("Node %d:%s (%d) [",node->node_id,node->label,node->type);
	    for(int j=0;j<node->num_parents;j++)
	    {
		printf("%d",node->parent_ids[j]);
		if(j<node->num_parents-1)
		    printf(",");
	    }
	    printf("]\n");
	}
    }
}
