#include <stddef.h>


#ifndef LAG_TYPE
#define LAG_TYPE

#define MAX_LAG_NODE_PARENTS_INIT 2

enum LAG_NODE_TYPE {LAG_LEAF=0,LAG_OR,LAG_AND};

struct lag_node
{
    char *label;
    enum LAG_NODE_TYPE type;
    int node_id;
    int *parent_ids;
    size_t num_parents;
    size_t max_parents;
    double prob;
    
};

void init_lag_node(struct lag_node *node, char *label,int node_id,
		   enum LAG_NODE_TYPE type,double prob);
void lag_node_add_parent(struct lag_node *node,int parent_id);
void destroy_lag_node(struct lag_node *node);



enum LAG_ATTACK_FRAGMENT
{
    LAG_COMPONENT_INTERACTION=0,
    LAG_SINGLE_EXPLOIT,
    LAG_MULTI_EXPLOIT,
    LAG_COMPONENT_IMPACT,
    LAG_MAGIC
};

#define MAX_LAG_NODES_INIT 32

struct lag
{
    char *name;
    enum LAG_ATTACK_FRAGMENT type;
    struct lag_node **nodes;
    size_t num_nodes;
    size_t max_nodes;
};

void init_lag(struct lag *lag, char *name, enum LAG_ATTACK_FRAGMENT type);
void lag_resize(struct lag *lag, size_t max_nodes);
void lag_add_node(struct lag *lag,struct lag_node *node);
int* lag_get_parents(struct lag *lag, struct lag_node *child,size_t *num_parents);
struct lag_node* lag_find_node(struct lag *lag, char *starts_with);
struct lag_node* lag_find_nodes(struct lag *lag, char *starts_with, size_t *num_res);
void destroy_lag(struct lag *lag);
void lag_pretty_print(struct lag *lag);

#endif
