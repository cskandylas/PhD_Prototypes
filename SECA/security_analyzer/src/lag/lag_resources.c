#include "lag_resources.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tokenize.h"


struct lag* lag_from_directory(char *directory, char *name, enum LAG_ATTACK_FRAGMENT lag_type)
{

    struct lag *lag=malloc(sizeof(struct lag));
    init_lag(lag,name,lag_type);
    char *verts_filename=malloc(strlen(directory)+strlen("/VERTICES.CSV")+1);
    sprintf(verts_filename,"%s/VERTICES.CSV",directory);
//    printf("Trying to open:%s\n",verts_filename);
    FILE *fp=fopen(verts_filename,"r");
    
    if(!fp)
    {
	fprintf(stderr,"Could not find file %s\n",verts_filename);
	return NULL;
    }
    fseek(fp, 0L, SEEK_END);
    long numbytes = ftell(fp);
    fseek(fp, 0, SEEK_SET);	
    char *text=malloc(numbytes+1);

    if(text == NULL)
    {
	fprintf(stderr,"Error in openning file:%s\n",verts_filename);
	return NULL;
    }
    size_t num_read;
    num_read=fread(text, sizeof(char), numbytes, fp);
    fclose(fp);
    text[numbytes]='\0';

    struct tokenizer line_tok;
    tokenizer_init(&line_tok,text,"\n");
    char *line;
    size_t n_lines=0;
    while((line=next_token(&line_tok))!=NULL)
    {
//	printf("Line %lu:%s\n",n_lines,line);
	n_lines++;
	free(line);
    }
//    printf("Found %lu lines in %s\n",n_lines,verts_filename);
    free(verts_filename);
    
    tokenizer_init(&line_tok,text,"\n");
    for(int i=0;i<n_lines;i++)
    {

	char *line=next_token(&line_tok);
	struct tokenizer quote_tok;
	tokenizer_init(&quote_tok,line,"\"");
	char *id=next_token(&quote_tok);
//	printf("node id:%s\n",id );
	char *label=next_token(&quote_tok);
//	printf("node label:%s\n",label);

	//skips over ","
	char *skip=next_token(&quote_tok);
	free(skip);
	char *type=next_token(&quote_tok);
//	printf("node_type:%s\n",type);
	char *prob=next_token(&quote_tok);
//	printf("node_probability:%s\n",prob);
	enum LAG_NODE_TYPE node_type;
	if(strcmp(type,"LEAF")==0)
	{
	    node_type=LAG_LEAF;
	}
	else if(strcmp(type,"OR")==0)
	{
	    node_type=LAG_OR;
	}
	else if(strcmp(type,"AND")==0)
	{
	    node_type=LAG_AND;
	}
	else
	{
	    fprintf(stderr,"Incorrect node type %s\n",type);
	}

	char *skip_comma=&prob[1];

	struct lag_node *node=malloc(sizeof(struct lag_node));
	init_lag_node(node,label,atoi(id),node_type,atof(skip_comma));
	lag_add_node(lag,node);
	free(id);
	free(label);
	free(prob);
	free(type);
	free(line);
    }
    free(text);
    
    char *arcs_filename=malloc(strlen(directory)+strlen("/ARCS.CSV")+1);
    sprintf(arcs_filename,"%s/ARCS.CSV",directory);

    fp=fopen(arcs_filename,"r");
    
    if(!fp)
    {
	fprintf(stderr,"Could not find file %s\n",arcs_filename);
	exit(-1);
    }
    fseek(fp, 0L, SEEK_END);
    numbytes = ftell(fp);
    fseek(fp, 0, SEEK_SET);	
    text=malloc(numbytes+1);

    if(text == NULL)
    {
	fprintf(stderr,"Error in openning file:%s\n",arcs_filename);
	return NULL;
    }
    num_read=fread(text, sizeof(char), numbytes, fp);
    fclose(fp);
    text[numbytes]='\0';

    
    tokenizer_init(&line_tok,text,"\n");
    n_lines=0;
    while((line=next_token(&line_tok))!=NULL)
    {
//	printf("Line %lu:%s\n",n_lines,line);
	n_lines++;
	free(line);
    }
//    printf("Found %lu lines in %s\n",n_lines,arcs_filename);
    free(arcs_filename);
    
    tokenizer_init(&line_tok,text,"\n");
    for(int i=0;i<n_lines;i++)
    {

	char *line=next_token(&line_tok);
	struct tokenizer comma_tok;
	tokenizer_init(&comma_tok,line,",");
	char *child=next_token(&comma_tok);
	char *parent=next_token(&comma_tok);
//	printf("%s's parent is %s\n",child,parent);
	int child_id=atoi(child);
	int parent_id=atoi(parent);

	lag_node_add_parent(lag->nodes[child_id],parent_id);
	free(child);
	free(parent);
	free(line);
    }
    
    
    free(text);
    return lag;
}
