#include "attack_fragment.h"
#include "arc_component.h"
#include "sec_clause.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void init_attack_fragment(struct attack_fragment *frag, enum attack_fragment_type type)
{
    //On init we only know desc+phase huh?
    frag->type=type;
    frag->comp=NULL;
    
    frag->num_preds=0;
    frag->max_preds=ATTACK_FRAGMENT_INIT_PREDS;
    frag->preds=malloc(frag->max_preds*sizeof(struct sec_predicate));
    frag->lag=NULL;
    frag->fsm=NULL;
    frag->ver_props=NULL;
    frag->num_ver_props=0;
    frag->goal=NULL;
    frag->marked=false;
    //set this to SIZE_T MAX
    frag->id=-1;
}

void destroy_attack_fragment(struct attack_fragment *frag)
{
    if(frag->preds)
    {
	for(int i=0;i<frag->num_preds;i++)
	{
	    destroy_predicate(&frag->preds[i]);
	}
	
    }
    free(frag->preds);
    
    if(frag->lag)
    {
	destroy_lag(frag->lag);
	free(frag->lag);
    }
    if(frag->fsm)
    {
	
	destroy_fsm(frag->fsm);
	free(frag->fsm);
    }
    if(frag->ver_props)
    {
	
	for(int i=0;i<frag->num_ver_props;i++)
	{
	    //free(frag->ver_props[i].property);
	}
	
	free(frag->ver_props);
    }

    if(frag->goal)
    {
	free(frag->goal);
    }
}


void add_predicate(struct attack_fragment *frag, struct sec_predicate *pred)
{
    
    if(frag->num_preds >= frag->max_preds)
    {
	struct sec_predicate *tmp_preds=realloc(frag->preds,frag->max_preds*2*sizeof(struct sec_predicate));
	//frag->preds=realloc(frag->preds,sizeof(struct sec_predicate)*frag->max_preds*2);
	if(tmp_preds)
	{	    
	    free(frag->preds);
	    frag->preds=tmp_preds;
	    frag->max_preds*=2;
	}
	
    }
    
    copy_predicate(&frag->preds[frag->num_preds],pred);
    frag->num_preds++;
}


void describe_fragment(struct attack_fragment *frag)
{
    switch (frag->type)
    {
    case  COMPONENT_INTERACTION:
	printf("Component Interaction Fragment\n");
	break;
    case    SINGLE_EXPLOIT:
	printf("Single_Exploit_%s_%s\n",frag->comp->comp_name,frag->goal);
	break;
    case    MULTI_EXPLOIT:
	printf("Multi_Exploit_%s_%s\n",frag->comp->comp_name,frag->goal);
	break;
    case    COMPONENT_IMPACT:
	printf("Component_Impact_%s_%s\n",frag->comp->comp_name,frag->goal);
	break;
    case    SYSTEM_SECURITY:
	printf("System Security Fragment\n");
	break;
    }
//    printf("Num Predicates: %zu\n",frag->num_preds);
}


char *fragment_to_str(struct attack_fragment *frag)
{
    size_t max_len=strlen("Component Interaction Fragment")
	+strlen(frag->comp->comp_name)+strlen(frag->goal)+1;
    char *frag_str=malloc(max_len);
    switch (frag->type)
    {
    case  COMPONENT_INTERACTION:
	sprintf(frag_str,"Component Interaction Fragment");
	break;
    case    SINGLE_EXPLOIT:
	sprintf(frag_str,"singleExploit_%s_%s",frag->comp->comp_name,frag->goal);
	break;
    case    MULTI_EXPLOIT:
	sprintf(frag_str,"multiExploit_%s_%s",frag->comp->comp_name,frag->goal);
	break;
    case    COMPONENT_IMPACT:
	sprintf(frag_str,"componentImpact_%s_%s",frag->comp->comp_name,frag->goal);
	break;
    case    SYSTEM_SECURITY:
	sprintf(frag_str,"System Security Fragment");
	break;
    }
    return frag_str;
}

void print_fragment_stats(struct attack_fragment *frag)
{
    if(frag->num_ver_props>0)
    {
	char *name=fragment_to_str(frag);
	printf("Name:%s Goal:%s Preds:%p Result:%s\n",name, frag->goal, frag->preds,frag->ver_props[0].result);
	free(name);
    }
    else
    {
	char *name=fragment_to_str(frag);
	printf("Name:%s\n",name);
	free(name);
    }
}


void print_fragment_marking(struct attack_fragment *frag)
{
    char *name=fragment_to_str(frag);
    if(frag->marked)
	printf("Name:%s Marked\n",name);
    else
	printf("Name:%s Unmarked\n",name);
    free(name);
}

enum attack_fragment_type str_to_fragment_type(char *str)
{
    if(strncmp(str,"singleExploit",strlen("singleExploit"))==0)
    {
	return SINGLE_EXPLOIT;
    }
    else if(strncmp(str,"multiExploit",strlen("multiExploit"))==0)
    {
	return MULTI_EXPLOIT;
    }
    else if(strncmp(str,"componentImpact",strlen("componentImpact"))==0)
    {
	return COMPONENT_IMPACT;
    }
    else
    {
	return COMPONENT_INTERACTION;
    }
}
