#include "sec_clause.h"
#include "lag.h"
#include "fsm.h"

#include <stdbool.h>


#ifndef SEC_ARTIFACT
#define SEC_ARTIFACT

enum attack_fragment_type
{
    COMPONENT_INTERACTION = 0,
    SINGLE_EXPLOIT,
    MULTI_EXPLOIT,
    COMPONENT_IMPACT,
    SYSTEM_SECURITY
};
	      

//how do I identify? pass what?
/*
      Maybe:
      description   : phase
      ic_comp_iface : PHASE_COMPROMISED
      ba_comp_iface : PHASE_BASIC_ATTACK
      ca_comp       : PHASE_COMPLEX_ATTACK
      ci_comp       : PHASE_COMPONENT_IMPACT
*/

#define ATTACK_FRAGMENT_INIT_PREDS 8

struct verification_property
{
    char *goal;
    char *property;
    char *result;
};


/*
  For SINGLE_EXPLOIT the goal is the vulnerability this fragment captures
  For MULTI_EXPLOIT the goal is ??
  For COMPONENT_IMPACT the goal is ??
 */

struct attack_fragment
{
    enum attack_fragment_type		 type;
    //associated component or NULL
    struct arc_component		*comp;
    struct sec_predicate		*preds;
    size_t				 num_preds;
    size_t				 max_preds;
    struct lag				*lag;
    struct fsm				*fsm;
    struct verification_property	*ver_props;
    size_t				 num_ver_props;
    bool				 marked;
    //This varies per fragment type...
    char                                *goal;
    //used to speed up dep_graph search 
    size_t                              id;
};

void init_attack_fragment(struct attack_fragment *frag, enum attack_fragment_type type);
void destroy_attack_fragment(struct attack_fragment *frag);

void add_predicate(struct attack_fragment *frag, struct sec_predicate *pred);
void describe_fragment(struct attack_fragment *frag);
char *fragment_to_str(struct attack_fragment *frag);
enum attack_fragment_type str_to_fragment_type(char *str);

// #DEBUG
void print_fragment_stats(struct attack_fragment *frag);
void print_fragment_marking(struct attack_fragment *frag);

#endif
