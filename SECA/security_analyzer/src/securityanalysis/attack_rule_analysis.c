#define _GNU_SOURCE
#include "attack_rule_analysis.h"
#include "fol_resources.h"
#include "named_resource.h"
#include "lag.h"

#include <string.h>
#include <stdio.h>


void attack_analysis_info_init(struct attack_analysis_info *aai, char *a_name)
{
    aai->attack_name=strdup(a_name);
    aai->dep_types=DEP_NONE;
    aai->required_vulns=NULL;
}

void attack_analysis_info_destroy(struct attack_analysis_info *aai)
{
    free(aai->attack_name);
    if(aai->required_vulns)
    {
	struct queue_entry *entry=aai->required_vulns->head;
	while(entry)
	{
	    free(entry->elem);
	    entry=entry->next;
	}
	
        queue_destroy(aai->required_vulns);
    }
}


struct attack_analysis_info *attack_rule_analysis(size_t *num_attacks)
{
    //load multi-exploit rules
    size_t num_clauses=0;
    struct sec_clause *clauses=clauses_from_file("Rules/multi_exploit_rules.P",&num_clauses);

    struct attack_analysis_info *attack_dependency_info=malloc(num_clauses*sizeof(struct attack_analysis_info));
    
    for(int i=0;i<num_clauses;i++)
    {


	
	struct sec_clause *clause=&clauses[i];
	struct sec_predicate *clause_head=clause->head;
	attack_analysis_info_init(&attack_dependency_info[i],clause_head->terms[clause->head->num_terms-1]);
	
	
	struct generic_queue *vulns_queue=NULL;
	
	for(int j=0;j<clause->tail_len;j++)
	{
	    
	    struct sec_predicate *pred=clause->tail[j];
	    //if predicate name is single exploit, add the vulnerability to the req_vulns hash_table
	    if(strcmp(pred->name,"singleExploit")==0)
	    {
		if(!vulns_queue)
		{
		    vulns_queue=malloc(sizeof(struct generic_queue));
		    queue_init(vulns_queue);
		}
		
		//printf("%s ",pred->terms[pred->num_terms-1]);
		//printf("MultiExploit(%s) depends on SingleExploit(%s)\n",clause->head->terms[1],pred->terms[pred->num_terms-1]);
		queue_push_back(vulns_queue,strdup(pred->terms[pred->num_terms-1]));

		if(attack_dependency_info[i].dep_types==DEP_NONE)
		{
		    attack_dependency_info[i].dep_types=DEP_SINGLE_EXPLOIT;
		}
		else if(attack_dependency_info[i].dep_types==DEP_MULTI_EXPLOIT)
		{
		    attack_dependency_info[i].dep_types=DEP_BOTH;
		}
		
		
	    }
	    else if(strcmp(pred->name,"multiExploit")==0)
	    {
		//TODO: ADD THE RECURSION TO FIND VULNS HERE
		//printf("MultiExploit(%s) depends on MultiExploit(%s)\n",clause->head->terms[1],pred->terms[1]);
		for(int k=0;k<i;k++)
		{
		    if(strcmp(attack_dependency_info[k].attack_name,pred->terms[1])==0)
		    {
			//printf("%s has already been computed\n",pred->terms[1]);
			if(!vulns_queue)
			{
			    vulns_queue=malloc(sizeof(struct generic_queue));
			    queue_init(vulns_queue);
			}
			struct queue_entry *entry=attack_dependency_info[k].required_vulns->head;
			while(entry)
			{
			    //printf("MultiExploit(%s) depends on SingleExploit(%s)\n",clause->head->terms[1],(char*)entry->elem);
			    queue_push_back(vulns_queue,entry->elem);
			    
			    entry=entry->next;
			}


			    break;
		    }
		}
		
		if(attack_dependency_info[i].dep_types==DEP_NONE)
		{
		    attack_dependency_info[i].dep_types=DEP_MULTI_EXPLOIT;
		}
		else if(attack_dependency_info[i].dep_types==DEP_SINGLE_EXPLOIT)
		{
		    attack_dependency_info[i].dep_types=DEP_BOTH;
		}
	    }
	}
	
	if(vulns_queue)
	{
	    attack_dependency_info[i].required_vulns=vulns_queue;
	    /*hash_table_insert(req_vulns,clause->head->terms[clause->head->num_terms-1],
			      strlen(clause->head->terms[clause->head->num_terms-1]),
			      vulns_queue,sizeof(struct generic_queue));
	    */
	}
	
			  
    }

    for(int i=0;i<num_clauses;i++)
    {
	destroy_predicate(clauses[i].head);
	free(clauses[i].head);
	
	for(int j=0;j<clauses[i].tail_len;j++)
	{
	    destroy_predicate(clauses[i].tail[j]);
	    free(clauses[i].tail[j]);
	}
	
	destroy_clause(&clauses[i]);
    }
    free(clauses);
    
    if(num_attacks)
    {
	*num_attacks=num_clauses;
    }
    
    return attack_dependency_info;
}

