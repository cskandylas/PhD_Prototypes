#include "Queue.h"

#ifndef ATTACK_RULE_ANALYSIS_H
#define ATTACK_RULE_ANALYSIS_H


enum dependency_types {DEP_NONE=0, DEP_SINGLE_EXPLOIT,DEP_MULTI_EXPLOIT,DEP_BOTH};


struct attack_analysis_info
{
    //actual attack name
    char *attack_name;
    enum dependency_types dep_types;
    //queue of vulns that can lead to this attack
    struct generic_queue *required_vulns;    
};

void attack_analysis_info_init(struct attack_analysis_info *aai, char *a_name);
void attack_analysis_info_destroy(struct attack_analysis_info *aai);
    
struct attack_analysis_info *attack_rule_analysis(size_t *num_attacks);


#endif
