#include "dep_graph.h"
#include "attack_fragment.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void init_dep_node(struct dep_node *d_node, size_t max_dependencies)
{
    if(max_dependencies>0)
	d_node->dependencies=malloc(max_dependencies*sizeof(struct dep_node*));
    else
	d_node->dependencies=NULL;

    
    d_node->max_dependencies=max_dependencies;
    for(int i=0;i<d_node->max_dependencies;i++)
    {
	d_node->dependencies[i]=NULL;
    }
    d_node->num_dependencies=0;
    d_node->pid=-1;
    d_node->frag=NULL;
    d_node->done=false;
    d_node->queued=false;
}

void dep_node_add_dep(struct dep_node *d_node, struct dep_node *dep)
{
    if(d_node->num_dependencies >= d_node->max_dependencies)
    {
	if(d_node->max_dependencies==0)
	    d_node->max_dependencies=1;
	d_node->max_dependencies*=2;
	d_node->dependencies=realloc(d_node->dependencies,d_node->max_dependencies*sizeof(struct dep_node*));
    }
    d_node->dependencies[d_node->num_dependencies]=dep;
    d_node->num_dependencies++;   
}

bool dep_node_blocked(struct dep_node *d_node)
{
    for(int i=0;i<d_node->num_dependencies;i++)
    {
	if(!d_node->dependencies[i]->done )
	{
	    return true;
	}
    }
    return false;
}

void destroy_dep_node(struct dep_node *d_node)
{
    free(d_node->dependencies);
}


void init_dep_graph(struct dep_graph *d_graph, size_t max_nodes)
{
    d_graph->max_nodes=max_nodes;
    d_graph->nodes=malloc(d_graph->max_nodes*sizeof(struct dep_node*));
    if(!d_graph->nodes)
    {
	fprintf(stderr,"Malloc failed?!\n. Exiting...");
	exit(-1);
    }
    d_graph->num_nodes=0;
    for(int i=0;i<max_nodes;i++)
    {
	d_graph->nodes[i]=NULL;
    }
}


void add_node_to_dep_graph(struct dep_graph *d_graph, size_t num_dependencies, struct attack_fragment *frag)
{
    
    struct dep_node *dep_node = malloc(sizeof(struct dep_node));
    init_dep_node(dep_node,num_dependencies);
    dep_node->frag=frag;
    if(!frag->marked)
	dep_node->done=true;
    else
	dep_node->done=false;
    if(d_graph->num_nodes>=d_graph->max_nodes)
    {
	d_graph->max_nodes*=2;
	d_graph->nodes=realloc(d_graph->nodes,d_graph->max_nodes*sizeof(struct dep_node*));
    }
    d_graph->nodes[d_graph->num_nodes]=dep_node;
    dep_node->frag->id=d_graph->num_nodes;
    d_graph->num_nodes++;
}

bool dep_graph_all_done(struct dep_graph *d_graph)
{
    for(int i=0;i<d_graph->num_nodes;i++)
    {
	if(d_graph->nodes[i]->done==false)
	{
	    return false;
	}
	
    }
    return true;
}

int dep_graph_num_done(struct dep_graph *d_graph)
{
    int count=0;
    for(int i=0;i<d_graph->num_nodes;i++)
    {
	if(d_graph->nodes[i]->done==true )
	{
	    printf("[%d] done!\n",i);
	    count++;
	}
    }
    return count;
}

int next_free(struct dep_graph *d_graph)
{
    for(int i=0;i<d_graph->num_nodes;i++)
    {
	//if(d_graph->nodes[i]->frag->marked)
	if(!d_graph->nodes[i]->done && !d_graph->nodes[i]->queued && !dep_node_blocked(d_graph->nodes[i]))
	{
	    return i;
	}
    }

    return -1;
}

void destroy_dep_graph(struct dep_graph *d_graph)
{
    for(int i=0;i<d_graph->num_nodes;i++)
    {
	destroy_dep_node(d_graph->nodes[i]);
    }
    free(d_graph->nodes);
}

void clear_dep_graph(struct dep_graph *d_graph)
{
    for(int i=0;i<d_graph->num_nodes;i++)
    {
	d_graph->nodes[i]->done=false;
	d_graph->nodes[i]->queued=false;
    }
}

void print_dep_graph_stats(struct dep_graph *d_graph)
{
    printf("%zu,%zu\n",d_graph->num_nodes,d_graph->max_nodes);
  for(int i=0;i<d_graph->num_nodes;i++)
  {
      struct dep_node *d_node=d_graph->nodes[i];
      printf("[%s,%zu,%d,%d,%d,%zu]\n",fragment_to_str(d_node->frag),
	     d_node->frag->id,d_node->frag->marked,d_node->done,d_node->queued,d_node->num_dependencies);
  }

}


/*VERY SLOW AVOID CALLING*/
struct dep_node *find_dep_node(struct dep_graph *d_graph, char *component_name,char *goal,enum attack_fragment_type f_t)
{
    for(int i=0;i<d_graph->num_nodes;i++)
    {
	if(strcmp(d_graph->nodes[i]->frag->comp->comp_name,component_name)==0
 	   && strcmp(d_graph->nodes[i]->frag->goal,goal)==0 
	   && d_graph->nodes[i]->frag->type==f_t)
	{
	    return d_graph->nodes[i];
	}
    }
    return NULL;
}

void mark_dfs(struct dep_node *node, bool skip)
{
    if(node)
    {
	//printf("Marking %s\n",fragment_to_str(node->frag));
	//if(!node->frag->done)
	//if(!node->frag->ver_props || !node->frag->ver_props->result)

	if(node->frag->ver_props && node->frag->ver_props->result && skip)
	{
	    //printf("Maybe skip: %s:%s\n",node->frag->comp->comp_name,node->frag->ver_props->result);
	    //return;
	}
	else
	{
	    //printf("Never skip: %s\n",node->frag->comp->comp_name);
	}
	
	{
	    //printf("Marking %s which has:%zu dependencies\n",fragment_to_str(node->frag),node->num_dependencies);
	    node->frag->marked=true;
	    for(int i=0;i<node->num_dependencies;i++)
	    {
		
		struct dep_node *dep_node = node->dependencies[i];
		mark_dfs(dep_node,skip);
	    }
	}
    }
}


void marked_graph_to_dot(struct dep_graph *dep_graph, char *dot_fname,bool pruned)
{

    FILE* dot_fp=fopen(dot_fname,"w");
    fprintf(dot_fp,"digraph DepGraph {\n");

    for(int i=0;i<dep_graph->num_nodes;i++)
    {
	struct dep_node *node=dep_graph->nodes[i];
	if(node) 
	{
	    if((pruned && node->frag->marked && node->frag->preds) || !pruned)
	    {
		for(int j=0;j<node->num_dependencies;j++)
		{
		    struct dep_node *dep_node=node->dependencies[j];
		    fprintf(dot_fp,"%zu -> %zu;\n",node->frag->id,node->dependencies[j]->frag->id);
		    fprintf(dot_fp,"%zu [label=\"%s\"];\n",node->frag->id,fragment_to_str(node->frag));
		    fprintf(dot_fp,"%zu [label=\"%s\"];\n",dep_node->frag->id,fragment_to_str(dep_node->frag));
		}
	    }
	}
    }
    
    fprintf(dot_fp,"}");
    fclose(dot_fp);
}



