#include <sys/types.h>
#include <stdbool.h>
#include <stddef.h>
#include "attack_fragment.h"
#include "arc_component.h"



#ifndef DEP_GRAPH_H
#define DEP_GRAPH_H

struct dep_graph
{
    struct dep_node **nodes;
    size_t num_nodes;
    size_t max_nodes;
};


struct dep_node
{
    bool done;
    bool queued;
    struct attack_fragment *frag;
    pid_t pid;
    //can be calculated thus this needs not be a linked-list.
    struct dep_node **dependencies;
    size_t num_dependencies;
    size_t max_dependencies;
};


void init_dep_node(struct dep_node *d_node,size_t max_dependencies);
void dep_node_add_dep(struct dep_node *d_node, struct dep_node *dep);
bool dep_node_blocked(struct dep_node *d_node);
void destroy_dep_node(struct dep_node *d_node);

void init_dep_graph(struct dep_graph *d_graph, size_t max_nodes);
void add_node_to_dep_graph(struct dep_graph *d_graph, size_t num_dependencies, struct attack_fragment *frag);
int next_free(struct dep_graph *d_graph);
bool dep_graph_all_done(struct dep_graph *d_graph);
int dep_graph_num_done(struct dep_graph *d_graph);
void destroy_dep_graph(struct dep_graph *d_graph);
void clear_dep_graph(struct dep_graph *d_graph);
void print_dep_graph_stats(struct dep_graph *d_graph);

struct dep_node *find_dep_node(struct dep_graph *d_graph, char *component_name,char *goal,enum attack_fragment_type f_t);
void mark_dfs(struct dep_node *node, bool skip);
void marked_graph_to_dot(struct dep_graph *dep_graph, char *dot_fname,bool pruned);


#endif
