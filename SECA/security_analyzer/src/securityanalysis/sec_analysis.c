#define _XOPEN_SOURCE 501

#include "sec_analysis.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>


#include "arc_component.h"
#include "fol_resources.h"
#include "named_resource.h"
#include "lag.h"

#include "sec_graph_gen.h"
#include "sec_automata_gen.h"
#include "rbtree.h"
#include "hashtable.h"
#include "Queue.h"


void init_component_ctx(struct sec_component_ctx *comp_ctx, struct arc_component *comp)
{
    //init component context members
    comp_ctx->component=comp;
    comp_ctx->max_se_fragments=SEC_ANALYSIS_INIT_MAX_FRAGMENTS;
    comp_ctx->max_me_fragments=SEC_ANALYSIS_INIT_MAX_FRAGMENTS;
    comp_ctx->max_ci_fragments=SEC_ANALYSIS_INIT_MAX_FRAGMENTS;
    comp_ctx->num_se_fragments=0;
    comp_ctx->num_me_fragments=0;
    comp_ctx->num_ci_fragments=0;
    comp_ctx->se_fragments=malloc(comp_ctx->max_se_fragments*sizeof(struct attack_fragment*));
    comp_ctx->me_fragments=malloc(comp_ctx->max_me_fragments*sizeof(struct attack_fragment*));
    comp_ctx->ci_fragments=malloc(comp_ctx->max_ci_fragments*sizeof(struct attack_fragment*));
}

void destroy_component_ctx(struct sec_component_ctx *comp_ctx)
{
    for(int i=0;i<comp_ctx->num_se_fragments;i++)
    {
	destroy_attack_fragment(comp_ctx->se_fragments[i]);
    }
    free(comp_ctx->se_fragments);

    for(int i=0;i<comp_ctx->num_me_fragments;i++)
    {
	destroy_attack_fragment(comp_ctx->me_fragments[i]);
    }
    free(comp_ctx->me_fragments);
    
    for(int i=0;i<comp_ctx->num_ci_fragments;i++)
    {
	destroy_attack_fragment(comp_ctx->ci_fragments[i]);
    }
    free(comp_ctx->ci_fragments);
}

void component_ctx_add_fragment(struct sec_component_ctx *comp_ctx,
				struct attack_fragment *frag)
{
    //printf("Adding %s to %s\n",fragment_to_str(frag),comp_ctx->component->comp_name);
    if(frag->type==SINGLE_EXPLOIT)
    {
    
	if(comp_ctx->num_se_fragments>=comp_ctx->max_se_fragments)
	{
	    comp_ctx->max_se_fragments*=2;
	    comp_ctx->se_fragments=realloc(comp_ctx->se_fragments,
					   comp_ctx->max_se_fragments*sizeof(struct attack_fragment*));
	}
	comp_ctx->se_fragments[comp_ctx->num_se_fragments]=frag;
	comp_ctx->num_se_fragments++;
//	printf("Num Fragments: %zu\n",comp_ctx->num_se_fragments);
    }
    else if(frag->type==MULTI_EXPLOIT)
    {
	
	
	if(comp_ctx->num_me_fragments>=comp_ctx->max_me_fragments)
	{
	    comp_ctx->max_me_fragments*=2;
	    comp_ctx->me_fragments=realloc(comp_ctx->me_fragments,
					   comp_ctx->max_me_fragments*sizeof(struct attack_fragment*));
	}
	comp_ctx->me_fragments[comp_ctx->num_me_fragments]=frag;
	comp_ctx->num_me_fragments++;
    }

    else if(frag->type==COMPONENT_IMPACT)
    {
    
	if(comp_ctx->num_ci_fragments>=comp_ctx->max_ci_fragments)
	{
	    comp_ctx->max_ci_fragments*=2;
	    comp_ctx->ci_fragments=realloc(comp_ctx->ci_fragments,
					   comp_ctx->max_ci_fragments*sizeof(struct attack_fragment*));
	}
	comp_ctx->ci_fragments[comp_ctx->num_ci_fragments]=frag;
	comp_ctx->num_ci_fragments++;
    }
    else
    {
	fprintf(stderr,"Invalid fragment type:%d\n",frag->type);
    }
}


static void estimate_component_impact_fragments(struct sec_analysis_ctx *sec_ctx)
{
    //add confidentiality and integrity always for now
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	struct attack_fragment *conf_frag=malloc(sizeof(struct attack_fragment));
	init_attack_fragment(conf_frag,COMPONENT_IMPACT);
	conf_frag->comp=sec_ctx->component_contexts[i].component;
	conf_frag->goal=strdup("confidentiality");
	component_ctx_add_fragment(&sec_ctx->component_contexts[i],conf_frag);
	

	struct attack_fragment *int_frag=malloc(sizeof(struct attack_fragment));
	init_attack_fragment(int_frag,COMPONENT_IMPACT);
	int_frag->comp=sec_ctx->component_contexts[i].component;
	int_frag->goal=strdup("integrity");
	component_ctx_add_fragment(&sec_ctx->component_contexts[i],int_frag);

	sec_ctx->num_fragments+=2;

    }
}

static void estimate_multi_exploit_fragments(struct sec_analysis_ctx *sec_ctx)
{
    struct attack_analysis_info *attack_info=sec_ctx->attack_rule_info;
    size_t num_attacks=sec_ctx->num_attack_rules;
    for(int k=0;k<sec_ctx->num_component_ctx;k++)
    {

	struct arc_component *comp=sec_ctx->component_contexts[k].component;
	
	for(int i=0;i<num_attacks;i++)
	{

	    
	    
	    if(attack_info[i].required_vulns)
	    {
		char *vuln=attack_info[i].required_vulns->head->elem;
		struct vulnerability_info v_temp;
		v_temp.vuln_name=vuln;
		struct vulnerability_info *v_found=rb_tree_find(sec_ctx->vuln_analysis_tree,&v_temp);
		if(v_found)
		{
		    int found=queue_find(v_found->vuln_components,comp->comp_name,str_cmp);
		    if(found==0)
		    {
			//create the fragment
			struct attack_fragment *frag=malloc(sizeof(struct attack_fragment));
			init_attack_fragment(frag,MULTI_EXPLOIT);
			frag->comp=comp;
			frag->goal=strdup(attack_info[i].attack_name);
			component_ctx_add_fragment(&sec_ctx->component_contexts[k],frag);
			sec_ctx->num_fragments++;
			
		    }
		}
	    }
	}
    }
}


static void estimate_single_exploit_for_vuln(struct sec_analysis_ctx *sec_ctx,
					     struct rb_node *node)
{
    if(!node)
	return;
    
    if(node->left)
    {
	estimate_single_exploit_for_vuln(sec_ctx,node->left);
    }

    if(node->right)
    {
	estimate_single_exploit_for_vuln(sec_ctx,node->right);
    }

    
    
    
    //for this node walk the
    struct vulnerability_info *vuln_info=node->data;
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	char *comp_name=sec_ctx->component_contexts[i].component->comp_name;
	if(queue_find(vuln_info->vuln_components,comp_name,str_cmp)==0)
	{
	    
	    struct attack_fragment *frag=malloc(sizeof(struct attack_fragment));
	    init_attack_fragment(frag,SINGLE_EXPLOIT);
	    frag->comp=sec_ctx->component_contexts[i].component;
	    frag->goal=strdup(vuln_info->vuln_name);
	    component_ctx_add_fragment(&sec_ctx->component_contexts[i],frag);
	    sec_ctx->num_fragments++;
	    
	}
    }
    
}

static void estimate_single_exploit_fragments(struct sec_analysis_ctx *sec_ctx)
{
    estimate_single_exploit_for_vuln(sec_ctx,sec_ctx->vuln_analysis_tree->root);
}


void analyse_vulnerabilities(struct sec_analysis_ctx *sec_ctx)
{
    //printf("Analyzing system vunlerabilities\n");
    struct sec_predicate *vuln_preds=knowledge_base_get_res(sec_ctx->k_b,"Vulnerability_Predicates");
    size_t num_vuln_preds=knowledge_base_num_res(sec_ctx->k_b,"Vulnerability_Predicates");
    sec_ctx->vuln_analysis_tree = vulnerability_analysis(vuln_preds,num_vuln_preds);
}



void fill_dependency_graph(struct sec_analysis_ctx *sec_ctx)
{  
    struct dep_graph *d_graph=sec_ctx->dep_graph;
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	for(int j=0;j<sec_ctx->component_contexts[i].num_se_fragments;j++)
	{
	    struct attack_fragment *frag=sec_ctx->component_contexts[i].se_fragments[j];
	    //describe_fragment(frag);
	    add_node_to_dep_graph(d_graph,0,frag);
	    
	    

	}

	for(int j=0;j<sec_ctx->component_contexts[i].num_me_fragments;j++)
	{
	    struct attack_fragment *frag=sec_ctx->component_contexts[i].me_fragments[j];
	    //describe_fragment(frag);
	    add_node_to_dep_graph(d_graph,0,frag);
	    
	    

	}
	for(int j=0;j<sec_ctx->component_contexts[i].num_ci_fragments;j++)
	{
 
	    struct attack_fragment *frag=sec_ctx->component_contexts[i].ci_fragments[j];
	    //scribe_fragment(frag);
	    add_node_to_dep_graph(d_graph,0,frag);
	}

	
    }
    
}




void init_sec_analysis_ctx(struct sec_analysis_ctx *sec_ctx, struct knowledge_base *k_b)
{
    if(!k_b)
    {
	fprintf(stderr, "Can't run security analysis without a valid knowledge_base\n");
	exit(-1);
    }
    sec_ctx->k_b=k_b;    
    if(!k_b->sys)
    {
	fprintf(stderr, "Can't run security analysis without a valid system\n");
	exit(-1);
    }


    //pre-analyses
    analyse_vulnerabilities(sec_ctx);
    sec_ctx->attack_rule_info=attack_rule_analysis(&sec_ctx->num_attack_rules);
    

    //create the fragments
    sec_ctx->num_fragments=0;
    sec_ctx->num_component_ctx=sec_ctx->k_b->sys->num_components;
    //printf("Num component contexts: %zu\n", sec_ctx->num_component_ctx);

    //create a component context per component
    sec_ctx->component_contexts=malloc(sec_ctx->num_component_ctx*sizeof(struct sec_component_ctx));
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	init_component_ctx(&sec_ctx->component_contexts[i],sec_ctx->k_b->sys->components[i]);
    }

    
    //for each component try to estimate the fragments
    estimate_single_exploit_fragments(sec_ctx);
    //estimate_multi_exploit_fragments(sec_ctx);
    //estimate_component_impact_fragments(sec_ctx);

    mark_all(sec_ctx);

    //create the dependency graph
    struct dep_graph *d_graph=malloc(sizeof(struct dep_graph));
    init_dep_graph(d_graph,sec_ctx->k_b->sys->num_components);
    //fill the graph
    //fill_dependency_graph(k_b,d_graph, sec_ctx);
    sec_ctx->dep_graph=d_graph;
}



void print_fragments(struct sec_analysis_ctx *sec_ctx)
{

    
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	for(int j=0;j<sec_ctx->component_contexts[i].num_se_fragments;j++)
	    describe_fragment(sec_ctx->component_contexts[i].se_fragments[j]);

	for(int j=0;j<sec_ctx->component_contexts[i].num_me_fragments;j++)
	    describe_fragment(sec_ctx->component_contexts[i].me_fragments[j]);

	for(int j=0;j<sec_ctx->component_contexts[i].num_ci_fragments;j++)
	    describe_fragment(sec_ctx->component_contexts[i].ci_fragments[j]);
    }
}


void add_predicate_to_fragment(struct sec_analysis_ctx *sec_ctx, struct sec_predicate *pred,
			       enum attack_fragment_type frag_type, char *component_name)
{


    //find component first then look for the correct fragment
    struct sec_component_ctx *comp_ctx=NULL;
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	if(strcmp(sec_ctx->component_contexts[i].component->comp_name,component_name)==0)
	{
	    
	    comp_ctx=&sec_ctx->component_contexts[i];
	    break;
	}
	
    }
    if(!comp_ctx)
	return;

    //have found component let's now figure out which fragment if any to add it to
    
    if(frag_type==SINGLE_EXPLOIT)
    {
	
	for(int i=0;i<comp_ctx->num_se_fragments;i++)
	{
	    
	    if(!comp_ctx->se_fragments[i]->marked)
	    {
		continue;
	    }
	    
	    
	    //if we are adding a vulnExists predicate we have to compare with the fragment's goal
	    if(strcmp(pred->name,"vulnExists")==0)
	    {
		//compare the fragment's goal and the 
		if(strcmp(comp_ctx->se_fragments[i]->goal,pred->terms[2])==0)
		{
		    add_predicate(comp_ctx->se_fragments[i],pred);
		}
	    }
	    //if we are adding a methodInvocation predicate we have to make sure the method has been included previously
	    else if(strcmp(pred->name,"reachable")==0)
	    {
		
		
		for(int j=0;j<comp_ctx->se_fragments[i]->num_preds;j++)
		{
		    
		    
		    if(strcmp(pred->terms[1],comp_ctx->se_fragments[i]->preds[j].terms[1])==0)
		    {
			add_predicate(comp_ctx->se_fragments[i],pred);
			break;
		    }
		}
	    }
	    
	}
    }
    
}
	    


void destroy_sec_analysis_ctx(struct sec_analysis_ctx *sec_ctx)
{
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	destroy_component_ctx(&sec_ctx->component_contexts[i]);	
    }
        
}


void update_sec_analysis_context(struct sec_analysis_ctx *sec_ctx,struct sec_analysis_ctx *old, struct rb_tree *removed, struct rb_tree *added)
{
    if(removed->size == 0 && added->size == 0)
    {
	fprintf(stderr,"Adaptation introduced no changes!\n");
    }
    
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	struct sec_component_ctx *comp_ctx=&sec_ctx->component_contexts[i];
	struct arc_component *comp=comp_ctx->component;

	//If not in removed or added, it was there before.
	if( comp && !rb_tree_find(removed,comp) && !rb_tree_find(added,comp))
	{
	    struct sec_component_ctx *old_comp_ctx=NULL;
	    
	    
	    //find the old component context for this component
	    for(int j=0;j<old->num_component_ctx;j++)
	    {
		struct arc_component *old_comp=old->component_contexts[j].component;
		
		if(strcmp(old_comp->comp_name,comp->comp_name)==0)
		{
		    old_comp_ctx=&old->component_contexts[j];
		    break;
		}
	    }
	    
	    //reuse the old component context
	    if(old_comp_ctx)
	    {

		//sec_ctx->component_contexts[i]=*old_comp_ctx;
		//sec_ctx->num_fragments+=old_comp_ctx->num_se_fragments;
		//sec_ctx->num_fragments+=old_comp_ctx->num_me_fragments;
		//sec_ctx->num_fragments+=old_comp_ctx->num_ci_fragments;

		
		//printf("%zu:%zu:%zu--%zu:%zu:%zu\n",
		//comp_ctx->num_se_fragments,comp_ctx->num_me_fragments,comp_ctx->num_ci_fragments,
		//     old_comp_ctx->num_se_fragments,old_comp_ctx->num_me_fragments,old_comp_ctx->num_ci_fragments);
		
		for(int j=0;j<old_comp_ctx->num_se_fragments;j++)
		{
		    comp_ctx->se_fragments[j]=old_comp_ctx->se_fragments[j];
		    //comp_ctx->num_se_fragments++;
		    sec_ctx->num_fragments++;
		    comp_ctx->se_fragments[j]->marked=false;
		    
		}
		
		for(int j=0;j<old_comp_ctx->num_me_fragments;j++)
		{
		    //printf("Copying:%s:%p\n",fragment_to_str(old_comp_ctx->me_fragments[j]),old_comp_ctx->me_fragments[j]->lag);
		    comp_ctx->me_fragments[j]=old_comp_ctx->me_fragments[j];
		    //comp_ctx->num_me_fragments++;
		    sec_ctx->num_fragments++;
		    comp_ctx->me_fragments[j]->marked=false;
		    comp_ctx->me_fragments[j]->id=-1;
		    
		}

		for(int j=0;j<old_comp_ctx->num_ci_fragments;j++)
		{
		    //printf("Copying:%s\n",fragment_to_str(old_comp_ctx->ci_fragments[j]));
		    comp_ctx->ci_fragments[j]=old_comp_ctx->ci_fragments[j];
		    //comp_ctx->num_ci_fragments++;
		    sec_ctx->num_fragments++;
		    comp_ctx->ci_fragments[j]->marked=false;
		    comp_ctx->ci_fragments[j]->id=-1;
		    //printf("%p\n",old_comp_ctx->ci_fragments[j]->lag);
		}
		//printf("%s: COPIED FROM OLD\n", comp->comp_name);
	    }
		
	    
	    
	    comp_ctx->num_me_fragments=old_comp_ctx->num_me_fragments;
	    comp_ctx->num_ci_fragments=old_comp_ctx->num_ci_fragments;
		
	}
	else
	{
	    //printf("%s: ALTERED/NEW\n",comp->comp_name);
	    for(int j=0;j<comp_ctx->num_se_fragments;j++)
	    {
		if(comp_ctx->se_fragments[j])
		{
		    comp_ctx->se_fragments[j]->marked=true;
		}
	    }
	    
	}
	
    }
    //print_fragment_markings(sec_ctx);
    
}


void inc_security_analysis(struct knowledge_base *k_b, struct rb_tree *removed, struct rb_tree *added)
{
    
    //runs pre-analyses so it is needed first.
    system_arch_to_preds(k_b);
    
    struct timespec start, finish;
    double elapsed;
    clock_gettime(CLOCK_MONOTONIC, &start);
    
    FILE *log_fp=fopen("log","w");
    //Get the old security context
    struct sec_analysis_ctx *sec_ctx=knowledge_base_get_res(k_b,"Security_Context");
    struct sec_analysis_ctx *new_ctx=malloc(sizeof(struct sec_analysis_ctx));
    init_sec_analysis_ctx(new_ctx,k_b);
    update_sec_analysis_context(new_ctx,sec_ctx,removed,added);
    knowledge_base_rem_res(k_b,"Security_Context");
    
    struct named_res *sec_ctx_res=malloc(sizeof(struct named_res));
    named_res_init(sec_ctx_res,"Security_Context",1,new_ctx);
    knowledge_base_add_res(k_b,sec_ctx_res);

    //
    //print_dep_graph_stats(new_ctx->dep_graph);
    fill_dependency_graph(new_ctx);
    
    //print_dep_graph_stats(new_ctx->dep_graph);
    
    

    graph_gen(k_b);
    
    clock_gettime(CLOCK_MONOTONIC, &finish);
    elapsed = (finish.tv_sec - start.tv_sec);
    elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;

    fprintf(log_fp,"Graph Generation time:%f\n", elapsed);
    printf("Graph Generation time:%f\n", elapsed);
    
    auto_gen(k_b);

    clock_gettime(CLOCK_MONOTONIC, &finish);
    elapsed = (finish.tv_sec - start.tv_sec);
    elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
    fprintf(log_fp,"Total analysis time:%f\n", elapsed);
    printf("Total analysis time:%f\n", elapsed);
    fclose(log_fp);
}



void seq_security_analysis(struct knowledge_base *k_b, char *prop_file)
{
    FILE *log_fp=fopen("log","w");
    struct timespec start, finish;
    double elapsed;
    clock_gettime(CLOCK_MONOTONIC, &start);
    system_arch_to_preds(k_b);
    one_graph_gen(k_b);
    clock_gettime(CLOCK_MONOTONIC, &finish);
    elapsed = (finish.tv_sec - start.tv_sec);
    elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
    fprintf(log_fp,"Graph Generation time:%f\n", elapsed);
    printf("Graph Generation time:%f\n", elapsed);
    one_auto_gen(k_b,prop_file);
    clock_gettime(CLOCK_MONOTONIC, &finish);
    elapsed = (finish.tv_sec - start.tv_sec);
    elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
    fprintf(log_fp,"Total analysis time:%f\n", elapsed);
    printf("Total analysis time:%f\n", elapsed);
    
    fclose(log_fp);
    
    //one_auto_gen(k_b);
}

			     

void security_analysis(struct knowledge_base *k_b, bool persist)
{
    FILE *log_fp=fopen("log","w");
    struct timespec start, finish;
    double elapsed;
    
    clock_gettime(CLOCK_MONOTONIC, &start);
    //create the sec_security_context
    struct sec_analysis_ctx *sec_ctx=malloc(sizeof(struct sec_analysis_ctx));
    system_arch_to_preds(k_b);
    init_sec_analysis_ctx(sec_ctx,k_b);
    struct named_res *sec_ctx_res=malloc(sizeof(struct named_res));
    named_res_init(sec_ctx_res,"Security_Context",1,sec_ctx);
    knowledge_base_add_res(k_b,sec_ctx_res);
    fill_dependency_graph(sec_ctx);
    //print_dep_graph_stats(sec_ctx->dep_graph);
    //generate attack graphs
    graph_gen(k_b);
    
    clock_gettime(CLOCK_MONOTONIC, &finish);
    elapsed = (finish.tv_sec - start.tv_sec);
    elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
    fprintf(log_fp,"Graph Generation time:%f\n", elapsed);
    printf("Graph Generation time:%f\n", elapsed);
    //Build and verify automata
    auto_gen(k_b);
    
    if(!persist)
    {	
	destroy_sec_analysis_ctx(sec_ctx);
	free(sec_ctx);
	knowledge_base_rem_res(k_b,"Security_Context");
    }
    
    security_analysis_cleanup(k_b);
   
    clock_gettime(CLOCK_MONOTONIC, &finish);
    elapsed = (finish.tv_sec - start.tv_sec);
    elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
    fprintf(log_fp,"Total analysis time:%f\n", elapsed);
    printf("Total analysis time:%f\n", elapsed);
    fclose(log_fp);

    
    //auto_gen(k_b);

}

void security_analysis_cleanup(struct knowledge_base *k_b)
{
    

    //THIS SHOULD GO TO THE RESOURCE RELEASE OF THE KNOWLEDGE BASE
    struct sec_predicate *component_preds=knowledge_base_get_res(k_b,"Component_Predicates");
    struct sec_predicate *invocation_preds=knowledge_base_get_res(k_b,"Invocation_Predicates");
    struct sec_predicate *vulns=knowledge_base_get_res(k_b,"Vulnerability_Predicates");
    struct sec_predicate *al=knowledge_base_get_res(k_b,"AttackerLocated_Predicates");
    struct sec_predicate *can_reach_preds=knowledge_base_get_res(k_b,"CanReach_Predicates");

    size_t num_vulns=knowledge_base_num_res(k_b,"Vulnerability_Predicates");
    size_t num_reachable=knowledge_base_num_res(k_b,"Reachable_Predicates");
    size_t num_junk=knowledge_base_num_res(k_b,"Junk_Predicates");
    size_t num_al=knowledge_base_num_res(k_b,"AttackerLocated_Predicates");
    size_t num_can_reach=knowledge_base_num_res(k_b,"CanReach_Predicates");
    
    for(int i=0;i<k_b->sys->num_components;i++)
    {
	destroy_predicate(&component_preds[i]);
    }
    free(component_preds);
    knowledge_base_rem_res(k_b,"Component_Predicates");

    for(int i=0;i<k_b->sys->num_invocations;i++)
    {
	destroy_predicate(&invocation_preds[i]);
    }
    free(invocation_preds);
    knowledge_base_rem_res(k_b,"Invocation_Predicates");

    
    for(int i=0;i<num_vulns;i++)
    {
	destroy_predicate(&vulns[i]);
    }
    free(vulns);
    knowledge_base_rem_res(k_b,"Vulnerability_Predicates");

    for(int i=0;i<num_al;i++)
    {
	destroy_predicate(&al[i]);
    }
    free(al);
    knowledge_base_rem_res(k_b,"AttackerLocated_Predicates");

    for(int i=0;i<num_can_reach;i++)
    {
	destroy_predicate(&can_reach_preds[i]);
    }
    free(can_reach_preds);
    knowledge_base_rem_res(k_b,"CanReach_Predicates");

    
    struct hash_table *vuln_preds=knowledge_base_get_res(k_b,"Required_Vulnerabilities");
    if(vuln_preds)
    {
	for(int i=0;i<vuln_preds->num_elements;i++)
	{
	    struct hash_queue *hq=vuln_preds->table[i];
	    struct hash_entry *entry=hq->head;
	    while(entry)
	    {
		char *attack=entry->key;
		printf("%s\n",attack);
		//struct generic_queue *vuln_queue=
		entry=entry->next;
	    }
	
	}
    }
    
}

void mark_all(struct sec_analysis_ctx *sec_ctx)
{
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	for(int j=0;j<sec_ctx->component_contexts[i].num_se_fragments;j++)
	{
	    sec_ctx->component_contexts[i].se_fragments[j]->marked=true;
	}
	//FOR NOW ONLY MARK SE FRAGMENTS
	/*
	for(int j=0;j<sec_ctx->component_contexts[i].num_me_fragments;j++)
	{
	    sec_ctx->component_contexts[i].me_fragments[j]->marked=true;
	}
	for(int j=0;j<sec_ctx->component_contexts[i].num_ci_fragments;j++)
	{
	    sec_ctx->component_contexts[i].ci_fragments[j]->marked=true;
	}
	*/
    }
}


void unmark_all(struct sec_analysis_ctx *sec_ctx)
{
      for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	for(int j=0;j<sec_ctx->component_contexts[i].num_se_fragments;j++)
	{
	    sec_ctx->component_contexts[i].se_fragments[j]->marked=false;
	}
	for(int j=0;j<sec_ctx->component_contexts[i].num_me_fragments;j++)
	{
	    sec_ctx->component_contexts[i].me_fragments[j]->marked=false;
	}
	for(int j=0;j<sec_ctx->component_contexts[i].num_ci_fragments;j++)
	{
	    sec_ctx->component_contexts[i].ci_fragments[j]->marked=false;
	}
    }
}



/*
void mark_updated(struct sec_analysis_ctx *sec_ctx,struct rb_tree *updated)
{
    for(int i=0;i<sec_ctx->num_attack_fragments;i++)
    {
	if(sec_ctx->attack_fragments[i].comp)
	    if(rb_tree_find(updated,sec_ctx->attack_fragments[i].comp))
	    {
		sec_ctx->attack_fragments[i].marked=true;
	    }
    }
}
*/

struct sec_component_ctx *get_component_context_for_component(struct sec_analysis_ctx *sec_ctx,char *name)
{
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	if(strcmp(sec_ctx->component_contexts[i].component->comp_name,name)==0)
	{
	    return &sec_ctx->component_contexts[i];
	}
    }
    return NULL;
}

struct attack_fragment *get_fragment_from_goal(struct sec_component_ctx *comp_ctx, enum attack_fragment_type type, char *goal)
{
    if(type==SINGLE_EXPLOIT)
    {
	for(int i=0;i<comp_ctx->num_se_fragments;i++)
	{
	    if(strcmp(comp_ctx->se_fragments[i]->goal,goal)==0)
	    {
		return comp_ctx->se_fragments[i];
	    }

	}
    }
    else if(type==MULTI_EXPLOIT)
    {
	for(int i=0;i<comp_ctx->num_me_fragments;i++)
	{
	    if(strcmp(comp_ctx->me_fragments[i]->goal,goal)==0)
	    {
		return comp_ctx->me_fragments[i];
	    }

	}
    }
    else if(type==COMPONENT_IMPACT)
    {
	for(int i=0;i<comp_ctx->num_ci_fragments;i++)
	{
	    if(strcmp(comp_ctx->ci_fragments[i]->goal,goal)==0)
	    {
		return comp_ctx->ci_fragments[i];
	    }

	}
    }
    return NULL;
}

void print_fragment_markings(struct sec_analysis_ctx * sec_ctx)
{    
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	struct sec_component_ctx *comp_ctx=&sec_ctx->component_contexts[i];
	//printf("Component: %s:%zu:%zu:%zu\n",comp_ctx->component->comp_name,comp_ctx->num_me_fragments,comp_ctx->num_se_fragments,comp_ctx->num_ci_fragments);
	for(int j=0;j<comp_ctx->num_se_fragments;j++)
	{
	    if(comp_ctx->se_fragments[j])
		print_fragment_marking(comp_ctx->se_fragments[j]);
	}
	for(int j=0;j<comp_ctx->num_me_fragments;j++)
	{
	    if(comp_ctx->me_fragments[j])
		print_fragment_marking(comp_ctx->me_fragments[j]);
	}

	for(int j=0;j<comp_ctx->num_ci_fragments;j++)
	{
	    if(comp_ctx->ci_fragments[j])
		print_fragment_marking(comp_ctx->ci_fragments[j]);
	}
    }
}
