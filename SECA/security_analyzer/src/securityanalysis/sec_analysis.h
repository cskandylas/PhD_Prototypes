#include "knowledge_base.h"
#include "arc_system.h"
#include "sec_clause.h"
#include "attack_fragment.h"
#include "dep_graph.h"
#include "rbtree.h"
#include "vulnerability_analysis.h"
#include "attack_rule_analysis.h"

#ifndef SEC_ANALYSIS
#define SEC_ANALYSIS



#define SEC_ANALYSIS_INIT_MAX_COMPONENTS 16
#define SEC_ANALYSIS_INIT_MAX_FRAGMENTS 8

struct sec_component_ctx
{
    struct arc_component        *component;
    struct attack_fragment	 **se_fragments;
    struct attack_fragment	 **me_fragments;
    struct attack_fragment	 **ci_fragments;
    size_t			 num_se_fragments;
    size_t                       max_se_fragments;
    size_t			 num_me_fragments;
    size_t                       max_me_fragments;
    size_t			 num_ci_fragments;
    size_t                       max_ci_fragments;
};

struct sec_analysis_ctx
{
    struct knowledge_base       *k_b;
    struct rb_tree              *vuln_analysis_tree;
    struct attack_analysis_info *attack_rule_info;
    size_t num_attack_rules;
    struct sec_component_ctx    *component_contexts;
    size_t                      num_component_ctx;
    size_t                      num_fragments;
    struct dep_graph            *dep_graph;
};


void init_component_ctx(struct sec_component_ctx *sec_comp_ctx, struct arc_component *comp);
void destroy_component_ctx(struct sec_component_ctx *comp_ctx);
struct attack_fragment *get_fragment_from_goal(struct sec_component_ctx *comp_ctx, enum attack_fragment_type type, char *goal);
void component_ctx_add_fragment(struct sec_component_ctx *comp_ctx,
				struct attack_fragment *frag);


void init_sec_analysis_ctx(struct sec_analysis_ctx *sec_ctx, struct knowledge_base *k_b);
void destroy_sec_analysis_ctx(struct sec_analysis_ctx *sec_ctx);
struct sec_component_ctx *get_component_context_for_component(struct sec_analysis_ctx *sec_ctx,char *name);

void system_arch_to_preds(struct knowledge_base *k_b);
struct sec_predicate* interaction_vuln_to_preds(struct knowledge_base *k_b, int *num_preds);
void print_fragments(struct sec_analysis_ctx *sec_ctx);
void add_predicate_to_fragment(struct sec_analysis_ctx *sec_ctx, struct sec_predicate *pred,
			       enum attack_fragment_type frag_type, char *component_name);



void security_analysis(struct knowledge_base *k_b, bool persist);
void inc_security_analysis(struct knowledge_base *k_b, struct rb_tree *removed, struct rb_tree *added);
void seq_security_analysis(struct knowledge_base *k_b,char *prop_file);
static void security_analysis_cleanup(struct knowledge_base *k_b);

void mark_all(struct sec_analysis_ctx *sec_ctx);
void unmark_all(struct sec_analysis_ctx *sec_ctx);
void mark_updated(struct sec_analysis_ctx *sec_ctx,struct rb_tree *updated);

void fill_dependency_graph(struct sec_analysis_ctx *sec_ctx);


//#debug
void print_fragment_markings(struct sec_analysis_ctx * sec_ctx);
#endif
