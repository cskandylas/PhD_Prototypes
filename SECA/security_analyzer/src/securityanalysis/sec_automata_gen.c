#define _XOPEN_SOURCE 501

#include "sec_automata_gen.h"

#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>  
#include <stdlib.h>    
#include <sys/wait.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <ctype.h>

#include "lag.h"
#include "fsm.h"
#include "sec_clause.h"
#include "lag_resources.h"
#include "fsm_resources.h"
#include "fol_resources.h"
#include "dep_graph.h"
#include "attack_fragment.h"

#include <sys/mman.h>

#include "named_resource.h"
#include "sec_analysis.h"
#include "dep_graph.h"
#include "rbtree.h"
#include "tokenize.h"


struct shared
{
    int idx;
    pid_t pid;
};

void* create_shared_memory(size_t size);

int verification_res_cmp(const void *v_p1, const void *v_p2)
{
    struct verification_property *ver_pro1=(struct verification_property*)v_p1;
    struct verification_property *ver_pro2=(struct verification_property*)v_p2;

    return strcmp(ver_pro1->goal,ver_pro2->goal);
}

void build_automaton(struct attack_fragment *frag)
{
    
    if(frag->ver_props)
    {
	//printf("A:Skipping: %s\n",fragment_to_str(frag));
	exit(0);
    }
    else
    {
	//printf("A:Building: %s\n",fragment_to_str(frag));
    }
    
    char input_dir[256]="";
    enum LAG_ATTACK_FRAGMENT lag_type;
    char graph_name[256]="";
    char automaton_file[256];
    char ver_results_file[256]="";
    char properties_file[256]="";
    char property[256]="";
    
    if(frag->type==SINGLE_EXPLOIT)
    {
	sprintf(input_dir,"Graphs/single_exploit/%s_%s",frag->comp->comp_name,frag->goal);
	sprintf(graph_name,"SingleExploit%s_%s",frag->comp->comp_name,frag->goal);
	sprintf(automaton_file,"Automata/single_exploit/%s_%s.prism",frag->comp->comp_name,frag->goal);
	sprintf(properties_file,"Automata/single_exploit/%s_%s.props",frag->comp->comp_name,frag->goal);
	sprintf(ver_results_file,"Verification/single_exploit/%s_%s",frag->comp->comp_name,frag->goal);
	sprintf(property,"P=? [ F c_singleExploit_%s_%s ]\n",frag->comp->comp_name,frag->goal);
	
	lag_type=LAG_SINGLE_EXPLOIT;
    }
    else if(frag->type==MULTI_EXPLOIT)
    {
	sprintf(input_dir,"Graphs/multi_exploit/%s_%s",frag->comp->comp_name,frag->goal);
	sprintf(graph_name,"MultiExploit%s_%s",frag->comp->comp_name,frag->goal);
	sprintf(automaton_file,"Automata/multi_exploit/%s_%s.prism",frag->comp->comp_name,frag->goal);
	sprintf(properties_file,"Automata/multi_exploit/%s_%s.props",frag->comp->comp_name,frag->goal);
	sprintf(ver_results_file,"Verification/multi_exploit/%s_%s",frag->comp->comp_name,frag->goal);
	sprintf(property,"P=? [ F c_multiExploit_%s_%s ]\n",frag->comp->comp_name,frag->goal);
	lag_type=LAG_MULTI_EXPLOIT;
    }
    else if(frag->type==COMPONENT_IMPACT)
    {
	sprintf(input_dir,"Graphs/component_impact/%s_%s",frag->comp->comp_name,frag->goal);
	sprintf(graph_name,"ComponentImpact%s_%s",frag->comp->comp_name,frag->goal);
	sprintf(automaton_file,"Automata/component_impact/%s_%s.prism",frag->comp->comp_name,frag->goal);
	sprintf(properties_file,"Automata/component_impact/%s_%s.props",frag->comp->comp_name,frag->goal);
	sprintf(ver_results_file,"Verification/component_impact/%s_%s",frag->comp->comp_name,frag->goal);
	sprintf(property,"P=? [ F c_componentImpact_%s_%s ]\n",frag->comp->comp_name,frag->goal);
	lag_type=LAG_COMPONENT_IMPACT;
    }

    
    
    
    struct lag *graph=lag_from_directory(input_dir,graph_name,lag_type);
    if(graph)
    {
	struct variable_generator *var_gen=malloc(sizeof(struct variable_generator));
	init_variable_generator(var_gen);
	struct fsm *fsm=fsm_from_lag(graph,var_gen);
	destroy_lag(graph);
	free(graph);

	if(1)
	{
	    //fsm_to_xml_file(automaton_file,fsm,"","fsm=FSM();\n system fsm;\n",queries,num_queries);
	    fsm_to_prism_model_file(automaton_file,fsm);
	    fsm_to_prism_properties_file(properties_file,property);
	    
	    destroy_fsm(fsm);
	    free(fsm);
	    free(var_gen);
	    FILE *fp=freopen(ver_results_file, "w", stdout); 
	    execlp ("/home/prosses/Tools/Prism/prism-4.7-linux64/bin/prism", "/home/prosses/Tools/Prism/prism-4.7-linux64/bin/prism",
		    automaton_file,
		    properties_file,
		    (char *)0);
	}
	else
	{
	    printf("NO queries for %s\n",input_dir);
	    destroy_fsm(fsm);
	    free(fsm);
	    free(var_gen);
	    exit(-1);
	}
    }
    else
    {
	fprintf(stderr,"There is no graph for %s:%d or a graph build error occured\n",frag->comp->comp_name,frag->type);
	exit(-1);
    }
    
}


void one_auto_gen(struct knowledge_base *k_b,char *prop_file)
{
    struct timespec start, finish;
    double elapsed;
    clock_gettime(CLOCK_MONOTONIC, &start);
    
    char *input_dir="Graphs/all_in_one";
    char *ver_results_file="Verification/all_in_one.res";
    char *automaton_file="Automata/all_in_one.prism";
    char *properties_file="Automata/valid.props";
    
    struct lag *graph=lag_from_directory(input_dir,"allinone",LAG_COMPONENT_IMPACT);

    if(graph)
    {
	struct variable_generator *var_gen=malloc(sizeof(struct variable_generator));
	init_variable_generator(var_gen);
	struct fsm *fsm=fsm_from_lag(graph,var_gen);

	
	int num_queries=0;
	
	destroy_lag(graph);
	free(graph);


	fsm_to_prism_model_file(automaton_file,fsm);
	    //fsm_to_xml_file(automaton_file,fsm,"","fsm=FSM();\n system fsm;\n",queries,num_queries);
	    
	
	    
	    if(1)
	    {
		destroy_fsm(fsm);
		free(fsm);
		free(var_gen);
	    
		pid_t pid=fork();
		if(pid==0)
		{
		    FILE *fp=freopen(ver_results_file, "w", stdout);
		    execlp ("/home/prosses/Tools/Prism/prism-4.7-linux64/bin/prism", "/home/prosses/Tools/Prism/prism-4.7-linux64/bin/prism",
			    automaton_file,
			    prop_file,
			    "-cuddmaxmem",
			    "12g",
			    (char *)0);
		    //run verifyta
		    /*
		      execlp ("/bin/bash", "bash",
		      "/home/prosses/Tools/UPPAAL/uppaal64-4.1.24/bin-Linux/verifyta",
		      "-a","0.01","-E","0.01", "-B", "0.01",
		      automaton_file,
		      "-u",
		      (char *)0);
		    */
		}
		else
		{
		    int status;
		    pid_t c_pid = waitpid(pid,NULL,0);
		    clock_gettime(CLOCK_MONOTONIC, &finish);
		    elapsed = (finish.tv_sec - start.tv_sec);
		    elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
		    printf("High level automata generation and verification  time:%f\n", elapsed);
		    
		}
	    }
	    else
	    {
		printf("NO queries for %s\n",input_dir);
		destroy_fsm(fsm);
		free(fsm);
		free(var_gen);
		exit(-1);
	    }
    }
    
}



char *read_file(char *filename);
char *parse_prism_results_file(char *results_file)
{
    
    struct tokenizer line_tok;
    
    char *res_text=read_file(results_file);
    tokenizer_init(&line_tok,res_text,"\n");
    char *line=NULL;
    while((line=next_token(&line_tok))!=NULL)
    {
	if(strstr(line,"Result:"))
	{
	    struct tokenizer space_tok;
	    tokenizer_init(&space_tok,line," ");
	    free(next_token(&space_tok));
	    char *res_string=next_token(&space_tok);
	    //printf("%s\n",res_string);
	    return res_string;
	}
    }


}


void update_fragment(struct attack_fragment *frag)
{

    if(frag->lag)
    {
	//printf("U: Skipping %s\n",fragment_to_str(frag));
	//return;
    }
    
    char input_dir[256]="";
    enum LAG_ATTACK_FRAGMENT lag_type;
    char graph_name[256]="";
    char automaton_file[256];
    char ver_results_file[256]="";
    //char goal[256]="";
    
    if(frag->type==SINGLE_EXPLOIT)
    {
	sprintf(input_dir,"Graphs/single_exploit/%s_%s",frag->comp->comp_name,frag->goal);
	sprintf(graph_name,"SingleExploit%s_%s",frag->comp->comp_name,frag->goal);
	sprintf(automaton_file,"Automata/single_exploit/%s_%s.prism",frag->comp->comp_name,frag->goal);
	sprintf(ver_results_file,"Verification/single_exploit/%s_%s",frag->comp->comp_name,frag->goal);
	lag_type=LAG_SINGLE_EXPLOIT;
    }
    else if(frag->type==MULTI_EXPLOIT)
    {
	sprintf(input_dir,"Graphs/multi_exploit/%s_%s",frag->comp->comp_name,frag->goal);
	sprintf(graph_name,"MultiExploit%s_%s",frag->comp->comp_name,frag->goal);
	sprintf(automaton_file,"Automata/multi_exploit/%s_%s.prism",frag->comp->comp_name,frag->goal);
	sprintf(ver_results_file,"Verification/multi_exploit/%s_%s",frag->comp->comp_name,frag->goal);
	lag_type=LAG_MULTI_EXPLOIT;
    }
    else if(frag->type==COMPONENT_IMPACT)
    {
	sprintf(input_dir,"Graphs/component_impact/%s_%s",frag->comp->comp_name,frag->goal);
	sprintf(graph_name,"ComponentImpact%s_%s",frag->comp->comp_name,frag->goal);
	sprintf(automaton_file,"Automata/component_impact/%s_%s.prism",frag->comp->comp_name,frag->goal);
	sprintf(ver_results_file,"Verification/component_impact/%s_%s",frag->comp->comp_name,frag->goal);
	lag_type=LAG_COMPONENT_IMPACT;
    }

    struct lag *graph=lag_from_directory(input_dir,graph_name,lag_type);
    if(graph)
    {
	frag->lag=graph;
	struct variable_generator *var_gen=malloc(sizeof(struct variable_generator));
	init_variable_generator(var_gen);
	struct fsm *fsm=fsm_from_lag(graph,var_gen);
	frag->fsm=fsm;
	


	
	
	size_t num_props=1;
	char *res=parse_prism_results_file(ver_results_file);

	struct verification_property *ver_props=malloc(sizeof(struct verification_property));
	
	if(ver_props)
	{
	    
	    ver_props->result=res;
	    frag->ver_props=ver_props;
	    
	}
	frag->num_ver_props=num_props;
	free(var_gen);
    }
    else
    {
	fprintf(stderr,"Could not locate graph for fragment: %s",fragment_to_str(frag));
    }

    
}

void generate_low_level_automata(struct knowledge_base *k_b)
{
    
    //fetch the security context
    struct sec_analysis_ctx *sec_ctx=knowledge_base_get_res(k_b,"Security_Context");
    
    //Fetch and clear the dependency graph
    struct dep_graph *d_graph =sec_ctx->dep_graph;
    //clear_dep_graph(d_graph);
    //print_dep_graph_stats(d_graph);
    
    for(int i=0;i<d_graph->num_nodes;i++)
    {
	if(d_graph->nodes[i]->frag->marked)
	    d_graph->nodes[i]->done=false;
	else
	    d_graph->nodes[i]->done=true;
    }
 
        
    //mmap some memory and cast it to an array of shared.
    void *sh_mem = create_shared_memory(sec_ctx->num_fragments*sizeof(struct shared));
    struct shared *shared=sh_mem;
        
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {

	for(int j=0;j<sec_ctx->component_contexts[i].num_se_fragments;j++)
	{
	    struct attack_fragment *frag=sec_ctx->component_contexts[i].se_fragments[j];
	    if(frag->num_preds >  0 /*&& frag->marked*/)
	    {
		
		int d_graph_idx=frag->id;
		d_graph->nodes[d_graph_idx]->queued=true;
		pid_t pid=fork();
		//pid_t pid= 0;
		if(pid<0)
		{
		    fprintf(stderr,"Couldn't fork to generate Single Exploit graph for %s\n",frag->comp->comp_name);
		    //exit(-1);
		}
		else if(pid==0)
		{
		    
		
		    //shared memory trick to pass the pid to the parent process
		    shared[d_graph_idx].idx=d_graph_idx;
		    shared[d_graph_idx].pid=getpid();
		    //build and verify the automaton
		    build_automaton(frag);
		}
		
	    }
	}
    }

    
    int status;
    pid_t pid;
    int count_done=0;
    while (!dep_graph_all_done(d_graph))
    {
	pid=waitpid(-1,&status,0);
	
	if(pid>0)
	{
	    for(int i=0;i<d_graph->num_nodes;i++)
	    {
		if(shared[i].pid==pid)
		{
		    //describe_fragment(d_graph->nodes[shared[i].idx]->frag);
		    
		    update_fragment(d_graph->nodes[shared[i].idx]->frag);
		    //printf("\n%s%d finished\n",d_graph->nodes[shared[i].idx].frag->comp->comp_name,d_graph->nodes[shared[i].idx].frag->type);
		    d_graph->nodes[shared[i].idx]->done=true;
		    break;
		}
	    }
	}
	
	int next_idx=next_free(d_graph);
	if(next_idx!=-1)
	{
	    //printf("\nAbout to queue: %s%d\n",d_graph->nodes[next_idx].frag->comp->comp_name,d_graph->nodes[next_idx].frag->type);
	    d_graph->nodes[next_idx]->queued=true;
	    
	    pid=fork();
	    
	    if(pid<0)
	    {
		fprintf(stderr,"Couldn't fork to generate graph for \n");
		//exit(-1);
	    }
	    else if(pid==0)
	    {
		//shared memory trick to pass the pid to the parent process
		shared[next_idx].idx=next_idx;
		shared[next_idx].pid=getpid();
		//run mulval
		build_automaton(d_graph->nodes[next_idx]->frag);
		exit(0);
	    }
	    
	    
	}
	
    }
//    printf("attack fragment graphs automata generated\n");
    munmap(shared,sec_ctx->num_fragments*sizeof(struct shared) );
    //destroy_dep_graph(d_graph);
    //free(d_graph);
}

char *extract_fragment(char *label)
{
    char *loc = strchr(label, ',');
    if (!loc )
    {
        return NULL;
    }

    int index = loc - label;
    char *frag_name=malloc(index+1);
    memcpy(frag_name,label,index);
    frag_name[index]='\0';
    char *paren_loc=strchr(frag_name,'(');
    *paren_loc='_';
    return frag_name;
}


char *create_variables_rb_tree(struct rb_tree *tree);
char *create_probabilities_rb_tree(struct rb_tree *tree);
char *make_high_level_guard(struct lag *lag, struct lag_node *node);

void abstract_fragment(struct attack_fragment *frag, struct rb_tree *declarations_set, struct fsm *fsm)
{
    //printf("Abstracting fragment:%s_%s %d\n",frag->comp->comp_name,frag->goal,frag->marked);
    for(int j=0;j<frag->lag->num_nodes;j++)
    {
	if(frag->marked && frag->lag->nodes[j] && node_is_goal(frag->lag,frag->lag->nodes[j]))
	{
	    char *name=make_capability(frag->lag->nodes[j]->label);
	    
	    char guard[4096*4];
	    memset(guard,0,4096*4);
	    sprintf(guard,"(c_%s=false)",name);
	    double prob=frag->lag->nodes[j]->prob;
	    //strcpy(guard,"");
	    char *alt_guard=make_high_level_guard(frag->lag,frag->lag->nodes[j]);
	    //printf("POSSIBLE GUARD REPLACEMENT %s\n",alt_guard);

	    //printf("VERIFICATION RESULT %s\n",frag->ver_props->result);
	    char *frag_name=extract_fragment(frag->lag->nodes[j]->label);
	    char update[128];
	    sprintf(update,"(c_%s'=true)",name);
	    char *declaration = malloc(256);
	    sprintf(declaration,"%s %s",name,frag->ver_props->result);
	    rb_tree_insert(declarations_set,declaration);
	    char *action=malloc(strlen(alt_guard)+strlen(update)+strlen(name)*2+512+strlen(frag_name)*2);
	    //printf("[]  %s -> %s;\n",guard,update);
	    
	    sprintf(action,"[] (!q_%s) &  %s -> (c_%s_prob) : %s + (1-c_%s_prob) : (q_%s'=true);\n",name,alt_guard,name,update,name,name);
	    //sprintf(action,"[] (!gave_up) &  %s -> (c_%s_prob) : %s + (1-c_%s_prob) : (gave_up'=true);\n",guard,name,update,name);
	    fsm_add_action(fsm,action);
	    //num_decls++;
	}
	
	
	
	
    }
    
}


int str_comp(const void *str1, const void *str2)
{
    const char *s1=str1;
    const char *s2=str2;
    return strcmp(s1,s2);
}


char *make_not_quit_entails_compromise(struct sec_analysis_ctx *sec_ctx)
{    
    //P=?[F (!q_singleExploit_server0_cwe78 & !q_singleExploit_client0_cwe98) => ( c_componentImpact_server0_confidentiality)]   
    char *prop=NULL;
    int count=0;
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	
	struct arc_component *comp=sec_ctx->component_contexts[i].component;
	//printf("%s\n",comp->comp_name);
	if(strncmp(comp->comp_name,"server",strlen("server"))==0
	   || strncmp(comp->comp_name,"lbproxy",strlen("lbproxy"))==0
	   || strncmp(comp->comp_name,"database",strlen("database"))==0)
	{
	    
	    
	    
	    for(int j=0;j<sec_ctx->component_contexts[i].num_ci_fragments;j++)
	    {
		struct attack_fragment *frag=sec_ctx->component_contexts[i].ci_fragments[j];
		
		
		//printf("%s\n",frag->comp->comp_name);
		if(frag->marked && frag->preds)
		{

		    char *se_frags=malloc(512);
		    memset(se_frags,'\0',512);


		    
		    for(int k=0;k<sec_ctx->component_contexts[i].num_se_fragments;k++)
		    {
			struct attack_fragment *se_frag=sec_ctx->component_contexts[i].se_fragments[k];
			if(se_frag->marked && se_frag->preds)
			{

			    char buff[128];
			    memset(buff,0,128);
			    char *to_append= fragment_to_str(se_frag);
			    strcpy(se_frags,"!q_");
			    strcat(se_frags,to_append);
			    int len=strlen(to_append);
			    int server_num=-1;
			    for(int i=0;i<len;i++)
			    {
				if(isdigit(to_append[i]))
				{
				    server_num=to_append[i]-'0';
				    //printf("server_num:%d\n",server_num);
				    sprintf(buff," & !q_singleExploit_client%d_cwe98",server_num);
				    strcat(se_frags,buff);
				    break;
				}
			    }
			    
			    free(to_append);
			}
		    }
		    //printf("se_frags:%s\n",se_frags);
		    
		    if(!prop)
		    {
			prop=malloc(32*1024);
			memset(prop,'\0',32*1024);
			strcat(prop,"P=? [G (  ");
		    }
		    
		    if(count>0)
		    {
			strcat(prop," & ");
		    }
		    
		    char part[128];
		    sprintf(part,"( (%s )   => (F c_componentImpact_%s_%s)  ) ",se_frags,comp->comp_name,frag->goal);
		    strcat(prop,part);
		    count++;
		    free(se_frags);
		}
	    }
	}
    }
    if(prop)
    {
	strcat(prop," ) ];");
    }
    if(!prop)
    {
	fprintf(stderr,"Nill Property\n");
	//exit(-1);
    }
    //printf("Selected Property to model check:%s\n",prop);
    
    return prop;
}



char *make_all_single_exploits(struct sec_analysis_ctx *sec_ctx)
{
    char *prop=NULL;
    int count=0;
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	
	struct arc_component *comp=sec_ctx->component_contexts[i].component; 
	for(int j=0;j<sec_ctx->component_contexts[i].num_se_fragments;j++)
	{
	    struct attack_fragment *frag=sec_ctx->component_contexts[i].se_fragments[j];
		
		
	    if(frag->marked && frag->preds)
	    {
		//printf("%s:%s:%p:%s\n",fragment_to_str(frag),frag->goal,frag->preds,frag->ver_props[0].result);
		if(!prop)
		{
		    prop=malloc(32*1024);
		    memset(prop,'\0',32*1024);
		    strcat(prop,"P=? [ F ( ");
		}
		
		if(count>0)
		{
		    strcat(prop," | ");
		}
		
		char part[128];
		sprintf(part,"c_singleExploit_%s_%s ",comp->comp_name,frag->goal);
		strcat(prop,part);
		count++;
	    }
	}
	    
    }
    if(prop)
    {
	strcat(prop," )];");
    }
    if(!prop)
    {
	fprintf(stderr,"Nill Property\n");
	//exit(-1);
    }
    //printf("Selected Property to model check:%s\n",prop);
    
    return prop;
}

char *make_no_impact_until_exploit(struct sec_analysis_ctx *sec_ctx)
{



    //P=? [ (!c_componentImpact_server0_confidentiality) U (c_singleExploit_server0_cwe78) ]
    //P=? [ ((!c_componentImpact_server0_confidentiality) U (c_singleExploit_server0_cwe78)) & ((!c_componentImpact_server1_confidentiality) U (c_singleExploit_server1_cwe78)) & ((!c_componentImpact_server2_confidentiality) U (c_singleExploit_server2_cwe78))]
    
    char *prop=NULL;
    int count=0;
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	
	struct arc_component *comp=sec_ctx->component_contexts[i].component;
	//printf("%s\n",comp->comp_name);
	if(strncmp(comp->comp_name,"server",strlen("server"))==0
	   || strncmp(comp->comp_name,"lbproxy",strlen("lbproxy"))==0
	   || strncmp(comp->comp_name,"database",strlen("database"))==0)
	{
	    
	    
	    
	    for(int j=0;j<sec_ctx->component_contexts[i].num_ci_fragments;j++)
	    {
		struct attack_fragment *frag=sec_ctx->component_contexts[i].ci_fragments[j];
		
		
		//printf("%s\n",frag->comp->comp_name);
		if(frag->marked && frag->preds)
		{

		    char *se_frags=malloc(512);
		    memset(se_frags,'\0',512);
		    //gather all single_exploits for comp
		    for(int k=0;k<sec_ctx->component_contexts[i].num_se_fragments;k++)
		    {
			struct attack_fragment *se_frag=sec_ctx->component_contexts[i].se_fragments[k];
			if(se_frag->marked && se_frag->preds)
			{
			    char *to_append= fragment_to_str(se_frag);
			    strcat(se_frags,to_append);
			    free(to_append);
				  
			    //printf("%s:%s:%p:%s\n",,se_frag->goal,se_frag->preds,se_frag->ver_props[0].result);
			}
		    }
		    
		    if(!prop)
		    {
			prop=malloc(32*1024);
			memset(prop,'\0',32*1024);
			strcat(prop,"P=? [(  ");
		    }
		    
		    if(count>0)
		    {
			strcat(prop," & ");
		    }
		    
		    char part[128];
		    sprintf(part,"( (!c_componentImpact_%s_%s) U (c_%s) ) ",comp->comp_name,frag->goal,se_frags);
		    strcat(prop,part);
		    count++;
		    free(se_frags);
		}
	    }
	}
    }
    if(prop)
    {
	strcat(prop," ) ];");
    }
    if(!prop)
    {
	fprintf(stderr,"Nill Property\n");
	//exit(-1);
    }
    //printf("Selected Property to model check:%s\n",prop);
    
    return prop;
}



char *make_systemdown(struct sec_analysis_ctx *sec_ctx)
{
    char *prop=NULL;
    int count=0;
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	
	struct arc_component *comp=sec_ctx->component_contexts[i].component;
	//printf("%s\n",comp->comp_name);
	if(strncmp(comp->comp_name,"server",strlen("server"))==0
	   || strncmp(comp->comp_name,"lbproxy",strlen("lbproxy"))==0
	   || strncmp(comp->comp_name,"database",strlen("database"))==0)
	{
	    
	    
	    
	    for(int j=0;j<sec_ctx->component_contexts[i].num_ci_fragments;j++)
	    {
		struct attack_fragment *frag=sec_ctx->component_contexts[i].ci_fragments[j];
		
		//printf("%s:%s:%p:%s\n",fragment_to_str(frag),frag->goal,frag->preds,frag->ver_props[0].result);
		if(frag->marked && frag->preds)
		{

		    if(!prop)
		    {
			prop=malloc(32*1024);
			memset(prop,'\0',32*1024);
			strcat(prop,"P=? [ F ( ");
		    }
		    
		    if(count>0)
		    {
			strcat(prop," | ");
		    }
		    
		    char part[128];
		    sprintf(part,"c_componentImpact_%s_%s ",comp->comp_name,frag->goal);
		    strcat(prop,part);
		    count++;
		}
	    }
	}
    }
    if(prop)
    {
	strcat(prop," )];");
    }
    if(!prop)
    {
	fprintf(stderr,"Nill Property\n");
	//exit(-1);
    }
    //printf("Selected Property to model check:%s\n",prop);
    
    return prop;
}


void regenerate_marking(struct knowledge_base *k_b)
{
    
    struct sec_analysis_ctx *sec_ctx=knowledge_base_get_res(k_b,"Security_Context");
    struct dep_graph *d_graph = sec_ctx->dep_graph;
    //print_dep_graph_stats(d_graph);
    unmark_all(sec_ctx);
    
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	
	struct arc_component *comp=sec_ctx->component_contexts[i].component;
	//printf("%s\n",comp->comp_name);
	if(strncmp(comp->comp_name,"server",strlen("server"))==0
	   || strncmp(comp->comp_name,"lbproxy",strlen("lbproxy"))==0
	   || strncmp(comp->comp_name,"database",strlen("database"))==0)
	{
	    for(int j=0;j<sec_ctx->component_contexts[i].num_ci_fragments;j++)
	    {
		struct attack_fragment *frag=sec_ctx->component_contexts[i].ci_fragments[j];
		{
		    mark_dfs(d_graph->nodes[frag->id],false);
		}
			     
	    }
	}
    }
    //marked_graph_to_dot(d_graph,"dependencies.dot",true);
    //marked_graph_to_dot(d_graph,"dependencies_full.dot",false);

}

void stitch_high_level(struct sec_analysis_ctx *sec_ctx,struct fsm *fsm, struct rb_tree *declarations_set)
{
    
    struct dep_graph *dep_graph = sec_ctx->dep_graph;
    
    bool added[dep_graph->num_nodes];

    for(int i=0;i<dep_graph->num_nodes;i++)
    {
	added[i]=false;
    }

    
    
    
    for(int i=0;i<dep_graph->num_nodes;i++)
    {
	struct dep_node *node=dep_graph->nodes[i];
	if(node && node->frag->marked && node->frag->ver_props)
	{
	    //printf("Node:%s:%zu\n",fragment_to_str(node->frag),node->num_dependencies);
	    
	    if(!added[node->frag->id])
	    {
		abstract_fragment(node->frag,declarations_set,fsm);
		added[node->frag->id]=true;
	    }
	    
	    for(int j=0;j<node->num_dependencies;j++)
	    {
		
		struct dep_node *dep_node=node->dependencies[j];
		
		if(!added[dep_node->frag->id])
		{
		    //printf("%s->%s\n",fragment_to_str(node->frag),fragment_to_str(dep_node->frag));
		    abstract_fragment(dep_node->frag,declarations_set,fsm);
		    added[dep_node->frag->id]=true;
		}
	    }
	}
    }

}

void generate_high_level_automaton(struct knowledge_base *k_b)
{   
    struct sec_analysis_ctx *sec_ctx=knowledge_base_get_res(k_b,"Security_Context");
    
    struct dep_graph *d_graph = sec_ctx->dep_graph;
    //use the dependency graph to pull the prerequisites
    regenerate_marking(k_b);
    char ver_results_file[256]="Verification/FSM.res";
    
    struct fsm *fsm=malloc(sizeof(struct fsm));
    init_fsm(fsm,"HighLevel");
    
    

    char *properties=malloc(1024*1024);
    memset(properties,'\0',1024*1024);
    char *system_down_prop=make_systemdown(sec_ctx);
    if(system_down_prop)
    {

	
	
	
	strcat(properties,system_down_prop);
	strcat(properties,"\n");
	
	char *prop1 = make_no_impact_until_exploit(sec_ctx);
	char *prop2 = make_all_single_exploits(sec_ctx);
	char *prop3 = make_not_quit_entails_compromise(sec_ctx);

	strcat(properties,prop1);
	strcat(properties,"\n");
	strcat(properties,prop2);
	strcat(properties,"\n");
	strcat(properties,prop3);
	
	//rbtree to use as a set to hold declarations
	struct rb_tree declarations_set;
	rb_tree_init(&declarations_set,str_comp);
	
	int num_decls=0;
	stitch_high_level(sec_ctx,fsm,&declarations_set);
  

    
	char *vars=create_variables_rb_tree(&declarations_set);
	char *probs=create_probabilities_rb_tree(&declarations_set);
	rb_tree_destroy(&declarations_set);
	
	fsm_set_variables(fsm,vars);
	fsm_set_probabilities(fsm,probs);
        
	char *automaton_file = "Automata/FSM.prism";
	char *properties_file = "Automata/valid.props";
	fsm_to_prism_model_file(automaton_file,fsm);
	fsm_to_prism_properties_file(properties_file,properties);
	
	pid_t pid=fork();
	if(pid==0)
	{
	    FILE *fp=freopen(ver_results_file, "w", stdout);
	    
	    execlp ("/home/prosses/Tools/Prism/prism-4.7-linux64/bin/prism", "/home/prosses/Tools/Prism/prism-4.7-linux64/bin/prism",
		    automaton_file,
		    properties_file,
		    "-cuddmaxmem", "13g",
		    (char *)0);
	    
	    exit(0);
	}
	else
	{
	    int status;
	    pid_t c_pid = waitpid(pid,NULL,0);
	    
	    destroy_fsm(fsm);
	    free(fsm);
	    destroy_dep_graph(d_graph);
	    free(d_graph);
	}
    }
  
}

void generate_low_level_automata_statistics(struct knowledge_base *k_b)
{

    /*
    struct sec_analysis_ctx *sec_ctx=knowledge_base_get_res(k_b,"Security_Context");

    printf("Fragments:\n");
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	for(int j=0;j<sec_ctx->component_contexts[i].num_se_fragments;j++)
	{
	    struct attack_fragment *frag=sec_ctx->component_contexts[i]->se_fragments[j];
	    if(frag->lag & frag->marked)
	    {
		printf("|Single_Exploit_%s|",sec_ctx->attack_fragments[i].comp->comp_name);
	    }
	    
	}
		    printf("|Multi_Exploit_%s|",sec_ctx->attack_fragments[i].comp->comp_name);
		    printf("|Component_Impact_%s|",sec_ctx->attack_fragments[i].comp->comp_name);
		    break;
		}
		printf("%zu|",sec_ctx->attack_fragments[i].fsm->num_actions);
		printf("\n");
	}
    }
    */
}


void auto_gen(struct knowledge_base *k_b)
{

    struct timespec start, finish;
    double elapsed;
    clock_gettime(CLOCK_MONOTONIC, &start);
    generate_low_level_automata(k_b);
    clock_gettime(CLOCK_MONOTONIC, &finish);
    elapsed = (finish.tv_sec - start.tv_sec);
    elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
    printf("Lower level automata generation and verification  time:%f\n", elapsed);
    generate_low_level_automata_statistics(k_b);
    
    clock_gettime(CLOCK_MONOTONIC, &start);
    generate_high_level_automaton(k_b);
    clock_gettime(CLOCK_MONOTONIC, &finish);
    elapsed = (finish.tv_sec - start.tv_sec);
    elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;

    printf("High level automata generation and verification  time:%f\n", elapsed);
}



/*
  void generate_compromise_graphs_automata(struct knowledge_base *k_b)
  {
  //For every compromise graph,create 3 states I,R,IC

  char *dir="Graphs/compromise";
  struct dirent *dp;
  DIR *dfd;
    
  if ((dfd = opendir(dir)) == NULL)
  {
  fprintf(stderr, "Can't open %s\n", dir);
  }
    
  char filename_qfd[512];
 
  while ((dp = readdir(dfd)) != NULL)
  {
  struct stat stbuf ;
  sprintf( filename_qfd , "%s/%s",dir,dp->d_name) ;
  if( stat(filename_qfd,&stbuf ) == -1 )
  {
  printf("Unable to stat file: %s\n",filename_qfd) ;
  continue ;
  }
	
  if ( ( stbuf.st_mode & S_IFMT ) == S_IFDIR )
  {

  if(!strcmp(dp->d_name,".")==0 && !strcmp(dp->d_name,"..")==0)
  {
  pid_t pid=fork();

	       

  if(pid<0)
  {
  fprintf(stderr,"Couldn't fork to generate component interaction graph\n");
  }
  else if(pid==0)
  {
		    
		    
  struct lag* compromise_graph=lag_from_directory(filename_qfd, dp->d_name, LAG_COMPROMISE);

  struct lag_node *compromised_node=lag_find_node(compromise_graph,"isCompromised");
  struct lag_node *rule_node=compromise_graph->nodes[compromised_node->parent_ids[0]];
		    
  struct lag_node *reachable_node=lag_find_node(compromise_graph,"reachable");
		    
  //generate the automaton
		    
  struct fsm *fsm=malloc(sizeof(struct fsm));
  init_fsm(fsm,"FSM","");
		    
		
  struct fsm_state *Istate=malloc(sizeof(struct fsm_state));
  init_fsm_state(Istate,"Init","Init","1");
  fsm_add_state(fsm,Istate);
		    
		    
		
  struct fsm_state *Rstate=malloc(sizeof(struct fsm_state));
  init_fsm_state(Rstate,"Reachable","Reachable","1");
  fsm_add_state(fsm,Rstate);
		    
  struct fsm_state *ICstate=malloc(sizeof(struct fsm_state));
  init_fsm_state(ICstate,"Compromised","Compromised","1");
  fsm_add_state(fsm,ICstate);
		
  struct fsm_transition *I_to_R=malloc(sizeof(struct fsm_transition));
  init_fsm_transition(I_to_R,Istate->id,Rstate->id,80,NULL,NULL,NULL);
  fsm_add_trans(fsm,I_to_R);
		    
  struct fsm_transition *R_to_IC=malloc(sizeof(struct fsm_transition));
  init_fsm_transition(R_to_IC,Rstate->id,ICstate->id,80,NULL,NULL,NULL);
  fsm_add_trans(fsm,R_to_IC);
		
  fsm_set_inital(fsm,Istate->id);
		    
  chdir("Automata/compromise");
  char **queries=malloc(1*sizeof(char*));
  queries[0]="Pr[#&lt;=1000](&lt;&gt;fsm.Compromised)";
		    

  char *fname=malloc(strlen(dp->d_name)+5);
  sprintf(fname,"%s.xml",dp->d_name);
  fsm_to_xml_file(fname,fsm,"","fsm=FSM();\n system fsm;\n",queries,1);
  free(queries);
		
  destroy_lag(compromise_graph);
  free(compromise_graph);
  destroy_fsm(fsm);
  free(fsm);
		    
  char *res=malloc(strlen(dp->d_name)+5);
  sprintf(res,"%s.res",dp->d_name);
  freopen(res, "a", stdout);
  execlp ("/home/prosses/Tools/UPPAAL/uppaal64-4.1.24/bin-Linux/verifyta", "verifyta",
  fname, "-q",(char *)0);
		    
  }
		
  int status;
  while ((pid = wait(&status)) > 0);
		
		
  }
	    
  }
	
  }
  if(dfd)
  {
  closedir(dfd);
  }
    
  }
  void generate_basic_attack_automata(struct knowledge_base *k_b)
  {
  //For every basic attack graph, create 3 states I,IC,BA
  char *dir="Graphs/basic_attack";
  struct dirent *dp;
  DIR *dfd;
    
  if ((dfd = opendir(dir)) == NULL)
  {
  fprintf(stderr, "Can't open %s\n", dir);
  }
    
  char filename_qfd[512];
 
  while ((dp = readdir(dfd)) != NULL)
  {
  struct stat stbuf ;
  sprintf( filename_qfd , "%s/%s",dir,dp->d_name) ;
  if( stat(filename_qfd,&stbuf ) == -1 )
  {
  printf("Unable to stat file: %s\n",filename_qfd) ;
  continue ;
  }
	
  if ( ( stbuf.st_mode & S_IFMT ) == S_IFDIR )
  {

  if(!strcmp(dp->d_name,".")==0 && !strcmp(dp->d_name,"..")==0)
  {
  pid_t pid=fork();

	       

  if(pid<0)
  {
  fprintf(stderr,"Couldn't fork to generate component interaction graph\n");
  }
  else if(pid==0)
  {
		    
		    
  struct lag* basic_attack_graph=lag_from_directory(filename_qfd, dp->d_name, LAG_BASIC_ATCK);

  struct lag_node *compromised_node=lag_find_node(basic_attack_graph,"isCompromised");
		    
  struct lag_node *basic_attack_node=lag_find_node(basic_attack_graph,"basicAttack");
  struct lag_node *rule_node=basic_attack_graph->nodes[basic_attack_node->parent_ids[0]];
		    
  //generate the automaton
		    
  struct fsm *fsm=malloc(sizeof(struct fsm));
  init_fsm(fsm,"FSM","");
		    
		
  struct fsm_state *Istate=malloc(sizeof(struct fsm_state));
  init_fsm_state(Istate,"Init","Init","1");
  fsm_add_state(fsm,Istate);
		    
		    
		
		    
		    
  struct fsm_state *ICstate=malloc(sizeof(struct fsm_state));
  init_fsm_state(ICstate,"Compromised","Compromised","1");
  fsm_add_state(fsm,ICstate);

  struct fsm_state *BAstate=malloc(sizeof(struct fsm_state));
  init_fsm_state(BAstate,"BasicAttack","BasicAttack","1");
  fsm_add_state(fsm,BAstate);
		
  struct fsm_transition *I_to_IC=malloc(sizeof(struct fsm_transition));
  init_fsm_transition(I_to_IC,Istate->id,ICstate->id,80,NULL,NULL,NULL);
  fsm_add_trans(fsm,I_to_IC);
		    
  struct fsm_transition *IC_to_BA=malloc(sizeof(struct fsm_transition));
  init_fsm_transition(IC_to_BA,ICstate->id,BAstate->id,80,NULL,NULL,NULL);
  fsm_add_trans(fsm,IC_to_BA);
		
  fsm_set_inital(fsm,Istate->id);
		    
  chdir("Automata/basic_attack");
  char **queries=malloc(1*sizeof(char*));
  queries[0]="Pr[#&lt;=1000](&lt;&gt;fsm.BasicAttack)";
		    

  char *fname=malloc(strlen(dp->d_name)+5);
  sprintf(fname,"%s.xml",dp->d_name);
  fsm_to_xml_file(fname,fsm,"","fsm=FSM();\n system fsm;\n",queries,1);
  free(queries);
		
  destroy_lag(basic_attack_graph);
  free(basic_attack_graph);
  destroy_fsm(fsm);
  free(fsm);
		    
  char *res=malloc(strlen(dp->d_name)+5);
  sprintf(res,"%s.res",dp->d_name);
  freopen(res, "a", stdout);
  execlp ("/home/prosses/Tools/UPPAAL/uppaal64-4.1.24/bin-Linux/verifyta", "verifyta",
  fname, "-q",(char *)0);
		    
  }
		
  int status;
  while ((pid = wait(&status)) > 0);
		
		
  }
	    
  }
	
  }
  if(dfd)
  {
  closedir(dfd);
  }
  }
  void generate_complex_attack_automata(struct knowledge_base *k_b)
  {
  //For every prerequisite, create I-->Pi,P(Pi), and Pi->I, 
  char *dir="Graphs/complex_attack";
  struct dirent *dp;
  DIR *dfd;
    
  if ((dfd = opendir(dir)) == NULL)
  {
  fprintf(stderr, "Can't open %s\n", dir);
  }
    
  char filename_qfd[512];
 
  while ((dp = readdir(dfd)) != NULL)
  {
  struct stat stbuf ;
  sprintf( filename_qfd , "%s/%s",dir,dp->d_name) ;
  if( stat(filename_qfd,&stbuf ) == -1 )
  {
  printf("Unable to stat file: %s\n",filename_qfd) ;
  continue ;
  }
	
  if ( ( stbuf.st_mode & S_IFMT ) == S_IFDIR )
  {

  if(!strcmp(dp->d_name,".")==0 && !strcmp(dp->d_name,"..")==0)
  {
  pid_t pid=fork();

	       

  if(pid<0)
  {
  fprintf(stderr,"Couldn't fork to generate component interaction graph\n");
  }
  else if(pid==0)
  {
		    
		    
  struct lag* complex_attack_graph=lag_from_directory(filename_qfd, dp->d_name, LAG_BASIC_ATCK);

  struct lag_node *complex_attack_node=lag_find_node(complex_attack_graph,"complexAttack");
  struct lag_node *rule_node=complex_attack_graph->nodes[complex_attack_node->parent_ids[0]];
		    
  //generate the automaton
		    
  struct fsm *fsm=malloc(sizeof(struct fsm));
  init_fsm(fsm,"FSM","");
		    
  //create Init state
  struct fsm_state *Istate=malloc(sizeof(struct fsm_state));
  init_fsm_state(Istate,"Init","Init","1");
  fsm_add_state(fsm,Istate);
  fsm_set_inital(fsm,Istate->id);
		    
  struct fsm_state *CAstate=malloc(sizeof(struct fsm_state));
  init_fsm_state(CAstate,"ComplexAttack","ComplexAttack","1");
  fsm_add_state(fsm,CAstate);

		    
  char*declarations=malloc(16+rule_node->num_parents*6);
  sprintf(declarations,"bool S[%lu]={",rule_node->num_parents);

  //S[0] && S[1]
  char *guard=malloc(20*rule_node->num_parents);
  memset(guard,0,20*rule_node->num_parents);
		    
  for(int i=0;i<rule_node->num_parents;i++)
  {
  //create prerequisite state
  struct lag_node *pre= complex_attack_graph->nodes[rule_node->parent_ids[i]];
  char *pre_state_name=malloc(16);
  sprintf(pre_state_name,"pre%d",i);
  struct fsm_state *pre_state=malloc(sizeof(struct fsm_state));
  init_fsm_state(pre_state,pre_state_name,pre_state_name,"1");
  fsm_add_state(fsm,pre_state);

  //create I to Pre transition
  char *pre_state_guard=malloc(16);
  sprintf(pre_state_guard,"S[%d]==false",i);
  char *pre_state_update=malloc(16);
  sprintf(pre_state_update,"S[%d]=true",i);
  struct fsm_transition *I_to_PRE=malloc(sizeof(struct fsm_transition));
  init_fsm_transition(I_to_PRE,Istate->id,pre_state->id,80,pre_state_guard,pre_state_update,NULL);
  fsm_add_trans(fsm,I_to_PRE);

  struct fsm_transition *PRE_to_I=malloc(sizeof(struct fsm_transition));
  init_fsm_transition(PRE_to_I,pre_state->id,Istate->id,100,NULL,NULL,NULL);
  fsm_add_trans(fsm,PRE_to_I);
  if(i<rule_node->num_parents-1)
  {
  strcat(declarations,"false,");
  char cur_guard[20];
  sprintf(cur_guard,"S[%d] &amp;&amp; ",i);
  strcat(guard,cur_guard);
  }
  else
  {
  strcat(declarations,"false");
  char cur_guard[20];
  sprintf(cur_guard,"S[%d]",i);
  strcat(guard,cur_guard);
  }

			
			
			
  }
  strcat(declarations,"};");

//		    printf("GUARD %s\n",guard);
fsm_set_declarations(fsm,declarations);
		    
//Create the Init to ComplexAttack transition
struct fsm_transition *I_to_CA=malloc(sizeof(struct fsm_transition));
init_fsm_transition(I_to_CA,Istate->id,CAstate->id,80,guard,NULL,NULL);
fsm_add_trans(fsm,I_to_CA);
		   



		    
chdir("Automata/complex_attack");
char **queries=malloc(1*sizeof(char*));
queries[0]="Pr[#&lt;=1000](&lt;&gt;fsm.ComplexAttack)";
		    

char *fname=malloc(strlen(dp->d_name)+5);
sprintf(fname,"%s.xml",dp->d_name);
fsm_to_xml_file(fname,fsm,"","fsm=FSM();\n system fsm;\n",queries,1);
free(queries);
		
destroy_lag(complex_attack_graph);
free(complex_attack_graph);
destroy_fsm(fsm);
free(fsm);
		    
char *res=malloc(strlen(dp->d_name)+5);
sprintf(res,"%s.res",dp->d_name);
freopen(res, "a", stdout);
execlp ("/home/prosses/Tools/UPPAAL/uppaal64-4.1.24/bin-Linux/verifyta", "verifyta",
fname, "-q",(char *)0);
		    
}
		
int status;
while ((pid = wait(&status)) > 0);
		
		
}
	    
}
	
}
if(dfd)
{
closedir(dfd);
}
}
void generate_component_impact_automata(struct knowledge_base *k_b)
{
    //for every basic and complex attack make 
}
void generate_component_security_automata(struct knowledge_base *k_b)
{
    
}

void generate_overall_security_automaton(struct knowledge_base *k_b)
{
    
}
*/
