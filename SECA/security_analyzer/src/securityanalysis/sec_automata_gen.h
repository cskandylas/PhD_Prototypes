#include "knowledge_base.h"


void auto_gen(struct knowledge_base *k_b);
void one_auto_gen(struct knowledge_base *k_b,char *prop_file);
void generate_compromise_graphs_automata(struct knowledge_base *k_b);
void generate_basic_attack_automata(struct knowledge_base *k_b);
void generate_complex_attack_automata(struct knowledge_base *k_b);
void generate_component_impact_automata(struct knowledge_base *k_b);
void generate_component_security_automata(struct knowledge_base *k_b);
void generate_overall_security_automaton(struct knowledge_base *k_b);
