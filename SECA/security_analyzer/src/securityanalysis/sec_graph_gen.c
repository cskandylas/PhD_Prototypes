#define _XOPEN_SOURCE 501


#include "sec_graph_gen.h"
#include "Queue.h"

#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>  
#include <stdlib.h>    
#include <sys/wait.h>
#include <stdio.h>
#include <string.h>

#include <sys/mman.h>

#include "fol_resources.h"
#include "named_resource.h"
#include "sec_analysis.h"
#include "dep_graph.h"

#include "vulnerability_analysis.h"
#include "attack_rule_analysis.h"
#include "sec_magic_graph.h"

#include "tokenize.h"
#include "rbtree.h"

#define MAX_PROCS 8


void update_graph_probabilities(char *fname);

struct shared
{
    int idx;
    pid_t pid;
    struct attack_fragment *frag;
};

void* create_shared_memory(size_t size);

void system_arch_to_preds(struct knowledge_base *k_b)
{
    struct arc_system *sys=k_b->sys;
    struct sec_predicate *component_preds=malloc(sys->num_components*sizeof(struct sec_predicate));
    struct sec_predicate *invocation_preds=malloc(sys->num_invocations*sizeof(struct sec_predicate));
    //struct sec_predicate *attacker_located_preds=malloc(sys->num_invocations*sizeof(struct sec_predicate));
    struct generic_queue *attacker_located_pred_q=malloc(sizeof(struct generic_queue));
    queue_init(attacker_located_pred_q);
    
    size_t num_vulns=0;
    for(int i=0;i<sys->num_components;i++)
    {
	//isComponent(comp).
	init_predicate(&component_preds[i],"isComponent");
	//POSSIBLY DEEP COPY ALL PREDICATE ELEMENTS
	predicate_add_term(&component_preds[i],sys->components[i]->comp_name);
	for(int j=0;j<sys->components[i]->num_interfaces;j++)
	{
	    struct arc_interface *iface=sys->components[i]->interfaces[j];
	    struct arc_property *prop=iface_find_property(iface,"exploit");
	    if(prop)
	    {
		struct tokenizer comma_tok;
		tokenizer_init(&comma_tok,prop->value.str_value,",");
		char *next_exploit;
		while((next_exploit=next_token(&comma_tok)))
		{
		    //printf("Exploit:%s\n",next_exploit);
		    free(next_exploit);
		    num_vulns++;
		}
	    }
	}
	/*
	if(strncmp(sys->components[i]->comp_name,"realmClient",11)==0 ||
	   strncmp(sys->components[i]->comp_name,"gameClient",10)==0  ||
	   strncmp(sys->components[i]->comp_name,"loginClient",11)==0 ||
	   strncmp(sys->components[i]->comp_name,"frontend",7)==0     ||
	   strncmp(sys->components[i]->comp_name,"internet",8)==0     
	    )    
	*/
	if(strncmp(sys->components[i]->comp_name,"client",strlen("client"))==0)
	{
	    struct sec_predicate *attacker_located_pred=malloc(sizeof(struct sec_predicate));
	    init_predicate(attacker_located_pred,"attackerLocated");
	    predicate_add_term(attacker_located_pred,sys->components[i]->comp_name);
	    queue_push_back(attacker_located_pred_q,attacker_located_pred);
	}
	
    }
    struct sec_predicate *attacker_located_preds=malloc(attacker_located_pred_q->size*sizeof(struct sec_predicate));
    //unload queue and destroy it
    struct queue_entry *entry=attacker_located_pred_q->head;
    int count=0;
    while(entry)
    {
	struct sec_predicate *attacker_located_pred=entry->elem;
	attacker_located_preds[count]=*attacker_located_pred;
	count++;
	entry=entry->next;
    }
    struct named_res *al_preds_res=malloc(sizeof(struct named_res));
    named_res_init(al_preds_res,"AttackerLocated_Predicates",attacker_located_pred_q->size,attacker_located_preds);
    knowledge_base_add_res(k_b,al_preds_res);

    queue_destroy(attacker_located_pred_q);
    

    struct named_res *comp_preds_res=malloc(sizeof(struct named_res));
    named_res_init(comp_preds_res,"Component_Predicates",sys->num_components,component_preds);
    knowledge_base_add_res(k_b,comp_preds_res);
    
    for(int i=0;i<sys->num_invocations;i++)
    {

	//methodInvocation(iface,from,to).
	init_predicate(&invocation_preds[i],"methodInvocation");
	//POSSIBLY DEEP COPY ALL PREDICATE ELEMENTS
	predicate_add_term(&invocation_preds[i],sys->invocations[i]->iface->if_name);
	predicate_add_term(&invocation_preds[i],sys->invocations[i]->from->comp_name);
	predicate_add_term(&invocation_preds[i],sys->invocations[i]->to->comp_name);
    }

    struct named_res *invo_preds_res=malloc(sizeof(struct named_res));
    named_res_init(invo_preds_res,"Invocation_Predicates",sys->num_invocations,invocation_preds);
    knowledge_base_add_res(k_b,invo_preds_res);


    if(num_vulns>0)
    {
	struct sec_predicate *vulnexists_preds=malloc(num_vulns*sizeof(struct sec_predicate));
	size_t idx=0;
	for(int i=0;i<sys->num_components;i++)
	{
	    for(int j=0;j<sys->components[i]->num_interfaces;j++)
	    {
		struct arc_interface *iface=sys->components[i]->interfaces[j];
		struct arc_property *prop=iface_find_property(iface,"exploit");
		if(prop)
		{

		    struct tokenizer comma_tok;
		    tokenizer_init(&comma_tok,prop->value.str_value,",");
		    char *next_exploit;
		    while((next_exploit=next_token(&comma_tok)))
		    {
			init_predicate(&vulnexists_preds[idx],"vulnExists");
			predicate_add_term(&vulnexists_preds[idx],sys->components[i]->comp_name);
			predicate_add_term(&vulnexists_preds[idx],iface->if_name);
			predicate_add_term(&vulnexists_preds[idx],next_exploit);
			idx++;	
			
		    }
		    
		    
		    
		    
		}
	    }
	}

	struct named_res *vuln_preds_res=malloc(sizeof(struct named_res));
	named_res_init(vuln_preds_res,"Vulnerability_Predicates",num_vulns,vulnexists_preds);
	knowledge_base_add_res(k_b,vuln_preds_res);
    }
    
    
}





void graph_gen(struct knowledge_base *k_b)
{
    
    //generate all attack graphs
    generate_interaction_graph(k_b);
    //exit(-1);
    //analyse_vulnerabilities(k_b);
    generate_exploit_graphs(k_b);
}


void run_mulval(struct sec_analysis_ctx *sec_ctx,struct  attack_fragment *frag)
{

    
    if(!frag || frag->lag)
    {
	//printf("M:Skipping:%s\n",fragment_to_str(frag));
	exit(0);
    }
    else
    {
	//printf("M:Building:%s:%p\n",fragment_to_str(frag),frag->lag);
    }
    
    
    char input_file[256]="";
    char target_dir[256]="";
    char rules_file[256]="";
    char goal[256]="";
    
    if(frag->type==SINGLE_EXPLOIT)
    {
	sprintf(input_file,"/home/prosses/Tools/security_analyzer/Predicates/single_exploit/%s_%s.P",frag->comp->comp_name,frag->goal);
	sprintf(target_dir,"Graphs/single_exploit/%s_%s",frag->comp->comp_name,frag->goal);
	strcpy(rules_file,"/home/prosses/Tools/security_analyzer/Rules/single_exploit_rules.P");
	sprintf(goal,"singleExploit(%s,%s)",frag->comp->comp_name,frag->goal);
    }
    else if(frag->type==MULTI_EXPLOIT)
    {
	sprintf(input_file,"/home/prosses/Tools/security_analyzer/Predicates/multi_exploit/%s_%s.P",frag->comp->comp_name,frag->goal);
	sprintf(target_dir,"Graphs/multi_exploit/%s_%s",frag->comp->comp_name,frag->goal);
	strcpy(rules_file,"/home/prosses/Tools/security_analyzer/Rules/multi_exploit_rules.P");
	sprintf(goal,"multiExploit(%s,%s)",frag->comp->comp_name,frag->goal);
    }
    else if(frag->type==COMPONENT_IMPACT)
    {
	sprintf(input_file,"/home/prosses/Tools/security_analyzer/Predicates/component_impact/%s_%s.P",frag->comp->comp_name,frag->goal);
	sprintf(target_dir,"Graphs/component_impact/%s_%s",frag->comp->comp_name,frag->goal);
	strcpy(rules_file,"/home/prosses/Tools/security_analyzer/Rules/component_impact_rules.P");
	sprintf(goal,"componentImpact(%s,%s)",frag->comp->comp_name,frag->goal);
	/*
	if(sec_ctx->dep_graph->nodes[frag->id].num_dependencies==0)
	{
	    fprintf(stderr,"No dependencies means no attack is possible for %s, skipping\n",frag->comp->comp_name);
	    exit(0);
	}
	*/
    }

    int result = mkdir(target_dir, 0777);
    //printf("%s\n%s\n%s\n%s\n",input_file,rules_file,target_dir,goal);
    //FILE *fp=freopen("/dev/null", "w", stdout); 

    
	execlp ("/home/prosses/Tools/security_analyzer/xsb_integration_test", "/home/prosses/Tools/security_analyzer/xsb_integration_test",
		input_file,
		rules_file,
		target_dir,
	        goal,
		(char *)0);
	exit(0);
    
    

    /*
	freopen("/dev/null", "w", stdout); 
	
	result=chdir(target_dir);
	execlp ("/bin/bash", "bash",
		"/home/prosses/Tools/mulval/utils/graph_gen.sh",
		"-r", rules_file,
		input_file,
		"-g", goal ,
		"-l",
		(char *)0);
	exit(-1);
    */
}


void update_predicates(struct attack_fragment *frag,struct knowledge_base *k_b)
{
    char input_file[256];
    char res_name[256];
    char goal[256];

    if(frag->type==SINGLE_EXPLOIT)
    {
	sprintf(input_file,"Graphs/single_exploit/%s_%s/VERTICES.CSV",frag->comp->comp_name,frag->goal);
	sprintf(res_name,"SingleExploit_%s_Predicates",frag->comp->comp_name);
	sprintf(goal,"singleExploit");
	
    }
    if(frag->type==MULTI_EXPLOIT)
    {
	sprintf(input_file,"Graphs/multi_exploit/%s_%s/VERTICES.CSV",frag->comp->comp_name,frag->goal);
	sprintf(res_name,"MultiExploit_%s_Predicates",frag->comp->comp_name);
	sprintf(goal,"multiExploit");
    }
    if(frag->type==COMPONENT_IMPACT)
    {
	sprintf(input_file,"Graphs/component_impact/%s_%s/VERTICES.CSV",frag->comp->comp_name,frag->goal);
	sprintf(res_name,"ComponentImpact_%s_Predicates",frag->comp->comp_name);
	sprintf(goal,"componentImpact");
    }
    
    
    //printf("Reading input file:%s\n",input_file);
    size_t num_preds=0;
    struct sec_predicate *preds=predicates_from_csv(input_file,&num_preds);
    if(num_preds>0)
    {
       update_graph_probabilities(input_file);
	size_t num_goals;
	struct sec_predicate *goal_preds=predicates_filter(preds,num_preds,goal,&num_goals);
	//ugly hack not sure if this is what I should be doing.
	
	for(int i=0;i<frag->num_preds;i++)
	    destroy_predicate(&frag->preds[i]);

	
	free(frag->preds);
	frag->preds=goal_preds;
	frag->num_preds=num_goals;

	
	//printf("Updated %s predicates for fragment %s %zu total predicates addded\n",goal,frag->comp->comp_name, num_goals);
	/*
	for(int i=0;i<frag->num_preds;i++)
	{

	    char *pred_str=predicate_to_str(&frag->preds[i]);
	    printf("Added Predicate %s\n",pred_str);
	    free(pred_str);
	}
	*/
	

    }
    else
    {
	fprintf(stderr,"No matching predicates found in %s assuming empty attack graph and carrying on!\n",input_file);
	frag->preds=NULL;
	frag->num_preds=0;
    }
    
    for(int i=0;i<num_preds;i++)
    {
	destroy_predicate(&preds[i]);
    }
    free(preds);
}

void prepare_input(struct attack_fragment *frag, struct sec_analysis_ctx *sec_ctx, struct knowledge_base *k_b)
{
    char input_file[256];
    if(frag->type==MULTI_EXPLOIT)
    {
	
	sprintf(input_file,"/home/prosses/Tools/security_analyzer/Predicates/multi_exploit/%s_%s.P",frag->comp->comp_name,frag->goal);
	remove(input_file);
	//just copy in the prerequisites;
	struct dep_node *d_node=sec_ctx->dep_graph->nodes[frag->id];
	
	for(int i=0;i<d_node->num_dependencies;i++)
	{
	    predicates_to_file(input_file,d_node->dependencies[i]->frag->preds,d_node->dependencies[i]->frag->num_preds,"a");
	}
	//also add the can reach predicates
	struct sec_predicate *can_reach_preds=knowledge_base_get_res(k_b,"CanReach_Predicates");
	size_t num_can_reach=knowledge_base_num_res(k_b,"CanReach_Predicates");
	predicates_to_file(input_file,can_reach_preds,num_can_reach,"a");
	
    }
    else if(frag->type==COMPONENT_IMPACT)
    {
	//only look at SingleExplot and MultiExploit of the same component
	sprintf(input_file,"/home/prosses/Tools/security_analyzer/Predicates/component_impact/%s_%s.P",frag->comp->comp_name,frag->goal);
	remove(input_file);
	
	
	    struct dep_node *d_node=sec_ctx->dep_graph->nodes[frag->id];
	    if(d_node)
	    {
		for(int i=0;i<d_node->num_dependencies;i++)
		{
		    predicates_to_file(input_file,d_node->dependencies[i]->frag->preds,d_node->dependencies[i]->frag->num_preds,"a");
		}
	    }
	
    }
}


void generate_interaction_graph(struct knowledge_base *k_b)
{
    char *interaction_graph_path="Graphs/interaction";
    //system_arch_to_preds(k_b);
    
    struct sec_predicate *component_preds=knowledge_base_get_res(k_b,"Component_Predicates");
    struct sec_predicate *invocation_preds=knowledge_base_get_res(k_b,"Invocation_Predicates");
    struct sec_predicate *al_preds=knowledge_base_get_res(k_b,"AttackerLocated_Predicates");
    size_t num_al=knowledge_base_num_res(k_b,"AttackerLocated_Predicates");
    
    
    predicates_to_file("Predicates/interaction/interaction_input.P",component_preds,k_b->sys->num_components,"w");
    predicates_to_file("Predicates/interaction/interaction_input.P",invocation_preds,k_b->sys->num_invocations,"a");
    predicates_to_file("Predicates/interaction/interaction_input.P",al_preds,num_al,"a");

    //fork to call MULVAL
    pid_t pid=fork();

    if(pid<0)
    {
	fprintf(stderr,"Couldn't fork to generate component interaction graph\n");
	exit(-1);
    }
    else if(pid==0)
    {
	//int result=chdir("Graphs/interaction");
	FILE *fp=freopen("/dev/null", "w", stdout); 
	//execlp("/bin/ls","ls","../../", (char*)0);

	execlp ("/home/prosses/Tools/security_analyzer/xsb_integration_test", "/home/prosses/Tools/security_analyzer/xsb_integration_test",
		"Predicates/interaction/interaction_input.P",
		"Rules/interaction_rules.P",
		"Graphs/interaction",
	        "reachable(_,_)",
	    (char *)0);

	/*
	execlp ("/bin/bash", "bash",
		"/home/prosses/Tools/mulval/utils/graph_gen.sh",
		"-r", "../../Rules/interaction_rules.P",
		"../../Predicates/interaction/interaction_input.P",
		"-g", "reachable(_,_)",
 		"-l",
		(char *)0);
	*/
    }
    
    int status;
    while ((pid = wait(&status)) > 0);
    //printf("generated component interaction graph in:%s\n",interaction_graph_path);


    size_t num_preds;
    struct sec_predicate *preds=predicates_from_csv("Graphs/interaction/VERTICES.CSV",&num_preds);
    size_t num_reachable;
    struct sec_predicate *reachable=predicates_filter(preds,num_preds,"reachable",&num_reachable);
    struct named_res *reachable_preds_res=malloc(sizeof(struct named_res));
    named_res_init(reachable_preds_res,"Reachable_Predicates",num_reachable,reachable);
    knowledge_base_add_res(k_b,reachable_preds_res);

    
    size_t num_can_reach;
    struct sec_predicate *can_reach=predicates_filter(preds,num_preds,"canReach",&num_can_reach);

    struct named_res *can_reach_preds_res=malloc(sizeof(struct named_res));
    named_res_init(can_reach_preds_res,"CanReach_Predicates",num_can_reach,can_reach);
    knowledge_base_add_res(k_b,can_reach_preds_res);
    
    
    for(int i=0;i<num_preds;i++)
    {
	destroy_predicate(&preds[i]);
    }
    free(preds);


    //just map it to the actual attack fragment to make the cleanup easier.
    //quick hack to avoid deep copying.
    struct sec_analysis_ctx *sec_ctx=knowledge_base_get_res(k_b,"Security_Context");
    //POSSIBLE BUG HERE.
    //free(sec_ctx->attack_fragments[0].preds);
}

char *read_file(char *filename);


struct action_probs
{
    char *action;
    double prob;
};

//QUICK HACK FOR NOW, MOVE LATER
#include "fsm_resources.h"

void update_graph_probabilities(char *fname)
{


    struct action_probs actions[1024];
    size_t num_actions=0;
    
    //printf("\n");
    struct tokenizer line_tok;
    
    char *probs_text=read_file("probs");
    tokenizer_init(&line_tok,probs_text,"\n");
    char *p_line=NULL;
    while((p_line=next_token(&line_tok))!=NULL)
    {
	struct tokenizer space_tok;
	tokenizer_init(&space_tok,p_line," ");
	
	char *action=next_token(&space_tok);
	char *prob_str=next_token(&space_tok);
	if(prob_str)
	{
	    
	    double prob=atof(prob_str);
	    actions[num_actions].action=action;
	    actions[num_actions].prob=prob;
	    num_actions++;
	    //printf("%s %s %f\n",action,prob_str,prob);
	    //printf("Prob in file:%f\n",prob);
	}
	else
	{
	    printf("Could not find probability %s, panic. Exiting.\n",action);
	    exit(-3);
	}
	free(p_line);
    }
    
    FILE *fp=fopen("VERTICES_ADJ","w");
    char *text=read_file(fname);
    //printf("fname:%s",fname);
    //printf("Text:%s",text);
    tokenizer_init(&line_tok,text,"\n");
    char *line;
    size_t n_lines=0;
    size_t n_predicates=0;
    while((line=next_token(&line_tok))!=NULL)
    {

	//printf("%s\n",line);
	struct tokenizer quote_tok;
	tokenizer_init(&quote_tok,line,"\"");
	char *discard=next_token(&quote_tok);
	char *thing=next_token(&quote_tok);
	
	

	float prob=atof(strrchr(line,',')+1);
	if(prob - 0.0 < 0.005 && strncmp(thing,"RULE",4)!=0)
	{
	    char *cap=make_capability(thing);
	    
	    bool panic=true;
	    
	    char *action=strrchr(cap, '_')+1;
	    //printf("Prob for:%s\n",action);
	    for(int i=0;i<num_actions;i++)
	    {
		//printf("[%s]-[%s]\n",action,actions[i].action);
		
		if(strcmp(actions[i].action,action)==0)
		{
		    //printf("Found:%s\n",action);
		    panic=false;
		    prob=actions[i].prob;
		    break;
		}
	    }

	    
	    
	    char *tmp=line;
	    char *new_line=malloc(strlen(line)+32);
	    snprintf(new_line,strlen(line)+8,"%s%.7f",line,prob);
	    free(line);
	    free(discard);
	    free(thing);
	    line=new_line;
	    
	}
	fprintf(fp,"%s\n",line);
	
    }
    fclose(fp);

    remove(fname);
    rename("VERTICES_ADJ", fname);
    //read VERTICES AND WRITE BACK PROBS
    //for
}




void one_graph_gen(struct knowledge_base *k_b)
{

    
    char *interaction_graph_path="Graphs/interaction";
    system_arch_to_preds(k_b);
    
    struct sec_predicate *component_preds=knowledge_base_get_res(k_b,"Component_Predicates");
    struct sec_predicate *invocation_preds=knowledge_base_get_res(k_b,"Invocation_Predicates");
    struct sec_predicate *al_preds=knowledge_base_get_res(k_b,"AttackerLocated_Predicates");
    size_t num_al=knowledge_base_num_res(k_b,"AttackerLocated_Predicates");
     
    predicates_to_file("Predicates/interaction/interaction_input.P",component_preds,k_b->sys->num_components,"w");
    predicates_to_file("Predicates/interaction/interaction_input.P",invocation_preds,k_b->sys->num_invocations,"a");
    predicates_to_file("Predicates/interaction/interaction_input.P",al_preds,num_al,"a");

    //fork to call MULVAL
    pid_t pid=fork();

    if(pid<0)
    {
	fprintf(stderr,"Couldn't fork to generate component interaction graph\n");
	exit(-1);
    }
    else if(pid==0)
    {
	//int result=chdir("Graphs/interaction");
	FILE *fp=freopen("/dev/null", "w", stdout); 
	//execlp("/bin/ls","ls","../../", (char*)0);

	execlp ("/home/prosses/Tools/security_analyzer/xsb_integration_test", "/home/prosses/Tools/security_analyzer/xsb_integration_test",
		"Predicates/interaction/interaction_input.P",
		"Rules/interaction_rules.P",
		"Graphs/interaction",
	        "reachable(_,_)",
	    (char *)0);
	exit(0);
    }
    
    int status;
    while ((pid = wait(&status)) > 0);
    //printf("generated component interaction graph in:%s\n",interaction_graph_path);


    size_t num_preds;
    struct sec_predicate *preds=predicates_from_csv("Graphs/interaction/VERTICES.CSV",&num_preds);
    size_t num_reachable;
    struct sec_predicate *reachable=predicates_filter(preds,num_preds,"reachable",&num_reachable);
    size_t num_can_reach;
    struct sec_predicate *can_reach=predicates_filter(preds,num_preds,"canReach",&num_can_reach);
    
    struct sec_predicate *vuln_preds=knowledge_base_get_res(k_b,"Vulnerability_Predicates");
    size_t num_vulns=knowledge_base_num_res(k_b,"Vulnerability_Predicates");
    

    
    for(int i=0;i<num_preds;i++)
    {
	destroy_predicate(&preds[i]);
    }
    free(preds);




//    exit(-2);
    predicates_to_file("Predicates/all_in_one.P",reachable,num_reachable,"w");
    predicates_to_file("Predicates/all_in_one.P",vuln_preds,num_vulns,"a");
    predicates_to_file("Predicates/all_in_one.P",can_reach,num_can_reach,"a");
    for(int i=0;i<num_reachable;i++)
    {
	destroy_predicate(&reachable[i]);
    }
    free(reachable);
    
    //fork to call MULVAL
    pid=fork();

    if(pid<0)
    {
	fprintf(stderr,"Couldn't fork to generate component interaction graph\n");
	exit(-1);
    }
    else if(pid==0)
    {
	//int result=chdir("Graphs/all_in_one");
	//freopen("/dev/null", "w", stdout);

	execlp ("/home/prosses/Tools/security_analyzer/xsb_integration_test", "/home/prosses/Tools/security_analyzer/xsb_integration_test",
		"Predicates/all_in_one.P",
		"Rules/rules_merged.P",
		"Graphs/all_in_one",
		"componentImpact(_,_)",
	    (char *)0);
	exit(0);

	

    }
    
    status=0;
    while ((pid = wait(&status)) > 0);
    update_graph_probabilities("Graphs/all_in_one/VERTICES.CSV");
}


void create_new_fragment_r(struct sec_analysis_ctx *sec_ctx, struct rb_node *new_frag_node)
{

    
    if(!new_frag_node)
	return;
    
    if(new_frag_node->left)
    {
	create_new_fragment_r(sec_ctx,new_frag_node->left);
    }
    if(new_frag_node->right)
    {
	create_new_fragment_r(sec_ctx,new_frag_node->right);
    }

    struct dependency *dep=new_frag_node->data;
    //printf("Creating new fragment:-%s\n",dep->top);
    //for(int i=0;i<dep->num_deps;i++)
    //printf("%s,",dep->depends_on[i]);
    //printf("\n");
    
    struct tokenizer tok;
    tokenizer_init(&tok,dep->top,"_");
    char *fragment_type=next_token(&tok);
    char *comp_name=next_token(&tok);
    char *goal=next_token(&tok);
    
    struct arc_component* comp= sys_find_component(sec_ctx->k_b->sys,comp_name);
    if(!comp)
    {
	fprintf(stderr,"Could not find component: %s, exiting\n",comp_name);
	exit(-1);
    }
    
    struct sec_component_ctx *comp_ctx= get_component_context_for_component(sec_ctx,comp_name);
    if(!comp_ctx)
    {
	fprintf(stderr,"Could not find component context for: %s, exiting\n",comp_name);
	exit(-1);
    }
    struct attack_fragment *new_frag=get_fragment_from_goal(comp_ctx,str_to_fragment_type(fragment_type),goal);
    if(new_frag==NULL)
    {
	new_frag=malloc(sizeof(struct attack_fragment));
	init_attack_fragment(new_frag,str_to_fragment_type(fragment_type));
	new_frag->comp=comp;
	new_frag->goal=goal;
	new_frag->marked=false;
	component_ctx_add_fragment(comp_ctx,new_frag);
	add_node_to_dep_graph(sec_ctx->dep_graph,dep->num_deps , new_frag);

    }
    else
    {
	
	//if the fragment id is invalid, assign a new one.
	/*
	if(new_frag->id >= sec_ctx->dep_graph->num_nodes || !sec_ctx->dep_graph->nodes[new_frag->id] || sec_ctx->dep_graph->nodes[new_frag->id]->frag!=new_frag )
	{
	    add_node_to_dep_graph(sec_ctx->dep_graph,dep->num_deps , new_frag);
	}
	*/
	
	
	
	
	
    }
	

    for(int i=0;i<dep->num_deps;i++)
    {
	tokenizer_init(&tok,dep->depends_on[i],"_");
	char *d_frag_type=next_token(&tok);
	char *d_comp_name=next_token(&tok);
	char *d_goal=next_token(&tok);
	struct sec_component_ctx *d_comp_ctx= get_component_context_for_component(sec_ctx,d_comp_name);
	if(!d_comp_ctx)
	{
	    fprintf(stderr,"Could not find component context for: %s, exiting\n",comp_name);
	    exit(-1);
	}
	
	struct attack_fragment *d_frag=get_fragment_from_goal(d_comp_ctx,str_to_fragment_type(d_frag_type), d_goal);
	
	free(d_frag_type);
	free(d_comp_name);
	free(d_goal);
	//if(!sec_ctx->dep_graph->nodes[new_frag->id]->done)
	//describe_fragment(new_frag);
	//describe_fragment(d_frag);
	//if(sec_ctx->dep_graph->nodes[new_frag->id]->num_dependencies > 0 && !sec_ctx->dep_graph->nodes[new_frag->id]->done)
	    dep_node_add_dep(sec_ctx->dep_graph->nodes[new_frag->id], sec_ctx->dep_graph->nodes[d_frag->id]);
    }
    
    
    free(fragment_type);
    free(comp_name);
		    
}


void create_new_fragments(struct sec_analysis_ctx *sec_ctx, struct rb_tree *new_fragments)
{
    create_new_fragment_r(sec_ctx,new_fragments->root);
}


void generate_exploit_graphs(struct knowledge_base *k_b)
{
    
    
    //fetch the security context
    struct sec_analysis_ctx *sec_ctx=knowledge_base_get_res(k_b,"Security_Context");
    //#debug
    //print_fragment_markings(sec_ctx);
    struct dep_graph *d_graph=sec_ctx->dep_graph;
    
    struct sec_predicate *vuln_preds=knowledge_base_get_res(k_b,"Vulnerability_Predicates");
    size_t num_vulns=knowledge_base_num_res(k_b,"Vulnerability_Predicates");
    struct sec_predicate *reachable_preds=knowledge_base_get_res(k_b,"Reachable_Predicates");
    size_t num_reachable=knowledge_base_num_res(k_b,"Reachable_Predicates");
    
    
    //for every vulnerable interface, add that predicate to the SingleExploit predicates
    for(int i=0;i<num_vulns;i++)
    {
	char *v_comp_name=vuln_preds[i].terms[0];
	add_predicate_to_fragment(sec_ctx,&vuln_preds[i],SINGLE_EXPLOIT,v_comp_name);
    }
    
    //OBS: Because we have added the vuln exists predicates, we know that we only need to add methods already there.
    //OBS: THIS IS AN UGLY HACK
    //for every reachable component and interface add the predicate to SingleExploit predicates
    for(int i=0;i<num_reachable;i++)
    {
	
	char *r_comp_name=reachable_preds[i].terms[0];
	add_predicate_to_fragment(sec_ctx,&reachable_preds[i],SINGLE_EXPLOIT,r_comp_name);
    }



    //write the singleexploit predicates
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	for(int j=0;j<sec_ctx->component_contexts[i].num_se_fragments;j++)
	{
	    struct attack_fragment *frag=sec_ctx->component_contexts[i].se_fragments[j];
	    if(frag->marked)
	    {
		char input_file[256];
		sprintf(input_file,"/home/prosses/Tools/security_analyzer/Predicates/single_exploit/%s_%s.P",frag->comp->comp_name,frag->goal);
		predicates_to_file(input_file, frag->preds, frag->num_preds,"w");
	    }
	}
    }
        
    size_t num_magic_preds;
    struct sec_predicate *magic_preds=prepare_magic_input(k_b,&num_magic_preds);
    struct lag *magic_graph=build_magic_graph(magic_preds,num_magic_preds);

    
    //mmap some memory and cast it to an array of shared.
    void *sh_mem = create_shared_memory(MAX_PROCS*sizeof(struct shared));
    struct shared *shared=sh_mem;

    struct generic_queue *queued_fragments=malloc(sizeof(struct generic_queue));
    queue_init(queued_fragments);
    size_t num_running=0;
    for(int i=0;i<MAX_PROCS;i++)
    {
	shared[i].pid=-2;
    }
    
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	for(int j=0;j<sec_ctx->component_contexts[i].num_se_fragments;j++)
	{
	    struct attack_fragment *frag=sec_ctx->component_contexts[i].se_fragments[j];
	    //describe_fragment(frag);
	    //print_fragment_stats(frag);
	    if(frag->num_preds > 0 )
	    {
		queue_push_back(queued_fragments,frag);
		frag->marked=true;
		//add_node_to_dep_graph(sec_ctx->dep_graph, 0,frag);
	    }
	    
	}
    }

    struct shared se_fragments[queued_fragments->size];
    struct generic_queue completed_fragments;
    queue_init(&completed_fragments);


    struct queue_entry *entry=queued_fragments->head;
    
    for(int i=0;i<queued_fragments->size;i++)
    {
	struct attack_fragment *frag=entry->elem;
	
	pid_t pid=fork();
	num_running++;
	if(pid<0)
	{
	    fprintf(stderr,"Couldn't fork to generate Single Exploit graph for %s\n",frag->comp->comp_name);
	    exit(-1);
	}
	else if(pid == 0)
	{
	    
	    //shared[i].frag=frag;
	    //shared[i].pid=getpid();
	    run_mulval(sec_ctx,frag);
	}
	else
	{
	se_fragments[i].pid=pid;
	se_fragments[i].frag=frag;
	entry=entry->next;
	}
    }

    for(int i=0;i<queued_fragments->size;i++)
    {
	
	waitpid(se_fragments[i].pid,NULL,0);
	char *last_finished=fragment_to_str(se_fragments[i].frag);
	queue_push_back(&completed_fragments,last_finished);
	update_predicates(se_fragments[i].frag,k_b);
	//free last_finished ? 

    }
    
    //print_dep_graph_stats(sec_ctx->dep_graph);
    //exit(-1);
    
    
    
    struct sec_predicate *can_reach_preds=knowledge_base_get_res(k_b,"CanReach_Predicates");
    size_t num_can_reach=knowledge_base_num_res(k_b,"CanReach_Predicates");

    struct rb_tree *new_multi_exploits=find_possible_multi_exploits(magic_graph,&completed_fragments,can_reach_preds,num_can_reach);

    
    
    create_new_fragments(sec_ctx,new_multi_exploits);
    queue_destroy(queued_fragments);
    
    //queue the multi-exploit fragments now

    
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {       
	for(int j=0;j<sec_ctx->component_contexts[i].num_me_fragments;j++)
	{
	    
	    struct attack_fragment *frag=sec_ctx->component_contexts[i].me_fragments[j];
	    if(!frag->lag && frag->marked)
	    {
		prepare_input(frag,sec_ctx,k_b);
		frag->marked=true;
		queue_push_back(queued_fragments,frag);
	    }
	}
    }


    struct shared me_fragments[queued_fragments->size];


    entry=queued_fragments->head;
    
    for(int i=0;i<queued_fragments->size;i++)
    {
	struct attack_fragment *frag=entry->elem;
	
	pid_t pid=fork();
	num_running++;
	if(pid<0)
	{
	    fprintf(stderr,"Couldn't fork to generate Single Exploit graph for %s\n",frag->comp->comp_name);
	    exit(-1);
	}
	else if(pid == 0)
	{
	    run_mulval(sec_ctx,frag);
	}
	else
	{
	me_fragments[i].pid=pid;
	me_fragments[i].frag=frag;
	entry=entry->next;
	}
    }

    for(int i=0;i<queued_fragments->size;i++)
    {
	
	waitpid(me_fragments[i].pid,NULL,0);
	char *last_finished=fragment_to_str(me_fragments[i].frag);
	queue_push_back(&completed_fragments,last_finished);
	update_predicates(me_fragments[i].frag,k_b);

    }
    


    queue_destroy(queued_fragments);
    //print_dep_graph_stats(d_graph);
    

    
    new_multi_exploits=find_possible_multi_exploits(magic_graph,&completed_fragments,can_reach_preds,num_can_reach);
    create_new_fragments(sec_ctx,new_multi_exploits);
    
    
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {       
	for(int j=0;j<sec_ctx->component_contexts[i].num_me_fragments;j++)
	{
	    
	    struct attack_fragment *frag=sec_ctx->component_contexts[i].me_fragments[j];
	    if(!frag->marked)
	    {
		prepare_input(frag,sec_ctx,k_b);
		queue_push_back(queued_fragments,frag);
	    }
	}
    }

    struct shared me2_fragments[queued_fragments->size];


    entry=queued_fragments->head;
    
    for(int i=0;i<queued_fragments->size;i++)
    {
	struct attack_fragment *frag=entry->elem;
	
	pid_t pid=fork();
	num_running++;
	if(pid<0)
	{
	    fprintf(stderr,"Couldn't fork to generate Single Exploit graph for %s\n",frag->comp->comp_name);
	    exit(-1);
	}
	else if(pid == 0)
	{
	    run_mulval(sec_ctx,frag);
	}
	else
	{
	me2_fragments[i].pid=pid;
	me2_fragments[i].frag=frag;
	entry=entry->next;
	}
    }

    for(int i=0;i<queued_fragments->size;i++)
    {
	
	waitpid(me2_fragments[i].pid,NULL,0);
	char *last_finished=fragment_to_str(me2_fragments[i].frag);
	queue_push_back(&completed_fragments,last_finished);
	update_predicates(me2_fragments[i].frag,k_b);

    }
    


    queue_destroy(queued_fragments);
    

    //printf("Finished Generating Multi Exploit Fragments %zu\n",completed_fragments.size);
    struct rb_tree *new_component_impacts=find_possible_component_impacts(magic_graph,&completed_fragments);
    create_new_fragments(sec_ctx,new_component_impacts);

//    print_dep_graph_stats(sec_ctx->dep_graph);

    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {       
	for(int j=0;j<sec_ctx->component_contexts[i].num_ci_fragments;j++)
	{
	    
	    struct attack_fragment *frag=sec_ctx->component_contexts[i].ci_fragments[j];
	    prepare_input(frag,sec_ctx,k_b);
	    queue_push_back(queued_fragments,frag);
	}
    }

    
    struct shared ci_fragments[queued_fragments->size];


    entry=queued_fragments->head;
    
    for(int i=0;i<queued_fragments->size;i++)
    {
	struct attack_fragment *frag=entry->elem;
	
	pid_t pid=fork();
	num_running++;
	if(pid<0)
	{
	    fprintf(stderr,"Couldn't fork to generate Single Exploit graph for %s\n",frag->comp->comp_name);
	    exit(-1);
	}
	else if(pid == 0)
	{
	    run_mulval(sec_ctx,frag);
	}
	else
	{
	    ci_fragments[i].pid=pid;
	    ci_fragments[i].frag=frag;
	    entry=entry->next;
	}
    }

    for(int i=0;i<queued_fragments->size;i++)
    {
	
	waitpid(ci_fragments[i].pid,NULL,0);
	char *last_finished=fragment_to_str(ci_fragments[i].frag);
	queue_push_back(&completed_fragments,last_finished);
	update_predicates(ci_fragments[i].frag,k_b);

    }
    


    queue_destroy(queued_fragments);
    
    munmap(shared,MAX_PROCS*sizeof(struct shared) );


    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	for(int j=0;j<sec_ctx->component_contexts[i].num_ci_fragments;j++)
	{
	    struct attack_fragment *frag=sec_ctx->component_contexts[i].ci_fragments[j];
	    mark_dfs(sec_ctx->dep_graph->nodes[frag->id],true);
	}
    }

    FILE *frag_map_fp=fopen("Automata/fragments.map","w");
    size_t num_frags=0;
    for(int i=0;i<sec_ctx->num_component_ctx;i++)
    {
	for(int j=0;j<sec_ctx->component_contexts[i].num_se_fragments;j++)
	{
	    struct attack_fragment *frag=sec_ctx->component_contexts[i].se_fragments[j];
	    char c= (frag->marked == true) ? 'Y' : 'N';
	    fprintf(frag_map_fp,"singleExploit_%s_%s: %c\n",frag->comp->comp_name,frag->goal,c);
	    //printf("singleExploit_%s_%s: %c\n",frag->comp->comp_name,frag->goal,c);
	    if(c=='Y')
		num_frags++;
	}
	for(int j=0;j<sec_ctx->component_contexts[i].num_me_fragments;j++)
	{
	    struct attack_fragment *frag=sec_ctx->component_contexts[i].me_fragments[j];
	    char c= (frag->marked == true) ? 'Y' : 'N';
	    fprintf(frag_map_fp,"multiExploit_%s_%s: %c\n",frag->comp->comp_name,frag->goal,c);
	    //printf("multiExploit_%s_%s: %c\n",frag->comp->comp_name,frag->goal,c);
	    if(c=='Y')
		num_frags++;
	}

	for(int j=0;j<sec_ctx->component_contexts[i].num_ci_fragments;j++)
	{
	    struct attack_fragment *frag=sec_ctx->component_contexts[i].ci_fragments[j];
	    char c= (frag->marked == true) ? 'Y' : 'N';
	    fprintf(frag_map_fp,"componentImpact_%s_%s: %c\n",frag->comp->comp_name,frag->goal,c);
	    //printf("componentImpact_%s_%s: %c\n",frag->comp->comp_name,frag->goal,c);
	    if(c=='Y')
		num_frags++;
	}
	
    }
    fclose(frag_map_fp);
    printf("Number of fragments: %zu\n",num_frags);
    //exit(-2);
    //print_dep_graph_stats(d_graph);
    
}


