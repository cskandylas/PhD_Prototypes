#include "knowledge_base.h"
#include "arc_system.h"
#include "sec_clause.h"

#ifndef GRAPH_GEN_H
#define GRAPH_GEN_H


void system_arch_to_preds(struct knowledge_base *k_b);

void graph_gen(struct knowledge_base *k_b);
void generate_interaction_graph(struct knowledge_base *k_b);
void generate_exploit_graphs(struct knowledge_base *k_b);
void one_graph_gen(struct knowledge_base *k_b);


#endif
