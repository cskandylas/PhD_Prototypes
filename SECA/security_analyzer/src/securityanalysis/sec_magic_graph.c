#define _GNU_SOURCE

#include "sec_magic_graph.h"
#include "sec_clause.h"
#include "fol_resources.h"
#include "lag_resources.h"
#include "tokenize.h"
#include "rbtree.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>  
#include <stdlib.h>    
#include <sys/wait.h>

struct sec_predicate *prepare_magic_input(struct knowledge_base *k_b, size_t *num_preds)
{
    struct sec_predicate *vuln_preds=knowledge_base_get_res(k_b,"Vulnerability_Predicates");
    size_t num_vulns=knowledge_base_num_res(k_b,"Vulnerability_Predicates");
    struct sec_predicate *reachable_preds=knowledge_base_get_res(k_b,"Reachable_Predicates");
    size_t num_reachable=knowledge_base_num_res(k_b,"Reachable_Predicates");
    struct sec_predicate *can_reach_preds=knowledge_base_get_res(k_b,"CanReach_Predicates");
    size_t num_can_reach=knowledge_base_num_res(k_b,"CanReach_Predicates");

    struct sec_predicate *magic_input=malloc(2*(num_vulns+num_reachable+num_can_reach)*sizeof(struct sec_predicate));
    size_t count=0;
    for(int i=0;i<num_vulns;i++)
    {
	copy_predicate(&magic_input[count],&vuln_preds[i]);
	free(magic_input[count].terms[0]);
	magic_input[count].terms[0]=strdup("a");
	count++;
	copy_predicate(&magic_input[count],&vuln_preds[i]);
	free(magic_input[count].terms[0]);
	magic_input[count].terms[0]=strdup("b");
	count++;
	     
    }
    for(int i=0;i<num_reachable;i++)
    {
	copy_predicate(&magic_input[count],&reachable_preds[i]);
	free(magic_input[count].terms[0]);
	magic_input[count].terms[0]=strdup("a");
	count++;
	copy_predicate(&magic_input[count],&reachable_preds[i]);
	free(magic_input[count].terms[0]);
	magic_input[count].terms[0]=strdup("b");
	count++;
	     
    }
    for(int i=0;i<num_can_reach;i++)
    {
	copy_predicate(&magic_input[count],&can_reach_preds[i]);
	free(magic_input[count].terms[0]);
	free(magic_input[count].terms[1]);
	magic_input[count].terms[0]=strdup("a");
	magic_input[count].terms[1]=strdup("b");
	count++;
	copy_predicate(&magic_input[count],&can_reach_preds[i]);
	free(magic_input[count].terms[0]);
	free(magic_input[count].terms[1]);
	magic_input[count].terms[0]=strdup("b");
	magic_input[count].terms[1]=strdup("a");
	count++;
	     
    }
    /*
    for(int i=0;i<2*(num_vulns+num_reachable+num_can_reach);i++)
    {
	printf("%s\n",predicate_to_str(&magic_input[i]));
    }
    */

    if(num_preds)
    {
	*num_preds=2*(num_vulns+num_reachable+num_can_reach);
    }
    
    return magic_input;
}

struct lag *build_magic_graph(struct sec_predicate *magic_preds, size_t num_magic_preds)
{

    //printf("Building the magic graph\n");
    predicates_to_file("Predicates/magic_input.P",magic_preds,num_magic_preds,"w");

        //fork to call MULVAL
    pid_t pid=fork();

    if(pid<0)
    {
	fprintf(stderr,"Couldn't fork to generate component interaction graph\n");
	exit(-1);
    }
    else if(pid==0)
    {
	//FILE *fp=freopen("/dev/null", "w", stdout); 
	execlp ("/home/prosses/Tools/security_analyzer/xsb_integration_test", "/home/prosses/Tools/security_analyzer/xsb_integration_test",
		"Predicates/magic_input.P",
		"Rules/rules_merged.P",
		"Graphs/magic",
	        "componentImpact(_,_)",
	    (char *)0);

    }
    
    int status;
    while ((pid = wait(&status)) > 0);

    struct lag *magic_graph=lag_from_directory("Graphs/magic","Magic",LAG_MAGIC);

    //printf("Magic Graph built\n");
    return magic_graph;
}
char * make_capability(char *label);


char *replace_a_or_b(char *capability, char *component)
{
    char *ret=calloc(strlen(component)+strlen(capability)+8,1);
    char *uloc=strchr(capability,'_');
    int offset=uloc-capability;
    memcpy(ret,capability,offset+1);
    strcat(ret,component);
    uloc=strrchr(capability,'_');
    strcat(ret,uloc);
    
    return ret;
}

char *replace_a_and_b(char *capability, char *a_rep,char *b_rep)
{
    char *ret=calloc(strlen(a_rep)+strlen(b_rep)+strlen(capability)+1,1);
    char *uloc=strchr(capability,'_');
    int offset=uloc-capability;
    memcpy(ret,capability,offset+1);
    strcat(ret,a_rep);
    strcat(ret,"_");
    strcat(ret,b_rep);
    uloc=strrchr(capability,'_');
    strcat(ret,uloc);
    
    return ret;
}

char *matches(struct lag *magic_graph, struct lag_node *node,struct generic_queue *built_fragments, char *last_built)
{

    if(node->type!=LAG_OR)
    {
	return NULL;
    }

    struct tokenizer uscore_tok;
    tokenizer_init(&uscore_tok,last_built,"_");
    char *l_frag_type=next_token(&uscore_tok);
    char *l_component=next_token(&uscore_tok);
    char *l_goal=next_token(&uscore_tok);    
    
    for(int i=0;i<node->num_parents;i++)
    {
	struct lag_node *parent = magic_graph->nodes[node->parent_ids[i]];
	//printf("Node %s:",node->label);
	bool match=true;

	//POSSIBLE BUG HERE.
	//This needs to have the same component as a;
	struct lag_node *gparent=gparent=magic_graph->nodes[parent->parent_ids[parent->num_parents-1]];
	char *cap=make_capability(gparent->label);
	tokenizer_init(&uscore_tok,cap,"_");
	int count=count_tokens(&uscore_tok);
	char *frag_type=next_token(&uscore_tok);
	int c=0;
	while(c<count-2)
	{
	    free(next_token(&uscore_tok));
	    c++;
	}
	char *goal=next_token(&uscore_tok);
	
	free(cap);
	if(strcmp(l_frag_type,frag_type)==0 && strcmp(l_goal,goal)==0)
	{
	    
	    //printf("Possible candidate:%s\n",node->label);
	    
	    
	    for(int j=1;j<parent->num_parents;j++)
	    {
		struct lag_node *gparent=magic_graph->nodes[parent->parent_ids[j]];
		//printf("Need to match:%s with elements from the queue\n",gparent->label);
		 
		
	    }
	    free(goal);
	    free(frag_type);
	    if(match)
	    {
		char *cap=make_capability(node->label);
		char *ret= replace_a_or_b(cap,l_component);
		free(cap);
		free(l_component);
		//printf("New Match:%s",ret);
		return ret;
	    }
	}
    }
    free(l_component);
    free(l_goal);
    free(l_frag_type);
    return false;
}

void exhaustive_match(struct lag *magic_graph,struct lag_node *node,
		      char **built_frags_array,size_t num_frags,
		      struct sec_predicate *can_reach_preds, size_t num_can_reach,
		      struct rb_tree *matches)
{
    char *node_cap=make_capability(node->label);

    struct tokenizer utok;
    
    for(int i=0;i<num_frags;i++)
    {

	tokenizer_init(&utok,built_frags_array[i],"_");
	
	char *a_frag=next_token(&utok);
	char *a_comp=next_token(&utok);
	char *a_goal=next_token(&utok);
	//printf("%s:%s:%s\n",a_frag,a_comp,a_goal);
	for(int j=0;j<num_frags;j++)
	{
	    //printf("Fixing b to be %s\n",built_frags_array[j]);
	    
	    tokenizer_init(&utok,built_frags_array[j],"_");
	
	    char *b_frag=next_token(&utok);
	    char *b_comp=next_token(&utok);
	    char *b_goal=next_token(&utok);
	
	    
	    for(int k=0;k<node->num_parents;k++)
	    {
		struct lag_node *parent=magic_graph->nodes[node->parent_ids[k]];
		bool a_match=false;
		bool b_match=false;
		bool cr_match=false;
		for(int l=0;l<parent->num_parents;l++)
		{
		    
		    
		    struct lag_node *gparent=magic_graph->nodes[parent->parent_ids[l]];
		    char *gp_cap=make_capability(gparent->label);
		    
		    tokenizer_init(&utok,gp_cap,"_");
		    char *gp_frag=next_token(&utok);
		    char *gp_comp=next_token(&utok);
		    char *gp_goal=next_token(&utok);
		    
		    /*
		    if(strcmp(gp_frag,"canReach")==0)
		    {
			char *can_reach_match=replace_a_and_b(gp_cap,b_comp,a_comp);
			for(int m=0;m<num_can_reach;m++)
			{
			    char *pred_str=predicate_to_str(&can_reach_preds[m]);
			    char *cr_cap=make_capability(pred_str);

			    printf("%d,%d %s:%s\n",strlen(cr_cap),strlen(can_reach_match),cr_cap,can_reach_match);
			    //
			    if(strcmp(cr_cap,can_reach_match)==0)
			    {
				printf("CanReach Match:%s:%s\n",cr_cap,can_reach_match);
				cr_match=true;
			    }
			    free(pred_str);
			    free(cr_cap);
			    
			}
			}*/
		    
		    
		    if(strcmp(gp_comp,"a")==0)
		    {
			char *gp_match=replace_a_or_b(gp_cap,a_comp);
			if(strcmp(gp_match,built_frags_array[i])==0)
			{
			    a_match=true;
			    //printf("a part matches:%s\n",gp_match);
			}
			
		    }
		    else if(strcmp(gp_comp,"b")==0)
		    {
			char *gp_match=replace_a_or_b(gp_cap,b_comp);
			if(strcmp(gp_match,built_frags_array[j])==0)
			{
			    b_match=true;
			    //printf("b part matches:%s\n",gp_match);
			}
		    }

		   
			
		    free(gp_cap);
		}
		if(a_match && b_match /*&& cr_match*/)
		{
		    
		    
		    
		    char *forged_can_reach=malloc(256);
		    sprintf(forged_can_reach,"canReach(%s,%s,_).",b_comp,a_comp);
		    char *uscore_idx=strchr(forged_can_reach,'_');
		    int len=uscore_idx-forged_can_reach;
		    
		    //printf("Need:%s--%d\n",forged_can_reach,len);
		    
		    //printf("NUMCANREACH:%d\n",num_can_reach);
		    for(int r=0;r<num_can_reach;r++)
		    {
			char *can_reach_str=predicate_to_str(&can_reach_preds[r]);
			//printf("Have:%s\n",can_reach_str);
			//printf("%s:%s\n",forged_can_reach,can_reach_str);
			
			if(strncmp(can_reach_str,forged_can_reach,len)==0)
			{
			    struct dependency *dep=malloc(sizeof(struct dependency));
			    dep->top=replace_a_or_b(node_cap,a_comp);
			    dep->depends_on=malloc(2*sizeof(char*));
			    dep->num_deps=2;
			    dep->depends_on[0]=strdup(built_frags_array[i]);
			    dep->depends_on[1]=strdup(built_frags_array[j]);
			    rb_tree_insert(matches,dep);
			}
			
			    
		    }
		    
		}
	    }
	    free(b_frag);
	    free(b_comp);
	    free(b_goal);
	}
	free(a_frag);
	free(a_comp);
	free(a_goal);
	
    }
    free(node_cap);
}

int dependency_compare(const void *dep1, const void *dep2)
{
    const struct dependency *d1=dep1;
    const struct dependency *d2=dep2;
    int top_cmp=strcmp(d1->top,d2->top)!=0;
    if(top_cmp!=0)
	return top_cmp;
    if(d1->num_deps<d2->num_deps)
	return -1;

    for(int i=0;i<d1->num_deps;i++)
    {
	int d_comp=strcmp(d1->depends_on[i],d2->depends_on[i]);
	if(d_comp!=0)
	    return d_comp;
    }
    return 0;
}

struct rb_tree *find_possible_multi_exploits(struct lag *magic_graph,
						   struct generic_queue *built_fragments,
						   struct sec_predicate *can_reach_preds,
						   size_t num_can_reach)
{
    
    char **built_frags_array=malloc(built_fragments->size*sizeof(char*));
    struct queue_entry *entry = built_fragments->head;

    int count=0;
    while(entry)
    {
	built_frags_array[count]=entry->elem;
	count++;
	entry=entry->next;
    }
    
    struct rb_tree *new_multi_exploits=malloc(sizeof(struct rb_tree));
    rb_tree_init(new_multi_exploits,dependency_compare);
    
    for(int i=0;i<magic_graph->num_nodes;i++)
    {
	
	if(magic_graph->nodes[i] && magic_graph->nodes[i]->type==LAG_OR )
	{
	    char *cap=make_capability(magic_graph->nodes[i]->label);
	    
	    struct tokenizer utok;
	    tokenizer_init(&utok,cap,"_");
	    char *fragment=next_token(&utok);
	    //printf("%s\n",fragment);
	    //only check multi exploits
	    free(cap);

	    if(strcmp(fragment,"multiExploit")==0)
	    {
		
		exhaustive_match(magic_graph,magic_graph->nodes[i],built_frags_array,built_fragments->size,can_reach_preds,num_can_reach,new_multi_exploits);
	    }
	    free(fragment);    
	}
    }
    return new_multi_exploits;
}

void exhaustive_match2(struct lag *magic_graph,struct lag_node *node,
		      char **built_frags_array,size_t num_frags,
		      struct rb_tree *matches)
{
    char *node_cap=make_capability(node->label);
    //printf("Trying to match %s\n",node_cap);

    struct tokenizer utok;
    
    for(int i=0;i<num_frags;i++)
    {
	//printf("Fixing a to be: %s\n",built_frags_array[i]);

	tokenizer_init(&utok,built_frags_array[i],"_");
	
	char *a_frag=next_token(&utok);
	char *a_comp=next_token(&utok);
	char *a_goal=next_token(&utok);
	for(int j=0;j<num_frags;j++)
	{
	    //printf("Fixing b to be %s\n",built_frags_array[j]);
	    
	    tokenizer_init(&utok,built_frags_array[j],"_");
	
	    char *b_frag=next_token(&utok);
	    char *b_comp=next_token(&utok);
	    char *b_goal=next_token(&utok);
	
	    
	    for(int k=0;k<node->num_parents;k++)
	    {
		struct lag_node *parent=magic_graph->nodes[node->parent_ids[k]];
		bool a_match=false;
		bool b_match=false;
		if(parent->num_parents==1)
		    b_match=true;
		for(int l=0;l<parent->num_parents;l++)
		{
		    
		    
		    struct lag_node *gparent=magic_graph->nodes[parent->parent_ids[l]];
		    char *gp_cap=make_capability(gparent->label);
		    
		    tokenizer_init(&utok,gp_cap,"_");
		    char *gp_frag=next_token(&utok);
		    char *gp_comp=next_token(&utok);
		    char *gp_goal=next_token(&utok);

		    
		    if(strcmp(gp_comp,"a")==0)
		    {
			char *gp_match=replace_a_or_b(gp_cap,a_comp);
			if(strcmp(gp_match,built_frags_array[i])==0)
			{
			    a_match=true;
			    //printf("a part matches:%s\n",gp_match);
			}
			
		    }
		    else if(strcmp(gp_comp,"b")==0)
		    {
			char *gp_match=replace_a_or_b(gp_cap,b_comp);
			if(strcmp(gp_match,built_frags_array[j])==0)
			{
			    b_match=true;
			    //printf("b part matches:%s\n",gp_match);
			}
		    }

		    if(a_match && b_match)
		    {
			if(parent->num_parents==2)
			{
			    //printf("Match: %s:-\n%s,\n%s\n",node_cap,built_frags_array[i],built_frags_array[j]);
			    struct dependency *dep=malloc(sizeof(struct dependency));
			    dep->top=replace_a_or_b(node_cap,a_comp);
			    dep->depends_on=malloc(2*sizeof(char*));
			    dep->num_deps=2;
			    dep->depends_on[0]=strdup(built_frags_array[i]);
			    dep->depends_on[1]=strdup(built_frags_array[j]);
			    rb_tree_insert(matches,dep);
			}
			else if (parent->num_parents==1)
			{
			    //printf("Match: %s:-\n%s\n",node_cap,built_frags_array[i]);
			    struct dependency *dep=malloc(sizeof(struct dependency));
			    dep->top=replace_a_or_b(node_cap,a_comp);
			    dep->depends_on=malloc(1*sizeof(char*));
			    dep->num_deps=1;
			    dep->depends_on[0]=strdup(built_frags_array[i]);
			    rb_tree_insert(matches,dep);
			}
		    }
			
		    free(gp_cap);
		}	
	    }
	    free(b_frag);
	    free(b_comp);
	    free(b_goal);
	}
	free(a_frag);
	free(a_comp);
	free(a_goal);
	
    }
    free(node_cap);
}


struct rb_tree *find_possible_component_impacts(struct lag *magic_graph,
					     struct generic_queue *built_fragments)
{
    char **built_frags_array=malloc(built_fragments->size*sizeof(char*));
    struct queue_entry *entry = built_fragments->head;

    int count=0;
    while(entry)
    {
	built_frags_array[count]=entry->elem;
	count++;
	entry=entry->next;
    }
    
    struct rb_tree *new_component_impacts=malloc(sizeof(struct rb_tree));
    rb_tree_init(new_component_impacts,dependency_compare);
    for(int i=0;i<magic_graph->num_nodes;i++)
    {
	if(magic_graph->nodes[i] && magic_graph->nodes[i]->type==LAG_OR )
	{
	    char *cap=make_capability(magic_graph->nodes[i]->label);
	    struct tokenizer utok;
	    tokenizer_init(&utok,cap,"_");
	    char *fragment=next_token(&utok);
	    //printf("%s\n",fragment);
	    //only check multi exploits
	    free(cap);

	    if(strcmp(fragment,"componentImpact")==0)
	    {
		exhaustive_match2(magic_graph,magic_graph->nodes[i],built_frags_array,built_fragments->size,new_component_impacts);
	    }
	    free(fragment);    
	}
    }
    return new_component_impacts;
}



struct generic_queue *peek_magic_graph(struct lag *magic_graph, struct generic_queue *built_fragments, char *last_built)
{
    printf("Finished generating %s peeking at graph for enabled fragments\n",last_built);
    struct generic_queue *new_fragments=NULL;
    bool match_found=false;
    for(int i=0;i<magic_graph->num_nodes;i++)
    {
	if(magic_graph->nodes[i] && magic_graph->nodes[i]->type==LAG_OR)
	{
	    //printf("Trying to match:%s\n",magic_graph->nodes[i]->label);
	    char *match=matches(magic_graph,magic_graph->nodes[i],built_fragments,last_built);
	    if(match)
	    {
		if(!new_fragments)
		{
		    new_fragments=malloc(sizeof(struct generic_queue));
		    queue_init(new_fragments);
		}
		
		queue_push_back(new_fragments,match);
		
	    }	    
	}
    }
    return new_fragments;
}
