#include "knowledge_base.h"
#include "Queue.h"
#include "lag.h"
#include "sec_clause.h"
 
#ifndef MAGIC_GRAPH_H
#define MAGIC_GRAPH_H

//TODO: Rename everything to meaningful names


struct sec_predicate *prepare_magic_input(struct knowledge_base *k_b, size_t *num_preds);
struct lag *build_magic_graph(struct sec_predicate *magic_preds, size_t num_magic_preds);
struct generic_queue *peek_magic_graph(struct lag *magic_graph, struct generic_queue *built_fragments, char *last_built);
char  *matches(struct lag *magic_graph, struct lag_node *node,struct generic_queue *built_fragments, char *last_built);
struct rb_tree *find_possible_multi_exploits(struct lag *magic_graph,
					     struct generic_queue *built_fragments,
					     struct sec_predicate *can_reach_preds,
					     size_t num_can_reach);
struct rb_tree *find_possible_component_impacts(struct lag *magic_graph,
					     struct generic_queue *built_fragments);

struct dependency
{
    char *top;
    char **depends_on;
    size_t num_deps;
};


#endif
