#include "named_resource.h"

#include <stdlib.h>
#include <string.h>

void named_res_init(struct named_res *named_res, char *name,size_t n_items, void *res)
{
    named_res->name=malloc(strlen(name)+1);
    strcpy(named_res->name,name);
    named_res->res=res;
    named_res->n_items=n_items;
}


void named_res_destroy(struct named_res *named_res)
{
    free(named_res->name);
}
