#include "arc_draw.h"

#include <curses.h>
#include <string.h>
#include <stdlib.h>


WINDOW* create_property_window(int y,int x,struct arc_property *prop)
{
    WINDOW *property_win=newwin(3,COMPONENT_CELL_WIDTH-2,y,x);
    wborder(property_win, ACS_BULLET, ACS_BULLET, ACS_BULLET, ACS_BULLET,ACS_BULLET, ACS_BULLET, ACS_BULLET, ACS_BULLET);
    char property_str[COMPONENT_CELL_WIDTH-4];
    strncpy(property_str,prop->name,COMPONENT_CELL_WIDTH-5);
    property_str[COMPONENT_CELL_WIDTH-5]='\0';
    mvwprintw(property_win,1,1,property_str);

    return property_win;
}


int interface_draw_height(struct arc_interface *iface)
{
    //Height= 3+3*num_properties
    return 3*(1+iface->num_properties);
}

WINDOW*  create_interface_window(int y,int x, struct arc_interface *iface)
{
    WINDOW *iface_win=newwin(interface_draw_height(iface),COMPONENT_CELL_WIDTH,y,x);
    wborder(iface_win, ACS_VLINE, ACS_VLINE, ACS_S7, ACS_S7, ACS_LTEE,ACS_RTEE, ACS_LTEE,ACS_RTEE );
    //make the left and right LTEE and RTEE
    mvwaddch(iface_win,1,0,ACS_RTEE);
    mvwaddch(iface_win,1,COMPONENT_CELL_WIDTH-1,ACS_LTEE);
    char iface_str[COMPONENT_CELL_WIDTH-2];
    strncpy(iface_str,iface->if_name,COMPONENT_CELL_WIDTH-2);
    iface_str[COMPONENT_CELL_WIDTH-3]='\0';
    mvwprintw(iface_win,1,1,iface_str);
    return iface_win;
}

int component_draw_height(struct arc_component *comp)
{
    //3 lines for each property and the name plus border, one line for status
    int win_height=3*comp->num_properties+4;
    for(int i=0;i<comp->num_interfaces;i++)
    {
	win_height+=interface_draw_height(comp->interfaces[i]);
    }

    return win_height;
}

WINDOW*  create_component_window(int y,int x, struct  arc_component *comp)
{
    WINDOW *comp_win=newwin(component_draw_height(comp),COMPONENT_CELL_WIDTH,y,x);
    box(comp_win,0,0);
    char comp_str[COMPONENT_CELL_WIDTH-2];
    strncpy(comp_str,comp->comp_name,COMPONENT_CELL_WIDTH-3);
    comp_str[COMPONENT_CELL_WIDTH-3]='\0';
    mvwprintw(comp_win,1,1,comp_str);
    mvwprintw(comp_win,component_draw_height(comp)-2,1,"    STATUS:OK");
    return comp_win;
}


void build_component_subtree(int y, int x, struct arc_component *comp,struct draw_node *comp_node)
{
    comp_node->entity=comp;
    comp_node->window=create_component_window(y,x,comp);
    comp_node->xpos=x;
    comp_node->ypos=y;

    //create the chidren
    comp_node->num_children=comp->num_properties+comp->num_interfaces;
    comp_node->children=malloc(comp_node->num_children*sizeof(struct draw_node));

    //adjust height
    int ypos=y+2;

    
    //for each interface
    for(int i=0;i<comp->num_interfaces;i++)
    {
	//create the node
	comp_node->children[i].entity=comp->interfaces[i];
	comp_node->children[i].window=create_interface_window(ypos,x,comp->interfaces[i]);
	comp_node->children[i].xpos=x;
	comp_node->children[i].ypos=ypos;
	

	
	
	comp_node->children[i].num_children=comp->interfaces[i]->num_properties;
	comp_node->children[i].children=malloc(comp_node->children[i].num_children* sizeof(struct draw_node));
	//comp_node->children=malloc(comp_node->num_children*sizeof(struct draw_node));
	//for each interface property:
	int inner_ypos=ypos+2;
	for(int j=0;j<comp->interfaces[i]->num_properties;j++)
	{
	    comp_node->children[i].children[j].entity=comp->interfaces[i]->properties[j];
	    comp_node->children[i].children[j].window=create_property_window(inner_ypos,x+1,comp->interfaces[i]->properties[j]);
	    comp_node->children[i].children[j].xpos=x+1;
	    comp_node->children[i].children[j].ypos=inner_ypos;
	    inner_ypos+=3;
	    
	}
	ypos+=interface_draw_height(comp->interfaces[i]);
    }

    int child_idx=comp->num_interfaces;
    for(int i=0;i<comp->num_properties;i++)
    {
	comp_node->children[child_idx+i].entity=comp->properties[i];
	
	comp_node->children[child_idx+i].xpos=x+1;
	comp_node->children[child_idx+i].ypos=ypos;
	comp_node->children[child_idx+i].window=create_property_window(ypos,x+1,comp->properties[i]);
	comp_node->children[child_idx+i].num_children=0;
	comp_node->children[child_idx+i].children=NULL;
	ypos+=3;
    }
    
}


void draw_component_subtree(struct draw_node *comp_node)
{
    wrefresh(comp_node->window);
    for(int i=0;i<comp_node->num_children;i++)
    {
	wrefresh(comp_node->children[i].window);
	for(int j=0;j<comp_node->children[i].num_children;j++)
	{
	    wrefresh(comp_node->children[i].children[j].window);
	}
    }
}

void destroy_component_subtree(struct draw_node *comp_node)
{
    
    if(comp_node->num_children>0)
    {
	
	for(int i=0;i<comp_node->num_children;i++)
	{
	    if(comp_node->children[i].num_children>0)
	    {
		for(int j=0;j<comp_node->children[i].num_children;j++)
		{	
		    //free(comp_node->children[i].children[j].window);
		}
		free(comp_node->children[i].children);
	    }	
	}
	free(comp_node->children);
    }
}

struct draw_tree* build_system_tree(struct arc_system *sys)
{
    //for each component in the system if it fits on the right place it there otherwise place it below
    struct draw_tree *system_tree=malloc(sizeof(struct draw_tree));
    //create the system node;
    system_tree->root=malloc(sizeof(struct draw_node));
    system_tree->root->entity=sys;
    system_tree->root->window=NULL;
    system_tree->root->num_children=sys->num_components;
    system_tree->root->children=malloc(sys->num_components*sizeof(struct draw_node));

    int max_x;
    int max_y;

    int xpos=1;
    int ypos=1;
    
    for(int i=0;i<sys->num_components;i++)
    {
	build_component_subtree(1,xpos,sys->components[i],&system_tree->root->children[i]);
	xpos+=COMPONENT_CELL_WIDTH+SPACE_BETWEEN_COMPONENTS;
	if(component_draw_height(sys->components[i])>ypos)
	    ypos=component_draw_height(sys->components[i]);
	//ypos+=component_draw_height(sys->components[i])+SPACE_BETWEEN_COMPONENTS;
    }

    system_tree->root->window=newwin(ypos+2,xpos+1,0,0);
    box(system_tree->root->window,0,0);
    
    return system_tree;
}

void draw_system_tree(struct draw_tree *tree)
{
    if(tree->root->window)
    {
	wrefresh(tree->root->window);
    }
    
    for(int i=0;i<tree->root->num_children;i++)
    {
	draw_component_subtree(&tree->root->children[i]);
    }
}

