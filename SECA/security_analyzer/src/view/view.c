#include <curses.h>
#include <stdlib.h>
#include <limits.h>

#include "arc_draw.h"
#include "line_draw.h"

#include "arc_property.h"
#include "arc_component.h"
#include "arc_interface.h"
#include "arc_parse.h"
#include "heap.h"
#include "astar.h"

int main(void)
{
    initscr();
    noecho();
    cbreak();
    curs_set(0);

    if ((LINES < 24) || (COLS < 80)) {
        endwin();
        puts("Your terminal needs to be at least 80x24");
        exit(2);
    }


    FILE *ifp=fopen("../../Tiny.arch","r");
    if(!ifp)
    {
		fprintf(stderr,"Could not find Tiny.arch!\n");
    }
    fseek(ifp, 0L, SEEK_END);
    long numbytes = ftell(ifp);
    fseek(ifp, 0, SEEK_SET);	
    char *text=malloc(numbytes+1);

    if(text == NULL)
	return 1;
    fread(text, sizeof(char), numbytes, ifp);
    fclose(ifp);
    text[numbytes]='\0';
    struct arc_system *sys=arc_parse(text);
    if(!sys)
    {
	fprintf(stderr,"Could not load base system architecture\nExiting!\n");
	exit(-1);
    }
    free(text);

    
    struct draw_tree *sys_tree=build_system_tree(sys);
    
    clear();
    refresh();
    {
	draw_system_tree(sys_tree);	
    }
    getchar();
    curs_set(1);
    endwin();

    return 0;
}


int main2(void)
{
    struct grid small_grid;
    small_grid.cells=malloc(20*sizeof(struct grid_coord*));
    small_grid.colision=malloc(20*sizeof(enum CELL_TYPE*));
    small_grid.cost=malloc(20*sizeof(int*));
    for(int i=0;i<20;i++)
    {
	small_grid.cells[i]=malloc(20*sizeof(struct grid_coord));
	small_grid.colision[i]=malloc(20*sizeof(enum CELL_TYPE));
	small_grid.cost[i]=malloc(20*sizeof(int));
    }
    for(int j=0;j<20;j++)
    {
	for(int i=0;i<20;i++)
	{
	    small_grid.cells[j][i].x=j;
	    small_grid.cells[j][i].y=i;
	    small_grid.colision[j][i]=CELL_EMPTY;
	    small_grid.cost[j][i]=INT_MAX;
	}
    }
    small_grid.colision[0][1]=CELL_FULL;
    small_grid.colision[1][1]=CELL_FULL;
    small_grid.colision[2][2]=CELL_FULL;
    small_grid.colision[2][1]=CELL_FULL;
    small_grid.width=20;
    small_grid.height=20;
    astar(&small_grid,small_grid.cells[0][0],small_grid.cells[19][19]);
    
    return 0;
}
